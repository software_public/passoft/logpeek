=======
History
=======

2016.235 (2018-05-22)
------------------
* First release on new build system.

2019.059 (2019-02-28)
------------------
* Should run under Python 2 and Python 3.
* Knows how to plot LOG channel files from the DMC. Place the mseed
  files in a directory/folder ending in ".mslogs".
* The menu item to change font sizes is now Change Font Sizes, instead
  of Fonts BIGGER and Fonts smaller.
* Fixed a couple of little file reading bugs.

2020.223 (2020-08-10)
---------------------
* Updated to work with Python 3
* Added a unit test to test logpeek import
* Updated list of platform specific dependencies to be installed when
  installing logpeek in dev mode (see setup.py)
* Installed and tested logpeek against Python3.[6,7,8] using tox
* Formatted Python code to conform to the PEP8 style guide
* Created conda package for logpeek that can run on Python3.[6,7,8]
* Updated .gitlab-ci.yml to run a linter and unit tests for Python3.[6,7,8]
  in GitLab CI pipeline

  2021.160(2021-06-09)
  ---------------------
  * Update version number that is displayed in GUI

  2022.1.0.0 (2022-01-11)
  ------------------
  * New versioning scheme
