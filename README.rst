=======
logpeek
=======

* Descrition: Produces a graphical display of Reftek 72A and 130 system State
of Health and raw seismic data information for field troubleshooting the
sensing and recording systems.

* Usage: logpeek

* Free software: GNU General Public License v3 (GPLv3)
