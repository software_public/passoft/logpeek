.TH LOGPEEK 4 "2005.266" "" "PASSCAL MANUAL PAGES"
.SH NAME
logpeek \- produces a graphical display of a Reftek 72- and 130-series
DAS log files.
.SH SYNOPSIS
logpeek [ -color | -gray | -night ]
.SH DESCRIPTION
\fBlogpeek\fR allows reading and examining in a graphical manner the
State of Health data from a Reftek log file produced by programs such
as ref2log and the ref2segy group of programs.
.sp
The program is started by entering its name on the command line.  The
program will examine the current working directory and present a list
of all of the log files in that directory.  The file to be examined is
selected from the list by double-clicking on its name in the list.
.sp
To change the directory edit the directory listed in the field below
the window's menu bar, or use the Browse... button and navigating to
the directory of interest.  When the directory to be examined has been
manually entered in the directory field pressing the Return key, or
clicking on the "Read Dir" button will generate a list of the log
files that the program finds in the new directory.
.sp
When a file has been selected it is opened and read.  Various lines of
interest in the file are parsed, and their information saved.  A
smaller second window will then be created.  The contents of the log
file will be read into that window.  The information of interest will
then be plotted in the graph area, and some information about the file
will be placed into the information area.  Status messages will be
displayed in the status line as this reading process is performed.
.sp
If there is an error file (files ending in ".err") with the same
basename as the selected log file that file will also be opened and
read.  If there are any lines of interest in the file another small
window will be created and the contents of the file will be displayed
there.  Data from the file will be plotted in the graph area along
with the data from the main/selected log file.
.sp
A additional window can be requested from the menu item Windows|GPS Plot,
that will display information about the DAS's position
(Latatude/Longitude) based on the GPS POSITION messages found in the
selected log file.
.sp
An additional window can be requested from the menu item Windows|Log
Search, that will allow the user to enter a string to search through
the current log for.  The searches are case sensitive.
.sp
The program examines the DAS serial number that it gleens from the
log file it is requested to read.  If the DAS serial number is in the
range 9000 to 9FFF then the DAS is perceived to be an RT130 model.
Any other number a REF TEK 72A series DAS.
.sp
The program has several different color modes which can be selected from
the Options menu, or from the command line.  The default mode is Color.
.SH MENU ITEMS and BUTTONS
.sp
All graphs possible are selected to be shown when the program is started.
Individual graphs can be turned on and off with the items in the Graphs
menu.
.TP
File|Quit
Quits the program.
.TP
Windows|GPS Plot
Displays a window that shows information about the DAS's position
(Latatude/Longitude) based on the GPS POSITION messages found in the
log file.
.TP
Windows|Log Search
Displays a window where a string of text may be entered.  The
currently plotted log file will be searched for lines containing the
string.  The search is case sensitive.
.TP
Graphs|(all of those items)
Use these items to turn individual graphs on and off.
.TP
Options|Set New Start/End Sets the default start and end times for the
current file's data to the timerange of the data that is currently
displayed.  "Start/End times altered" will be displayed in the status
area after this function has been used.  The file must be reloaded to
return to the file's original timerange.
.TP
Options|Ignore Timing Errors
Normally the program will read a log file until it finds the line
identifying the first State Of Health (SOH) block of messages.  It
will obtain the date and time from that line.  All lines read before
that first SOH block will be skipped.  Lines will continue to be read
and as long as a line's time is later than the line preceeding it.  If
there was a timing problem with the DAS, and the timestamps in the log
file are really messed up (e.g. the time jumps forward several years
and then jumps back to the current year, thus causing the program to
not process the remainder of the file after the time jumps back) then
the program can be forced to process all of the lines in the file that
are readable by checking the Ignore Timing Errors option.
Options|Show ERRs
A checkbox option that controls display of the ERR CHx graphs that are
displayed when a ".err" file, associated with a ".log" file, has been
read and processed.
.TP
Options|Show YYYYmmmDD
A checkbox option that controls the format of the date displayed when
the Control-click ("C"lock click) function is used.  See below.
.TP
Options|Color
Changes the color mode to "Color".
.TP
Options|Gray
Changes the color mode to "Gray".
.TP
Options|Night
Changes the color mode to "Night".
.TP
Help|Help
Brings up a text window with hints about the program's usage.
.TP
Help|About
Shows an About... box.
.TP
Stop Reading
The Stop Reading button halts the reading of a log file.  This may be
used if it is decided that the file selected is not the correct one,
or is too long to wait to be read.  The button will only stop logpeek
while reading the log file.  Once that point has been passed logpeek
can not be stopped until after the data has been plotted.
.SH GRAPH OPTIONS
The graph area supports several clicking options to allow detailed
examination of the graphed information.  The program uses only the
left mouse button by itself, and in conjuction with either the Shift
key or the Control key.
.TP
Regular click (Point click)
Clicking on the graph near a plotted point (or line if the points are
close together) will direct the program to display in the "LOGPEEK -
Log File Lines" window the line in the log file that corresponds to
the point clicked on.
.sp
Clicking below the tick marks at the bottom of the graphs will scroll
the display ahead or backward in time.  Clicking to the right of
center of the graphs will scroll ahead in time, and to the left of
center backward in time.  The amount of scroll is 90% of the currently
displayed time range so there will be some overlap between subsequent
displays.
.TP
Control-click ("C"lock click)
Clicking on the graph while holding down the Control key will display
a vertical rule with the time corresponding to the X-axis position of
the click displayed above the rule.  The time above the rule is
computed based on the start and end times of the displayed graphs, and
the pixel position of the mouse cursor.  This will undoubtably cause
some rounding errors since the number of seconds covered by the graphs
will usually be much larger than the number of graph pixels.
.sp
Clicking just to the left of the beginning of the graphs will place
the rule at the starting pixel of the graphs.  Likewise, clicking just
to the right of the end of the graphs will place the rule at the last
pixel of the graphs.
.sp
Performing a Control-click below the timeline at the bottom of the
graph area will toggle the extension of the tick marks of the
timeline's to the top of the graph area.
.sp
To remove the rule and the time, and/or the extended tick marks,
perform a Control-click in the area of the graph occupied by the graph
labels on the left side of the graph area.
.TP
Shift-click ("S"election click)
Clicking on the graph while holding down the Shift key will allow the
user to select an area of the graph to be zoomed in on.  The first
Shift-click will display a vertical rule marking one boundry of the
area to be selected.  The second Shift-click will select the other
boundry.  The graph area will be cleared and the selected area
redrawn.  "(zoomed)" will be displayed in the status area until the
graphs return to the original scale, or the Set New Start/End menu
function has been used.
.sp
Clicking just to the left of the beginning of the graphs will place
the rule at the starting pixel of the graphs.  Likewise, clicking just
to the right of the end of the graphs will place the rule at the last
pixel of the graphs.
.sp
To return to the original scale of the log file perform a Shift-click
in the area of the graph occupied by the graph labels on the left side
of the graph area.  The program will step back out through each of the
previous selection ranges until it reaches the original time range.
.sp
The first selection boundry can be cancelled by Shift-clicking in the
area of the graph occupied by the graph labels on the left side of the
graph area.
Right-click (Zapping)
Clicking the right mouse button on a point will remove that point from
the data set.  "x point(s) zapped" will be displayed in the status
area after this function has been used.  The file must be reloaded to
recover and re-plot the zapped points.
.SH GRAPHED ITEMS
Below is an explanation of each of the items on the graph.
.TP
DPS-CLK DIFF
Points on this graph are generated by lines in the log files like:
.sp
172:01:41:21 DSP CLOCK DIFFERENCE: 0 SECS AND -989 MSECS
.sp
This graph will only be displayed if these messages are in the log file.
.TP
PHASE ERR
Points on this graph are generated by lines in the log file like:
.sp
172:02:41:19 INTERNAL CLOCK PHASE ERROR OF 3526 USECONDS
.TP
JERKS/DSP SETS
Points on this line are generated by lines like:
.sp
172:23:41:44 INTERNAL CLOCK TIME JERK # 11 OCCURRED AT 23:41:44.994
.sp
172:23:41:45 DSP CLOCK SET: OLD=01:172:23:41:45.996, NEW=01:172:23:41:45.007
.TP
GPS ON-OFF
Points on this line are generated by lines like:
.sp
173:00:39:58 GPS: POWER IS TURNED ON  (72-series instruments)
.sp
173:00:39:58 EXTERNAL CLOCK WAKEUP  (RT130-series instruments)
.TP
GPS LK-UNLK
Points on this graph are generated by lines like:
.sp
173:00:41:53 EXTERNAL CLOCK IS LOCKED
.sp
173:00:46:53 EXTERNAL CLOCK IS UNLOCKED
.TP
DUMP CALL
Points on this line are generated by lines like:
.sp
173:01:17:25 AUTO DUMP CALLED
.sp
173:01:18:03 AUTO DUMP COMPLETE
.TP
EVENTS DSx
Points on these lines are generated by lines like:
.sp
DAS: 0108  EV: 0377  DS: 2  FST = 2001:173:01:09:51:447  TT = 2001:173:01:09:51:447  NS: 144005  SPS: 40  ETO: 0
.sp
Which line a point is plotted on is determined by the data stream
value following "DS:" in the log file line.
.TP
TEMP and VOLTS
Points on these two graphs are generated by lines like:
.sp
173:02:17:04 BATTERY VOLTAGE = 13.5V, TEMPERATURE = 28C
.TP
ERR CHx
Data from an error file.  Each line corresponds to a data channel.
Represented are the overlaps and gaps in the timing information.  The
length of the line indicates the amount of the time jump.  A red line
indicates that there was a correction made to the system clock which
changed the time backwards (overlap), but the value in the .err file
will be a positive number.  A green line indicates that the time was
jumped forward (gap).  Areas on the graphs are generated by lines
like:
.sp
Time jump: R353.01/01.353.00.19.11.0394.6 obs 00:21:38.031  corr +1 ms
.sp
The error files are created by programs like ref2mseed.
.TP
RESET/POWERUP
Points on this line are generated by lines like:
.sp
159:15:08:57 SYSTEM POWERUP 2: UNIT 108, CPU VER 03.00A  
.TP
DISCREPS
These points are plotted when lines with "DISCREPANCY" are found.  These
are some test lines that have been put in by REF TEK (2005SEP22).
.TP
ERROR/WARNING
Lines placed into the log file by programs like ref2segy that have the
word "ERROR" or "WARNING" in them.  The position of the dots on the
graph for these messages may, at times, not make any sense.  These
message lines in the log files do not have a time stamp, so the
program just uses the time from the last line that had a time stamp,
which, since a lot of the warning messages have to do with clock
problems, means that the dots will show up in the wrong place.
.TP
RE-CENTER
.sp
Generated by lines in the log file like:
086:16:04:36 SENSOR 1 MASS RE-CENTER: MANUAL
.TP
ACQ ON-OFF
Points on this line are generated by lines like:
.sp
159:15:09:11 ACQUISITION STARTED
253:22:57:43 ACQUISITION STOP REQUESTED
.TP
SOH/DATA DEFS
SOH points on this line are generated by lines like:
.sp
State of Health 01:159:15:06:02:839 ST: 0108
.sp
Data Definition points on this line are generated by lines like:
.sp
Data Stream Definition 01:159:15:08:59:392 ST: 0108
.sp
This block of information is normally followed by lines for the other
parameter blocks such as:
.sp
Station Channel Definition 01:159:15:08:59:392 ST: 0108
.sp
Calibration Definition 01:159:15:08:59:392 ST: 0108
.sp
etc.
.SH GPS PLOT
The GPS Plot display plots all of the Latitude and Longitude values
found in "GPS POSITION" messages of the log file, after a bit of
massaging to prevent bad location readings from corrupting the
results.  To arrive at the average location and plot a sensible group
of points, the program first calculates the standard deviation of all
of the points found in the file.  It then throws out any points with a
greater than 1 sigma error.  It then recalculates the standard
deviation of the remaining points and throws out any points with a
greater than 3 sigma error.  It then recalculates the average position
of the remaining points and draws the plot.  The elevation value is a
simple average of the elevation values from the last group of points.
.SH OPTIONS
.TP
-color
Starts the program in the "Color" color mode.
.TP
-gray
Starts the program in the "Gray" color mode.
.TP
-night
Starts the program in the "Night" color mode.
.SH TIME DETAILS
The quality of log files can range from perfect to ridiculous.  Most
lines in the log files do not know what year it is.  The program
begins reading lines and ignores all of them that it comes to until
the first "State of Health" line is reached.  The program uses the
timestamp from that line, which contains the year, as the beginning
time of the file.  Lines timestamped before that time, either because
of a file problem, or a real DAS clock/timing problem of some sort,
will be ignored.  These lines will be counted as "skipped" lines.  The
number of lines skipped while reading the file will be displayed in
the information area of the display (lower left-hand corner).  For a
well-behaved DAS/normally produced log file this number will be 2 --
PASSCAL programs that produce log files place a line with their name,
and the blank line before the first "State of Health" line.  If the
number of skipped lines is large then the file may have to be looked
at manually with an editor, or see the Options|Ignore Timing Errors
option below.
.sp
In addition to individual lines timestamped before the begin time of
the file being rejected, subsequent "State of Health" lines are also
checked against each other.  If a "State of Health" line with time X
is found, and the next "State of Health" line has a time before X all
lines will be skipped until the next "State of Health" line with a
time after X is found.  If a DAS generates timestamps way in the
future, and then at some point jumps back to the correct time, then it
is possible that the program may skip all lines from the wrong time
point on.  The file may need to be manually edited to get the program
to plot anything.
.sp
Because of the way lines are rejected above, it is possible that a few
data points from the beginning of a new year may be lost.  Most lines
do not know what year it is, and the first lines of a new year will be
evaluated as being recorded on day 001 of the 'old' year until a
"State of Health" message is found (usually no more that every 20th
line of a log file) which will then set the internal year value to the
new year.  Only three lines were skipped in a test log file that was
produced during the 2001-2002 year change, and of the three lines only
one contained information that would have been recorded and plotted.
.sp
Internally all dates/times are converted to the number of seconds
since Jan 1, 1970.  If you have log files with times before 1970 I
have no idea what will happen.
.sp
The Options|Ignore Timing Errors checkbox may be checked which will
plot everything no matter how bad any of the timestamps are.  The
first lines of the file (usually two) before the first State of Health
line will still be skipped.
.SH DISPLAY ANOMOLIES
Python, which most of the program is written in, and specifically the
Tkinter addition to Python, isn't as refined as it needs to be to keep
a really annoying thing from happening when the main display window is
resized.  Some window managers, the program that actually controls
everything that you see on the screen, will generate many "configure
events" while the window is being resized.  This will cause logpeek to
try and redraw the graphs as you are moving the mouse.  Depending on
the number of points this can take several seconds.  This can cause
logpeek to redraw the graphs upto several times after the window has
been resized.  To avoid this the option in the window manager that
causes the window to be redrawn while being resized should be
disabled.  In most cases this will cause the window manager to send
logpeek only one configure event after the window has been resized and
the mouse button released.
.SH AUTHOR
Bob Greschke, September 2005 (Original May 2002)
