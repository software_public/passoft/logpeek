#! /usr/bin/env picpython
#   Reads through one of those HUGE Antelope log files and pulls out everything
#   for an individual station to create a resonable approximation of a ref2segy
#   log file.  This version is just the beginning.
#

from sys import exit

PROG_VERSION = "2023.2.0.0"
PROG_NAME = "HUNT"


print(PROG_NAME, PROG_VERSION)
print()
InFile = input("Antelope file to hunt through: ")
if InFile == "":
    print("No input file entered.")
    print("\a")
    exit()
try:
    Fpi = open(InFile, "r")
except Exception as e:
    print("Oops!")
    print(str(e))
    print()
    exit()
# If this is a true Antelope log file this should get the station name
Line = Fpi.readline()
Parts = Line.split(" ")
Station = ""
if len(Parts) > 3:
    Station = Parts[3][0:len(Parts[3]) - 1]
Fpi.seek(0)
if Station != "":
    Answer = input("Is this the correct station name?: %s (Y or N): " %
                   Station)
if Answer.upper() == "N":
    Station = input("Enter the station name (for output file name): ")
if Station == "":
    print("No station name (output file name) entered.")
    print("\a")
    exit()
try:
    Fpo = open(Station + ".log", "w")
except Exception as e:
    print("Oops!")
    print(str(e))
    print()
    exit()

Year = input("Start year of this file's data (4 digits): ")
if len(Year) != 4:
    print("4-digit years are required.")
    print("\a")
    exit()

print("Input file: %s" % InFile)
print("Output file: %s.log" % Station)
print("Working...")
First = False
Count = 0
for Line in Fpi:
    Count += 1
    if Count % 100000 == 0:
        print("Working on line %d..." % Count)
    Where = Line.find("LOG:")
    if Where != -1:
        if First is False:
            LLine = Line[Where + 5:]
# Make up a "State of Health" line using the timestamp of the first LOG line
# found and the Year entered above
            Parts = LLine.split(" ")
            Fpo.write("State of Health  %s:%s:000   ST: %s\n" %
                      (Year[2:], Parts[0], Station))
            First = True
        Fpo.write("%s\n" % Line[Where + 5:].strip())

Fpi.close()
Fpo.close()
print("Finished.\a")
