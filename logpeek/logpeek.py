#! /usr/bin/env python
# BEGIN PROGRAM: LOGPEEK
# Started: 2001.040
# By: Bob Greschke

from calendar import monthcalendar, setfirstweekday
from copy import deepcopy
from fnmatch import fnmatch
from math import cos, sqrt
from os import (R_OK, W_OK, access, environ, getcwd, listdir, makedirs, remove,
                rename, sep)
from os.path import (abspath, basename, dirname, exists, getsize, isdir,
                     isfile)
from subprocess import call
from sys import argv, exit, platform, stdout, version_info
from time import gmtime, localtime, sleep, strftime, time
from warnings import filterwarnings
from zipfile import ZipFile, is_zipfile

PROGSystem = platform[:3].lower()
PROG_NAME = "LOGPEEK"
PROG_NAMELC = "logpeek"
PROG_VERSION = "2023.2.0.0"
PROG_LONGNAME = "RT130 Log File and Data Peeker"
PROG_SETUPSVERS = "A"

# This will stay "" if there is no passed path/filename supplied on the command
# line.
CLAFile = ""
# If set it tells the program that the user is running it from the command
# line which affects how the initial paths are set.
PROGCLRunning = 0
# If True the program will ignore the saved main display geometry information.
PROGIgnoreGeometry = False
# -x will start the program without reading previously saved setups.
PROGIgnoreSetups = False
# Not used by this program.
PROG_SETUPSUSECWD = False
for Arg in argv[1:]:
    if Arg == "-#":
        stdout.write("%s\n" % PROG_VERSION)
        exit(0)
    elif Arg == "-c":
        PROGCLRunning = 1
# The user might include a directory name or something after the -c. This will
# make it be ignored.
        break
    elif Arg == "-g":
        PROGIgnoreGeometry = True
    elif Arg == "-x":
        PROGIgnoreSetups = True
    else:
        # If it is none of the above then treat the argument as a
        # path/filename.
        CLAFile = Arg

########################
# BEGIN: versionChecks()
# LIB:versionChecks():2019.044
#   Checks the current version of Python and sets up a couple of things for the
#   rest of the program to use.
#   Obviously this is not a real function. It's just a collection of things for
#   all programs to check and making it look like a library function makes it
#   easy to update everywhere.
PROG_PYVERSION = "%d.%d.%d" % (
    version_info[0], version_info[1], version_info[2])
if PROG_PYVERSION.startswith("3"):
    from tkinter import *
    from tkinter.font import Font
    from urllib.request import urlopen
    # if PROG_NAME == "WEBEDIT":
    #     from html.parser import HTMLParser
    #     # From Pillow. I don't know if Python Imaging Library will ever be
    #     # for Py3.
    #     from PIL import Image, ImageTk
    astring = str
    anint = int
    arange = range
else:
    stdout.write("Unsupported Python version: %s\nStopping.\n" %
                 PROG_PYVERSION)
    exit(0)
# Nice. Right-click on a Mac using Anaconda Tkinter generates a <Button-2>
# event, instead of <Button-3>.
B2Glitch = False
if PROGSystem == "dar":
    B2Glitch = True
# These should be big enough for my programs, and not bigger than any system
# they run on can handle. These are around in some form on some systems with
# some versions, but with these figuring out what is where is moot.
maxInt = 1E100
maxFloat = 1.0E100
# END: versionChecks

# TESTING-PROFILE. Uncomment.
# import profile

filterwarnings("ignore")
setfirstweekday(6)

# This is way up here so StringVars and IntVars can be declared throughout the
# code.
Root = Tk()
Root.withdraw()

# For the program's forms, Text fields, buttons...
PROGBar = {}
PROGCan = {}
PROGEnt = {}
PROGFrm = {}
PROGMsg = {}
PROGTxt = {}
PROGSb = {}

#####################
# BEGIN: option_add()
# LIB:option_add():2019.037
#   A collection of setup items common to most of my programs.
# Where all of the vars for saving and loading setups are kept.
PROGSetups = []
# These may be altered by loading the setups. If they end up the same as
# PROGScreen<Height/Width>OrigNow then the program will know that it is
# running on the same screen as before.
PROGScreenHeightSaved = IntVar()
PROGScreenWidthSaved = IntVar()
PROGScreenHeightSaved.set(Root.winfo_screenheight())
PROGScreenWidthSaved.set(Root.winfo_screenwidth())
PROGSetups += ["PROGScreenHeightSaved", "PROGScreenWidthSaved"]
# Alter these if you want to fool the program into doing something on a small
# screen.
PROGScreenHeightNow = Root.winfo_screenheight()
PROGScreenWidthNow = Root.winfo_screenwidth()
# Fonts: a constant nagging problem, though less now. I've stopped trying
# to micromanage them and mostly let the user suffer with what the system
# decides to use.
# Some items (like ToolTips and Help text) may not want to have their fonts
# resizable. So in that case use these Orig fonts whose values do not get saved
# to the setups.
PROGOrigMonoFont = Text().cget("font")
PROGOrigPropFont = Entry().cget("font")
# Only two fonts. If something needs more it will have to modify these.
PROGMonoFont = Font(font=Text()["font"])
PROGMonoFontSize = IntVar()
PROGMonoFontSize.set(PROGMonoFont["size"])
# I think this is some damn Linux-Tcl/Tk bug of some kind, but CentOS7/Tk8.5
# (in 2018) reported a "size" of 0. Just set these to -12, so resizing the
# font does something sensible. Same below.
if PROGMonoFontSize.get() == 0:
    PROGMonoFont["size"] = -12
    PROGMonoFontSize.set(-12)
# Entry() is used because it seems to be messed with less on different
# systems unlike Label() font which can be set to some bizarre stuff.
PROGPropFont = Font(font=Entry()["font"])
# Used by some plotting routines.
PROGPropFontHeight = PROGPropFont.metrics("ascent") + \
    PROGPropFont.metrics("descent")
PROGPropFontSize = IntVar()
PROGPropFontSize.set(PROGPropFont["size"])
if PROGPropFontSize.get() == 0:
    PROGPropFont["size"] = -12
    PROGPropFontSize.set(-12)
Root.option_add("*Font", PROGPropFont)
Root.option_add("*Text*Font", PROGMonoFont)
if PROGSystem == "dar":
    PROGSystemName = "Darwin"
elif PROGSystem == "lin":
    PROGSystemName = "Linux"
elif PROGSystem == "win":
    PROGSystemName = "Windows"
elif PROGSystem == "sun":
    PROGSystemName = "Sun"
else:
    PROGSystemName = "Unknown (%s)" % PROGSystem
# Depending on the Tkinter version or how it was compiled the scroll bars can
# get pretty narrow and hard to grab.
if Scrollbar().cget("width") < "16":
    Root.option_add("*Scrollbar*width", "16")
# Just using RGB for everything since some things don't handle color names
# correctly, like PIL on macOS doesn't handle "green" very well.
# b = dark blue, was the U value for years, but it can be hard to see, so U
#     was lightened up a bit.
# Orange should be #FF7F00, but #DD5F00 is easier to see on a white background
# and it still looks OK on a black background.
# Purple should be A020F0, but that was a little dark.
# "X" should not be used. Including X at the end of a passed color pair (or by
# itself) indicates that a Toplevel or dialog box should use grab_set_global()
# which is not a color.
Clr = {"B": "#000000", "C": "#00FFFF", "G": "#00FF00", "M": "#FF00FF",
       "R": "#FF0000", "O": "#FF7F00", "W": "#FFFFFF", "Y": "#FFFF00",
       "E": "#DFDFDF", "A": "#8F8F8F", "K": "#3F3F3F", "U": "#0070FF",
       "N": "#007F00", "S": "#7F0000", "y": "#7F7F00", "u": "#ADD8E6",
       "s": "#FA8072", "p": "#FFB6C1", "g": "#90EE90", "r": "#EFEFEF",
       "P": "#AA22FF", "b": "#0000FF"}
# This is just if the program wants to let the user know what the possibilities
# are.
ClrDesc = {"B": "black", "C": "cyan", "G": "green", "M": "magenta",
           "R": "red", "O": "orange", "W": "white", "Y": "yellow",
           "E": "light gray", "A": "gray", "K": "dark gray", "U": "blue",
           "N": "dark green", "S": "dark red", "y": "dark yellow",
           "u": "light blue", "s": "salmon", "p": "light pink",
           "g": "light green", "r": "very light gray", "P": "purple",
           "b": "dark blue"}
# Now things get ugly. cget("bg") can return a hex triplet, or a color word,
# or something like "systemWindowBody". All of my programs will attempt to use
# the Root background value as their default value. This will determine what
# needs to be done to get a hex triplet value for the Root background color.
Clr["D"] = Root.cget("background")
if Clr["D"].startswith("#"):
    pass
else:
    Value = Root.winfo_rgb(Clr["D"])
    if max(Value) < 256:
        Clr["D"] = "#%02X%02X%02X" % Value
    else:
        Clr["D"] = "#%04X%04X%04X" % Value
ClrDesc["D"] = "default"
# The color of a button (like a "Stop" button) may be checked to see if a
# command is still active. Specifically set this so things just work later on.
# Same thing for a Label used as an indicator.
Root.option_add("*Button*background", Clr["D"])
Root.option_add("*Label*background", Clr["D"])
Root.option_add("*Button*takeFocus", "0")
# Newer versions of Tkinter are setting this to 1. That's too small.
Root.option_add("*Button*borderWidth", "2")
Root.option_add("*Canvas*borderWidth", "0")
Root.option_add("*Canvas*highlightThickness", "0")
Root.option_add("*Checkbutton*anchor", "w")
Root.option_add("*Checkbutton*takeFocus", "0")
Root.option_add("*Checkbutton*borderWidth", "1")
Root.option_add("*Entry*background", Clr["W"])
Root.option_add("*Entry*foreground", Clr["B"])
Root.option_add("*Entry*highlightThickness", "2")
Root.option_add("*Entry*insertWidth", "3")
Root.option_add("*Entry*highlightColor", Clr["B"])
Root.option_add("*Entry*disabledBackground", Clr["D"])
Root.option_add("*Entry*disabledForeground", Clr["B"])
Root.option_add("*Listbox*background", Clr["W"])
Root.option_add("*Listbox*foreground", Clr["B"])
Root.option_add("*Listbox*selectBackground", Clr["G"])
Root.option_add("*Listbox*selectForeground", Clr["B"])
Root.option_add("*Listbox*takeFocus", "0")
Root.option_add("*Listbox*exportSelection", "0")
Root.option_add("*Radiobutton*takeFocus", "0")
Root.option_add("*Radiobutton*borderWidth", "1")
Root.option_add("*Scrollbar*takeFocus", "0")
# When the slider is really small this might help make it easier to see, but
# I don't know what the system might set for the slider color.
Root.option_add("*Scrollbar*troughColor", Clr["A"])
Root.option_add("*Text*background", Clr["W"])
Root.option_add("*Text*foreground", Clr["B"])
Root.option_add("*Text*takeFocus", "0")
Root.option_add("*Text*highlightThickness", "0")
Root.option_add("*Text*insertWidth", "0")
Root.option_add("*Text*width", "0")
Root.option_add("*Text*padX", "3")
Root.option_add("*Text*padY", "3")
# To control the color of the buttons better in X-Windows programs.
Root.option_add("*Button*activeBackground", Clr["D"])
Root.option_add("*Checkbutton*activeBackground", Clr["D"])
Root.option_add("*Radiobutton*activeBackground", Clr["D"])
# Used by various time functions.
# First day of the month for each non-leap year month MINUS 1. This will get
# subtracted from the DOY, so a DOY of 91, minus the first day of April 90
# (91-90) will leave the 1st of April. The 365 is the 1st of Jan of the next
# year.
PROG_FDOM = (0, 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365)
# Max days per month.
PROG_MAXDPMNLY = (0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
PROG_MAXDPMLY = (0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
# Not very friendly to other countries, but...
PROG_CALMON = ("", "JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE",
               "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER",
               "DECEMBER")
PROG_CALMONS = ("", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG",
                "SEP", "OCT", "NOV", "DEC")
PROG_MONNUM = {"JAN": 1, "FEB": 2, "MAR": 3, "APR": 4, "MAY": 5, "JUN": 6,
               "JUL": 7, "AUG": 8, "SEP": 9, "OCT": 10, "NOV": 11, "DEC": 12}
# For use with the return of the calendar module weekday function.
PROG_DOW = ("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun")
# Stores the number of seconds to the beginning of a year so they don't have
# to be recalculated all the time.
Y2EPOCH = {}
# First characters that can be used to modifiy searches (only used in ITS so
# far).
PROG_SEARCHMODS = [">=", "<=", "!=", "!~", ">", "<", "=", "~", "_", "*"]
# A second set of help lines that describes error messages that may be seen by
# the user. It gets filled in by the headers of the modules that generate the
# messages, and the contents will be inserted in the regular help by
# formHELP().
HELPError = ""
# END: option_add

# Colors for everything. They get assigned as soon as we know what color mode
# we are in (color or b&w).
DClr = {"ACQStart": "", "ACQStartBG": "", "ACQStop": "", "ACQStopBG": "",
        "MFCAN": "", "GPSCanvas": "", "GPSDots": "", "ClockTime": "",
        "DCDIFF": "", "DCDIFFBG": "", "DEFS": "", "DEFSBG": "", "DISCREP": "",
        "DISCREPBG": "", "DRSET": "", "DRSETBG": "", "DUMPOn": "",
        "DUMPOnBG": "", "DUMPOff": "", "DUMPOffBG": "", "ERROR": "",
        "ERRORBG": "", "EVT": "", "EVTBG": "", "GPSErr": "", "GPSErrBG": "",
        "GPSOff": "", "GPSOffBG": "", "GPSOn": "", "GPSOnBG": "", "GPSLK": "",
        "GPSLKBG": "", "ICPE": "", "ICPEBG": "", "JERK": "", "JERKBG": "",
        "Label": "", "MaxMinL": "", "MRC": "", "MRCBG": "", "NETDown": "",
        "NETDownBG": "", "NETUp": "", "NETUpBG": "", "Plot": "", "PWRUP": "",
        "PWRUPBG": "", "CLKPWR": "", "CLKPWRBG": "", "RESET": "",
        "RESETBG": "", "SOH": "", "SOHBG": "", "TEMP": "", "TEMPBG": "",
        "TMJMPPos": "", "TMJMPPosBG": "", "TMJMPNeg": "", "TMJMPNegBG": "",
        "VOLT": "", "VOLTBG": "", "BKUP": "", "BKUPBG": "", "BKUPG": "",
        "BKUPGBG": "", "BKUPB": "", "BKUPBBG": "", "BKUPU": "", "BKUPUBG": "",
        "MPP": "", "MPG": "", "MPB": "", "MPU": "", "MPH": "", "WARN": "",
        "WARNBG": "", "WARNM": "", "WARNBGM": ""}
PROGColorModeRVar = StringVar()
PROGColorModeRVar.set("B")
PROGGeometryVar = StringVar()
PROGSetups += ["PROGColorModeRVar", "PROGGeometryVar"]

# These will be used by warning messages to let the user know they are about
# to have time to go get some coffee.
PROGBIG_LOG = 20000000
PROGBIG_REF = 1000000000
PROGBIG_UCF = 4000000000
PROGBIG_ZCF = 3000000000

# Try to get the machine's network name. It will be put in the Root.title().
PROGHostname = ""
try:
    from socket import gethostname
    PROGHostname = gethostname().split(".")[0]
except Exception:
    pass


# ==============================================
# BEGIN: ========== PROGRAM FUNCTIONS ==========
# ==============================================


#####################
# BEGIN: allPlotsOn()
# FUNC:allPlotsOn():2013.231
def allPlotsOn():
    for Key in list(DGrf.keys()):
        # These take up a lot of display space, so don't automatically turn
        # them on.
        if Key != "DU1" and Key != "DU2" and Key != "MP123" and \
                Key != "MP456":
            DGrf[Key].set(1)
    reconfigDisplay()
    return
# END: allPlotsOn


##########################################
# BEGIN: aveQGPSPositions(Positions, Dict)
# FUNC:aveQGPSPositions():2012.089
def aveQGPSPositions(Positions, Dict):
    Lat = 0.0
    Long = 0.0
    Elev = 0.0
    for Position in Positions:
        Lat += Position[0]
        Long += Position[1]
        Elev += Position[2]
    Dict["latf"] = Lat / len(Positions)
    Dict["lonf"] = Long / len(Positions)
    Dict["elef"] = Elev / len(Positions)
# Normalize the latf to 0 at NP to 180 at SP.
    if Dict["latf"] < 0.0:
        Dict["latn"] = 90.0 + abs(Dict["latf"])
    else:
        Dict["latn"] = 90.0 - Dict["latf"]
# Normalize the long to 0 at W180 to 360 at E180.
    if Dict["lonf"] < 0.0:
        Dict["lonn"] = 180.0 - abs(Dict["lonf"])
    else:
        Dict["lonn"] = 180.0 + Dict["lonf"]
    return Dict
# END: aveQGPSPositions


##############################
# BEGIN: class BButton(Button)
# LIB:BButton():2009.239
#   A sub-class of Button() that adds a bit of additional color control and
#   that adds a space before and after the text on Windows systems, otherwise
#   the edge of the button is right at the edge of the text.
class BButton(Button):
    def __init__(self, master=None, **kw):
        if PROGSystem == "win" and "text" in kw:
            if kw["text"].find("\n") == -1:
                kw["text"] = " " + kw["text"] + " "
            else:
                # Add " " to each end of each line so all lines get centered.
                parts = kw["text"].split("\n")
                ntext = ""
                for part in parts:
                    ntext += " " + part + " \n"
                kw["text"] = ntext[:-1]
# Some systems have the button change color when rolled over.
        if "bg" in kw:
            kw["activebackground"] = kw["bg"]
        if "fg" in kw:
            kw["activeforeground"] = kw["fg"]
        Button.__init__(self, master, **kw)
# END: BButton


#######################
# BEGIN: bcd2Int(InStr)
# FUNC:bcd2Int():2018.240
#   Converts the passed 1 or 2 character/byte BCD value into an integer by
#   looking up the values (AND'ing and >>'ing slow things down).
#   There's no error checking to keep it fast so don't pass it things like
#   an "empty" string.
#   Since it is global just using BCDTable in-place in the code and not calling
#   bcd2Int() makes things even faster.
#   The tables go all of the way from 0 to 255. Some BCD items that get passed
#   are not digits 0-9, so this will handle that situation as well as handle
#   garbage numbers from data corruption. The returned value may not be
#   correct, but nothing will crash.
#   "My" programs pass 1 or 2 character strings in Python 2, and 1 byte ints or
#   2 byte bytes in Python 3.
BCDTable = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0, 0, 0, 10, 11, 12, 13,
            14, 15, 16, 17, 18, 19, 0, 0, 0, 0, 0, 0, 20, 21, 22, 23, 24, 25,
            26, 27, 28, 29, 0, 0, 0, 0, 0, 0, 30, 31, 32, 33, 34, 35, 36, 37,
            38, 39, 0, 0, 0, 0, 0, 0, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49,
            0, 0, 0, 0, 0, 0, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 0, 0, 0,
            0, 0, 0, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 0, 0, 0, 0, 0, 0,
            70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 0, 0, 0, 0, 0, 0, 80, 81,
            82, 83, 84, 85, 86, 87, 88, 89, 0, 0, 0, 0, 0, 0, 90, 91, 92, 93,
            94, 95, 96, 97, 98, 99, 154, 155, 156, 57, 158, 159, 160, 161, 162,
            163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175,
            176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188,
            189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201,
            202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214,
            215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227,
            228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240,
            241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253,
            254, 255]
BCDTable2 = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09",
             "00", "00", "00", "00", "00", "00", "10", "11", "12", "13", "14",
             "15", "16", "17", "18", "19", "00", "00", "00", "00", "00", "00",
             "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "00",
             "00", "00", "00", "00", "00", "30", "31", "32", "33", "34", "35",
             "36", "37", "38", "39", "00", "00", "00", "00", "00", "00", "40",
             "41", "42", "43", "44", "45", "46", "47", "48", "49", "00", "00",
             "00", "00", "00", "00", "50", "51", "52", "53", "54", "55", "56",
             "57", "58", "59", "00", "00", "00", "00", "00", "00", "60", "61",
             "62", "63", "64", "65", "66", "67", "68", "69", "00", "00", "00",
             "00", "00", "00", "70", "71", "72", "73", "74", "75", "76", "77",
             "78", "79", "00", "00", "00", "00", "00", "00", "80", "81", "82",
             "83", "84", "85", "86", "87", "88", "89", "00", "00", "00", "00",
             "00", "00", "90", "91", "92", "93", "94", "95", "96", "97", "98",
             "99", "00", "00", "00", "00", "00", "00",
             "A0", "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9",
             "AA", "AB", "AC", "AD", "AE", "AF",
             "B0", "B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9",
             "BA", "BB", "BC", "BD", "BE", "BF",
             "C0", "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9",
             "CA", "CB", "CC", "CD", "CE", "CF",
             "D0", "D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9",
             "DA", "DB", "DC", "DD", "DE", "DF",
             "E0", "E1", "E2", "E3", "E4", "E5", "E6", "E7", "E8", "E9",
             "EA", "EB", "EC", "ED", "EE", "EF",
             "F0", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9",
             "FA", "FB", "FC", "FD", "FE", "FF"]


def bcd2Int(In):
    # This should only trigger in Python 2, but must be before bytes, which
    # will also trigger in Python 2.
    if isinstance(In, str):
        if len(In) == 2:
            return int(BCDTable2[ord(In[0])] + BCDTable2[ord(In[1])], 16)
        else:
            return BCDTable[ord(In)]
    elif isinstance(In, int):
        return BCDTable[In]
    elif isinstance(In, bytes):
        In = In.decode("latin-1")
        return int(BCDTable2[ord(In[0])] + BCDTable2[ord(In[1])], 16)
# END: bcd2Int


################################
# BEGIN: beep(Howmany, e = None)
# LIB:beep():2018.235
#   Just rings the terminal bell the number of times requested.
# NEEDS: from time import sleep
#        updateMe()
PROGNoBeepingCRVar = IntVar()
PROGSetups += ["PROGNoBeepingCRVar"]


def beep(Howmany, e=None):
    if PROGNoBeepingCRVar.get() == 0:
        # In case someone passes something wild.
        if Howmany > 20:
            Howmany = 20
        for i in arange(0, Howmany):
            Root.bell()
            if i < Howmany - 1:
                updateMe(0)
                sleep(.15)
    return
# END: beep


##########################
# BEGIN: busyCursor(OnOff)
# LIB:busyCursor():2018.236
# Needs PROGFrm, and updateMe().
DefCursor = Root.cget("cursor")


def busyCursor(OnOff):
    if OnOff == 0:
        TheCursor = DefCursor
    else:
        TheCursor = "watch"
    Root.config(cursor=TheCursor)
    for Fram in list(PROGFrm.values()):
        if Fram is not None:
            Fram.config(cursor=TheCursor)
    updateMe(0)
    return
# END: busyCursor


#########################################
# BEGIN: buttonBG(Butt, Colr, State = "")
# LIB:buttonBG():2018.234
def buttonBG(Butt, Colr, State=""):
    # Try since this may get called without the button even existing.
    try:
        if isinstance(Butt, astring):
            # Set both items to keep the button from changing colors on *NIXs
            # when the mouse rolls over. Also only use the first character of
            # the passed value so we can pass bg/fg color pairs.
            # If the State is "" leave it alone, otherwise change it to what we
            # are told.
            if len(State) == 0:
                PROGButs[Butt].configure(bg=Clr[Colr[0]],
                                         activebackground=Clr[Colr[0]])
            else:
                PROGButs[Butt].configure(bg=Clr[Colr[0]],
                                         activebackground=Clr[Colr[0]],
                                         state=State)
        else:
            if len(State) == 0:
                Butt.configure(bg=Clr[Colr[0]],
                               activebackground=Clr[Colr[0]])
            else:
                Butt.configure(bg=Clr[Colr[0]],
                               activebackground=Clr[Colr[0]], state=State)
        updateMe(0)
    except Exception:
        pass
    return
# END: buttonBG


#################################################################
# BEGIN: canText(Can, Cx, Cy, Color, Str, Anchor = "w", Tag = "")
# LIB:canText():2018.234
#   Used to print text to a canvas such that the color of individual words
#   in a line can be changed. Use 0 for Cx to tell the routine to place this
#   Str at the end of the last Str passed.
CANTEXTLastX = 0
CANTEXTLastWidth = 0


def canText(Can, Cx, Cy, Color, Str, Anchor="w", Tag=""):
    global CANTEXTLastX
    global CANTEXTLastWidth
    if Cx == 0:
        Cx = CANTEXTLastX
    if isinstance(Color, astring):
        # This way it can be passed "W" or #000000.
        if Color.startswith("#") is False:
            FClr = Clr[Color[0]]
        else:
            FClr = Color
        if len(Tag) == 0:
            ID = Can.create_text(Cx, Cy, text=Str, fill=FClr,
                                 font=PROGPropFont, anchor=Anchor)
        else:
            ID = Can.create_text(Cx, Cy, text=Str, fill=FClr,
                                 font=PROGPropFont, anchor=Anchor, tags=Tag)
# This may be an input from getAColor().
    elif isinstance(Color, tuple):
        if len(Tag) == 0:
            ID = Can.create_text(Cx, Cy, text=Str, fill=Color[0],
                                 font=PROGPropFont, anchor=Anchor)
        else:
            ID = Can.create_text(Cx, Cy, text=Str, fill=Color[0],
                                 font=PROGPropFont, anchor=Anchor, tags=Tag)
    else:
        if len(Tag) == 0:
            ID = Can.create_text(Cx, Cy, text=Str, fill="white",
                                 font=PROGPropFont, anchor=Anchor)
        else:
            ID = Can.create_text(Cx, Cy, text=Str, fill="white",
                                 font=PROGPropFont, anchor=Anchor, tags=Tag)
    L, T, R, B = Can.bbox(ID)
# -1: I don't know if this is a Tkinter bug or if it just happens to be
# specific to the font that is being used or what, but it has to be done.
    CANTEXTLastX = R - 1
    CANTEXTLastWidth = R - L
    return ID
# END: canText


###############################################################################
# BEGIN: center(Parent, TheFrame, Where, InOut, Show = True, CenterX = 0, \
#                CenterY = 0)
# LIB:center():2019.002
#   Where tells the function where in relation to the Parent TheFrame should
#   show up. Use the diagram below to figure out where things will end up.
#   Where can also be NX,SX,EX,etc. to force edge of the display checking for\
#   when you absolutely, positively don't want something coming up off the
#   edge of the display. "C" or "" can be used to put TheFrame in the center
#   of Parent.
#
#      +---------------+
#      | NW    N    NE |
#      |               |
#      | W     C     E |
#      |               |
#      | SW    S    SE |
#      +---------------+
#
#   Set Parent to None to use the whole display as the parent.
#   Set InOut to "I" or "O" to control if TheFrame shows "I"nside or "O"outside
#   the Parent (does not apply if the Parent is None).
#
#   CenterX and CenterY not equal to zero overrides everything.
#
def center(Parent, TheFrame, Where, InOut, Show=True, CenterX=0,
           CenterY=0):
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    if isinstance(TheFrame, astring):
        TheFrame = PROGFrm[TheFrame]
# Size of the display(s). Still won't be good for dual displays, but...
    DW = PROGScreenWidthNow
    DH = PROGScreenHeightNow
# Kiosk mode. Just take over the whole screen. Doesn't check for, but only
# works for Root. The -30 is a fudge, because systems lie about their height
# (but not their age).
    if Where == "K":
        Root.geometry("%dx%d+0+0" % (DW, DH - 30))
        Root.deiconify()
        Root.lift()
        updateMe(0)
        return
# So all of the dimensions get updated.
    updateMe(0)
    FW = TheFrame.winfo_reqwidth()
    if TheFrame == Root:
        # Different systems have to be compensated for a little because of
        # differences in the reported heights (mostly title and menu bar
        # heights). Some systems include the height and some don't, and, of
        # course, it all depends on the font sizes, so there is little chance
        # of this fudge ever being 100% correct.
        if PROGSystem == "dar" or PROGSystem == "win":
            FH = TheFrame.winfo_reqheight()
        else:
            FH = TheFrame.winfo_reqheight() + 50
    else:
        FH = TheFrame.winfo_reqheight()
# Find the center of the Parent.
    if CenterX == 0 and CenterY == 0:
        if Parent is None:
            PX = 0
            PY = 0
            PW = PROGScreenWidthNow
            PH = PROGScreenHeightNow
# A PW of >2560 (the width of a 27" iMac) probably means the user has two
# monitors. Tkinter just gets fed the total width and the smallest display's
# height, so just set the size to 1024x768 and then let the user resize and
# reposition as needed. It's what they get for being so lucky.
            if PW > 2560:
                PW = 1024
                PH = 768
            CenterX = PW / 2
            CenterY = PH / 2 - 25
        elif Parent == Root:
            PX = Parent.winfo_x()
            PW = Parent.winfo_width()
            CenterX = PX + PW / 2
# Macs, Linux and Suns think the top of the Root window is below the title
# and menu bars.  Windows thinks the top of the window is the top of the
# window, so adjust the window heights accordingly to try and cover that up.
# Same problem as the title and menu bars.
            if PROGSystem == "win":
                PY = Parent.winfo_y()
                PH = Parent.winfo_height()
            else:
                PY = Parent.winfo_y() - 50
                PH = Parent.winfo_height() + 50
            CenterY = PY + PH / 2
        else:
            PX = Parent.winfo_x()
            PW = Parent.winfo_width()
            CenterX = PX + PW / 2
            PY = Parent.winfo_y()
            PH = Parent.winfo_height()
            CenterY = PY + PH / 2
# Can't put forms outside the whole display.
        if Parent is None or InOut == "I":
            InOut = 1
        else:
            InOut = -1
        HadX = False
        if Where.find("X") != -1:
            Where = Where.replace("X", "")
            HadX = True
        if Where == "C":
            XX = CenterX - FW / 2
            YY = CenterY - FH / 2
        elif Where == "N":
            XX = CenterX - FW / 2
            YY = PY + (50 * InOut)
        elif Where == "NE":
            XX = PX + PW - FW - (50 * InOut)
            YY = PY + (50 * InOut)
        elif Where == "E":
            XX = PX + PW - FW - (50 * InOut)
            YY = CenterY - TheFrame.winfo_reqheight() / 2
        elif Where == "SE":
            XX = PX + PW - FW - (50 * InOut)
            YY = PY + PH - FH - (50 * InOut)
        elif Where == "S":
            XX = CenterX - TheFrame.winfo_reqwidth() / 2
            YY = PY + PH - FH - (50 * InOut)
        elif Where == "SW":
            XX = PX + (50 * InOut)
            YY = PY + PH - FH - (50 * InOut)
        elif Where == "W":
            XX = PX + (50 * InOut)
            YY = CenterY - TheFrame.winfo_reqheight() / 2
        elif Where == "NW":
            XX = PX + (50 * InOut)
            YY = PY + (50 * InOut)
# Try to make sure the system's title bar buttons are visible (which may not
# always be functioning, but there you go).
        if HadX is True:
            # None are on the bottom.
            if (CenterY + FH / 2) > DH:
                YY = YY - ((CenterY + FH / 2) - DH + 20)
# Never want things off the top.
            if YY < 0:
                YY = 10
# But now it is OS-dependent.
# Buttons in upper-left. Fix the right edge, but then check to see if it needs
# to be moved back to the right.
            if PROGSystem == "dar" or PROGSystem == "sun":
                if (CenterX + FW / 2) > DW:
                    XX = XX - ((CenterX + FW / 2) - DW + 20)
                if XX < 0:
                    XX = 10
# Opposite corner.
            elif PROGSystem == "lin" or PROGSystem == "win":
                if XX < 0:
                    XX = 10
                if (CenterX + FW / 2) > DW:
                    XX = XX - ((CenterX + FW / 2) - DW + 20)
        TheFrame.geometry("+%i+%i" % (XX, YY))
    else:
        # Just do what we're told.
        TheFrame.geometry("+%i+%i" % (CenterX - FW / 2, CenterY - FH / 2))
    if Show is True:
        TheFrame.deiconify()
        TheFrame.lift()
    updateMe(0)
    return
# END: center


###################################
# BEGIN: changeDecodeMode(e = None)
# FUNC:changeDecodeMode():2013.207
def changeDecodeMode(e=None):
    if OPTDecodeModeCVar.get() == 1:
        Answer = formMYD(Root,
                         (("I Know", LEFT, "ok"),
                          ("Cancel It", LEFT, "cancel")),
                         "cancel", "YB", "Just Checking.",
                         "You know that turning this option on will slow raw "
                         "data decoding (for the Raw Data Plot) down "
                         "considerably, right? And that the data set to be "
                         "read should be pretty small, or only be a couple of "
                         "days long using the From/To date fields, right? And "
                         "that reading too much data may cause LOGPEEK, and "
                         "maybe even your computer, to crash, right?")
        if Answer == "cancel":
            OPTDecodeModeCVar.set(0)
    return
# END: changeDecodeMode


#####################################################################
# BEGIN: changeMainDirs(Parent, Which, Mode, Var, Title, WhichDir="")
# LIB:changeMainDirs():2018.234
# Needs PROGFrm, formMYDF(), and msgLn().
#   Mode = formMYDF() mode value (some callers may not want to allow directory
#          creation, for example).
#      1 = just picking
#      2 = picking and creating
#  D,W,M = may be added to the Mode for the main directories Default button
#          (see formMYDF()).
#   Var = if not None the selected directory will be placed there, instead of
#         one of the "main" Vars. May not be used in all programs.
#   Title = Will be used for the title of the form if Var is not None.
# WhichDir = If supplied this will be 'which directory?' was changed in the
#            change message when Which is "self".
#   Not all programs will use all items.
def changeMainDirs(Parent, Which, Mode, Var, Title, WhichDir=""):
    # The caller can pass either.
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    if Which == "theall":
        # Just use the directory as a starting point.
        Answer = formMYDF(Parent, Mode, "Pick A Main Directory For All",
                          PROGMsgsDirVar.get(), "")
        if len(Answer) == 0:
            return (1, "", "Nothing done.", 0, "")
        else:
            # Some of these may not exist in a program.
            try:
                PROGDataDirVar.set(Answer)
            except Exception:
                pass
            try:
                PROGMsgsDirVar.set(Answer)
            except Exception:
                pass
            try:
                PROGWorkDirVar.set(Answer)
            except Exception:
                pass
            return (0, "WB", "All main directories changed to\n   %s" %
                    Answer, 0, "")
    elif Which == "thedata":
        Answer = formMYDF(Parent, Mode, "Pick A Main Data Directory",
                          PROGDataDirVar.get(), "")
        if len(Answer) == 0:
            return (1, )
        elif Answer == PROGDataDirVar.get():
            return (0, "", "Main data directory unchanged.", 0, "")
        else:
            PROGDataDirVar.set(Answer)
            return (0, "WB", "Main Data directory changed to\n   %s" %
                    Answer, 0, "")
    elif Which == "themsgs":
        Answer = formMYDF(Parent, Mode, "Pick A Main Messages Directory",
                          PROGMsgsDirVar.get(), "")
        if len(Answer) == 0:
            return (1, )
        elif Answer == PROGMsgsDirVar.get():
            return (0, "", "Main messages directory unchanged.", 0, "")
        else:
            PROGMsgsDirVar.set(Answer)
            return (0, "WB", "Main Messages directory changed to\n   %s" %
                    Answer, 0, "")
    elif Which == "thework":
        Answer = formMYDF(Parent, Mode, "Pick A Main Work Directory",
                          PROGWorkDirVar.get(), "")
        if len(Answer) == 0:
            return (1, )
        elif Answer == PROGWorkDirVar.get():
            return (0, "", "Main work directory unchanged.", 0, "")
        else:
            PROGWorkDirVar.set(Answer)
            return (0, "WB", "Main work directory changed to\n   %s" %
                    Answer, 0, "")
# Var and Title must be set for "self".
    elif Which == "self":
        Answer = formMYDF(Parent, Mode, Title, Var.get(), Title)
        if len(Answer) == 0:
            return (1, )
        elif Answer == Var.get():
            if len(WhichDir) == 0:
                return (0, "", "Directory unchanged.", 0, "")
            else:
                return (0, "", "%s directory unchanged." % WhichDir, 0, "")
        else:
            Var.set(Answer)
        if len(WhichDir) == 0:
            return (0, "WB", "Directory changed to\n   %s" % Answer, 0, "")
        else:
            return (0, "WB", "%s directory changed to\n   %s" %
                    (WhichDir, Answer), 0, "")
    return
###############################################################################
# BEGIN: changeMainDirsCmd(Parent, Which, Mode, Var, Title, WhichDir, e = None)
# FUNC:changeMainDirsCmd():2014.062


def changeMainDirsCmd(Parent, Which, Mode, Var, Title, WhichDir, e=None):
    Ret = changeMainDirs(Parent, Which, Mode, Var, Title, WhichDir)
    if Ret[0] == 0:
        # Some programs may not have a messages area.
        try:
            msgLn(0, Ret[1], Ret[2], True, Ret[3])
        except Exception:
            pass
    return
# END: changeMainDirs


###########################
# BEGIN: cleanAFilename(In)
# LIB:cleanAFilename():2013.246
#   Goes through the passed string and turns any sep characters or colons into
#   -.
def cleanAFilename(In):
    Out = ""
    for C in In:
        if C == sep or C == ":":
            C = "-"
        Out += C
    return Out
# END: cleanAFilename


##################################################
# BEGIN: compFs(DirVar, FileVar, VarSet, e = None)
# LIB:compFs():2018.310
# Needs setMsg(), beep()
#     from fnmatch import fnmatch
#     from os.path import basename, dirname, isdir
#   Attempts to complete the directory or file name in an Entry field using
#   the Tab key.
#   - If DirVar is set to a field's StringVar and FileVar is None then the
#     routine only looks for directories.
#   - If DirVar and FileVar are set to StringVars then it works to find a
#     complete fliespec, but getting the path and filenames from different
#     fields.
#   - If DirVar is None and FileVar is set to a StringVar then it trys to
#     complete path and filenames.
#
#   Call compFsSetup() after the Entry field has been created.
def compFs(DirVar, FileVar, VarSet, e=None):
    if VarSet is not None:
        setMsg(VarSet, "", "")
# ---- Caller only wants directories.
    if DirVar is not None and FileVar is None:
        Dir = dirname(DirVar.get())
# This is a slight gotchya. If the field is empty that might mean that the user
# means "/" should be the starting point. If it is they will have to enter the
# / since if we are on Windows I'd have no idea what the default should be.
        if len(Dir) == 0:
            beep(2)
            return
        if Dir.endswith(sep) is False:
            Dir += sep
# Now get what must be a partial directory name, treat it as a file name, but
# then only allow the result of everything to be a directory.
        PartialFile = basename(DirVar.get())
        if len(PartialFile) == 0:
            beep(2)
            return
        PartialFile += "*"
        Files = listdir(Dir)
        Matched = []
        for File in Files:
            if fnmatch(File, PartialFile):
                Matched.append(File)
        if len(Matched) == 0:
            beep(2)
            return
        elif len(Matched) == 1:
            Dir = Dir + Matched[0]
# If whatever matched is not a directory then just beep and return, otherwise
# make it look like a directory and put it into the field.
            if isdir(Dir) is False:
                beep(2)
                return
            if Dir.endswith(sep) is False:
                Dir += sep
            DirVar.set(Dir)
            e.widget.icursor(END)
            return
        else:
            # Get the max number of characters that matched and put the partial
            # directory path into the Var. If Dir+PartialDir is really the
            # directory the user wants they will have to add the sep themselves
            # since with multiple matches I won't know what to do. Consider
            # DIR DIR2 DIR3 with a compFsMaxMatch() return of DIR. The
            # directory DIR would always be selected and set as the path which
            # may not be what the user wanted. Now this could cause trouble
            # downstream since I'm leaving a path in the field without a
            # trailing sep (everything tries to avoid doing that), so the
            # caller will have to worry about that.
            PartialDir = compFsMaxMatch(Matched)
            DirVar.set(Dir + PartialDir)
            e.widget.icursor(END)
            beep(1)
            return
    else:
        # ---- Find a file, but the filespec is in one field.
        if DirVar is None and FileVar is not None:
            Dir = dirname(FileVar.get())
            Which = 1
# ---- Find a file, but path and file are in separate fields.
        elif DirVar is not None and FileVar is not None:
            Dir = dirname(DirVar.get())
            Which = 2
        if len(Dir) == 0:
            beep(2)
            return
        if Dir.endswith(sep) is False:
            Dir += sep
        PartialFile = basename(FileVar.get())
        if len(PartialFile) == 0:
            beep(2)
            return
# Match anything to what the user has entered for the file name.
        PartialFile += "*"
        Files = listdir(Dir)
        Matched = []
        for File in Files:
            if fnmatch(File, PartialFile):
                Matched.append(File)
        if len(Matched) == 0:
            beep(2)
            return
        elif len(Matched) == 1:
            File = Matched[0]
            Filespec = Dir + File
# We can stick a directory name in a single field, but the user will have to
# change the directory field if they are separate (it may be that the user
# is not allowed to change the directory field value and I don't want to
# violate that, plus that could be really confusing for everyone when we have
# multiple matches).
            if isdir(Filespec):
                if Which == 1:
                    if Filespec.endswith(sep) is False:
                        Filespec += sep
                    FileVar.set(Filespec)
                    e.widget.icursor(END)
                    return
                elif Which == 2:
                    beep(2)
                    return
            if Which == 1:
                FileVar.set(Filespec)
            elif Which == 2:
                FileVar.set(File)
            e.widget.icursor(END)
            return
        else:
            PartialFile = compFsMaxMatch(Matched)
            if Which == 1:
                FileVar.set(Dir + PartialFile)
            elif Which == 2:
                FileVar.set(PartialFile)
            e.widget.icursor(END)
            beep(1)
            return
################################
# BEGIN: compFsMaxMatch(TheList)
# FUNC:compFsMaxMatch():2018.310
#   Goes through the items in TheList (should be str's) and returns the string
#   that matches the start of all of the items.
#   This is the same as the library function maxMatch().


def compFsMaxMatch(TheList):
    # This should be the only special case. What is the sound of one thing
    # matching itself?
    if len(TheList) == 1:
        return TheList[0]
    Accum = ""
    CharIndex = 0
# If anything goes wrong just return whatever we've accumulated. This will end
# by no items being in TheList or one of the items running out of characters
# (the try) or by the TargetChar not matching a character from one of the
# items (the raise).
    try:
        while True:
            TargetChar = TheList[0][CharIndex]
            for ItemIndex in arange(1, len(TheList)):
                if TargetChar != TheList[ItemIndex][CharIndex]:
                    raise Exception
            Accum += TargetChar
            CharIndex += 1
    except Exception:
        pass
    return Accum
##############################################################
# BEGIN: compFsSetup(LFrm, LEnt, DirVar, FileVar, VarSet = "")
# FUNC:compFsSetup():2010.255
#   Just sets up the bind's for the passed Entry field. See compFs() for the
#   DirVar and FileVar explainations.


def compFsSetup(LFrm, LEnt, DirVar, FileVar, VarSet):
    LEnt.bind("<FocusIn>", Command(compFsTabOff, LFrm))
    LEnt.bind("<FocusOut>", Command(compFsTabOn, LFrm))
    LEnt.bind("<Key-Tab>", Command(compFs, DirVar, FileVar, VarSet))
    return
#####################################
# BEGIN: compFsTabOff(LFrm, e = None)
# FUNC:compFsTabOff():2010.225


def compFsTabOff(LFrm, e=None):
    LFrm.bind("<Key-Tab>", compFsNullCall)
    return
###################################
# BEGIN: compFsTabOn(LFrm, e = None)
# FUNC:compFsTabOn():2010.225


def compFsTabOn(LFrm, e=None):
    LFrm.unbind("<Key-Tab>")
    return
#################################
# BEGIN: compFsNullCall(e = None)
# FUNC:compFsNullCall():2012.294


def compFsNullCall(e=None):
    return "break"
# END: compFs


# Most of the convertFunctions items are not used by LOGPEEK.
# LOGPEEK only uses:
#   DEPSHOT_FIELDS
#   TSPSHOT_FIELDS
#   SINFO_FIELDS
#   convertDict2DepfileSHOT()
#   convertDict2TSPSfEntry()
#   convertDict2SInfo()
###########################
# BEGIN: convertFunctions()
# LIB:convertFunctions():2019.024
#   A collection of functions for dealing with the input and output of the
#   various supported file types used for shot and receiver position and
#   time information.
#
#   Internally the all information is kept in a "LUNCH" (the program that
#   started all of this) dictionary (which is now called a CONVERT
#   dictionary). Importing, exporting and merging the receiver, shot
#   information is done by using the various elements in each dictionary.
#   Reading in a file of information will create a List of these Dictionaries
#   with each file receiver or shot entry being contained in one dictionary
#   item.
#
#   Generally there is a reader function to read something into a dictionary
#   and a writer to export information from a dictionary to some display or
#   file format (except there is no reader for KML files).  Then there are
#   a few accessory functions to help things along in dealing with the
#   common things callers will want to do to the dictionary values.
#
# A Dictionary of the default information values. Dictionary values should
# be set to these right after an empty dictionary is created by calling
# convertDictDefaults().
# A key of "text" is reserved. It is used when blank or comment lines read
# from a source of entries are retained. In this case "text" is the one and
# only key that may be in the dictionary for the blank or comment line,
# although an "liid" usually slips in there with the "text" item.
ConvertDefaults = \
    {"dtyp": "?",  # Dictionary type: SHOT, RECV, "?", others...
     # This may be used someday for verification so that a
     # function for dealing with receivers does not get
     # called with a dictionary full of shot information.
     # It's just starting to be used.
     "idid": "?",   # Shot (FF)ID, Receiver SN, (whichever)
     "arry": "A",   # Array designation for shots or receivers
     "stid": "?",   # Station ID. Shot point number or name for shots, flag
     # number for receivers. A123 is supported, but won't
     # work for true SEGY shot gathers.
     "lati": "?",   # Latitude
     "long": "?",   # Longitude
     "elev": "?",   # Elevation in meters
     "latf": 0.0,   # Float version of latitude
     "lonf": 0.0,   # Float version of longitude
     "elef": 0.0,   # Float version of elevation
     "latn": 90.0,  # Normalized version of latitude (for mapping). 90=0
     "lonn": 180.0,  # Normalized version of longitude (for mapping). 180=0
     "shtm": "0",   # Shot time YYYY:DOY:HH:MM:SS.sss
     "shte": 0.0,   # Shot time epoch
     "size": "0.0",  # Shot size in kilograms
     "dpth": "0.0",  # Shot depth in meters
     "rtyp": "R",   # (R)egular or (U)phole receiver
     "sens": "?",   # Sensor type/information/serial number/etc.
     "chan": "0",   # Data channel number (0,1,2,3)
     "dept": "?",   # Deployment team name
     "comm": "",    # General comments
     "shts": "*",   # Shots that apply to this receiver (* 1-3 3,5,6 etc.)
     "dptm": "0",   # Deployment time
     "dptf": 0.0,   # Float epoch (RT125) of dptm
     "putm": "0",   # Pickup time
     "putf": float(maxInt),   # Float epoch (RT125) of putm
     "pret": "0.0",  # Pre-trigger time for gather (seconds)
     "post": "0.0",  # Post-trigger time for gather (seconds)
     "radi": "0.0",  # Radius of receivers to include in gather (km.mmm)
     "samp": "0",   # Sample rate to use (sps)
     "rvel": "0",   # Radial velocity to use for gather (m/s)
     "tspy": 0,     # TSP: Shot time YYYY
     "tspd": 0,     # TSP: Shot time DOY
     "tsph": 0,     # TSP: Shot time HH
     "tspm": 0,     # TSP: Shot time MM
     "tsps": 0.0,   # TSP: Shot time SS.sss
     "tsaz": "0",   # TSP: Azimuth value
     "depu": "?",   # This entry made during (D)eployment or (P)ickup
     "dver": "",    # Data version. Depends on the source.
     "time": "?",   # The time of the entry, YYYY:DOY:HH:MM:SS.sss UT
     "styp": "?",   # Sensor type (40HzV, 4Hz, etc.)
     "file": "?",   # Data file the information was extracted from
     "bxid": "?",   # COCI: Box (transcase) ID
     "stat": "?",   # COCI: Current status (In/Out).
     "infg": "",    # COCI: "In" flag. Will be blank or "--IN--".
     "flag": "",    # COCI: A not-printed internal use flag. Others can use
     # this as well.
     "file": "",    # A file name or spec for use by the program that is
     # using the convertFunctions.
     "liid": 0,     # List ID. Filled in by some convertFunctions as a
     # Dictionary index/ID within the returned List for
     # others to use for various things.
     "rmod": "?"}   # Receiver model name
# These were no longer used when LUNCH was simplified in mid-2012.
#        "hgeo":"?",   # LUNCH: Height above geoid in meters
#        "sats":"?",   # LUNCH: Number of satellites the GPSR was seeing
#        "calc":"?",   # LUNCH: Number of satellites the GPSR was using for the
# position fix
#        "fixk":"?",   # LUNCH: Fix kind: G=GPS, D=DGPS, P=PPS fix,
# F=Float RTK, # E=Estimated, M=Manual input,
# S=Simulation mode, R=RTK, I=Invalid, ?=Unknown
#        "fixt":"?",   # LUNCH: Fix type (NF, 2D, 3D, other unsupported values
# (numbers) may appear)
#        "vdop":"?",   # LUNCH: Vertical Dilution of Precision
#        "hdop":"?",   # LUNCH: Horizontal Dilution of Precision
#        "tdop":"?",   # LUNCH: Time Dilution of Precision
#        "pdop":"?",   # LUNCH: Position Dilution of Precision
#        "vepe":"?",   # LUNCH: Vertical Position Error
#        "hope":"?",   # LUNCH: Horizontal Position Error
#        "osep":"?",   # LUNCH: Overall Spherical Estimated Position Error
#        "cctm":"?",   # LUNCH: Control computer time
#        "ltgm":"?",   # LUNCH: Difference between control computer and GMT
# time (should be the time zone setting in hours)
#        "ccid":"?",   # LUNCH: The control computer ID
#        "lver":"?",   # LUNCH: LUNCH program version
#        "chef":"?",   # LUNCH: was the GPS monitor running (i.e. was the
# program collecting information from the GPS)?
# (Y/N)
#        "avtm":"?",   # LUNCH: How much time was the position information
# averaged by the program before sending it to the
# Texan? (seconds)
#        "timc":"?",   # LUNCH: Was the Time field being filled-in with the
# time coming from the GPS [or manually]? (Y/N)
#        "latc":"?",   # LUNCH: Was the Latitude field value being filled-in
# with the latitude value from the GPS [or manually]?
# (Y/N)
#        "lonc":"?",   # LUNCH: Was the Longitude field value being filled-in
# with the longitude value from the GPS [or manually]?
# (Y/N)
#        "elec":"?",   # LUNCH: Was the Elevation field value being filled-in
# with the elevation value from the GPS [or manually]?
# (Y/N)
#        "send":"?",   # LUNCH: Was sending enabled? (Y/N)

# Layout of the deployment file line format information. The block format is
# just key/value pairs that can be in any order following lines with "RECV",
# or "SHOT" as the only thing on the line.
DEPRECV_FIELDS = ("RECV IDID STID ARRY RMOD CHAN SENS RTYP LATI LONG ELEV "
                  "DEPT DPTM PUTM SHTS COMM")
DEPRECVFIELDS = 16  # RECV; plus the stuff below
DEPRECV_REQU = 15
DEPRECV_RTAG = 0    # RECV;
DEPRECV_IDID = 1    # Receiver/Texan ID
DEPRECV_STID = 2    # Station ID
DEPRECV_ARRY = 3    # Array/Sub-array designation
DEPRECV_RMOD = 4    # Receiver/Texan model (like RT125A)
DEPRECV_CHAN = 5    # Channel number (0, 1, 2 or 3)
DEPRECV_SENS = 6    # Sensor type/information
DEPRECV_RTYP = 7    # Receiver type ("R"egular, "U"phole)
DEPRECV_LATI = 8    # Latitude
DEPRECV_LONG = 9    # Longitude
DEPRECV_ELEV = 10   # Elevation
DEPRECV_DEPT = 11   # Deployment team
DEPRECV_DPTM = 12   # Deployment time
DEPRECV_PUTM = 13   # Pickup time
DEPRECV_SHTS = 14   # Shot list
DEPRECV_COMM = 15   # Comments

DEPSHOT_FIELDS = ("SHOT STID ARRY IDID LATI LONG ELEV TIME PRET POST RADI "
                  "SAMP DPTH SIZE RVEL COMM")
DEPSHOTFIELDS = 16  # SHOT; plus the stuff below
DEPSHOT_REQU = 15
DEPSHOT_STAG = 0    # SHOT;
DEPSHOT_STID = 1    # Shot point name/ID
DEPSHOT_ARRY = 2    # Array/Sub-array designation
DEPSHOT_IDID = 3    # Shot ID (basically the FFID)
DEPSHOT_LATI = 4    # Latitude (NDD.ddd)
DEPSHOT_LONG = 5    # Longitude (EDDD.ddd)
DEPSHOT_ELEV = 6    # Elevation (m)
DEPSHOT_TIME = 7    # Shot time (YYYY:DOY:HH:MM:SS.sss)
DEPSHOT_PRET = 8    # Pre-trigger length (secs)
DEPSHOT_POST = 9    # Post-trigger length (secs)
DEPSHOT_RADI = 10   # Radius of receivers to include (km.mmm)
DEPSHOT_SAMP = 11   # Sample rate to use (sps)
DEPSHOT_DPTH = 12   # Depth of shot (m)
DEPSHOT_SIZE = 13   # Size of shot (kg)
DEPSHOT_RVEL = 14   # Radial velocity to use for gather
DEPSHOT_COMM = 15   # Comments

# Layout of the TSP shot file.
TSPSHOT_FIELDS = "TSPY TSPD TSPH TSPM TSPS IDID POST TSAZ"
TSPSHOTFIELDS = 8   # YYYY,DOY,HH,MM...
TSPSHOT_REQU = 7    # Azimuth is optional
TSPSHOT_TSPY = 0    # Year
TSPSHOT_TSPD = 1    # DOY
TSPSHOT_TSPH = 2    # Hour
TSPSHOT_TSPM = 3    # Min
TSPSHOT_TSPS = 4    # Sec
TSPSHOT_IDID = 5    # Shot ID
TSPSHOT_POST = 6    # Shot length to cut
TSPSHOT_TSAZ = 7    # Azimuth

# TSP dasfile fields.
TSPDAS_FIELDS = "STID IDID"
TSPDASFIELDS = 2
TSPDAS_STID = 0   # Station ID (name)
TSPDAS_IDID = 1   # Texan ID

# TSP geometry file fields.
TSPGEOM_FIELDS = "STID LATI LONG ELEV"
TSPGEOMFIELDS = 4
TSPGEOM_STID = 0   # Station ID
TSPGEOM_LATI = 1   # Latitude (N00.000)
TSPGEOM_LONG = 2   # Longitude (E000.000)
TSPGEOM_ELEV = 3   # Elevation (m)

# Simple receiver info file layout.
RINFO_FIELDS = "STID IDID LATI LONG ELEV"
RINFOFIELDS = 5
RINFO_STID = 0
RINFO_IDID = 1
RINFO_LATI = 2
RINFO_LONG = 3
RINFO_ELEV = 4

# Simple shot info file layout.
SINFO_FIELDS = "STID IDID TIME [LATI LONG ELEV]"
SINFOFIELDS = 6
SINFO_REQU = 3
SINFO_STID = 0
SINFO_IDID = 1
SINFO_TIME = 2
SINFO_LATI = 3
SINFO_LONG = 4
SINFO_ELEV = 5

# COCI form information.
COCI_FIELDS = "TIME IDID CHAN BXID DEPT INFG"
COCIFIELDS = 6   # Not including the internal COCI_FLAG in dicts.
COCI_REQU = 5    # Not including the INFG (In/Out) flag item.
COCI_TIME = 0
COCI_IDID = 1
COCI_CHAN = 2
COCI_BXID = 3
COCI_DEPT = 4
COCI_INFG = 5
# Used internally by some functions. This value never gets printed so it's not
# included in the counts above.
COCI_FLAG = 6

#########################
# BEGIN: lunchLineSpecs()
#    What LUNCH saves to the Texan comments and/or to it's local data file.
# $L1; dver; time; depu; rtyp; idid; chan; stid; lati;
#                                     long; elev; styp; sser; dept
LUNCH_FIELDS = ("TAG DVER TIME DEPU RTYP IDID CHAN STID LATI LONG ELEV STYP "
                "SSER DEPT")
LUNCHDVERS = "B"
LUNCHL1FIELDS = 14
LUNCHL1REQ = 11
LUNCHL1_TAG = 0
LUNCHL1_DVER = 1
LUNCHL1_TIME = 2
LUNCHL1_DEPU = 3
LUNCHL1_RTYP = 4
LUNCHL1_IDID = 5
LUNCHL1_CHAN = 6
LUNCHL1_STID = 7
LUNCHL1_LATI = 8
LUNCHL1_LONG = 9
LUNCHL1_ELEV = 10
LUNCHL1_STYP = 11
LUNCHL1_SSER = 12
LUNCHL1_DEPT = 13

# Identifier markers for receiver/shot GPX and KML files.
RGPX_FIELDS = "XML style"
SGPX_FIELDS = "XML style"
AKML_FIELDS = "XML style"

# ----- Deployment files/entries -----
##################################################
# BEGIN: convertDepfileRS2Dicts(FLT, What, Style)
# FUNC:convertDepfileRS2Dicts():2013.108
#   Calls the other dep file conversion functions to read in the contents of
#   a file that has RECV and/or SHOT information in it.
#   Style must be "line" or "block".


def convertDepfileRS2Dicts(FLT, What, Style):
    Entries = []
    for Which in ("RECV", "SHOT"):
        Ret = convertDepfile2Dicts(FLT, What, Which, Style)
        if Ret[0] != 0:
            return Ret
        if len(Ret[1]) > 0:
            Entries += Ret[1]
# We need to redo these since we may have combined two different sets above.
    ListID = 0
    for Entry in Entries:
        # "dtyp" item was filled in above.
        Entry["liid"] = ListID
        ListID += 1
    return (0, Entries)


######################################################
# BEGIN: convertDepfile2Dicts(FLT, What, Which, Style)
# FUNC:convertDepfile2Dicts():2019.024
#   Reads a RAWMEET (the program that started this) deployment file (depfile)
#   or lines from a depfile file, depfile lines in a Text(), or in a List and
#   converts RECV or SHOT blocks, or lines of information into a List of
#   convert dictionaries.
#
#   What describes what FLT is: file, list, text or txt.
#   Which is RECV or SHOT.
#   Style tells the function to expect a "line" (depl) or "block" (depb)
#   style depfile.
#
#   Returns (0, [Dict, Dict...]) if everything went OK.
#   Returns (-1, Color, Message, Beeps, FLT) if something goes wrong.
#   Returns (x, Color, Message, Beeps, FLT) for other error messages where x
#       will be the line number where the problem was, which will be the line
#       number in a Text() field that the caller can use to highlight the bad
#       line.
CONVERTRECVDepLineDesc = "Dep (Line) = Deployment file, RECV line format"
CONVERTRECVDepLineTip = ("RECV; TexanID; StaID; Arry; RModel; Chan; Sensor; "
                         "RType;\n     Lat; Long; Elev; DepTeam; Down; Up; "
                         "Shots; [Comment]")
CONVERTRECVDepBlockDesc = ("Dep (Block) = Deployment file, RECV block format "
                           "file")
CONVERTRECVDepBlockTip = "RECV\n   IDID=1234\n   STID=4321\n   RTYP=R\n   etc."
CONVERTSHOTDepLineDesc = "Dep (Line) = Deployment file, SHOT line format"
CONVERTSHOTDepLineTip = ("SHOT; StaID; Arry; ShotID; Lat; Long; Elev; Time;\n "
                         "    PreTrig; Post; Radius; SR; Depth; Size; RadVel; "
                         "[Comment]")
CONVERTSHOTDepBlockDesc = "Dep (Line) = Deployment file, SHOT block format"
CONVERTSHOTDepBlockTip = "SHOT\n   IDID=12\n   STID=4321\n   POST=30\n   etc."
CONVERTRSDepLineDesc = ("DepRS (Line) = Deployment file, RECV and/or SHOT "
                        "line format")
CONVERTRSDepLineTip = "%s\n%s" % (CONVERTRECVDepLineTip, CONVERTSHOTDepLineTip)
CONVERTRSDepBlockDesc = ("DepRS (Block) = Deployment file, RECV and/or SHOT "
                         "block format")
CONVERTRSDepBlockTip = "RECV or SHOT\n   IDID=12\n   LATI=N34.5\n   etc."


def convertDepfile2Dicts(FLT, What, Which, Style):
    Ret4 = ""
    if What == "file":
        # Least chance of trouble files.
        Ret = readFileLineRB(FLT)
        if Ret[0] != 0:
            return (-1, Ret[1], Ret[2], Ret[3], Ret[4], Ret[5])
        Lines = Ret[1]
        Ret4 = FLT
    elif What == "list":
        Lines = FLT
    elif What == "text":
        Lines = []
        N = 1
        while True:
            if len(FLT.get("%d.0" % N)) == 0:
                break
            Lines.append(FLT.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    elif What == "txt":
        LTxt = PROGTxt[FLT]
        Lines = []
        N = 1
        while True:
            if len(LTxt.get("%d.0" % N)) == 0:
                break
            Lines.append(LTxt.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    Entries = []
    ListID = 0
    GotSome = False
    N = 0
# ----- Information block (new) type -----
    if Style == "depb":
        for Line in Lines:
            N += 1
# A line can never start with a number in this format.
            if len(Line) == 0 or Line.startswith("#") or intt(Line) == 0:
                continue
# Some strange stuff can come out of programs like Excel.
            Line = convertOneBlank(Line)
# RECV or SHOT is on a line by itself.
            if Line == Which:
                if GotSome is True:
                    Dict["dtyp"] = Which
# Finish off a previous entry, then get the next Dict ready.
                    Dict["liid"] = ListID
                    ListID += 1
                    convertDictPolish(Dict)
                    Entries.append(Dict)
                Dict = {}
# Always make sure the Dict values are at least the default values.
                convertDictDefaults(Dict)
                Dict["flag"] = "n"
                GotSome = False
                continue
# If it is not RECV or SHOT and has a = in it.
            if Line.find("=") != -1:
                Parts2 = Line.split("=", 1)
                Parts = []
                for Part in Parts2:
                    Parts.append(Part.strip())
# This should never fail unless the Line contains just '<something>='.
                try:
                    # Only the currently defined keys in ConvertDefaults are
                    # allowed. This helps to filter out junk, but not very
                    # well.
                    if Parts[0] in Dict:
                        Dict[Parts[0]] = Parts[1]
                        GotSome = True
                except Exception:
                    return (N, "RW", "Bad line (%d): %s" % (N, Line), 2, Ret4)
        if GotSome is True:
            Dict["dtyp"] = Which
            Dict["liid"] = ListID
            convertDictPolish(Dict)
            Entries.append(Dict)
# ----- Old flat line type -----
    elif Style == "depl":
        for Line in Lines:
            N += 1
            if len(Line) == 0 or Line.startswith("#"):
                continue
            if Line.startswith(Which + ";"):
                if Which == "RECV":
                    Parts2 = Line.split(";", DEPRECV_REQU)
                    Parts = []
                    for Part in Parts2:
                        Parts.append(Part.strip())
                    if len(Parts) < DEPRECV_REQU:
                        continue
                    Parts += (DEPRECVFIELDS - len(Parts)) * [""]
                    Dict = {}
                    convertDictDefaults(Dict)
                    Ret = convertCertifyIntegerP("idid", Parts[DEPRECV_IDID],
                                                 "Receiver ID")
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret4)
                    Dict["idid"] = Ret[1]
                    Ret = convertCertifyTextULN("stid", Parts[DEPRECV_STID])
                    Dict["stid"] = Ret[1]
                    Ret = convertCertifyArray("arry", Parts[DEPRECV_ARRY])
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret4)
                    Dict["arry"] = Ret[1]
                    Ret = convertCertifyTextULN("rmod", Parts[DEPRECV_RMOD])
                    Dict["rmod"] = Ret[1]
                    Ret = convertCertifyCHAN(Parts[DEPRECV_CHAN])
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret4)
                    Dict["chan"] = Ret[1]
                    Ret = convertCertifyTextULN("sens", Parts[DEPRECV_SENS])
                    Dict["sens"] = Ret[1]
                    Ret = convertCertifyRTYP(Parts[DEPRECV_RTYP])
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret4)
                    Dict["rtyp"] = Ret[1]
                    Ret = convertCertifyLATILONG("lati", Parts[DEPRECV_LATI])
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret4)
                    Dict["lati"] = Ret[1]
                    Ret = convertCertifyLATILONG("long", Parts[DEPRECV_LONG])
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret4)
                    Dict["long"] = Ret[1]
                    Ret = convertCertifyFloat2("elev", Parts[DEPRECV_ELEV],
                                               "Elevation")
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret4)
                    Dict["elev"] = Ret[1]
                    Ret = convertCertifyTextULN("dept", Parts[DEPRECV_DEPT])
                    Dict["dept"] = Ret[1]
                    Ret = convertCertifyTime("dptm", "s",
                                             Parts[DEPRECV_DPTM], "Deploy")
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret4)
                    Dict["dptm"] = Ret[1]
                    Ret = convertCertifyTime("putm", "s",
                                             Parts[DEPRECV_PUTM], "Pickup")
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret)
                    Dict["putm"] = Ret[1]
                    Dict["shts"] = Parts[DEPRECV_SHTS]
                    Dict["comm"] = Parts[DEPRECV_COMM]
                    if len(Dict["comm"]) == 0:
                        Dict["comm"] = ConvertDefaults["comm"]
                    Dict["dtyp"] = "RECV"
                    Dict["liid"] = ListID
                    ListID += 1
                    convertDictPolish(Dict)
                    Entries.append(Dict)
                elif Which == "SHOT":
                    Parts2 = Line.split(";", DEPSHOT_REQU)
                    Parts = []
                    for Part in Parts2:
                        Parts.append(Part.strip())
                    if len(Parts) < DEPSHOT_REQU:
                        continue
                    Parts += (DEPSHOTFIELDS - len(Parts)) * [""]
                    Dict = {}
                    convertDictDefaults(Dict)
                    Ret = convertCertifyTextULN("stid", Parts[DEPSHOT_STID])
                    Dict["stid"] = Ret[1]
                    Ret = convertCertifyArray("arry", Parts[DEPSHOT_ARRY])
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret4)
                    Dict["arry"] = Ret[1]
                    Ret = convertCertifyIntegerP("idid", Parts[DEPSHOT_IDID],
                                                 "Shot ID")
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret4)
                    Dict["idid"] = Ret[1]
                    Ret = convertCertifyLATILONG("lati", Parts[DEPSHOT_LATI])
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret4)
                    Dict["lati"] = Ret[1]
                    Ret = convertCertifyLATILONG("long", Parts[DEPSHOT_LONG])
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret4)
                    Dict["long"] = Ret[1]
                    Ret = convertCertifyFloat2("elev", Parts[DEPSHOT_ELEV],
                                               "Elevation")
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret4)
                    Dict["elev"] = Ret[1]
                    Ret = convertCertifyTime("shtm", "s",
                                             Parts[DEPSHOT_TIME], "Shot")
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret4)
                    Dict["shtm"] = Ret[1]
                    Ret = convertCertifyFloat3PZ("pret", Parts[DEPSHOT_PRET],
                                                 "Pre-Trigger Length")
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret4)
                    Dict["pret"] = Ret[1]
                    Ret = convertCertifyFloat3PZ("post", Parts[DEPSHOT_POST],
                                                 "Post-Trigger Length")
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret4)
                    Dict["post"] = Ret[1]
                    Ret = convertCertifyFloat3PZ("post", Parts[DEPSHOT_RADI],
                                                 "Radius Of Receivers")
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret4)
                    Dict["radi"] = Ret[1]
                    Ret = convertCertifyIntegerPZ("samp",
                                                  Parts[DEPSHOT_SAMP],
                                                  "Sample Rate")
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret4)
                    Dict["samp"] = Ret[1]
                    Ret = convertCertifyFloat2("dpth", Parts[DEPSHOT_DPTH],
                                               "Depth Of Shot")
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret4)
                    Dict["dpth"] = Ret[1]
                    Ret = convertCertifyFloatPZ("size", Parts[DEPSHOT_SIZE],
                                                "Shot Size")
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret4)
                    Dict["size"] = Ret[1]
                    Ret = convertCertifyFloatPZ("rvel", Parts[DEPSHOT_RVEL],
                                                "Radial Velocity")
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s" %
                                (N, Ret[2], Line), 2, Ret4)
                    Dict["rvel"] = Ret[1]
# There may not be any comment.
                    Dict["comm"] = Parts[DEPSHOT_COMM]
                    if len(Dict["comm"]) == 0:
                        Dict["comm"] = ConvertDefaults["comm"]
                    Dict["dtyp"] = "SHOT"
                    Dict["liid"] = ListID
                    ListID += 1
                    convertDictPolish(Dict)
                    Entries.append(Dict)
    return (0, Entries)
########################################################################
# BEGIN: convertDict2DepfileRECV(Dict, Format, Strict, Minimize = False)
# FUNC:convertDict2DepfileRECV():2012.131
#    Returns a deployment file RECV information block or line from the info in
#    the passed Dict.
#    If Strict is true then the dtyp must be "RECV".
#    If Minimize is True then only items that do not match the default values
#    will be included in the block format.


def convertDict2DepfileRECV(Dict, Format, Strict, Minimize=False):
    if Strict is True and Dict["dtyp"] != "RECV":
        return (1, "")
    if Format == "depl":
        return (0, "RECV;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s" %
                (Dict["idid"], Dict["stid"], Dict["arry"], Dict["rmod"],
                 Dict["chan"], Dict["sens"], Dict["rtyp"], Dict["lati"],
                 Dict["long"], Dict["elev"], Dict["dept"], Dict["dptm"],
                 Dict["putm"], Dict["shts"], Dict["comm"]))
    elif Format == "depb":
        Return = "RECV\n"
        for Item in ("idid", "arry", "stid", "rmod", "chan", "sens",
                     "rtyp", "lati", "long", "elev", "dept", "dptm", "putm",
                     "shts", "comm"):
            if Minimize is True and Dict[Item] == ConvertDefaults[Item]:
                continue
            Return += "   %s=%s\n" % (Item, Dict[Item])
        return (0, Return)
########################################################################
# BEGIN: convertDict2DepfileSHOT(Dict, Format, Strict, Minimize = False)
# FUNC:convertDict2DepfileSHOT():2012.121
#    Returns a deployment file SHOT information block or line from the info
#    in the passed Dict.
#    If Strict is True then the dtyp must be "SHOT".
#    If Minimize is True then only items that do not match the default values
#    will be included in the block format.


def convertDict2DepfileSHOT(Dict, Format, Strict, Minimize=False):
    if Strict is True and Dict["dtyp"] != "SHOT":
        return (1, "")
    if Format == "depl":
        return (0, "SHOT;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s" %
                (Dict["stid"], Dict["arry"], Dict["idid"], Dict["lati"],
                 Dict["long"], Dict["elev"], Dict["shtm"], Dict["pret"],
                 Dict["post"], Dict["radi"], Dict["samp"], Dict["dpth"],
                 Dict["size"], Dict["rvel"], Dict["comm"]))
    elif Format == "depb":
        Return = "SHOT\n"
        for Item in ("stid", "arry", "idid", "lati", "long", "elev",
                     "shtm", "pret", "post", "radi", "samp", "dpth", "size",
                     "rvel", "comm"):
            if Minimize is True and Dict[Item] == ConvertDefaults[Item]:
                continue
            Return += "   %s=%s\n" % (Item, Dict[Item])
        return (0, Return)
# ----- GPX files/entries -----
###################################################
# BEGIN: convertGPX2Dicts(FLT, What, WptName, DTyp)
# FUNC:convertGPX2Dicts():2019.024
#   Reads a GPX file or GPX file lines in a List or a Text() with GPX stuff
#   in it and returns CONVERT-style dictionaries in a list. This is mostly for
#   importing and being able to plot position information collected by
#   handheld GPSR's or from survey equipment.


def convertGPX2Dicts(FLT, What, WptName, DTyp):
    Ret4 = ""
    if What == "file":
        Ret = readFileLinesRB(FLT)
        if Ret[0] != 0:
            return (-1, Ret[1], Ret[2], Ret[3], Ret[4], Ret[5])
        Lines = Ret[1]
        Ret4 = FLT
    elif What == "list":
        Lines = FLT
    elif What == "text":
        Lines = []
        N = 1
        while True:
            if len(FLT.get("%d.0" % N)) == 0:
                break
            Lines.append(FLT.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    elif What == "txt":
        LTxt = PROGTxt[FLT]
        Lines = []
        N = 1
        while True:
            if len(LTxt.get("%d.0" % N)) == 0:
                break
            Lines.append(LTxt.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    Entries = []
    InWpt = False
    EntryCount = 0
    ListID = 0
    N = 0
    for Line in Lines:
        N += 1
# This has to be here instead of telling readFileLines/RB() to do it, because
# readFileLines/RB() may not have been used to get to this point.
        Line = Line.strip()
# Comments are not part of the standard, but you know those users.
        if len(Line) == 0 or Line.startswith("#"):
            continue
        try:
            if Line.startswith("<wpt"):
                Dict = {}
                convertDictDefaults(Dict)
                InWpt = True
            if InWpt is True:
                if Line.find("lat=") != -1:
                    Index = Line.index("lat=") + 4
                    Value = convertGetDQuoted(Line[Index:])
                    Ret = convertCertifyLATILONG("lati", Value)
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s." %
                                (N, Ret[2], Line), Ret[3], Ret4)
                    Value = pm2nsew("lat", Ret[1])
                    Dict["lati"] = Value
# This should always be true since they are on the same line.
                    if Line.find("lon=") != -1:
                        Index = Line.index("lon=") + 4
                        Value = convertGetDQuoted(Line[Index:])
                        Ret = convertCertifyLATILONG("long", Value)
                        if Ret[0] != 0:
                            return (N, Ret[1], "Bad line (%d): %s\n%s." %
                                    (N, Ret[2], Line), Ret[3], Ret4)
                        Value = pm2nsew("lon", Ret[1])
                        Dict["long"] = Value
                elif Line.startswith("<ele>") and Line.endswith("</ele>"):
                    Value = Line[Line.index("<ele>") + 5:
                                 Line.index("</ele>")].strip()
                    Ret = convertCertifyFloat2("elev", Value, "Elevation")
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s." %
                                (N, Ret[2], Line), Ret[3], Ret4)
                    Dict["elev"] = Ret[1]
# This really doesn't have any use for receivers.
                elif Line.startswith("<time>") and Line.endswith("</time>"):
                    Value = Line[Line.index("<time>") + 6:
                                 Line.index("</time>")].strip()
                    Value = dt2TimeDT(3, Value)
                    Ret = convertCertifyTime("time", "s", Value, "Time")
                    Dict["time"] = Value
                elif Line.startswith("<geoidheight>") and \
                        Line.endswith("</geoidheight>"):
                    Value = Line[Line.index("<geoidheight>") + 13:
                                 Line.index("</geoidheight>")].strip()
                    Ret = convertCertifyFloat(Value)
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s." %
                                (N, Ret[2], Line), Ret[3], Ret4)
                    Dict["hgeo"] = Ret[1]
                elif Line.startswith("<name>") and Line.endswith("</name>"):
                    Value = Line[Line.index("<name>") + 6:
                                 Line.index("</name>")].strip()
                    if Value.find("-") == -1:
                        if WptName == "idid":
                            Ret = convertCertifyIntegerP("idid", Value,
                                                         "Receiver ID")
                            if Ret[0] != 0:
                                return (N, Ret[1], "Bad line (%d): %s\n%s." %
                                        (N, Ret[2], Line), Ret[3], Ret4)
                            Dict["idid"] = Ret[1]
                        elif WptName == "stid":
                            Ret = convertCertifyTextULN("stid", Value)
                            Dict["stid"] = Ret[1]
# If this is the case the item will have to be an integer, since idid must be
# an integer.
                        elif WptName == "stid-idid":
                            Ret = convertCertifyIntegerP("stid", Value,
                                                         "Station ID/Receiver "
                                                         "ID")
                            if Ret[0] != 0:
                                return (N, Ret[1], "Bad line (%d): %s\n%s." %
                                        (N, Ret[2], Line), Ret[3], Ret4)
                            Dict["stid"] = Ret[1]
                            Dict["idid"] = Ret[1]
                    else:
                        # If there is a hyphen then it has to be station
                        # ID-idid.
                        Parts = Value.split("-")
                        Ret = convertCertifyTextULN("stid", Parts[0])
                        Dict["stid"] = Ret[1]
# We know this HAS to be an integer, so this may be a jolt to the user.
                        Ret = convertCertifyIntegerP("idid", Parts[1],
                                                     "Receiver ID")
                        if Ret[0] != 0:
                            return (N, Ret[1], "Bad line (%d): %s\n%s." %
                                    (N, Ret[2], Line), Ret[3], Ret4)
                        Dict["idid"] = Ret[1]
                elif Line.startswith("<cmt>") and Line.endswith("</cmt>"):
                    Value = Line[Line.index("<cmt>") + 5:
                                 Line.index("</cmt>")].strip()
                    Dict["comm"] = Value
                elif Line.startswith("<sat>") and Line.endswith("</sat>"):
                    Value = Line[Line.index("<sat>") + 5:
                                 Line.index("</sat>")].strip()
                    Ret = convertCertifyInteger("sats", Value, "Satillites")
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s." %
                                (N, Ret[2], Line), Ret[3], Ret4)
                    Dict["sats"] = Ret[1]
                elif Line.startswith("<hdop>") and Line.endswith("</hdop>"):
                    Value = Line[Line.index("<hdop>") + 6:
                                 Line.index("</hdop>")].strip()
                    Ret = convertCertifyInteger("hdop", Value,
                                                "Horizontal Dilution Of "
                                                "Precision")
                    if Ret[0] != 0:
                        return (N, Ret[1], "Bad line (%d): %s\n%s." %
                                (N, Ret[2], Line), Ret[3], Ret4)
                    Dict["hdop"] = Ret[1]
                elif Line.startswith("<desc>") and Line.endswith("</desc>"):
                    KVLine = Line[Line.index("<desc>") + 6:
                                  Line.index("</desc>")].strip()
                    # This is a special case.
                    if KVLine.startswith("PACK: "):
                        KVLine = KVLine[6:]
                        KVPairs = KVLine.split()
                        for KVPair in KVPairs:
                            # Who knows what may be in the desc area.
                            try:
                                Key, Value = KVPair.split("=", 1)
                                if Key in Dict:
                                    Dict[Key] = Value
                            except Exception:
                                return (N, "RW", "Bad line (%d): %s\nBad Key-"
                                        "Value pair." % (N, Line), 2, Ret4)
                elif Line.startswith("</wpt>"):
                    EntryCount += 1
# We have to put SOMETHING in here or the routine that checks for things like
# duplicate Texan IDs will go crazy.
                    if Dict["idid"] == ConvertDefaults["idid"]:
                        Dict["idid"] = str(EntryCount)
# Set the dtyp to what the caller says or GPX.
                    if len(DTyp) != 0:
                        Dict["dtyp"] = DTyp
                    else:
                        Dict["dtyp"] = "GPX"
                    Dict["liid"] = ListID
                    ListID += 1
# Double check since the creator of the GPX file will not know the default
# requirements for everything if it was not made by convertDict2GPXEntry().
                    if What == "file":
                        convertSetDictDefaults(Dict)
# Fills in things like latf from the lati value.
                    convertDictPolish(Dict)
                    Entries.append(Dict)
                    InWpt = False
        except Exception as e:
            return (N, "RW", "Bad line (%d): %s\n%s" % (N, Line, e), 2, Ret4)
    return (0, Entries)
####################################################################
# BEGIN: convertDict2GPXEntry(Dict, Type, GPXName, From, To, Strict)
# FUNC:convertDict2GPXEntry():2018.238
#   For a header pass "head:<any comment>" in GPXName.
#   For a trailer pass "tail" in GPXName.


def convertDict2GPXEntry(Dict, Type, GPXName, From, To, Strict):
    Return = ""
    if GPXName.startswith("head:"):
        Parts2 = GPXName.split(":", 1)
        Parts = []
        for Part in Parts2:
            Parts.append(Part.strip())
        Return += "<?xml version=\"1.0\" ?>\n"
        Return += "<gpx xmlns=\"http://www.topografix.com/GPX/1/1\"\n"
        Return += "creator=\"%s %s\" version=\"1.1\">\n" % (PROG_NAME,
                                                            PROG_VERSION)
        Return += "<!-- GPX Information (%s) -->\n" % getGMT(0)
# The caller may not have anything to say.
        try:
            if len(Parts[1]) != 0:
                Return += "<!-- %s -->\n" % Parts[1]
        except Exception:
            pass
        if len(From) != 0 and len(To) != 0:
            Return += "<!-- From %s to %s -->" % (From, To)
        elif len(From) != 0:
            Return += "<!-- From %s -->" % From
        elif len(To) != 0:
            Return += "<!-- To %s -->" % To
# Do the strip so the string always ends without a \n so the callers don't go
# nuts since the rest of the functions do not add a \n.
        return (0, Return.strip())
    elif GPXName == "tail":
        Return += "</gpx>"
        return (0, Return)
    if Strict is True and Dict["dtyp"] != Type:
        return (1, "")
    Lat = nsew2pm("lat", Dict["lati"])
    Lon = nsew2pm("lon", Dict["long"])
    Return += "\n"
    Return += "<wpt lat=\"%s\" lon=\"%s\">\n" % (Lat, Lon)
    Return += "<ele>%s</ele>\n" % Dict["elev"]
# The <desc> element is perfect for keeping track of "stuff".
    Desc = "PACK: "
    if GPXName == "stid":
        Return += "<name>%s</name>\n" % Dict["stid"]
# Do this, otherwise they would be Lost In Translation.
        Desc += "idid=%s " % Dict["idid"]
    elif GPXName == "idid":
        Return += "<name>%s</name>\n" % Dict["idid"]
        Desc += "stid=%s " % Dict["stid"]
    elif GPXName == "stid-idid":
        Return += "<name>%s-%s</name>\n" % (Dict["stid"], Dict["idid"])
    else:
        Return += "<name>%s</name>\n" % str(GPXName)
    if len(Dict["comm"]) != 0:
        Return += "<cmt>%s</cmt>\n" % Dict["comm"]
    if Type == "RECV":
        # Retain all of these, but only other convertFunctions will know what
        # to do with them.
        Desc += "rtyp=%s chan=%s dept=%s dptm=%s putm=%s" % (Dict["rtyp"],
                                                             Dict["chan"],
                                                             Dict["dept"],
                                                             Dict["dptm"],
                                                             Dict["putm"])
        Return += "<desc>%s</desc>\n" % Desc.strip()
# Apparently this must follow the description.
        if Dict["rtyp"] == "R":
            Return += "<sym>Circle, Green</sym>\n"
        elif Dict["rtyp"] == "U":
            Return += "<sym>Circle, Blue</sym>\n"
        else:
            Return += "<sym>Circle, Orange</sym>\n"
    elif Type == "SHOT":
        Desc += "shtm=%s arry=%s" % (Dict["shtm"], Dict["arry"])
        Return += "<desc>%s</desc>\n" % Desc.strip()
        Return += "<sym>Circle, Red</sym>\n"
# No \n.
    Return += "</wpt>"
    return (0, Return)
# ----- KML entries -----
######################################################
# BEGIN: convertDict2KMLEntry(Dict, KMLName, From, To)
# FUNC:convertDict2KMLEntry():2018.238
#   For a header pass "head:<comment>" in KMLName.
#   For a trailer pass "tail" in KMLName.


def convertDict2KMLEntry(Dict, KMLName, From, To):
    Return = ""
    if KMLName.startswith("head:"):
        Parts2 = KMLName.split(":", 1)
        Parts = []
        for Part in Parts2:
            Parts.append(Part.strip())
        Return += "<?xml version=\"1.0\" ?>\n"
        Return += "<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n"
        Return += "<!-- KML Information (%s) -->\n" % getGMT(0)
# The caller may not have anything to say.
        try:
            if len(Parts[1]) != 0:
                Return += "<!-- %s -->\n" % Parts[1]
        except Exception:
            pass
        if len(From) != 0 and len(To) != 0:
            Return += "<!-- From %s to %s -->\n" % (From, To)
        elif len(From) != 0:
            Return += "<!-- From %s -->\n" % From
        elif len(To) != 0:
            Return += "<!-- To %s -->\n" % To
        Return += "<Folder>"
        return (0, Return)
    elif KMLName == "tail":
        Return += "</Folder>\n"
        Return += "</kml>"
        return (0, Return)
    Lat = nsew2pm("lat", Dict["lati"])
    Lon = nsew2pm("lon", Dict["long"])
    Return += "\n"
    Return += "<Placemark>\n"
    if KMLName == "stid":
        Return += "<name>%s</name>\n" % str(Dict["stid"])
    elif KMLName == "idid":
        Return += "<name>%s</name>\n" % Dict["idid"]
    elif KMLName == "stid-idid":
        Return += "<name>%s-%s</name>\n" % (Dict["stid"], str(Dict["idid"]))
    Return += "<Point>\n"
    Return += "<coordinates>%s, %s, %s</coordinates>\n" % (Lon, Lat,
                                                           Dict["elev"])
    Return += "</Point>\n"
    Return += "</Placemark>"
    return (0, Return)


# ----- TSP dasfiles/shotfile/geometry file entries -----
######################################
# BEGIN: convertTSPDf2Dicts(FLT, What)
# FUNC:convertTSPDf2Dicts():2019.024
#   Reads a TSP dasfile or lines from a TSP dasfile file or TSP dasfile lines
#   in a Text() and converts each line of information into a CONVERT
#   dictionary.
#   The expected format is
#       stid idid
#
#   Can have this special comment line "# LINE:"
#       stid idid
#       # LINE: Line A
#       stid idid...
#   The items may be whitespace, comma, semi-colon or tab delimited
#   (semi-colon and tab is not part of the TSP standard).
CONVERTTSPDfTip = "StaID TexanID"
CONVERTTSPDfDesc = "TSP dasfile = TSP program 'dasfile' format file"


def convertTSPDf2Dicts(FLT, What):
    Ret4 = ""
    if What == "file":
        Ret = readFileLinesRB(FLT)
        if Ret[0] != 0:
            return (-1, Ret[1], Ret[2], Ret[3], Ret[4], Ret[5])
        Lines = Ret[1]
        Ret4 = FLT
    elif What == "list":
        Lines = FLT
    elif What == "text":
        Lines = []
        N = 1
        while True:
            if len(FLT.get("%d.0" % N)) == 0:
                break
            Lines.append(FLT.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    elif What == "txt":
        LTxt = PROGTxt[FLT]
        Lines = []
        N = 1
        while True:
            if len(LTxt.get("%d.0" % N)) == 0:
                break
            Lines.append(LTxt.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    Entries = []
    ListID = 0
    N = 0
    for Line in Lines:
        N += 1
        Line = Line.strip()
# If the line does not start with a number it might be something like a GPX
# file line.
        if len(Line) == 0:
            continue
# This is a non-standard TSP thing.
        if Line.startswith("#"):
            if Line.find("# LINE:") != -1:
                Parts = Line.split(":", 1)
                Dict = {}
                convertDictDefaults(Dict)
# This will mean something to someone. There is no station 0.
                Dict["stid"] = "0"
                if len(Parts) == 1:
                    Dict["idid"] = ""
                else:
                    Dict["idid"] = Parts[1].strip()
                Dict["dtyp"] = "RECV"
                Dict["liid"] = ListID
                ListID += 1
                Entries.append(Dict)
            continue
        if intt(Line) == 0:
            continue
        if Line.find(",") != -1:
            Parts2 = Line.split(",")
            Parts = []
            for Part in Parts2:
                Parts.append(part.strip())
        elif Line.find(";") != -1:
            Parts2 = Line.split(";")
            Parts = []
            for Part in Parts2:
                Parts.append(part.strip())
        else:
            Parts = Line.split()
# There are just a couple tests we can do.
        if len(Parts) != TSPDASFIELDS:
            continue
# If we are here then this must be a TSP dasfile line?
        Dict = {}
        convertDictDefaults(Dict)
        Ret = convertCertifyTextULN("stid", Parts[TSPDAS_STID])
        Dict["stid"] = Ret[1]
        Ret = convertCertifyIntegerP("idid", Parts[TSPDAS_IDID], "Receiver ID")
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line),
                    Ret[3], Ret4)
# If the receiver ID is greater than 10000 then subtract 10000 (TSP used 10000
# added to the ID number up until v3, then it stopped).
        Value = int(Ret[1])
        if Value > 10000:
            Value -= 10000
        Dict["idid"] = str(Value)
        Dict["dtyp"] = "RECV"
        Dict["liid"] = ListID
        ListID += 1
# We don't run convertSetDictDefaults() or convertDictPolish() here since
# we've already checked to make sure that the required fields were there and
# TSP dasfiles don't contain anything that needs to be calculated.
        Entries.append(Dict)
    return (0, Entries)
############################################################
# BEGIN: convertDict2TSPDfEntry(Dict, Strict, AddTk = False)
# FUNC:convertDict2TSPDfEntry():2012.121
#    Returns a TSP dasfile line from the info in the passed Dict.


def convertDict2TSPDfEntry(Dict, Strict, AddTk=False):
    # Older versions of TSP needed 10000 added to the receiver IDs, but V3
    # does not, though that could come back to bite Harder someday (instrument
    # serial numbers will have to start over with lower numbers at some point).
    if Strict is True and Dict["dtyp"] != "RECV":
        return (1, "")
    Value = intt(Dict["idid"])
    if Value <= 0:
        return (1, "RW", "Bad receiver ID: %s" % Dict["idid"], 2, "")
    if AddTk is True and Value < 10000:
        Value += 10000
    return (0, "%s %d" % (Dict["stid"], Value))


######################################
# BEGIN: convertTSPGf2Dicts(FLT, What)
# FUNC:convertTSPGf2Dicts():2019.024
#   Reads a TSP geometry file or lines from a TSP geometry file or TSP
#   geometry lines from a Text() and converts each line of information into a
#   CONVERT dictionary. The expected format is
#       StationID  lati  long  elev
#   The items may be whitespace, comma, semi-colon or tab delimited
#   (semi-colon and tab is not part of the TSP standard).
CONVERTTSPGfTip = "StaID Lat Long Elev"
CONVERTTSPGfDesc = "TSP Geom = TSP program geometry format file"


def convertTSPGf2Dicts(FLT, What):
    Ret4 = ""
    if What == "file":
        Ret = readFileLinesRB(FLT)
        if Ret[0] != 0:
            return (-1, Ret[1], Ret[2], Ret[3], Ret[4], Ret[5])
        Lines = Ret[1]
        Ret4 = FLT
    elif What == "list":
        Lines = FLT
    elif What == "text":
        Lines = []
        N = 1
        while True:
            if len(FLT.get("%d.0" % N)) == 0:
                break
            Lines.append(FLT.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    elif What == "txt":
        LTxt = PROGTxt[FLT]
        Lines = []
        N = 1
        while True:
            if len(LTxt.get("%d.0" % N)) == 0:
                break
            Lines.append(LTxt.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    Entries = []
    ListID = 0
    N = 0
    for Line in Lines:
        N += 1
        Line = Line.strip()
# If the line does not start with a number it might be something like a GPX
# file line.
        if len(Line) == 0 or Line.startswith("#") or intt(Line) == 0:
            continue
        if Line.find(",") != -1:
            Parts2 = Line.split(",")
            Parts = []
            for Part in Parts2:
                Parts.append(Part.strip())
        elif Line.find(";") != -1:
            Parts2 = Line.split(";")
            Parts = []
            for Part in Parts2:
                Parts.append(Part.strip())
        else:
            Parts = Line.split()
# There are just a few tests we can do, like there should be 4 fields and
# everything should be a number or the 2nd and 3rd ones might start with an
# N/S or E/W.
        if len(Parts) != TSPGEOMFIELDS:
            continue
        Dict = {}
        convertDictDefaults(Dict)
        Ret = convertCertifyTextULN("stid", Parts[TSPGEOM_STID])
        Dict["stid"] = Ret[1]
        Ret = convertCertifyLATILONG("lati", Parts[TSPGEOM_LATI])
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line),
                    Ret[3], Ret4)
        Dict["lati"] = Ret[1]
        Ret = convertCertifyLATILONG("long", Parts[TSPGEOM_LONG])
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line),
                    Ret[3], Ret4)
        Dict["long"] = Ret[1]
        Ret = convertCertifyFloat2("elev", Parts[TSPGEOM_ELEV], "Elevation")
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line),
                    Ret[3], Ret4)
        Dict["elev"] = Ret[1]
        Dict["dtyp"] = "GEOM"
        Dict["liid"] = ListID
        ListID += 1
# We don't run convertSetDictDefaults() here since we've already checked to
# make sure that the required fields were there.
        convertDictPolish(Dict)
        Entries.append(Dict)
    return (0, Entries)
############################################
# BEGIN: convertDict2TSPGfEntry(Dict, Delim)
# FUNC:convertDict2TSPGfEntry():2012.125
#    Returns a TSP geometry line from the info in the passed Dict.


def convertDict2TSPGfEntry(Dict, Delim):
    if Delim == "comma":
        Delim = ","
    elif Delim == "space":
        Delim = " "
    elif Delim == "semi":
        Delim = ";"
    elif Delim == "tab":
        Delim = "\t"
    return (0, "%s%s%s%s%s%s%s" %
            (Dict["stid"], Delim, nsew2pm("lat", Dict["lati"]), Delim,
             nsew2pm("lon", Dict["long"]), Delim, Dict["elev"]))


######################################
# BEGIN: convertTSPSf2Dicts(FLT, What)
# FUNC:convertTSPSf2Dicts():2019.024
#   Reads a TSP geometry file or lines from a TSP geometry file or TSP
#   geometry lines from a Text() and converts each line of information into a
#   CONVERT dictionary. The expected format is
#       2009, 103, 10, 45, 0.0, 1, 60.0, 240
#   where the first five parts are the date/time to begin cutting, the ShotID,
#   the length of time to cut for, then the sign change azimuth (used by TSP
#   when applying geometry).
#   The stid Dict value is set to the idid (FFID) value, because these files
#   do not have any stid information.
#   The commas may instead be blanks (TSP spec), semi-colons (not spec) or
#   tabs (not spec).
#   The azimuth value may be omitted. The function will skip over lines
#   starting with # (not spec).
#   Returns (0, [{<info>}, {<info>}, ...]).
CONVERTTSPSfTip = "Year DOY Hour Min Sec ShotID Post Azimuth"
CONVERTTSPSfDesc = "TSP shotfile = TSP program shot format file"


def convertTSPSf2Dicts(FLT, What):
    Ret4 = ""
    if What == "file":
        Ret = readFileLinesRB(FLT)
        if Ret[0] != 0:
            return (-1, Ret[1], Ret[2], Ret[3], Ret[4], Ret[5])
        Lines = Ret[1]
        Ret4 = FLT
    elif What == "list":
        Lines = FLT
    elif What == "text":
        Lines = []
        N = 1
        while True:
            if len(FLT.get("%d.0" % N)) == 0:
                break
            Lines.append(FLT.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    elif What == "txt":
        LTxt = PROGTxt[FLT]
        Lines = []
        N = 1
        while True:
            if len(LTxt.get("%d.0" % N)) == 0:
                break
            Lines.append(LTxt.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    Entries = []
    ListID = 0
    N = 0
    for Line in Lines:
        N += 1
        Line = Line.strip()
# If the line does not start with a number it might be something like a GPX
# file line.
        if len(Line) == 0 or Line.startswith("#") or intt(Line) == 0:
            continue
        if Line.find(",") != -1:
            Parts2 = Line.split(",")
            Parts = []
            for Part in Parts2:
                Parts.append(Part.strip())
        elif Line.find(";") != -1:
            Parts2 = Line.split(";")
            Parts = []
            for Part in Parts2:
                Parts.append(Part.strip())
        else:
            Parts = Line.split()
        if len(Parts) < TSPSHOT_REQU:
            continue
        Dict = {}
        convertDictDefaults(Dict)
# Take the parts and build an shtm value then check that. convertDictPolish()
# will split it up into tspy, tspd, etc.
        try:
            Value = "%04d:%03d:%02d:%02d:%06.3f" % (int(Parts[TSPSHOT_TSPY]),
                                                    int(Parts[TSPSHOT_TSPD]),
                                                    int(Parts[TSPSHOT_TSPH]),
                                                    int(Parts[TSPSHOT_TSPM]),
                                                    float(Parts[TSPSHOT_TSPS]))
        except ValueError:
            return (N, "RW", "Bad line (%d): Bad time value.\n%s" % (N, Line),
                    2, Ret4)
        Ret = convertCertifyTime("shtm", "ms", Value, "Shot")
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s.\n%s" % (N, Ret[2], Line),
                    Ret[3], Ret4)
        Dict["shtm"] = Ret[1]
        Ret = convertCertifyIntegerP("idid", Parts[TSPSHOT_IDID], "Shot ID")
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s.\n%s" % (N, Ret[2], Line),
                    Ret[3], Ret4)
        Dict["idid"] = Ret[1]
        # For lack of anything better.
        Dict["stid"] = Ret[1]
        Ret = convertCertifyFloat3PZ("post", Parts[TSPSHOT_POST],
                                     "Post Trigger")
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s.\n%s" % (N, Ret[2], Line),
                    Ret[3], Ret4)
        Dict["post"] = Ret[1]
        # FINISHME - is this -180 to +180 or what? So we can validate it.
        # It's optional.
        try:
            Value = Parts[TSPSHOT_TSAZ]
            Ret = convertCertifyInteger("tsaz", Parts[TSPSHOT_TSAZ], "Azimuth")
            if Ret[0] != 0:
                return (N, Ret[1], "Bad line (%d): %s.\n%s" %
                        (N, Ret[2], Line), Ret[3], Ret4)
            Dict["tsaz"] = Ret[1]
        except IndexError:
            pass
        Dict["dtyp"] = "SHOT"
        Dict["liid"] = ListID
        ListID += 1
# We don't run convertSetDictDefaults() here since we've already checked to
# make sure that the required fields were there.
        convertDictPolish(Dict)
        Entries.append(Dict)
    return (0, Entries)
####################################################
# BEGIN: convertDict2TSPSfEntry(Dict, Delim, Strict)
# FUNC:convertDict2TSPSfEntry():2012.125
#    Returns a TSP shotfile line from the info in the passed Dict.


def convertDict2TSPSfEntry(Dict, Delim, Strict):
    if Strict is True and Dict["dtyp"] != "SHOT":
        return (1, "")
    if Delim == "comma":
        Delim = ","
    elif Delim == "space":
        Delim = " "
    elif Delim == "semi":
        Delim = ";"
    elif Delim == "tab":
        Delim = "\t"
    return (0, "%d%s%03d%s%02d%s%02d%s%06.3f%s%s%s%s%s%s" %
            (Dict["tspy"], Delim, Dict["tspd"], Delim, Dict["tsph"], Delim,
             Dict["tspm"], Delim, Dict["tsps"], Delim, Dict["idid"], Delim,
             Dict["post"], Delim, Dict["tsaz"]))


##########################################
# BEGIN: convertTSPDfSfGf2Dicts(FLT, What)
# FUNC:convertTSPDfSfGf2Dicts():2013.108
#   Calls the other TSP conversion functions to read in the contents of a file
#   that has das and/or shot and/or geometry file information in it.
CONVERTTSPDfSfGfDesc = ("TSP D/S/G = TSP dasfile and/or shotfile and/or "
                        "geometry file entries in the same file")
CONVERTTSPDfSfGfTip = "%s\nand/or\n%s\nand/or\n%s" % (CONVERTTSPDfTip,
                                                      CONVERTTSPSfTip,
                                                      CONVERTTSPGfTip)
# Some functions (like for mapping) need the geometry info.
CONVERTTSPDfSfwithGfDesc = ("TSP D/S-G = TSP dasfile and/or shotfile with "
                            "geometry file entries in the same file")
CONVERTTSPDfSfwithGfTip = "%s\nand/or\n%s\nwith\n%s" % (CONVERTTSPDfTip,
                                                        CONVERTTSPSfTip,
                                                        CONVERTTSPGfTip)


def convertTSPDfSfGf2Dicts(FLT, What):
    # Look for das and shot entries first, then get any geometry info, match up
    # the station IDs and fill in the das/shot entry positions.
    Entries = []
    Ret = convertTSPDf2Dicts(FLT, What)
    if Ret[0] != 0:
        return Ret
    if len(Ret[1]) > 0:
        Entries += Ret[1]
    Ret = convertTSPSf2Dicts(FLT, What)
    if Ret[0] != 0:
        return Ret
    if len(Ret[1]) > 0:
        Entries += Ret[1]
    Ret = convertTSPGf2Dicts(FLT, What)
    if Ret[0] != 0:
        return Ret
    if len(Ret[1]) > 0:
        # Fill in the positions for the stations listed in the geometry
        # information that match previous shot or dasfile entries. Geometry
        # information by itself usually isn't much use.
        for Geom in Ret[1]:
            for Entry in Entries:
                if Entry["stid"] == Geom["stid"]:
                    Entry["lati"] = Geom["lati"]
                    Entry["long"] = Geom["long"]
                    Entry["elev"] = Geom["elev"]
                    convertDictPolish(Entry)
    # We need to redo these since we may have combined two different sets
    # above.
    ListID = 0
    for Entry in Entries:
        Entry["liid"] = ListID
        ListID += 1
    return (0, Entries)
################################################
# BEGIN: convertTSPDfSfGfKeepGf2Dicts(FLT, What)
# FUNC:convertTSPDfSfGfKeepGf2Dicts():2013.108
#   Calls the other TSP conversion functions to read in the contents of a file
#   that has das and/or shot and/or geometry file information in it.
#
#   This is like convertTSPDfSfGf2Dicts(), except that it also keeps geometry
#   entries that don't match any das or shot file entries. This would be for
#   something like a mapping routine to call where the actual das or shot IDs
#   are not as important as just getting a point plotted on a map.


def convertTSPDfSfGfKeepGf2Dicts(FLT, What):
    # Look for das and shot entries first, then get any geometry info, match up
    # the station IDs and fill in the das/shot entry positions. If the geometry
    # info doesn't match any entry in Entries then append the geometry info.
    Entries = []
    Ret = convertTSPDf2Dicts(FLT, What)
    if Ret[0] != 0:
        return Ret
    if len(Ret[1]) > 0:
        Entries += Ret[1]
    Ret = convertTSPSf2Dicts(FLT, What)
    if Ret[0] != 0:
        return Ret
    if len(Ret[1]) > 0:
        Entries += Ret[1]
    Ret = convertTSPGf2Dicts(FLT, What)
    if Ret[0] != 0:
        return Ret
    if len(Ret[1]) > 0:
        Append = []
        for Geom in Ret[1]:
            Found = False
            for Entry in Entries:
                if Entry["stid"] == Geom["stid"]:
                    Entry["lati"] = Geom["lati"]
                    Entry["long"] = Geom["long"]
                    Entry["elev"] = Geom["elev"]
                    convertDictPolish(Entry)
                    Found = True
                    break
            if Found is False:
                Append.append(Geom)
        if len(Geom) > 0:
            Entries += Append
# We need to redo these since we may have combined two different sets above.
    ListID = 0
    for Entry in Entries:
        Entry["liid"] = ListID
        ListID += 1
    return (0, Entries)


# ----- Simple R and S functions -----
######################################
# BEGIN: convertRInfo2Dicts(FLT, What)
# FUNC:convertRInfo2Dicts():2019.024
#   Reads a text file or lines from a text field or from a List of lines and
#   converts each line of information to a CONVERT dictionary. The expected
#   format is
#       stid  idid  lati  long  elev  or
#       stid, idid, lati, long, elev  or
#       stid; idid; lati; long; elev
#       or \t separated.
CONVERTRInfoTip = "StaID TexanID Lat Long Elev"
CONVERTRInfoDesc = "RInfo = Receiver information format file"


def convertRInfo2Dicts(FLT, What):
    Ret4 = ""
    if What == "file":
        Ret = readFileLinesRB(FLT)
        if Ret[0] != 0:
            return (-1, Ret[1], Ret[2], Ret[3], Ret[4], Ret[5])
        Lines = Ret[1]
        Ret4 = FLT
    elif What == "list":
        Lines = FLT
    elif What == "text":
        Lines = []
        N = 1
        while True:
            if len(FLT.get("%d.0" % N)) == 0:
                break
            Lines.append(FLT.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    elif What == "txt":
        LTxt = PROGTxt[FLT]
        Lines = []
        N = 1
        while True:
            if len(LTxt.get("%d.0" % N)) == 0:
                break
            Lines.append(LTxt.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    Entries = []
    ListID = 0
    N = 1
    for Line in Lines:
        N += 1
        Line = Line.strip()
# If the line does not start with a number it might be something like a GPX
# file line.
        if len(Line) == 0 or Line.startswith("#"):
            continue
        if Line.find(",") != -1:
            Parts2 = Line.split(",")
            Parts = []
            for Part in Parts2:
                Parts.append(Part.strip())
        elif Line.find(";") != -1:
            Parts2 = Line.split(";")
            Parts = []
            for Part in Parts2:
                Parts.append(Part.strip())
        else:
            Parts = Line.split()
# There are just a few tests we can do, like there should be 5 fields and
# most should be a number or the 3rd and 4th ones might start with an N/S or
# E/W.
        if len(Parts) != RINFOFIELDS:
            continue
        Dict = {}
        convertDictDefaults(Dict)
        Ret = convertCertifyTextULN("stid", Parts[RINFO_STID])
        Dict["stid"] = Ret[1]
        Ret = convertCertifyIntegerP("idid", Parts[RINFO_IDID],
                                     "Receiver ID")
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line), 2,
                    Ret4)
        Dict["idid"] = Ret[1]
        Ret = convertCertifyLATILONG("lati", Parts[RINFO_LATI])
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line), 2,
                    Ret4)
        Dict["lati"] = Ret[1]
        Ret = convertCertifyLATILONG("long", Parts[RINFO_LONG])
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line), 2,
                    Ret4)
        Dict["long"] = Ret[1]
        Ret = convertCertifyFloat2("elev", Parts[RINFO_ELEV], "Elevation")
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line), 2,
                    Ret4)
        Dict["elev"] = Ret[1]
        Dict["dtyp"] = "RECV"
        Dict["liid"] = ListID
        ListID += 1
# We don't run convertSetDictDefaults() here since we've already checked to
# make sure that the required fields were there.
        convertDictPolish(Dict)
        Entries.append(Dict)
    return (0, Entries)
#############################################################
# BEGIN: convertDict2RInfo(Dict, Delim, Strict, PlusTime = 0)
# FUNC:convertDict2RInfo():2014.195
#    Returns a simple receiver info line from the info in the passed Dict.
#    If PlusTime is not 0 then the Dict["time"] field value will be appended.
#    This might be the time the waypoint was made using a handheld GPS
#    receiver derrived from GPX information.


def convertDict2RInfo(Dict, Delim, Strict, PlusTime=0):
    if "text" in Dict:
        return (0, Dict["text"])
    if Strict is True and Dict["dtyp"] != "RECV":
        return (1, "")
    if Delim == "comma":
        Delim = ","
    elif Delim == "space":
        Delim = " "
    elif Delim == "semi":
        Delim = ";"
    elif Delim == "tab":
        Delim = "\t"
    Return = "%s%s%s%s%s%s%s%s%s" % (Dict["stid"], Delim, Dict["idid"],
                                     Delim, Dict["lati"], Delim, Dict["long"],
                                     Delim, Dict["elev"])
    if PlusTime != 0:
        Return += "%s%s" % (Delim, Dict["time"])
    return (0, Return)


######################################
# BEGIN: convertRList2Dicts(FLT, What)
# FUNC:convertSRList2Dicts():2019.024
#   Reads a text file or lines from a text file and converts each line of
#   information to a Convert dictionary. The expected format is
#       1234
#       1235
#       1236...
#   Can have this special comment line "# LINE:"
#       1234
#       # LINE: Line B
#       1235...
CONVERTRListTip = "ID\nID\netc."
CONVERTRListDesc = "RList = Simple list of receiver IDs file"


def convertRList2Dicts(FLT, What):
    Ret4 = ""
    if What == "file":
        Ret = readFileLinesRB(FLT)
        if Ret[0] != 0:
            return (-1, Ret[1], Ret[2], Ret[3], Ret[4], Ret[5])
        Lines = Ret[1]
        Ret4 = FLT
    elif What == "list":
        Lines = FLT
    elif What == "text":
        Lines = []
        N = 1
        while True:
            if len(FLT.get("%d.0" % N)) == 0:
                break
            Lines.append(FLT.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    elif What == "txt":
        LTxt = PROGTxt[FLT]
        Lines = []
        N = 1
        while True:
            if len(LTxt.get("%d.0" % N)) == 0:
                break
            Lines.append(LTxt.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    Entries = []
    ListID = 0
    N = 0
    for Line in Lines:
        N += 1
        Line = Line.strip()
        if len(Line) == 0:
            continue
# This is a non-standard thing.
        if Line.startswith("#"):
            if Line.find("# LINE:") != -1:
                Parts = Line.split(":", 1)
                Dict = {}
                convertDictDefaults(Dict)
# This will mean something to someone. There is no station 0.
                Dict["stid"] = "0"
                if len(Parts) == 1:
                    Dict["idid"] = ""
                else:
                    Dict["idid"] = Parts[1].strip()
                Dict["dtyp"] = "RECV"
                Dict["liid"] = ListID
                ListID += 1
                Entries.append(Dict)
            continue
# If the line does not start with a number it might be something like a GPX
# file line.
        if intt(Line) == 0:
            continue
        Parts = Line.split()
        if len(Parts) != 1:
            continue
        Dict = {}
        convertDictDefaults(Dict)
        Value = intt(Line)
# Just in case someone made the list from an old TSP dasfile. (v3 of TSP
# stopped using IDs with 10,000 added to them.)
        if Value > 10000:
            Value -= 10000
        Ret = convertCertifyIntegerP("idid", Line, "Receiver ID")
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line), 2,
                    Ret4)
        Dict["idid"] = Line
        Dict["dtyp"] = "RECV"
        Dict["liid"] = ListID
        ListID += 1
# Make this up.
        Dict["stid"] = str(ListID)
# No convertDictPolish() needed.
        Entries.append(Dict)
    return (0, Entries)
########################################
# BEGIN: convertDict2RList(Dict, Strict)
# FUNC:convertDict2RList():2012.122
#    Returns a simple receiver info line from the info in the passed Dict.


def convertDict2RList(Dict, Strict):
    if "text" in Dict:
        return (0, Dict["text"])
    if Strict is True and Dict["dtyp"] != "RECV":
        return (1, "")
    return (0, "%s" % Dict["idid"])


######################################
# BEGIN: convertSInfo2Dicts(FLT, What)
# FUNC:convertSInfo2Dicts():2019.024
#   Reads a text file or lines from a text file and converts each line of
#   information to a CONVERT dictionary. The expected format is
#       stid  idid  YYYY:DOY:HH:MM:SS.sss[ lati long elev]
#       stid, idid, YYYY:DOY:HH:MM:SS.sss[, lati long elev]
#       stid; idid; YYYY:DOY:HH:MM:SS.sss[; lati long elev]
#       or \t separated.
#   If all of lati, long and elev are there and are "good" then they will be
#   used.
CONVERTSInfoTip = "StaID ShotID Time [Lati Long Elev]"
CONVERTSInfoDesc = "SInfo = Shot information format file"


def convertSInfo2Dicts(FLT, What):
    Ret4 = ""
    if What == "file":
        Ret = readFileLinesRB(FLT)
        if Ret[0] != 0:
            return (-1, Ret[1], Ret[2], Ret[3], Ret[4], Ret[5])
        Lines = Ret[1]
        Ret4 = FLT
    elif What == "list":
        Lines = FLT
    elif What == "text":
        Lines = []
        N = 1
        while True:
            if len(FLT.get("%d.0" % N)) == 0:
                break
            Lines.append(FLT.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    elif What == "txt":
        LTxt = PROGTxt[FLT]
        Lines = []
        N = 1
        while True:
            if len(LTxt.get("%d.0" % N)) == 0:
                break
            Lines.append(LTxt.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    Entries = []
    ListID = 0
    N = 0
    for Line in Lines:
        N += 1
        Line = Line.strip()
# If the line does not start with a number it might be something like a GPX
# file line.
        if len(Line) == 0 or Line.startswith("#"):
            continue
        if Line.find(",") != -1:
            Parts2 = Line.split(",")
            Parts = []
            for Part in Parts2:
                Parts.append(Part.strip())
        elif Line.find(";") != -1:
            Parts2 = Line.split(";")
            Parts = []
            for Part in Parts2:
                Parts.append(Part.strip())
        else:
            Parts = Line.split()
# There are just a few tests we can do, like there should be 3 or 6 fields.
        Items = len(Parts)
        if Items != 3 and Items != 6:
            continue
        Dict = {}
        convertDictDefaults(Dict)
        Ret = convertCertifyTextULN("stid", Parts[SINFO_STID])
        Dict["stid"] = Ret[1]
        Ret = convertCertifyIntegerP("idid", Parts[SINFO_IDID], "Shot ID")
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line), 2,
                    Ret4)
        Dict["idid"] = Ret[1]
        Ret = convertCertifyTime("shtm", "ms", Parts[SINFO_TIME], "Shot")
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line), 2,
                    Ret4)
        Dict["shtm"] = Ret[1]
        if Items == 6:
            # We won't complain if these are bad. We just won't use them. All
            # three have to be good.
            RetLati = convertCertifyLATILONG("lati", Parts[SINFO_LATI])
            if RetLati[0] == 0:
                RetLong = convertCertifyLATILONG("long", Parts[SINFO_LONG])
                if RetLong[0] == 0:
                    RetElev = convertCertifyFloat2("elev", Parts[SINFO_ELEV],
                                                   "Elevation")
                    if RetElev[0] == 0:
                        Dict["lati"] = RetLati[1]
                        Dict["long"] = RetLong[1]
                        Dict["elev"] = RetElev[1]
        Dict["dtyp"] = "SHOT"
        Dict["liid"] = ListID
        ListID += 1
# We don't run convertSetDictDefaults() here since we've already checked to
# make sure that the required fields were there.
        convertDictPolish(Dict)
        Entries.append(Dict)
    return (0, Entries)


######################################
# BEGIN: convertSList2Dicts(FLT, What)
# FUNC:convertSList2Dicts():2019.024
#   Reads a text file or lines from a text file and converts each line of
#   information to a Convert dictionary. The expected format is
#       shottime (in YYYY:DOY:HH:MM:SS.sss)
#       shottime
#       shottime...
#   Anything can follow the shot time.
CONVERTSListTip = "YYYY:DOY:HH:MM:SS.sss\nYYYY:DOY:HH:MM:SS.sss\netc."
CONVERTSListDesc = "SList = Simple list of shot times format file"


def convertSList2Dicts(FLT, What):
    Ret4 = ""
    if What == "file":
        Ret = readFileLinesRB(FLT)
        if Ret[0] != 0:
            return (-1, Ret[1], Ret[2], Ret[3], Ret[4], Ret[5])
        Lines = Ret[1]
        Ret4 = FLT
    elif What == "list":
        Lines = FLT
    elif What == "text":
        Lines = []
        N = 1
        while True:
            if len(FLT.get("%d.0" % N)) == 0:
                break
            Lines.append(FLT.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    elif What == "txt":
        LTxt = PROGTxt[FLT]
        Lines = []
        N = 1
        while True:
            if len(LTxt.get("%d.0" % N)) == 0:
                break
            Lines.append(LTxt.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    Entries = []
    ListID = 0
    N = 0
    for Line in Lines:
        N += 1
        Line = Line.strip()
        if Line.startswith("#"):
            continue
        if Line.find(":") == -1:
            continue
        Dict = {}
        convertDictDefaults(Dict)
        Ret = convertCertifyTime("shtm", "ms", Line, "Shot")
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line), 2,
                    Ret4)
        Dict["shtm"] = Ret[1]
        Dict["dtyp"] = "SHOT"
        Dict["liid"] = ListID
        ListID += 1
        convertDictPolish(Dict)
        Entries.append(Dict)
    return (0, Entries)
#############################################################
# BEGIN: convertDict2SInfo(Dict, Delim, Strict, PlusTime = 0)
# FUNC:convertDict2SInfo():2014.195
#    Returns a simple shot file line from the info in the passed Dict.
#    If PlusTime is not 0 the Dict["time"] value will be appended to the
#    returned line. This might be the time a waypoint is made if the input
#    source is a GPX file.


def convertDict2SInfo(Dict, Delim, Strict, PlusTime=0):
    if "text" in Dict:
        return (0, Dict["text"])
    if Strict is True and Dict["dtyp"] != "SHOT":
        return (1, "")
    if Delim == "comma":
        Delim = ","
    elif Delim == "space":
        Delim = " "
    elif Delim == "semi":
        Delim = ";"
    elif Delim == "tab":
        Delim = "\t"
# If the position info has been filled in then include it.
    if Dict["lati"] != ConvertDefaults["lati"] and \
            Dict["long"] != ConvertDefaults["long"] and \
            Dict["elev"] != ConvertDefaults["elev"]:
        Return = "%s%s%s%s%s%s%s%s%s%s%s" % (Dict["stid"], Delim,
                                             Dict["idid"], Delim,
                                             Dict["shtm"], Delim,
                                             Dict["lati"], Delim,
                                             Dict["long"], Delim,
                                             Dict["elev"])
    else:
        Return = "%s%s%s%s%s" % (Dict["stid"], Delim, Dict["idid"], Delim,
                                 Dict["shtm"])
    if PlusTime != 0:
        Return += "%s%s" % (Delim, Dict["time"])
    return (0, Return)
########################################
# BEGIN: convertDict2SList(Dict, Strict)
# FUNC:convertDict2SList():2012.122
#    Returns a shot time from the info in the passed Dict.


def convertDict2SList(Dict, Strict):
    if "text" in Dict:
        return (0, Dict["text"])
    if Strict is True and Dict["dtyp"] != "SHOT":
        return (1, "")
    return (0, "%s" % Dict["shtm"])


#########################################
# BEGIN: convertRListTwo2Dicts(FLT, What)
# FUNC:convertRListTwo2Dicts():2019.024
#   Converts REALLY simple lists of DASs into Dicts:
#       1234
#       4321
#       etc.
#   This is a specialized function that ONLY sets a value for item idid and
#   stid ("?") (and liid) in the dictionary.
CONVERTRListTwoTip = "ID\nID\netc."
CONVERTRListTwoDesc = "RList = Simple list of receiver IDs file"


def convertRListTwo2Dicts(FLT, What):
    Ret4 = ""
    if What == "file":
        Ret = readFileLinesRB(FLT)
        if Ret[0] != 0:
            return (-1, Ret[1], Ret[2], Ret[3], Ret[4], Ret[5])
        Lines = Ret[1]
        Ret4 = FLT
    elif What == "list":
        Lines = FLT
    elif What == "text":
        Lines = []
        N = 1
        while True:
            if len(FLT.get("%d.0" % N)) == 0:
                break
            Lines.append(FLT.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    elif What == "txt":
        LTxt = PROGTxt[FLT]
        Lines = []
        N = 1
        while True:
            if len(LTxt.get("%d.0" % N)) == 0:
                break
            Lines.append(LTxt.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    Entries = []
    ListID = 0
    N = 0
    for Line in Lines:
        N += 1
        Line = Line.strip()
# If the line does not start with a number it might be something like a GPX
# file line.
        if len(Line) == 0 or Line.startswith("#") or intt(Line) == 0:
            continue
        Dict = {}
# We won't call any routines to set up the whole Dict since there is no other
# information.
        Ret = convertCertifyIntegerP("idid", Line, "ID")
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line), 2,
                    Ret4)
        Dict["idid"] = Ret[1]
        Dict["stid"] = Ret[1]
        Dict["dtyp"] = "RECV"
        Dict["liid"] = ListID
        ListID += 1
# We don't run convertSetDictDefaults() here since we've already checked to
# make sure that the id is OK (it's at least a number).
        Entries.append(Dict)
    return (0, Entries)
#######################################
# BEGIN: convertSList2Dicts2(FLT, What)
# FUNC:convertSList2Dicts2():2019.024
#   This is a specialized function that ONLY sets a value for item shtm (and
#   liid) in the dictionary.


def convertSList2Dicts2(FLT, What):
    Ret4 = ""
    if What == "file":
        Ret = readFileLinesRB(FLT)
        if Ret[0] != 0:
            return (-1, Ret[1], Ret[2], Ret[3], Ret[4], Ret[5])
        Lines = Ret[1]
        Ret4 = FLT
    elif What == "list":
        Lines = FLT
    elif What == "text":
        Lines = []
        N = 1
        while True:
            if len(FLT.get("%d.0" % N)) == 0:
                break
            Lines.append(FLT.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    elif What == "txt":
        LTxt = PROGTxt[FLT]
        Lines = []
        N = 1
        while True:
            if len(LTxt.get("%d.0" % N)) == 0:
                break
            Lines.append(LTxt.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    Entries = []
    ListID = 0
    N = 0
    for Line in Lines:
        N += 1
        Line = Line.strip()
        if len(Line) == 0 or Line.startswith("#"):
            continue
        Dict = {}
# We won't call any routines to set up the whole Dict since there is no other
# information.
        Ret = convertCertifyTime("shtm", "ms", Line, "Shot time")
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line), 2,
                    Ret4)
        Dict["shtm"] = Ret[1]
        Dict["dtyp"] = "SHOT"
        Dict["liid"] = ListID
        ListID += 1
# We don't run convertSetDictDefaults() here since we've already checked to
# make sure that the time is OK.
        Entries.append(Dict)
    return (0, Entries)


#######################################
# BEGIN: convertRSInfo2Dicts(FLT, What)
# FUNC:convertRSInfo2Dicts():2013.108
#   Calls the other conversion functions to read in the contents of a file
#   that has receiver and/or shot information in it.
CONVERTRSInfoDesc = ("RSInfo = Receiver and/or shot information lines in the "
                     "same file.")
CONVERTRSInfoTip = "%s\nand/or\n%s" % (CONVERTRInfoTip, CONVERTSInfoTip)


def convertRSInfo2Dicts(FLT, What):
    Entries = []
    Ret = convertRInfo2Dicts(FLT, What)
    if Ret[0] != 0:
        return Ret
    if len(Ret[1]) > 0:
        Entries += Ret[1]
    Ret = convertSInfo2Dicts(FLT, What)
    if Ret[0] != 0:
        return Ret
    if len(Ret[1]) > 0:
        Entries += Ret[1]
# We need to redo these since we may have combined stuff above.
    ListID = 0
    for Entry in Entries:
        # "dtyp" was set above.
        Entry["liid"] = ListID
        ListID += 1
    return (0, Entries)


# ----- For LUNCH system -----
######################################
# BEGIN: convertLunch2Dicts(FLT, What)
# FUNC:convertLunch2Dicts():2019.024
#   Reads a text file or lines from a text file and converts each line of
#   information to a CONVERT dictionary. The expected format is
#       $L1 dver time depu rtyp idid chan stid lati long elev [styp sser dept]
CONVERTLunchDesc = "Lunch = LUNCH system information line."
CONVERTLunchTip = ("$T1 DVer Time DePu RType RecvID Chan\n   StaID Lati Long "
                   "Elev [SType SSer Dept]")


def convertLunch2Dicts(FLT, What):
    Ret4 = ""
    if What == "file":
        Ret = readFileLinesRB(FLT)
        if Ret[0] != 0:
            return (-1, Ret[1], Ret[2], Ret[3], Ret[4], Ret[5])
        Lines = Ret[1]
        Ret4 = FLT
    elif What == "list":
        Lines = FLT
    elif What == "text":
        Lines = []
        N = 1
        while True:
            if len(FLT.get("%d.0" % N)) == 0:
                break
            Lines.append(FLT.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    elif What == "txt":
        LTxt = PROGTxt[FLT]
        Lines = []
        N = 1
        while True:
            if len(LTxt.get("%d.0" % N)) == 0:
                break
            Lines.append(LTxt.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    Entries = []
    ListID = 0
    N = 0
    for Line in Lines:
        N += 1
        Line = Line.strip()
# If the line does not start with a number it might be something like a GPX
# file line.
        if len(Line) == 0 or Line.startswith("#") or intt(Line) == 0:
            continue
        if Line.find(",") != -1:
            Parts2 = Line.split(",")
            Parts = []
            for Part in Parts2:
                Parts.append(Part.strip())
        elif Line.find(";") != -1:
            Parts2 = Line.split(";")
            Parts = []
            for Part in Parts2:
                Parts.append(Part.strip())
        else:
            Parts = Line.split()
# There are just a few tests we can do, like there should be 3 or 6 fields.
        Items = len(Parts)
        if Parts[LUNCHL1_DVER] != "B":
            continue
        if Items != LUNCHL1REQ and Items != LUNCHL1FIELDS:
            continue
        Dict = {}
        convertDictDefaults(Dict)
        Ret = convertCertifyTime("time", "s", Parts[LUNCHL1_TIME], "Time")
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line), 2,
                    Ret4)
        Dict["time"] = Ret[1]
        convertCertifyDEPU(Parts[LUNCHL1_DEPU])
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line), 2,
                    Ret4)
        Dict["depu"] = Value
        Ret = convertCertifyRTYP(Parts[LUNCHL1_RTYP])
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line), 2,
                    Ret4)
        Dict["rtyp"] = Ret[1]
        Ret = convertCertifyIntegerP("idid", Parts[LUNCHL1_IDID],
                                     "Recveiver ID")
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line), 2,
                    Ret4)
        Dict["idid"] = Ret[1]
        Ret = convertCertifyCHAN(Parts[LUNCHL1_CHAN])
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line), 2,
                    Ret4)
        Dict["chan"] = Ret[1]
        Ret = convertCertifyTextULN("stid", Parts[LUNCHL1_STID])
        Dict["stid"] = Ret[1]
        Ret = convertCertifyLATILONG("lati", Parts[LUNCHL1_LATI])
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line), 2,
                    Ret4)
        Dict["lati"] = Ret[1]
        Ret = convertCertifyLATILONG("long", Parts[LUNCHL1_LONG])
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line), 2,
                    Ret4)
        Dict["long"] = Ret[1]
        Ret = convertCertifyFloat2("elev", Parts[LUNCHL1_ELEV], "Elevation")
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line), 2,
                    Ret4)
        Dict["elev"] = Ret[1]
# These are optional, though they will probably always be there.
        if len(Parts) == LUNCHL1FIELDS:
            Ret = convertCertifyTextULN("styp", Parts[LUNCHL1_STYP])
            Dict["styp"] = Ret[1]
            Ret = convertCertifyTextULN("sser", Parts[LUNCHL1_SSER])
            Dict["sser"] = Ret[1]
            Ret = convertCertifyDepTeam(Parts[LUNCHL1_DEPT])
            Dict["dept"] = Ret[1]
        Dict["dtyp"] = "RECV"
        Dict["liid"] = ListID
        ListID += 1
# We don't run convertSetDictDefaults() here since we've already checked to
# make sure that the required fields were there.
        convertDictPolish(Dict)
        Entries.append(Dict)
    return (0, Entries)
# ----- COCI form functions -----
#########################################################
# BEGIN: convertCOCI2Dicts(FLT, What, KeepAll, Delimited)
# FUNC:convertCOCI2Dicts():2019.024
#   Reads the information lines from the Check-Out/Check-In form from a file,
#   passed List, passed Text() or just a text line and converts them into
#      (0, [Dict, Dict...]).
#   The caller should make sure that there are at least the right number of
#   fields before coming here if passing an individual line, because if there
#   is not enough once the line is split up then the line will be ignored.
#
#   If KeepAll is True then blank and comment lines will be kept and the
#   resulting dictionary for either of those will have just one item with a
#   key of "text".
#
#   If Delimited is True then the function will check for , ; tab and then
#   space delimiters and split the lines up that way.


def convertCOCI2Dicts(FLT, What, KeepAll, Delimited):
    Ret4 = ""
    if What == "file":
        Ret = readFileLinesRB(FLT)
        if Ret[0] != 0:
            return (-1, Ret[1], Ret[2], Ret[3], Ret[4], Ret[5])
        Lines = Ret[1]
        Ret4 = FLT
    elif What == "line":
        Lines = [FLT]
    elif What == "list":
        Lines = FLT
    elif What == "text":
        Lines = []
        N = 1
        while True:
            if len(FLT.get("%d.0" % N)) == 0:
                break
            Lines.append(FLT.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    elif What == "txt":
        LTxt = PROGTxt[FLT]
        Lines = []
        N = 1
        while True:
            if len(LTxt.get("%d.0" % N)) == 0:
                break
            Lines.append(LTxt.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    Entries = []
# This is a "serial number" for a Dict that just happens to also be the List
# index position of a Dict for others to use when they need to refer back to
# an entry's information. It will be Dict["liid"]. This does not apply to
# "text"-only Dictionaries since they will never be map-plotted or anything
# like that.
    ListID = 0
    N = 0
    for Line in Lines:
        N += 1
        if len(Line) == 0 or Line.startswith("#"):
            if KeepAll is True:
                Dict = {"text": Line}
                Entries.append(Dict)
            continue
        if Delimited is True:
            if Line.find(",") != -1:
                Parts2 = Line.split(",", COCI_REQU)
                Parts = []
                for Part in Parts2:
                    Parts.append(Part.strip())
            elif Line.find(";") != -1:
                Parts2 = Line.split(";", COCI_REQU)
                Parts = []
                for Part in Parts2:
                    Parts.append(Part.strip())
# Covers tabs and anything else.
            else:
                Line = convertOneBlank(Line)
                Parts = Line.split(" ", COCI_REQU)
        else:
            # Do the convertOneBlank()-split() shuffle so the routine doesn't
            # count comment words as other items because of multiple spaces
            # in-a-row.
            Line = convertOneBlank(Line)
            Parts = Line.split(" ", COCI_REQU)
        if len(Parts) < COCI_REQU:
            continue
        Dict = {}
        convertDictDefaults(Dict)
        Ret = convertCertifyTime("time", "m", Parts[COCI_TIME], "Entry")
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line), 2,
                    Ret4)
        Dict["time"] = Ret[1]
        Ret = convertCertifyIntegerP("idid", Parts[COCI_IDID], "Receiver ID")
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line), 2,
                    Ret4)
        Dict["idid"] = Ret[1]
        Ret = convertCertifyCHAN(Parts[COCI_CHAN])
        if Ret[0] != 0:
            return (N, Ret[1], "Bad line (%d): %s\n%s" % (N, Ret[2], Line), 2,
                    Ret4)
        Dict["chan"] = Ret[1]
        Ret = convertCertifyTextULN("bxid", Parts[COCI_BXID])
        Dict["bxid"] = Ret[1]
        Ret = convertCertifyTextULN("dept", Parts[COCI_DEPT])
        Dict["dept"] = Ret[1]
# There may not be any "In" flag.
        try:
            Dict["infg"] = Parts[COCI_INFG]
        except Exception:
            Dict["infg"] = ConvertDefaults["infg"]
        Dict["dtyp"] = "COCI"
        Dict["liid"] = ListID
        ListID += 1
# We don't run convertSetDictDefaults() here since we've already checked to
# make sure that the required fields were there. We don't need to run
# convertDictPolish() either since there won't be anything for it to do.
        Entries.append(Dict)
# Strip off any pesky trailing blank line(s).
    if KeepAll is True:
        while len(Entries) > 0:
            if "text" in Entries[-1]:
                if len(Entries[-1]["text"]) == 0:
                    Entries = Entries[:-1]
                else:
                    break
            else:
                break
    return (0, Entries)
##########################################
# BEGIN: convertDict2COCILine(Dict, Delim)
# FUNC:convertDict2COCILine():2011.125


def convertDict2COCILine(Dict, Delim):
    if "text" in Dict:
        return Dict["text"]
    if Delim == "comma":
        Delim = ","
    elif Delim == "space":
        Delim = " "
    elif Delim == "semi":
        Delim = ";"
    elif Delim == "tab":
        Delim = "\t"
    Return = "%s%s%s%s%s%s%s%s%s" % (Dict["time"], Delim, Dict["idid"],
                                     Delim, Dict["chan"], Delim, Dict["bxid"],
                                     Delim, Dict["dept"])
    if Dict["infg"] != ConvertDefaults["infg"]:
        # This keep the user from entering anything else.
        Return += "%s%s" % (Delim, "--IN--")
    return Return
#############################################
# BEGIN: convertDict2COCIEntry(Dict, IncTime)
# FUNC:convertDict2COCIEntry():2011.191
#   WARNING: Keep layout the same as formCOCIItems2Entry().


def convertDict2COCIEntry(Dict, IncTime):
    if "text" in Dict:
        return Dict["text"]
    if IncTime is False:
        Return = "%-4s  %s  %-7s  %-6s" % (Dict["idid"], Dict["chan"],
                                           Dict["bxid"], Dict["dept"])
    else:
        Return = "%s  %-4s  %s  %-7s  %-6s" % (Dict["time"], Dict["idid"],
                                               Dict["chan"], Dict["bxid"],
                                               Dict["dept"])
    if Dict["infg"] != ConvertDefaults["infg"]:
        Return += "  --IN--"
    return Return.strip()
# ----- Just read and return lines -----
#####################################
# BEGIN: convertText2Dicts(FLT, What)
# FUNC:convertText2Dicts():2019.024


def convertText2Dicts(FLT, What):
    if What == "file":
        Ret = readFileLinesRB(FLT)
        if Ret[0] != 0:
            return (-1, Ret[1], Ret[2], Ret[3], Ret[4], Ret[5])
        Lines = Ret[1]
    elif What == "line":
        Lines = [FLT]
    elif What == "list":
        Lines = FLT
    elif What == "text":
        Lines = []
        N = 1
        while True:
            if len(FLT.get("%d.0" % N)) == 0:
                break
            Lines.append(FLT.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    elif What == "txt":
        LTxt = PROGTxt[FLT]
        Lines = []
        N = 1
        while True:
            if len(LTxt.get("%d.0" % N)) == 0:
                break
            Lines.append(LTxt.get("%d.0" % N, "%d.0" % (N + 1)).strip())
            N += 1
    Entries = []
    for Line in Lines:
        Dict = {"text": Line}
        Entries.append(Dict)
    return (0, Entries)
# ----- Certification functions -----
#####################################
# BEGIN: convertCertifyArray(Key, In)
# FUNC:convertCertifyArray():2018.235


def convertCertifyArray(Key, In):
    Value = In.upper()
    if Value == ConvertDefaults[Key]:
        return (0, Value)
    if len(Value) == 0:
        return (0, ConvertDefaults[Key])
    if len(Value) > 2:
        return (-1, "RW",
                "Array/Sub-array value must be two characters or less: %s" %
                Value, 2, "")
    return (0, Value)
##################################
# BEGIN: convertCertifyDepTeam(In)
# FUNC:convertCertifyDepeaTm():2018.235
#   Even though the default value for the deplyment team might be something
#   like "?" that won't fly in things like filenames on most (if not all)
#   systems. This can be called to weed out those kinds of characters.


def convertCertifyDepTeam(In):
    if len(In) == 0:
        return (0, "")
    Return = ""
    for C in In:
        if C.isalpha() is False and C.isdigit() is False:
            Return += "_"
        elif C == "?" or C == "*" or C == ":" or C == ";" or C == "/" or \
                C == "\\":
            Return += "_"
        else:
            Return += C
    return (0, Return)
###############################
# BEGIN: convertCertifyRTYP(In)
# FUNC:convertCertifyRTYP():2018.235


def convertCertifyRTYP(In):
    if len(In) == 0:
        return (0, ConvertDefaults["rtyp"])
    Value = In.upper()
# The old way: Uphole? (Y/N).
    if Value == "N":
        Value = "R"
    if Value == "Y":
        Value = "U"
    if Value != "R" and Value != "U":
        return (-1, "RW", "Receiver type must be R or U.", 2, "")
    return (0, Value)
###############################
# BEGIN: convertCertifyDEPU(In)
# FUNC:convertCertifyDEPU():2018.235


def convertCertifyDEPU(In):
    if len(In) == 0:
        return (0, ConvertDefaults["depu"])
    Value = In.upper()
    if Value != "D" and Value != "P":
        return (-1, "RW", "Deploy/Pickup must be D or P.", 2, "")
    return (0, Value)
###############################
# BEGIN: convertCertifyCHAN(In)
# FUNC:convertCertifyCHAN():2018.235


def convertCertifyCHAN(In):
    if len(In) == 0:
        return (0, ConvertDefaults["chan"])
    try:
        Value = int(In)
        if Value < 0 or Value > 3:
            return (-1, "RW", "Channel value must be 0, 1, 2 or 3.", 2, "")
    except Exception:
        return (-1, "RW", "Channel value must be an integer (0-3).", 2, "")
    return (0, In)
########################################
# BEGIN: convertCertifyLATILONG(Key, In)
# FUNC:convertCertifyLATILONG():2018.235


def convertCertifyLATILONG(Key, In):
    if In == ConvertDefaults[Key]:
        return (0, In)
    if len(In) == 0:
        return (0, ConvertDefaults[Key])
    Ret = checkLatiLong(Key, In, 7)
    if Ret[0] != 0:
        return (-1, Ret[1], Ret[2], Ret[3], Ret[4])
    return (0, Ret[1])
#################################################
# BEGIN: convertCertifyTime(Key, Which, In, Name)
# FUNC:convertCertifyTime():2018.235


def convertCertifyTime(Key, Which, In, Name):
    if In == ConvertDefaults[Key]:
        return (0, In)
    if len(In) == 0:
        return (0, ConvertDefaults[Key])
    Ret = dt2Time(11, 11, In, True)
    if Ret[0] != 0:
        return (-1, Ret[1], "%s date/time: %s" % (Name, Ret[2]), Ret[3],
                Ret[4])
    if len(Ret[1]) == 0:
        return (-1, "RW", "%s date/time: Was blank." % Name, 2)
# The time comes back in "ms", so alter it for the other requests.
    if Which == "s":
        return (0, Ret[1][:-4])
    elif Which == "m":
        return (0, Ret[1][:-7])
    return (0, Ret[1])
###########################################
# BEGIN: convertCertifyFloat(Key, In, Name)
# FUNC:convertCertifyFloat():2018.235


def convertCertifyFloat(Key, In, Name):
    # If it is equal to the allowed default value then it's OK.
    if In == ConvertDefaults[Key]:
        return (0, In)
    if len(In) == 0:
        return (0, ConvertDefaults[Key])
    try:
        Value = float(In)
    except Exception:
        return (-1, "RW", "%s must be a number: %s" % (Name, In), 2, "")
# Return the same type as the default, or return whatever the caller passed.
    if isinstance(ConvertDefaults[Key], float):
        return (0, Value)
    return (0, In)
############################################
# BEGIN: convertCertifyFloat2(Key, In, Name)
# FUNC:convertCertifyFloat2():2018.235


def convertCertifyFloat2(Key, In, Name):
    # If it is equal to the allowed default value then it's OK.
    if In == ConvertDefaults[Key]:
        return (0, In)
    if len(In) == 0:
        return (0, ConvertDefaults[Key])
    try:
        Value = float(In)
    except Exception:
        return (-1, "RW", "%s must be a number: %s" % (Name, In), 2, "")
# Return the same type as the default.
    if isinstance(ConvertDefaults[Key], float):
        return (0, float("%.2f" % Value))
    return (0, "%.2f" % Value)
#############################################
# BEGIN: convertCertifyFloatPZ(Key, In, Name)
# FUNC:convertCertifyFloatPZ():2018.235


def convertCertifyFloatPZ(Key, In, Name):
    if In == ConvertDefaults[Key]:
        return (0, In)
    if len(In) == 0:
        return (0, ConvertDefaults[Key])
    try:
        Value = float(In)
        if Value < 0.0:
            return (-1, "RW", "%s must be a number, >= 0.0: %s" % (Name, In),
                    2, "")
    except Exception:
        return (-1, "RW", "%s must be a number: %s" % (Name, In), 2, "")
    if isinstance(ConvertDefaults[Key], float):
        return (0, Value)
    return (0, In)
##############################################
# BEGIN: convertCertifyFloat2PZ(Key, In, Name)
# FUNC:convertCertifyFloat2PZ():2018.235


def convertCertifyFloat2PZ(Key, In, Name):
    if In == ConvertDefaults[Key]:
        return (0, In)
    if len(In) == 0:
        return (0, ConvertDefaults[Key])
    try:
        Value = float(In)
        if Value < 0.0:
            return (-1, "RW", "%s must be a number, >= 0.0: %s" % (Name, In),
                    2, "")
    except Exception:
        return (-1, "RW", "%s must be a number: %s" % (Name, In), 2, "")
    if isinstance(ConvertDefaults[Key], float):
        return (0, float("%.2" % Value))
    return (0, "%.2f" % Value)
##############################################
# BEGIN: convertCertifyFloat3PZ(Key, In, Name)
# FUNC:convertCertifyFloat3PZ():2018.235


def convertCertifyFloat3PZ(Key, In, Name):
    if In == ConvertDefaults[Key]:
        return (0, In)
    if len(In) == 0:
        return (0, ConvertDefaults[Key])
    try:
        Value = float(In)
        if Value < 0.0:
            return (-1, "RW", "%s must be a number, >= 0.0: %s" % (Name, In),
                    2, "")
    except Exception:
        return (-1, "RW", "%s must be a number: %s" % (Name, In), 2, "")
    if isinstance(ConvertDefaults[Key], float):
        return (0, float("%.3" % Value))
    return (0, "%.3f" % Value)
#############################################
# BEGIN: convertCertifyInteger(Key, In, Name)
# FUNC:convertCertifyInteger():2018.235


def convertCertifyInteger(Key, In, Name):
    if In == ConvertDefaults[Key]:
        return (0, In)
    if len(In) == 0:
        return (0, ConvertDefaults[Key])
    try:
        Value = int(In)
        if Value < 1:
            return (-1, "RW", "%s must be an integer, > 0: %s" % (Name, In),
                    2, "")
    except Exception:
        return (-1, "RW", "%s must be an integer: %s" % (Name, In), 2, "")
    if isinstance(ConvertDefaults[Key], anint):
        return (0, Value)
    return (0, In)
##############################################
# BEGIN: convertCertifyIntegerP(Key, In, Name)
# FUNC:convertCertifyIntegerP():2018.235


def convertCertifyIntegerP(Key, In, Name):
    # If it is equal to the allowed default value then it's OK.
    if In == ConvertDefaults[Key]:
        return (0, In)
    if len(In) == 0:
        return (0, ConvertDefaults[Key])
    try:
        Value = int(In)
        if Value < 1:
            return (-1, "RW", "%s must be an integer > 0: %s" % (Name, In),
                    2, "")
    except Exception:
        return (-1, "RW", "%s must be an integer: %s" % (Name, In), 2, "")
    if isinstance(ConvertDefaults[Key], anint):
        return (0, Value)
    return (0, In)
###############################################
# BEGIN: convertCertifyIntegerPZ(Key, In, Name)
# FUNC:convertCertifyIntegerPZ():2018.234


def convertCertifyIntegerPZ(Key, In, Name):
    if In == ConvertDefaults[Key]:
        return (0, In)
    if len(In) == 0:
        return (0, ConvertDefaults[Key])
    try:
        Value = int(In)
        if Value < 0:
            return (-1, "RW", "%s must be an integer, >= 0: %s" % In, 2, "")
    except Exception:
        return (-1, "RW", "%s must be an integer: %s" % In, 2, "")
    if isinstance(ConvertDefaults[Key], anint):
        return (0, Value)
    return (0, In)
#####################################
# BEGIN: convertCertifyTextU(Key, In)
# FUNC:convertCertifyTextU():2018.235


def convertCertifyTextU(Key, In):
    Value = In.upper()
    if Value == ConvertDefaults[Key]:
        return (0, Value)
    if len(Value) == 0:
        return (0, ConvertDefaults[Key])
    return (0, Value)
################################################################
# BEGIN: convertCertifyTextULN(Key, In, Name = "", Warn = False)
# FUNC:convertCertifyTextULN():2018.235
#   Uppercases, squeezes out whilespace, and only passes back letters and
#   numbers. Returns an error message if Warn is True and something is found.


def convertCertifyTextULN(Key, In, Name="", Warn=False):
    Value = In.upper().strip()
    if Warn is False:
        if len(Value) == 0:
            return (0, ConvertDefaults[Key])
        if Value == ConvertDefaults[Key]:
            return (0, Value)
        Value.replace(" ", "")
        Return = ""
        for C in Value:
            if C.isalpha() or C.isdigit():
                Return += C
        if len(Return) == 0:
            return (0, ConvertDefaults[Key])
        return (0, Return)
    else:
        if len(Value) == 0:
            return (-1, "RW", "%s is empty." % Name, 2, "")
        if Value == ConvertDefaults[Key]:
            return (0, Value)
        try:
            Value.index(" ")
        except Exception:
            return (-1, "RW", "%s contains spaces: %s" % (Name, Value), 2, "")
        for C in Value:
            if C.isalpha() or C.isdigit():
                continue
            return (-1, "RW", "%s can only be letters and numbers: %s" %
                    (Name, Value), 2, "")
    return (0, Value)
# ----- Accessory functions -----
######################################
# BEGIN: convertChan2Arry(Chan, Which)
# FUNC:convertChan2Arry():2011.170


def convertChan2Arry(Chan, Which):
    Chan = str(Chan)
    if Which == "0123ABCD":
        if Chan == "0":
            return "A"
        elif Chan == "1":
            return "B"
        elif Chan == "2":
            return "C"
        elif Chan == "3":
            return "D"
    elif Which == "0123AACD":
        if Chan == "0" or Chan == "1":
            return "A"
        elif Chan == "2":
            return "C"
        elif Chan == "3":
            return "D"
    elif Which == "0123AAAA":
        if Chan == "0" or Chan == "1" or Chan == "2" or Chan == "3":
            return "A"
    return "Z"
###################################
# BEGIN: convertCountEntries(Dicts)
# FUNC:convertCountEntries():2011.049
#   Goes through the Dict and counts how many actual information entries there
#   are exclusive of the number of dictionaries with just "text" keys in them.


def convertCountEntries(Dicts):
    Entries = 0
    for Dict in Dicts:
        if "text" in Dict:
            continue
        Entries += 1
    return Entries
###########################################
# BEGIN: convertDict2PickleDict(Dict, Keys)
# FUNC:convertDict2PickleDict():2018.231
#   Takes the passed Dict and 'pickleizes' it into a text string that is
#   passed back to the caller. Keys can be a List of Dict keys to include.
#   If Keys is [] then all keys will be included.
#   Returns (0, String) if everything goes OK, otherwise a standard error
#   message will be returned. The return code for the error message will be -1
#   just to keep it consistant with the rest of the conversion functions.


def convertDict2PickleDict(Dict, Keys):
    Return = {}
    if len(Keys) == 0:
        Keys = list(Dict.keys())
    for Key in Keys:
        try:
            Value = Dict[Key]
        except Exception:
            return (-1, "RW", "Bad key value: '%s'" % Key, 2, "")
        Return[Key] = Value
    return (0, str(Return))
##################################################
# BEGIN: convertDictPolish(Dict, Model = "RT125A")
# FUNC:convertDictPolish():2013.037
#   Goes through the special dictionary entries, like lati, and fills in the
#   float version latf so downstream functions don't have to do it.
#   Should be called as a last thing after importing information into the Dict.


def convertDictPolish(Dict, Model="RT125A"):
    if Dict["lati"] != ConvertDefaults["lati"]:
        Dict["latf"] = floatt(nsew2pm("lat", Dict["lati"]))
# Normalize the lati to 0 at NP to 180 at SP.
        if Dict["latf"] < 0.0:
            Dict["latn"] = 90.0 + abs(Dict["latf"])
        else:
            Dict["latn"] = 90.0 - Dict["latf"]
    else:
        Dict["latf"] = ConvertDefaults["latf"]
        Dict["latn"] = ConvertDefaults["latn"]
    if Dict["long"] != ConvertDefaults["long"]:
        Dict["lonf"] = floatt(nsew2pm("lon", Dict["long"]))
# Normalize the long to 0 at W180 to 360 at E180.
        if Dict["lonf"] < 0.0:
            Dict["lonn"] = 180.0 - abs(Dict["lonf"])
        else:
            Dict["lonn"] = 180.0 + Dict["lonf"]
    else:
        Dict["lonf"] = ConvertDefaults["lonf"]
        Dict["lonn"] = ConvertDefaults["lonn"]
    if Dict["elev"] != ConvertDefaults["elev"]:
        Dict["elef"] = floatt(Dict["elev"])
    else:
        Dict["elef"] = ConvertDefaults["elef"]
    if Dict["dptm"] != ConvertDefaults["dptm"]:
        Dict["dptf"] = dt2Time(11, -1, Dict["dptm"])
    else:
        Dict["dptf"] = ConvertDefaults["dptf"]
    if Dict["putm"] != ConvertDefaults["putm"]:
        Dict["putf"] = dt2Time(11, -1, Dict["putm"])
    else:
        Dict["putf"] = ConvertDefaults["putf"]
    if Dict["shtm"] != ConvertDefaults["shtm"]:
        # We don't need to check the Ret value here, because it will always be
        # 0 since we are not asking the function to verify the values.
        YYYY, MMM, DD, DOY, HH, MM, SS = dt2Time(11, 0, Dict["shtm"])
        Dict["tspy"] = YYYY
        Dict["tspd"] = DOY
        Dict["tsph"] = HH
        Dict["tspm"] = MM
        Dict["tsps"] = SS
        Dict["shte"] = dt2Time(11, -1, Dict["shtm"])
    else:
        Dict["shte"] = ConvertDefaults["shte"]
    return
##################################
# BEGIN: convertDictDefaults(Dict)
# FUNC:convertDictDefaults():2011.047
#   Fills in the passed dictionary with the 'I know nothing!' defaults. The
#   caller can then fill in the values it knows.


def convertDictDefaults(Dict):
    for Key in ConvertDefaults:
        Dict[Key] = ConvertDefaults[Key]
    return
#####################################
# BEGIN: convertSetDictDefaults(Dict)
# FUNC:convertSetDictDefaults():2018.235
#   Fills in the passed dictionary with the 'I know nothing!' defaults if a
#   value is blank.


def convertSetDictDefaults(Dict):
    for Key in list(Dict.keys()):
        # This will always trip on the comm item if there are no comments, but
        # that's OK.
        if len(Dict[Key]) == 0:
            Dict[Key] = ConvertDefaults[Key]
    return
#######################################
# BEGIN: convertGetDASs(Filespec, Type)
# FUNC:convertGetDASs():2018.235
#      (Code, Color, Message, Beep, Filespec) on error or
#      (0, [Dict, Dict...]) if OK.
#      (0, []) if no receiver information was found.


def convertGetDASs(Filespec, Type):
    if exists(Filespec) is False:
        return (1, "RW", "File does not exist.\n   %s" % Filespec, 2, Filespec)
    if Type == "depb" or Type == "depl":
        Ret = convertDepfile2Dicts(Filespec, "file", "RECV", Type)
    elif Type == "tspdf":
        Ret = convertTSPDf2Dicts(Filespec, "file")
    elif Type == "rinfo":
        Ret = convertRInfo2Dicts(Filespec, "file")
    elif Type == "rlist":
        Ret = convertRList2Dicts(Filespec, "file")
    else:
        return (2, "MW", "Unknown receiver file type: '%s'" % Type, 3,
                Filespec)
    if Ret[0] == 0:
        if len(Ret[1]) != 0:
            return (0, Ret[1])
    return (0, [])
####################################
# BEGIN: convertGetDQuoted(InString)
# FUNC:convertGetDQuoted():2008.273
#    Extracts the first occurance of stuff from InString between double quotes.


def convertGetDQuoted(InString):
    InQuotes = False
    Return = ""
    for C in InString:
        if C == "\"":
            if InQuotes is False:
                InQuotes = True
                continue
            elif InQuotes is True:
                return Return
        if InQuotes is True:
            Return += C
    return Return
########################################
# BEGIN: convertGetShots(Filespec, Type)
# FUNC:convertGetShots():2018.235
#      (Code, Color, Message, Beep, Filespec) on error or
#      (0, [Dict, Dict...]) if OK.
#      (0, []) no problems, but no shot information was found.


def convertGetShots(Filespec, Type):
    # Otherwise we'll never know.
    if exists(Filespec) is False:
        return (1, "RW", "File does not exist.\n   %s" % Filespec, 2, Filespec)
    if Type == "depb" or Type == "depl":
        Ret = convertDepfile2Dicts(Filespec, "file", "SHOT", Type)
    elif Type == "tspsf":
        Ret = convertTSPSf2Dicts(Filespec, "file")
    elif Type == "sinfo":
        Ret = convertSInfo2Dicts(Filespec, "file")
    elif Type == "slist":
        Ret = convertSList2Dicts(Filespec, "file")
    else:
        return (2, "MW", "Unknown shot file type: '%s'" % Type, 3, Filespec)
    if Ret[0] == 0:
        if len(Ret[1]) != 0:
            return (0, Ret[1])
    return (0, [])
#######################################################################
# BEGIN: convertMergeDicts(Dicts1, Dicts2, MatchKeys, CopyKeys, Create)
# FUNC:convertMergeDicts():2018.233
#   Goes through each Dict in Dicts1 and merges information from Dicts2 into
#   it based on matches using MatchKeys in both. If an item from a dict in
#   Dicts2 is not the default value it will replace the value in Dict1, so
#   pass more complete dicts in Dicts1 and dicts for Dicts2 that have info
#   that would add to what is already in Dicts1.
#   What gets replaced in Dict1 from Dict2 can be passed in CopyKeys. If
#   CopyKeys is just [] then all items in Dict1/Dict2 will be checked.
#   If Create is 1 then any Dict2 not matching any entry in Dict1 will be
#   appended to Dicts1.


def convertMergeDicts(Dicts1, Dicts2, MatchKeys, CopyKeys, Create):
    NewDicts = []
    for Dict2 in Dicts2:
        Found = False
        for Dict1 in Dicts1:
            Match = False
# Go through all of the MatchKeys. If any one of them do not match then jump
# out.
            for MatchKey in MatchKeys:
                if Dict1[MatchKey] == Dict2[MatchKey]:
                    Match = True
                else:
                    Match = False
                    break
            if Match is True:
                # Copy over the designated Keys if the values are not
                # ConvertDefaults{} values.
                if len(CopyKeys) > 0:
                    for Key in CopyKeys:
                        if Key in Dict2:
                            if Key in Dict1:
                                if Dict2[Key] != ConvertDefaults[Key]:
                                    Dict1[Key] = Dict2[Key]
                                else:
                                    pass
                            else:
                                if Create == 1:
                                    Dict1[Key] = Dict2[Key]
                                else:
                                    pass
# If no list was specified then just check all of the items.
                else:
                    Keys2 = list(Dict2.keys())
                    for Key2 in Keys2:
                        if Key2 in Dict1:
                            if Dict2[Key2] != ConvertDefaults[Key2]:
                                Dict1[Key2] = Dict2[Key2]
                            else:
                                pass
                        else:
                            if Create == 1:
                                Dict1[Key2] = Dict2[Key2]
                            else:
                                pass
                Found = True
                break
            elif Match is False:
                pass
        if Found is False and Create == 1:
            NewDicts.append(Dict2)
    if len(NewDicts) > 0:
        Dicts1 += NewDicts
    return
############################
# BEGIN: convertOneBlank(In)
# FUNC:convertOneBlank():2012.134


def convertOneBlank(In):
    # Look for any tabs first and turn them into one space then look for
    # multiple spaces.
    In = In.replace("\t", " ")
    while In.find("  ") != -1:
        In = In.replace("  ", " ")
    return In
# END: convertFunctions


###########################
# BEGIN: changeDateFormat()
# FUNC:changeDateFormat():2018.235
#   Makes sure that the format of any data in the from/to fields matches the
#   format selected in the Options menu.
def changeDateFormat():
    updateMe(0)
    Format = OPTDateFormatRVar.get()
    for Fld in ("From", "To"):
        eval("%sDateVar" % Fld).set(
            eval("%sDateVar" % Fld).get().strip().upper())
        if len(eval("%sDateVar" % Fld).get()) != 0:
            Ret = dt2Time(0, 81, eval("%sDateVar" % Fld).get(), True)
            if Ret[0] != 0:
                setMsg("MF", Ret[1], "%s: %s" % (Fld, Ret[2]), Ret[3])
                return False
            eval("%sDateVar" % Fld).set(Ret[1])
# For the time range form formTMRNG().
    for Fld in ("From", "To"):
        eval("TMRNG%sVar" % Fld).set(eval("TMRNG%sVar" %
                                          Fld).get().strip().upper())
        if len(eval("TMRNG%sVar" % Fld).get()) != 0:
            Ret = dt2Time(0, 81, eval("TMRNG%sVar" % Fld).get(), True)
            if Ret[0] != 0:
                setMsg("TMRNG", Ret[1], "%s: %s" % (Fld, Ret[2]), Ret[3])
                return False
            eval("TMRNG%sVar" % Fld).set(Ret[1])
    return True
# END: changeDateFormat


#######################################
# BEGIN: checkForUpdates(Parent = Root)
# LIB:checkForUpdates():2019.028
#   Finds the "new"+PROG_NAMELC+".txt" file created by the program webvers at
#   the URL and checks to see if the version in that file matches the version
#   of this program.
VERS_DLALLOW = True
VERS_VERSURL = "http://www.passcal.nmt.edu/~bob/passoft/"
VERS_PARTS = 4
VERS_NAME = 0
VERS_VERS = 1
VERS_USIZ = 2
VERS_ZSIZ = 3


def checkForUpdates(Parent=Root):
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    formMYD(Parent, (("(OK)", TOP, "ok"),), "ok", "WB", "What's \"git\"?",
            "Eventually you will need to use \"git\" to check for updates.")
    formMYD(Parent, (), "", "CB", "", "Checking...")
# Otherwise the menu doesn't go away on slow connections while url'ing.
    updateMe(0)
# Get the file that tells us about the current version on the server.
# One line:  PROG; version; original size; compressed size
    try:
        Fp = urlopen(VERS_VERSURL + "new" + PROG_NAMELC + ".txt")
        Line = Fp.readlines()
        Fp.close()
        formMYDReturn("")
# If nothing was returned Line will be [] and that will except, also do the
# decode for Py3, otherwise it comes with a b' in front.
        Line = Line[0].decode("latin-1")
# If the file doesn't exist you get something like
#     <!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
# How unhandy.
        if Line.find("DOCTYPE") != -1:
            Line = ""
    except Exception:
        Line = ""
# If we didn't get this then there must have been a problem.
    if len(Line) == 0:
        formMYDReturn("")
        formMYD(Parent, (("(OK)", TOP, "ok"), ), "ok", "RW",
                "It's Probably The Net.",
                "There was an error obtaining the version information from "
                "PASSCAL.", "", 2)
        return
    Parts2 = Line.split(";")
    Parts = []
    for Part in Parts2:
        Parts.append(Part.strip())
    Parts += (VERS_PARTS - len(Parts)) * [""]
    if PROG_VERSION < Parts[VERS_VERS]:
        # Some programs don't need to be professionally installed, some do.
        if VERS_DLALLOW is True:
            Answer = formMYD(Parent, (("Download New Version", TOP,
                                       "dlprod"), ("(Don't)", TOP, "dont"), ),
                             "dont", "YB", "Oh Oh...",
                             "This is an old version of %s.\nThe current "
                             "version generally available is %s." %
                             (PROG_NAME, Parts[VERS_VERS]), "", 2)
            if Answer == "dont":
                return
            if Answer == "dlprod":
                Ret = checkForUpdatesDownload(Parent, Answer, Parts)
            if Ret == "quit":
                progQuitter(True)
                return
        elif VERS_DLALLOW is False:
            Answer = formMYD(Parent, (("(OK)", TOP, "ok"), ),
                             "ok", "YB", "Tell Someone.",
                             "This is an old version of %s.\nThe current "
                             "version generally available is %s." %
                             (PROG_NAME, Parts[VERS_VERS]), "", 2)
            return
    elif PROG_VERSION == Parts[VERS_VERS]:
        Answer = formMYD(Parent, (("Download Anyway", TOP, "dlprod"),
                                  ("(OK)", TOP, "ok")),
                         "ok", "", "Good To Go.", "This copy of %s is up to "
                         "date." % PROG_NAME)
        if Answer == "ok":
            return
        if Answer == "dlprod":
            Ret = checkForUpdatesDownload(Parent, Answer, Parts)
        if Ret == "quit":
            progQuitter(True)
        return
    elif PROG_VERSION > Parts[VERS_VERS]:
        if VERS_DLALLOW is False:
            Answer = formMYD(Parent, (("(OK)", TOP, "ok"), ), "ok", "GB",
                             "All Right!", "Congratulations! This is a newer "
                             "version of %s than is generally available. "
                             "Everyone else probably still has version %s." %
                             (PROG_NAME, Parts[VERS_VERS]))
        elif VERS_DLALLOW is True:
            Answer = formMYD(Parent, (("Download Older Version", TOP,
                                       "dlprod"),
                                      ("(No, Thanks.)", TOP, "ok"), ),
                             "ok", "GB", "All Right!!",
                             "Congratulations! This is a newer version of %s "
                             "than is generally available. Everyone else "
                             "probably still has version %s. You can download "
                             "and use the older version if you want." %
                             (PROG_NAME, Parts[VERS_VERS]))
        if Answer == "ok" or Answer == "keep":
            return
        if Answer == "dlprod":
            Ret = checkForUpdatesDownload(Parent, Answer, Parts)
        if Ret == "quit":
            progQuitter(True)
        return
    return
######################################################
# BEGIN: checkForUpdatesDownload(Parent, Which, Parts)
# FUNC:checkForUpdatesDownload():2019.028


def checkForUpdatesDownload(Parent, Which, Parts):
    formMYD(Parent, (), "", "CB", "", "Downloading...")
    ZSize = int(Parts[VERS_ZSIZ])
    try:
        if Which == "dlprod":
            GetFile = "new%s.zip" % PROG_NAMELC
            Fpr = urlopen(VERS_VERSURL + GetFile)
# SetupsDir may not be the best place to put it, but at least it will be
# consistant and not dependent on where the user was working (like it was).
# If a program does not use PROGSetupsDirVar it will be the current working
# directory.
        SetupsDir = PROGSetupsDirVar.get()
        if len(SetupsDir) == 0:
            SetupsDir = "%s%s" % (abspath("."), sep)
        try:
            # Just open() so I don't interfere with what the OS wants to do.
            Fpw = open(SetupsDir + GetFile, "w")
        except Exception as e:
            formMYDReturn("")
            formMYD(Parent, (("(OK)", TOP, "ok"),), "ok", "RW", "Gasp!",
                    "Error downloading %s\n\n%s" % (GetFile, e))
            return
        DLSize = 0
        while True:
            Buffer = Fpr.read(20000).decode("latin-1")
            if len(Buffer) == 0:
                break
            Fpw.write(Buffer)
            DLSize += 20000
            formMYDMsg("Downloading (%d%%)...\n" % (100 * DLSize / ZSize))
    except Exception as e:
        # They may not exist.
        try:
            Fpr.close()
        except Exception:
            pass
        try:
            Fpw.close()
        except Exception:
            pass
        formMYDReturn("")
        formMYD(Parent, (("(OK)", TOP, "ok"), ), "ok", "MW", "Ooops.",
                "Error downloading new version.\n\n%s" % e, "", 3)
        return ""
    Fpr.close()
    Fpw.close()
    formMYDReturn("")
    if Which == "dlprod":
        Answer = formMYD(Parent, (("Quit %s" % PROG_NAME, TOP, "quit"),
                                  ("Don't Quit", TOP, "cont")),
                         "cont", "GB", "Finished?",
                         "The downloaded program file has been saved "
                         "as\n\n%s\n\nYou should quit %s using the Quit "
                         "button below, unzip the downloaded file, test the "
                         "new program file to make sure it is OK, then rename "
                         "it %s.py and move it to the proper location to "
                         "replace the old version.\n\nTAKE NOTE OF WHERE THE "
                         "FILE HAS BEEN DOWNLOADED TO!" %
                         (SetupsDir + GetFile, PROG_NAME, PROG_NAMELC))
    if Answer == "quit":
        return "quit"
    return ""
# END: checkForUpdates


##############################################
# BEGIN: checkLatiLong(Which, InValue, Format)
# LIB:checkLatiLong():2016.007
#   Accepts N00.000, 00.000N, or 00.000 (assumed N or E) values and returns
#   the requested Format.
def checkLatiLong(Which, InValue, Format):
    Value = InValue.upper().strip()
    if Which == "lati":
        try:
            if Value.startswith("N") or Value.startswith("+"):
                Lat = float(Value[1:])
                Sign = "N"
            elif Value.startswith("S") or Value.startswith("-"):
                Lat = abs(float(Value[1:]))
                Sign = "S"
            elif Value.endswith("N"):
                Lat = floatt(Value)
                Sign = "N"
            elif Value.endswith("S"):
                Lat = floatt(Value)
                Sign = "S"
            else:
                #                raise ValueError
                Lat = floatt(Value)
                Sign = "N"
                if Lat < 0.0:
                    Sign = "S"
            if Lat > 90.0:
                return (1, "RW", "Latitude > 90.0.", 2, "")
            if Format == 0:
                if Sign == "S":
                    Lat = -Lat
                return (0, Lat)
            elif Format == 7:
                return (0, Sign + "%010.7f" % Lat)
        except ValueError:
            return (1, "RW", "Bad latitude: '%s'" % Value, 2, "")
    elif Which == "long":
        try:
            if Value.startswith("E") or Value.startswith("+"):
                Long = float(Value[1:])
                Sign = "E"
            elif Value.startswith("W") or Value.startswith("-"):
                Long = abs(float(Value[1:]))
                Sign = "W"
            elif Value.endswith("E"):
                Long = floatt(Value)
                Sign = "E"
            elif Value.endswith("W"):
                Long = floatt(Value)
                Sign = "W"
            else:
                #                raise ValueError
                Long = floatt(Value)
                Sign = "E"
                if Long < 0.0:
                    Sign = "W"
            if Long > 180.0:
                return (1, "RW", "Longitude > 180.0.", 2, "")
            if Format == 0:
                if Sign == "W":
                    Long = -Long
                return (0, Long)
            elif Format == 7:
                return (0, Sign + "%011.7f" % Long)
        except ValueError:
            return (1, "RW", "Bad longitude: '%s'" % Value, 2, "")
# END: checkLatiLong


##########################################################################
# BEGIN: filterSOH(Filespec, StopBut, RunningVar, Strip = False, Bees = 0)
# FUNC:filterSOH():2018.346
#   This is geared to read a "text" file and get rid of rt2ms() entered lines
#   and try to recover lines that follow mseed headers when reading LOG files
#   requested from the DMC. It calls readFileLinesRB() then skips lines that
#   start with 6-digit, or more, numbers, and gets rid of lines that have ^@
#   in them from the beginning to the last ^@ which should be the start of a
#   regular SOH message.
def filterSOH(Fp, StopBut, RunningVar, Strip=False, N=0):
    Ret = readFileLinesRB(Fp, Strip, N)
    if Ret[0] != 0:
        return Ret
    RawLines = Ret[1]
    Lines = []
    LineCount = 0
    for Line in RawLines:
        LineCount += 1
        if LineCount % 10000 == 0:
            StopBut.update()
            if RunningVar.get() == 0:
                # Return what we have.
                return (0, Lines)
        if Line.find(chr(0)) != -1:
            Index = Line.rindex(chr(0)) + 1
            Line = Line[Index:]
# Leftovers of an rt2ms comment (7 numbers) or mseed header (6 numbers). This
# should cover both since all of the lines to keep will start with words or
# DDD:HH or be blank.
        try:
            int(Line.lstrip()[:6])
            continue
        except ValueError:
            pass
        Lines.append(Line)
    return (0, Lines)
# END: filterSOH


#######################
# BEGIN: findZipSep(Zp)
# LIB:findZipSep():2018.345
#   This is tricky. The file names from the .zip are strings and they may be
#   the "wrong" separator for this system. Go through the files and set
#   ZipSep as best we can with the slash to use. Windows is supposed to
#   handle either one, but I can't find a reference for it working
#   specifically with zip files, and I have reason to believe it doesn't,
#   otherwise I would not have written this.
#   It may have only been a Python 3 problem.
def findZipSep(Zp):
    Sep = ""
    for File in Zp.namelist():
        if File.find("/") != -1:
            Sep = "/"
        if File.find("\\") != -1:
            Sep = "\\"
        if Sep != "":
            break
    return Sep
# END: findZipSep


#################################
# BEGIN: formClose(Who, e = None)
# LIB:formClose():2018.236
#   Handles closing a form. Who can be a PROGFrm[] item or a Tcl pointer.
def formClose(Who, e=None):
    # In case it is a "busy" form that takes a long time to close.
    updateMe(0)
    if isinstance(Who, astring):
        # The form may not exist or PROGFrm[Who] may be pointing to a "lost"
        # form, or who knows what, so try.
        try:
            if PROGFrm[Who] is None:
                return
            PROGFrm[Who].destroy()
        except Exception:
            pass
    else:
        Who.destroy()
    try:
        PROGFrm[Who] = None
    except Exception:
        pass
    return
###############################
# BEGIN: formCloseAll(e = None)
# FUNC:formCloseAll():2018.231
#   Goes through all of the forms and shuts them down.


def formCloseAll(e=None):
    for Frmm in list(PROGFrm.keys()):
        # Try to call a formXControl() function. Some may have them and some
        # may not.
        try:
            # Expecting from formXControl() functions:
            # Ret[0] 0 == Continue.
            # Ret[0] 1 == Problem solved, can continue.
            # Ret[0] 2 == Problem has or has not been resolved, should stop.
            # What actually gets done is handled by the caller this just passes
            # back anything that is not 0.
            Ret = eval("form%sControl" % Frmm)("close")
            if Ret[0] != 0:
                return Ret
        except Exception:
            formClose(Frmm)
    return (0, )
# END: formClose


######################
# BEGIN: class Command
# LIB:Command():2006.114
#   Pass arguments to functions from button presses and menu selections! Nice!
#   In your declaration:  ...command = Command(func, args,...)
#   Also use in bind() statements
#       x.bind("<****>", Command(func, args...))
class Command:
    def __init__(self, func, *args, **kw):
        self.func = func
        self.args = args
        self.kw = kw

    def __call__(self, *args, **kw):
        args = self.args + args
        kw.update(self.kw)
        self.func(*args, **kw)
# END: Command


####################################
# BEGIN: diskSizeFormat(Which, Size)
# LIB:diskSizeFormat():2013.036
#   Which = b or e. See below.
def diskSizeFormat(Which, Size):
    # The user must do whatever it takes to pass bytes. Which determines what
    # will be returned: b=millions/1024K or e=mega/1000K bytes.
    # Binary 2^20, 1024K.
    if Which == "b":
        if Size >= 1099511627776:
            return "%.2fTiB" % (Size / 1099511627776.0)
        elif Size >= 1073741824:
            return "%.2fGiB" % (Size / 1073741824.0)
        elif Size >= 1048576:
            return "%.2fMiB" % (Size / 1048576.0)
        elif Size >= 1024:
            return "%.2fKiB" % (Size / 1024.0)
        else:
            return "%dB" % Size
    # Engineering 10^6, 1000K.
    elif Which == "e":
        if Size >= 1000000000000:
            return "%.2fTB" % (Size / 1000000000000.0)
        elif Size >= 1000000000:
            return "%.2fGB" % (Size / 1000000000.0)
        elif Size >= 1000000:
            return "%.2fMB" % (Size / 1000000.0)
        elif Size >= 1000:
            return "%.2fKB" % (Size / 1000.0)
        else:
            return "%dB" % Size
    return "Error"
# END: diskSizeFormat


###############################################################
# BEGIN: dt2Time(InFormat, OutFormat, DateTime, Verify = False)
# LIB:dt2Time():2018.270
#   InFormat = -1 = An Epoch has been passed.
#               0 = Figure out what was passed.
#           other = Use if the caller knows exactly what they have.
#   OutFormat = -1 = Epoch
#                0 = Y M D D H M S.s
#                1 = Uses OPTDateFormatRVar value.
#            other = whatever supported format the caller wants
#   The format of the time will always be HH:MM:SS.sss.
#   Returns (0/1, <answer or error msg>) if Verify is True, or <answer/0/"">
#   if Verify is False. Confusing, but most of the calls are with Verify set
#   to False, and having to always put [1] at the end of each call was getting
#   old fast. This probably will cause havoc if there are other errors like
#   passing date/times that cannot be deciphered, or passing bad In/OutFormat
#   codes (just "" will be returned), but that's the price of progress. You'll
#   still have to chop off the milliseconds if they are not wanted ([:-4]).
#   Needs option_add(), intt(), floatt(), rtnPattern()
def dt2Time(InFormat, OutFormat, DateTime, Verify=False):
    global Y2EPOCH
    if InFormat == -1:
        YYYY = 1970
        while True:
            if YYYY % 4 != 0:
                if DateTime >= 31536000:
                    DateTime -= 31536000
                else:
                    break
            elif YYYY % 100 != 0 or YYYY % 400 == 0:
                if DateTime >= 31622400:
                    DateTime -= 31622400
                else:
                    break
            else:
                if DateTime >= 31536000:
                    DateTime -= 31536000
                else:
                    break
            YYYY += 1
        DOY = 1
        while DateTime >= 86400:
            DateTime -= 86400
            DOY += 1
        HH = 0
        while DateTime >= 3600:
            DateTime -= 3600
            HH += 1
        MM = 0
        while DateTime >= 60:
            DateTime -= 60
            MM += 1
        SS = DateTime
        MMM, DD = dt2Timeydoy2md(YYYY, DOY)
    else:
        DateTime = DateTime.strip().upper()
        # The caller will have to decide if these returns are OK or not.
        if len(DateTime) == 0:
            if OutFormat - 1:
                if Verify is False:
                    return 0.0
                else:
                    return (0, 0.0)
            elif OutFormat == 0:
                if Verify is False:
                    return 0, 0, 0, 0, 0, 0, 0.0
                else:
                    return (0, 0, 0, 0, 0, 0, 0, 0.0)
            elif InFormat == 5:
                if Verify is False:
                    return "00:00:00:00"
                else:
                    return (0, "00:00:00:00")
            else:
                if Verify is False:
                    return ""
                else:
                    return (0, "")
# The overall goal of the decode will be to get the passed string time into
# YYYY, MMM, DD, DOY, HH, MM integers and SS.sss float values then proceed to
# the encoding section ready for anything.
# These will try and figure out what the date is between the usual formats.
# Checks first to see if the date and time are together (like with a :) or if
# there is a space between them.
        if InFormat == 0:
            Parts = DateTime.split()
            if len(Parts) == 1:
                # YYYY:DOY:... - 71:2 will pass.
                if DateTime.find(":") != -1:
                    Parts = DateTime.split(":")
# There has to be something that looks like YYYY:DOY.
                    if len(Parts) >= 2:
                        InFormat = 11
# YYYY-MM-DD:HH:...
# If there was only one thing and there are dashes then it could be Y-M-D or
# Y-M-D:H:M:S. Either way there must be 3 parts.
                elif DateTime.find("-") != -1:
                    Parts = DateTime.split("-")
                    if len(Parts) == 3:
                        InFormat = 21
# YYYYMMMDD:HH:... - 68APR3 will pass.
                elif (DateTime.find("A") != -1 or DateTime.find("E") != -1 or
                        DateTime.find("O") != -1 or DateTime.find("U") != -1):
                    InFormat = 31
# YYYYDOYHHMMSS - Date/time must be exactly like this.
                elif len(DateTime) == 13:
                    if rtnPattern(DateTime) == "0000000000000":
                        InFormat = 41
# YYYYMMDDHHMMSS - Date/time must be exactly like this.
                elif len(DateTime) == 14:
                    if rtnPattern(DateTime) == "00000000000000":
                        InFormat = 51
# (There is no 1974JAN23235959 that I know of, but the elif for it would be
# here. ->
# There were two parts.
            else:
                Date = Parts[0]
                Time = Parts[1]
# YYYY:DOY HH:MM...
                if Date.find(":") != -1:
                    # Must have at least YYYY:DOY.
                    Parts = Date.split(":")
                    if len(Parts) >= 2:
                        InFormat = 12
# May be YYYY-MM-DD HH:MM...
                elif Date.find("-") != -1:
                    Parts = Date.split("-")
                    if len(Parts) == 3:
                        InFormat = 22
# YYYYMMMDD - 68APR3 will pass.
                elif (Date.find("A") != -1 or Date.find("E") != -1 or
                      Date.find("O") != -1 or Date.find("U") != -1):
                    InFormat = 32
# If it is still 0 then something is wrong.
            if InFormat == 0:
                if Verify is False:
                    return ""
                else:
                    return (1, "RW", "Bad date/time(%d): '%s'" %
                            (InFormat, DateTime), 2, "")
# These can be fed from the Format 0 stuff above, or called directly if the
# caller knows what DateTime is.
        if InFormat < 20:
            # YYYY:DOY:HH:MM:SS.sss
            # Sometimes this comes as  YYYY:DOY:HH:MM:SS:sss. We'll look for
            # that here.
            # (It's a Reftek thing.)
            if InFormat == 11:
                DT = DateTime.split(":")
                DT += (5 - len(DT)) * ["0"]
                if len(DT) == 6:
                    DT[4] = "%06.3f" % (intt(DT[4]) + intt(DT[5]) / 1000.0)
                    DT = DT[:-1]
# YYYY:DOY HH:MM:SS.sss
            elif InFormat == 12:
                Parts = DateTime.split()
                Date = Parts[0].split(":")
                Date += (2 - len(Date)) * ["0"]
                Time = Parts[1].split(":")
                Time += (3 - len(Time)) * ["0"]
                DT = Date + Time
            else:
                if Verify is False:
                    return ""
                else:
                    return (1, "MWX", "dt2Time: Unknown InFormat code (%d)." %
                            InFormat, 3, "")
# Two-digit years shouldn't happen a lot, so this is kinda inefficient.
            if DT[0] < "100":
                YYYY = intt(Date[0])
                if YYYY < 70:
                    YYYY += 2000
                else:
                    YYYY += 1900
                DT[0] = str(YYYY)
# After we have all of the parts then do the check if the caller wants.
            if Verify is True:
                Ret = dt2TimeVerify("ydhms", DT)
                if Ret[0] != 0:
                    return Ret
# I'm using intt() and floatt() throughout just because it's safer than the
# built-in functions.
# This trick makes it so the Epoch for a year only has to be calculated once
# during a program's run.
            YYYY = intt(DT[0])
            DOY = intt(DT[1])
            MMM, DD = dt2Timeydoy2md(YYYY, DOY)
            HH = intt(DT[2])
            MM = intt(DT[3])
            SS = floatt(DT[4])
        elif InFormat < 30:
            # YYYY-MM-DD:HH:MM:SS.sss
            if InFormat == 21:
                Parts = DateTime.split(":", 1)
                Date = Parts[0].split("-")
                Date += (3 - len(Date)) * ["0"]
# Just the date must have been supplied.
                if len(Parts) == 1:
                    Time = ["0", "0", "0"]
                else:
                    Time = Parts[1].split(":")
                    Time += (3 - len(Time)) * ["0"]
                DT = Date + Time
# YYYY-MM-DD HH:MM:SS.sss
            elif InFormat == 22:
                Parts = DateTime.split()
                Date = Parts[0].split("-")
                Date += (3 - len(Date)) * ["0"]
                Time = Parts[1].split(":")
                Time += (3 - len(Time)) * ["0"]
                DT = Date + Time
# If parts of 23 are missing we will fill them in with Jan, 1st, or 00:00:00.
# If parts of 24 are missing we will format to the missing item then stop and
# return what we have.
            elif InFormat == 23 or InFormat == 24:
                # The /DOY may or may not be there.
                if DateTime.find("/") == -1:
                    Parts = DateTime.split()
                    Date = Parts[0].split("-")
                    if InFormat == 23:
                        Date += (3 - len(Date)) * ["1"]
                    else:
                        # The -1's will stand in from the missing items.
                        # OutFormat=24 will figure it out from there.
                        Date += (3 - len(Date)) * ["-1"]
                    if len(Parts) == 2:
                        Time = Parts[1].split(":")
                        if InFormat == 23:
                            Time += (3 - len(Time)) * ["0"]
                        else:
                            Time += (3 - len(Time)) * ["-1"]
                    else:
                        if InFormat == 23:
                            Time = ["0", "0", "0"]
                        else:
                            Time = ["-1", "-1", "-1"]
# Has a /. We'll only use the YYYY-MM-DD part and assume nothing about the DOY
# part.
                else:
                    Parts = DateTime.split()
                    Date = Parts[0].split("-")
                    if InFormat == 23:
                        Date += (3 - len(Date)) * ["0"]
                    elif InFormat == 24:
                        Date += (3 - len(Date)) * ["-1"]
                    Date[2] = Date[2].split("/")[0]
                    if len(Parts) == 2:
                        Time = Parts[1].split(":")
                        if InFormat == 23:
                            Time += (3 - len(Time)) * ["0"]
                        else:
                            Time += (3 - len(Time)) * ["-1"]
                    else:
                        if InFormat == 23:
                            Time = ["0", "0", "0"]
                        else:
                            Time = ["1-", "-1", "-1"]
                DT = Date + Time
            else:
                if Verify is False:
                    return ""
                else:
                    return (1, "MWX", "dt2Time: Unknown InFormat code (%d)." %
                            InFormat, 3, "")
            if DT[0] < "100":
                YYYY = intt(DT[0])
                if YYYY < 70:
                    YYYY += 2000
                else:
                    YYYY += 1900
                DT[0] = str(YYYY)
            if Verify is True:
                if InFormat != 24:
                    Ret = dt2TimeVerify("ymdhms", DT)
                else:
                    Ret = dt2TimeVerify("xymdhms", DT)
                if Ret[0] != 0:
                    return Ret
            YYYY = intt(DT[0])
            MMM = intt(DT[1])
            DD = intt(DT[2])
# This will get done in OutFormat=24.
            if InFormat != 24:
                DOY = dt2Timeymd2doy(YYYY, MMM, DD)
            else:
                DOY = -1
            HH = intt(DT[3])
            MM = intt(DT[4])
            SS = floatt(DT[5])
        elif InFormat < 40:
            # YYYYMMMDD:HH:MM:SS.sss
            if InFormat == 31:
                Parts = DateTime.split(":", 1)
                Date = Parts[0]
                if len(Parts) == 1:
                    Time = ["0", "0", "0"]
                else:
                    Time = Parts[1].split(":")
                    Time += (3 - len(Time)) * ["0"]
# YYYYMMMDD HH:MM:SS.sss
            elif InFormat == 32:
                Parts = DateTime.split()
                Date = Parts[0]
                Time = Parts[1].split(":")
                Time += (3 - len(Time)) * ["0"]
            else:
                if Verify is False:
                    return ""
                else:
                    return (1, "MWX", "dt2Time: Unknown InFormat code (%d)." %
                            InFormat, 3, "")
# Date is still "YYYYMMMDD", so just make place holders.
            DT = ["0", "0", "0"] + Time
            YYYY = intt(Date)
            if YYYY < 100:
                if YYYY < 70:
                    YYYY += 2000
                else:
                    YYYY += 1900
            MMM = 0
            DD = 0
            M = 1
            for Month in PROG_CALMONS[1:]:
                try:
                    i = Date.index(Month)
                    MMM = M
                    DD = intt(Date[i + 3:])
                    break
                except Exception:
                    pass
                M += 1
            if Verify is True:
                # DT values need to be strings for the dt2TimeVerify() intt()
                # call. It's assumed the values would come from split()'ing
                # something, so they would normally be strings to begin with.
                DT[0] = str(YYYY)
                DT[1] = str(MMM)
                DT[2] = str(DD)
                Ret = dt2TimeVerify("ymdhms", DT)
                if Ret[0] != 0:
                    return Ret
            DOY = dt2Timeymd2doy(YYYY, MMM, DD)
            HH = intt(Time[0])
            MM = intt(Time[1])
            SS = intt(Time[2])
        elif InFormat < 50:
            # YYYYDOYHHMMSS
            if InFormat == 41:
                if DateTime.isdigit() is False:
                    if Verify is False:
                        return ""
                    else:
                        return (1, "RW", "Non-digits in value.", 2)
                YYYY = intt(DateTime[:4])
                DOY = intt(DateTime[4:7])
                HH = intt(DateTime[7:9])
                MM = intt(DateTime[9:11])
                SS = floatt(DateTime[11:])
                if Verify is True:
                    DT = []
                    DT.append(str(YYYY))
                    DT.append(str(DOY))
                    DT.append(str(HH))
                    DT.append(str(MM))
                    DT.append(str(SS))
                    Ret = dt2TimeVerify("ydhms", DT)
                    if Ret[0] != 0:
                        return Ret
                MMM, DD = dt2Timeydoy2md(YYYY, DOY)
            else:
                if Verify is False:
                    return ""
                else:
                    return (1, "MWX", "dt2Time: Unknown InFormat code (%d)." %
                            InFormat, 3, "")
        elif InFormat < 60:
            # YYYYMMDDHHMMSS
            if InFormat == 51:
                if DateTime.isdigit() is False:
                    if Verify is False:
                        return ""
                    else:
                        return (1, "RW", "Non-digits in value.", 2)
                YYYY = intt(DateTime[:4])
                MMM = intt(DateTime[4:6])
                DD = intt(DateTime[6:8])
                HH = intt(DateTime[8:10])
                MM = intt(DateTime[10:12])
                SS = floatt(DateTime[12:])
                if Verify is True:
                    DT = []
                    DT.append(str(YYYY))
                    DT.append(str(MMM))
                    DT.append(str(DD))
                    DT.append(str(HH))
                    DT.append(str(MM))
                    DT.append(str(SS))
                    Ret = dt2TimeVerify("ymdhms", DT)
                    if Ret[0] != 0:
                        return Ret
                DOY = dt2Timeymd2doy(YYYY, MMM, DD)
            else:
                if Verify is False:
                    return ""
                else:
                    return (1, "MWX", "dt2Time: Unknown InFormat code (%d)." %
                            InFormat, 3, "")
# If the caller just wants to work with the seconds we'll split it up and
# return the number of seconds into the day. In this case the OutFormat value
# will not be used.
        elif InFormat == 100:
            Parts = DateTime.split(":")
            Parts += (3 - len(Parts)) * ["0"]
            if Verify is True:
                Ret = dt2TimeVerify("hms", Parts)
                if Ret[0] != 0:
                    return Ret
            if Verify is False:
                return (intt(Parts[0]) * 3600) + (intt(Parts[1]) * 60) + \
                    float(Parts[2])
            else:
                return (0, (intt(Parts[0]) * 3600) + (intt(Parts[1]) * 60) +
                        float(Parts[2]))
# Now that we have all of the parts do what the caller wants and return the
# result.
# Return the Epoch.
    if OutFormat == -1:
        try:
            Epoch = Y2EPOCH[YYYY]
        except KeyError:
            Epoch = 0.0
            for YYY in arange(1970, YYYY):
                if YYY % 4 != 0:
                    Epoch += 31536000.0
                elif YYY % 100 != 0 or YYY % 400 == 0:
                    Epoch += 31622400.0
                else:
                    Epoch += 31536000.0
            Y2EPOCH[YYYY] = Epoch
        Epoch += ((DOY - 1) * 86400.0) + (HH * 3600.0) + (MM * 60.0) + SS
        if Verify is False:
            return Epoch
        else:
            return (0, Epoch)
    elif OutFormat == 0:
        if Verify is False:
            return YYYY, MMM, DD, DOY, HH, MM, SS
        else:
            return (0, YYYY, MMM, DD, DOY, HH, MM, SS)
    elif OutFormat == 1:
        Format = OPTDateFormatRVar.get()
        if Format == "YYYY:DOY":
            OutFormat = 11
        elif Format == "YYYY-MM-DD":
            OutFormat = 22
        elif Format == "YYYYMMMDD":
            OutFormat = 32
# Usually used for troubleshooting.
        elif len(Format) == 0:
            try:
                Epoch = Y2EPOCH[YYYY]
            except KeyError:
                for YYY in arange(1970, YYYY):
                    if YYY % 4 != 0:
                        Epoch += 31536000.0
                    elif YYY % 100 != 0 or YYY % 400 == 0:
                        Epoch += 31622400.0
                    else:
                        Epoch += 31536000.0
                Y2EPOCH[YYYY] = Epoch
            Epoch += ((DOY - 1) * 86400.0) + (HH * 3600.0) + (MM * 60.0) + SS
            if Verify is False:
                return Epoch
            else:
                return (0, Epoch)
# This is the easiest way I can think of to keep an SS of 59.9999 from being
# rounded and formatted to 60.000. Some stuff at some point may slip into the
# microsecond realm. Then this whole library of functions may have to be
# changed.
    if SS % 1 > .999:
        SS = int(SS) + .999
# These OutFormat values are the same as InFormat values, plus others.
# YYYY:DOY:HH:MM:SS.sss
    if OutFormat == 11:
        if Verify is False:
            return "%d:%03d:%02d:%02d:%06.3f" % (YYYY, DOY, HH, MM, SS)
        else:
            return (0, "%d:%03d:%02d:%02d:%06.3f" % (YYYY, DOY, HH, MM, SS))
# YYYY:DOY HH:MM:SS.sss
    elif OutFormat == 12:
        if Verify is False:
            return "%d:%03d %02d:%02d:%06.3f" % (YYYY, DOY, HH, MM, SS)
        else:
            return (0, "%d:%03d %02d:%02d:%06.3f" % (YYYY, DOY, HH, MM, SS))
# YYYY:DOY - just because it's popular (for LOGPEEK) and it saves having the
# caller always doing  .split()[0]
    elif OutFormat == 13:
        if Verify is False:
            return "%d:%03d" % (YYYY, DOY)
        else:
            return (0, "%d:%03d" % (YYYY, DOY))
# YYYY:DOY:HH:MM:SS - just because it's really popular in POCUS and other
# programs.
    elif OutFormat == 14:
        if Verify is False:
            return "%d:%03d:%02d:%02d:%02d" % (YYYY, DOY, HH, MM, int(SS))
        else:
            return (0, "%d:%03d:%02d:%02d:%02d" % (YYYY, DOY, HH, MM, int(SS)))
# YYYY-MM-DD:HH:MM:SS.sss
    elif OutFormat == 21:
        if Verify is False:
            return "%d-%02d-%02d:%02d:%02d:%06.3f" % (YYYY, MMM, DD, HH, MM,
                                                      SS)
        else:
            return (0, "%d-%02d-%02d:%02d:%02d:%06.3f" % (YYYY, MMM, DD, HH,
                                                          MM, SS))
# YYYY-MM-DD HH:MM:SS.sss
    elif OutFormat == 22:
        if Verify is False:
            return "%d-%02d-%02d %02d:%02d:%06.3f" % (YYYY, MMM, DD, HH, MM,
                                                      SS)
        else:
            return (0, "%d-%02d-%02d %02d:%02d:%06.3f" % (YYYY, MMM, DD, HH,
                                                          MM, SS))
# YYYY-MM-DD/DOY HH:MM:SS.sss
    elif OutFormat == 23:
        if Verify is False:
            return "%d-%02d-%02d/%03d %02d:%02d:%06.3f" % (YYYY, MMM, DD, DOY,
                                                           HH, MM, SS)
        else:
            return (0, "%d-%02d-%02d/%03d %02d:%02d:%06.3f" % (YYYY, MMM, DD,
                                                               DOY, HH, MM,
                                                               SS))
# Some portion of YYYY-MM-DD HH:MM:SS. Returns integer seconds.
# In that this is a human-entered thing (programs don't store partial
# date/times) we'll return whatever was entered without the /DOY, since the
# next step would be to do something like look it up in a database.
    elif OutFormat == 24:
        DateTime = "%d" % YYYY
        if MMM != -1:
            DateTime += "-%02d" % MMM
        else:
            # Return what we have if this item was not provided, same on down.
            if Verify is False:
                return DateTime
            else:
                return (0, DateTime)
        if DD != -1:
            DateTime += "-%02d" % DD
        else:
            if Verify is False:
                return DateTime
            else:
                return (0, DateTime)
        if HH != -1:
            DateTime += " %02d" % HH
        else:
            if Verify is False:
                return DateTime
            else:
                return (0, DateTime)
        if MM != "-1":
            DateTime += ":%02d" % MM
        else:
            if Verify is False:
                return DateTime
            else:
                return (0, DateTime)
# Returns integer second since the caller has no idea what is coming back.
        if SS != "-1":
            DateTime += ":%02d" % SS
        if Verify is False:
            return DateTime
        else:
            return (0, DateTime)
# YYYY-MM-DD
    elif OutFormat == 25:
        if Verify is False:
            return "%d-%02d-%02d" % (YYYY, MMM, DD)
        else:
            return (0, "%d-%02d-%02d" % (YYYY, MMM, DD))
# YYYYMMMDD:HH:MM:SS.sss
    elif OutFormat == 31:
        if Verify is False:
            return "%d%s%02d:%02d:%02d:%06.3f" % (YYYY, PROG_CALMONS[MMM], DD,
                                                  HH, MM, SS)
        else:
            return (0, "%d%s%02d:%02d:%02d:%06.3f" % (YYYY, PROG_CALMONS[MMM],
                                                      DD, HH, MM, SS))
# YYYYMMMDD HH:MM:SS.sss
    elif OutFormat == 32:
        if Verify is False:
            return "%d%s%02d %02d:%02d:%06.3f" % (YYYY, PROG_CALMONS[MMM], DD,
                                                  HH, MM, SS)
        else:
            return (0, "%d%s%02d %02d:%02d:%06.3f" % (YYYY, PROG_CALMONS[MMM],
                                                      DD, HH, MM, SS))
# YYYYDOYHHMMSS.sss
    elif OutFormat == 41:
        if Verify is False:
            return "%d%03d%02d%02d%06.3f" % (YYYY, DOY, HH, MM, SS)
        else:
            return (0, "%d%03d%02d%02d%06.3f" % (YYYY, DOY, HH, MM, SS))
# YYYYMMDDHHMMSS.sss
    elif OutFormat == 51:
        if Verify is False:
            return "%d%02d%02d%02d%02d%06.3f" % (YYYY, MMM, DD, HH, MM, SS)
        else:
            return (0, "%d%02d%02d%02d%02d%06.3f" % (YYYY, MMM, DD, HH, MM,
                                                     SS))
# Returns what ever OPTDateFormatRVar is set to.
# 80 is dt, 81 is d and 82 is t.
    elif OutFormat == 80 or OutFormat == 81 or OutFormat == 82:
        if OPTDateFormatRVar.get() == "YYYY:DOY":
            if OutFormat == 80:
                if Verify is False:
                    return "%d:%03d:%02d:%02d:%06.3f" % (YYYY, DOY, HH, MM, SS)
                else:
                    return (0, "%d:%03d:%02d:%02d:%06.3f" % (YYYY, DOY, HH, MM,
                                                             SS))
            elif OutFormat == 81:
                if Verify is False:
                    return "%d:%03d" % (YYYY, DOY)
                else:
                    return (0, "%d:%03d" % (YYYY, DOY))
            elif OutFormat == 82:
                if Verify is False:
                    return "%02d:%02d:%06.3f" % (HH, MM, SS)
                else:
                    return (0, "%02d:%02d:%06.3f" % (HH, MM, SS))
        elif OPTDateFormatRVar.get() == "YYYY-MM-DD":
            if OutFormat == 80:
                if Verify is False:
                    return "%d-%02d-%02d %02d:%02d:%06.3f" % (YYYY, MMM, DD,
                                                              HH, MM, SS)
                else:
                    return (0, "%d-%02d-%02d %02d:%02d:%06.3f" % (YYYY, MMM,
                                                                  DD, HH, MM,
                                                                  SS))
            elif OutFormat == 81:
                if Verify is False:
                    return "%d-%02d-%02d" % (YYYY, MMM, DD)
                else:
                    return (0, "%d-%02d-%02d" % (YYYY, MMM, DD))
            elif OutFormat == 82:
                if Verify is False:
                    return "%02d:%02d:%06.3f" % (HH, MM, SS)
                else:
                    return (0, "%02d:%02d:%06.3f" % (HH, MM, SS))
        elif OPTDateFormatRVar.get() == "YYYYMMMDD":
            if OutFormat == 80:
                if Verify is False:
                    return "%d%s%02d %02d:%02d:%06.3f" % (YYYY,
                                                          PROG_CALMONS[MMM],
                                                          DD, HH, MM, SS)
                else:
                    return (0, "%d%s%02d %02d:%02d:%06.3f" %
                            (YYYY, PROG_CALMONS[MMM], DD, HH, MM, SS))
            elif OutFormat == 81:
                if Verify is False:
                    return "%d%s%02d" % (YYYY, PROG_CALMONS[MMM], DD)
                else:
                    return (0, "%d%s%02d" % (YYYY, PROG_CALMONS[MMM], DD))
            elif OutFormat == 82:
                if Verify is False:
                    return "%02d:%02d:%06.3f" % (HH, MM, SS)
                else:
                    return (0, "%02d:%02d:%06.3f" % (HH, MM, SS))
        elif len(OPTDateFormatRVar.get()) == 0:
            if Verify is False:
                return str(DateTime)
            else:
                return (0, str(DateTime))
    else:
        if Verify is False:
            return ""
        else:
            return (1, "MWX", "dt2Time: Unknown OutFormat code (%d)." %
                    OutFormat, 3, "")
################################
# BEGIN: dt2Timedhms2Secs(InStr)
# FUNC:dt2Timedhms2Secs():2018.235
#   Returns the number of seconds in strings like 1h30m.


def dt2Timedhms2Secs(InStr):
    InStr = InStr.replace(" ", "").lower()
    if len(InStr) == 0:
        return 0
    Chars = list(InStr)
    Value = 0
    SubValue = ""
    for Char in Chars:
        if Char.isdigit():
            SubValue += Char
        elif Char == "s":
            Value += intt(SubValue)
            SubValue = ""
        elif Char == "m":
            Value += intt(SubValue) * 60
            SubValue = ""
        elif Char == "h":
            Value += intt(SubValue) * 3600
            SubValue = ""
        elif Char == "d":
            Value += intt(SubValue) * 86400
            SubValue = ""
# Must have just been passed a number with no s m h or d or 1h30 which will be
# treated as 1 hour, 30 seconds.
    if len(SubValue) != 0:
        Value += intt(SubValue)
    return Value
#############################################
# BEGIN: dt2TimeDT(Format, DateTime, Fix = 2)
# FUNC:dt2TimeDT():2018.270
#   This is for the one-off time conversion items that have been needed here
#   and there for specific items. Input is some string of date and/or time.
#   Some of these can be done with dt2Time(), but these are more for when the
#   code knows exactly what it has and exactly what it needs. It's program
#   dependent and maybe useful to others.
#     Format = 1 = [dd]hhmmss or even blank to [DD:]HH:MM:SS
#                  Replaces dhms2DHMS().
#              2 = YYYY:DOY:HH:MM:SS to ISO8601 time YYYY-MM-DDTHH:MM:SSZ.
#                  No time zone conversion is done (or any error checking) so
#                  you need to make sure the passed time is really Zulu or this
#                  will return a lie.
#                  Replaces YDHMS28601().
#              3 = The opposite of above.
#                  Replaces iso86012YDHMS().
#              4 = Adds /DOY to a passed date/time that can be just the date,
#                  but that must be a YYYY-MM-DD format. Date and time must be
#                  separated by a space. It's the only date format that I've
#                  ever wanted to add the DOY to. Returns the fixed up
#                  date/time. Does NO error checking, and, as you can see, if
#                  the date is not the right format it will choke causing an
#                  embarrassing crash. You have been warned.
#                  Replaces addDDD().
#              5 = Anti-InFormat 4. Is OK if /DOY is not there.
#                  Replaces remDDD().
#              6 = yyydoyhhmmss... to YYYY:DOY:HH:MM...
#                  Replaces ydhmst2YDHMST().
#              7 = YYYY:DOY:HH:MM:SS -> YYYY-MM-DD:HH:MM:SS.
#                  Replaces ydhms2ymdhmsDash().
#              8 = YYYYMMDDHHMMSS to YYYY-MM-DD HH:MM:SS
#              9 = YYYYMMDDHHMMSS to YYYY-MM-DD
#             10 = HH:MM:SS to int H, M, S
#             11 = Seconds to HH:MM:SS (Use Fix to pad with extra blanks if
#                  large hour numbers are expected)
#             12 = HH:MM:SS/HH:MM/HH/blank to seconds


def dt2TimeDT(Format, DateTime, Fix=2):
    # Everything wants a string, except 11. Python 3 will probably pass
    # DateTime as bytes.
    if Format != 11:
        if isinstance(DateTime, astring) is False:
            DateTime = DateTime.decode("latin-1")
    if Format == 1:
        # Check the length before the .strip(). DateTime may be just blanks,
        # but we'll use the length to determine what to return.
        Len = len(DateTime)
        DateTime = DateTime.strip()
        if len(DateTime) == 0:
            return "00:00:00:00"
        if Len == 6:
            return "%s:%s:%s" % (DateTime[:2], DateTime[2:4], DateTime[4:6])
        else:
            return "%s:%s:%s:%s" % (DateTime[:2], DateTime[2:4], DateTime[4:6],
                                    DateTime[6:8])
    elif Format == 2:
        Parts = DateTime.split(":")
        if len(Parts) != 5:
            return ""
        MM, DD, = dt2Timeydoy2md(intt(Parts[0]), intt(Parts[1]))
        return "%s-%02d-%02dT%s:%s:%sZ" % (Parts[0], MM, DD, Parts[2],
                                           Parts[3], Parts[4])
    elif Format == 3:
        Parts = DateTime.split("T")
        if len(Parts) != 2:
            return ""
# Who knows what may be wrong with the passed time.
        try:
            Parts2 = Parts[0].split("-")
            YYYY = intt(Parts2[0])
            MMM = intt(Parts2[1])
            DD = intt(Parts2[2])
            DOY = dt2Timeymd2doy(YYYY, MMM, DD)
            Parts2 = Parts[1].split(":")
            HH = intt(Parts2[0])
            MM = intt(Parts2[1])
            SS = intt(Parts2[2])
        except Exception:
            return ""
        return "%d:%03d:%02d:%02d:%02d" % (YYYY, DOY, HH, MM, SS)
    elif Format == 4:
        Parts = DateTime.split()
        if len(Parts) == 0:
            return ""
        Parts2 = Parts[0].split("-")
        YYYY = intt(Parts2[0])
        MMM = intt(Parts2[1])
        DD = intt(Parts2[2])
        DOY = dt2Timeymd2doy(YYYY, MMM, DD)
        if len(Parts) == 1:
            return "%s/%03d" % (Parts[0], DOY)
        else:
            return "%s/%03d %s" % (Parts[0], DOY, Parts[1])
    elif Format == 5:
        Parts = DateTime.split()
        if len(Parts) == 0:
            return ""
# A little bit of protection.
        try:
            i = Parts[0].index("/")
            Parts[0] = Parts[0][:i]
        except ValueError:
            pass
        if len(Parts) == 1:
            return Parts[0]
        else:
            return "%s %s" % (Parts[0], Parts[1])
# yyyydoyhhmmssttt or yyyydoyhhmmss.
    elif Format == 6:
        Len = len(DateTime)
        DateTime = DateTime.strip()
# yyyydoyhhmm
        if Len == 12:
            if len(DateTime) == 0:
                return "0000:000:00:00"
            return "%s:%s:%s:%s" % (DateTime[:4], DateTime[4:7], DateTime[7:9],
                                    DateTime[9:11])
# yyyydoyhhmmss  All of the fields that may contain this are 14 bytes long
# with a trailing space which got removed above (it was that way in LOGPEEK
# anyways).
        elif Len == 14:
            if len(DateTime) == 0:
                return "0000:000:00:00:00"
            return "%s:%s:%s:%s:%s" % (DateTime[:4], DateTime[4:7],
                                       DateTime[7:9], DateTime[9:11],
                                       DateTime[11:13])
# yyyydoyhhmmssttt
        elif Len == 16:
            if len(DateTime) == 0:
                return "0000:000:00:00:00:000"
            return "%s:%s:%s:%s:%s:%s" % (DateTime[:4], DateTime[4:7],
                                          DateTime[7:9], DateTime[9:11],
                                          DateTime[11:13], DateTime[13:])
        return ""
    elif Format == 7:
        Parts = DateTime.split(":")
        YYYY = intt(Parts[0])
        DOY = intt(Parts[1])
        MMM, DD = dt2Timeydoy2md(YYYY, DOY)
        return "%d-%02d-%02d:%s:%s:%s" % (YYYY, MMM, DD, Parts[2], Parts[3],
                                          Parts[4])
# YYYYMMDDHHMMSS to YYYY-MM-DD HH:MM:SS
    elif Format == 8:
        if len(DateTime) == 14:
            return "%s-%s-%s %s:%s:%s" % (DateTime[0:4], DateTime[4:6],
                                          DateTime[6:8], DateTime[8:10],
                                          DateTime[10:12], DateTime[12:14])
        return ""
# YYYYMMDDHHMMSS to YYYY-MM-DD
    elif Format == 9:
        if len(DateTime) == 14:
            return "%s-%s-%s" % (DateTime[0:4], DateTime[4:6], DateTime[6:8])
        return ""
# HH:MM:SS to int H, M, S
    elif Format == 10:
        try:
            Parts2 = DateTime.split(":")
            Parts = []
            for Part in Parts2:
                Parts.append(intt(Part))
            Parts += (3 - len(Parts)) * [0]
        except Exception:
            return 0, 0, 0
        return Parts[0], Parts[1], Parts[2]
# Seconds to HH:MM:SS (use Fix to pad with spaces)
    elif Format == 11:
        HH = DateTime // 3600
        Sub = HH * 3600
        MM = (DateTime - Sub) // 60
        Sub += MM * 60
        SS = (DateTime - Sub)
        H = "%02d" % HH
        if Fix > 2:
            H = "%*s" % (Fix, H)
        return "%s:%02d:%02d" % (H, MM, SS)
# HH:MM:SS to seconds
    elif Format == 12:
        try:
            Parts2 = DateTime.split(":")
            Parts = []
            for Part in Parts2:
                Parts.append(intt(Part))
            Parts += (3 - len(Parts)) * [0]
        except Exception:
            return 0
        return Parts[0] * 3600 + Parts[1] * 60 + Parts[2]
######################################################################
# BEGIN: dt2TimeMath(DeltaDD, DeltaSS, YYYY, MMM, DD, DOY, HH, MM, SS)
# LIB:dt2TimeMath():2013.039
#   Adds or subtracts (depending on the sign of DeltaDD/DeltaSS) the requested
#   amount of time to/from the passed date.  Returns a tuple with the results:
#           (YYYY, MMM, DD, DOY, HH, MM, SS)
#   Pass -1 or 0 for MMM/DD, or DOY depending on which is to be used. If MMM/DD
#   are >=0 then MMM, DD, and DOY will be filled in on return. If MMM/DD is -1
#   then just DOY will be used/returned. If MMM/DD are 0 then DOY will be used
#   but MMM and DD will also be returned.
#   SS will be int or float depending on what was passed.


def dt2TimeMath(DeltaDD, DeltaSS, YYYY, MMM, DD, DOY, HH, MM, SS):
    DeltaDD = int(DeltaDD)
    DeltaSS = int(DeltaSS)
    if YYYY < 1000:
        if YYYY < 70:
            YYYY += 2000
        else:
            YYYY += 1900
# Work in DOY.
    if MMM > 0:
        DOY = dt2Timeymd2doy(YYYY, MMM, DD)
    if DeltaDD != 0:
        if DeltaDD > 0:
            Forward = 1
        else:
            Forward = 0
        while True:
            # Speed limit the change to keep things simpler.
            if DeltaDD < -365 or DeltaDD > 365:
                if Forward == 1:
                    DOY += 365
                    DeltaDD -= 365
                else:
                    DOY -= 365
                    DeltaDD += 365
            else:
                DOY += DeltaDD
                DeltaDD = 0
            if YYYY % 4 != 0:
                Leap = 0
            elif YYYY % 100 != 0 or YYYY % 400 == 0:
                Leap = 1
            else:
                Leap = 0
            if DOY < 1 or DOY > 365 + Leap:
                if Forward == 1:
                    DOY -= 365 + Leap
                    YYYY += 1
                else:
                    YYYY -= 1
                    if YYYY % 4 != 0:
                        Leap = 0
                    elif YYYY % 100 != 0 or YYYY % 400 == 0:
                        Leap = 1
                    else:
                        Leap = 0
                    DOY += 365 + Leap
            if DeltaDD == 0:
                break
    if DeltaSS != 0:
        if DeltaSS > 0:
            Forward = 1
        else:
            Forward = 0
        while True:
            # Again, speed limit just to keep the code reasonable.
            if DeltaSS < -59 or DeltaSS > 59:
                if Forward == 1:
                    SS += 59
                    DeltaSS -= 59
                else:
                    SS -= 59
                    DeltaSS += 59
            else:
                SS += DeltaSS
                DeltaSS = 0
            if SS < 0 or SS > 59:
                if Forward == 1:
                    SS -= 60
                    MM += 1
                    if MM > 59:
                        MM = 0
                        HH += 1
                        if HH > 23:
                            HH = 0
                            DOY += 1
                            if DOY > 365:
                                if YYYY % 4 != 0:
                                    Leap = 0
                                elif YYYY % 100 != 0 or YYYY % 400 == 0:
                                    Leap = 1
                                else:
                                    Leap = 0
                                if DOY > 365 + Leap:
                                    YYYY += 1
                                    DOY = 1
                else:
                    SS += 60
                    MM -= 1
                    if MM < 0:
                        MM = 59
                        HH -= 1
                        if HH < 0:
                            HH = 23
                            DOY -= 1
                            if DOY < 1:
                                YYYY -= 1
                                if YYYY % 4 != 0:
                                    DOY = 365
                                elif YYYY % 100 != 0 or YYYY % 400 == 0:
                                    DOY = 366
                                else:
                                    DOY = 365
            if DeltaSS == 0:
                break
    if MMM != -1:
        MMM, DD = dt2Timeydoy2md(YYYY, DOY)
    return (YYYY, MMM, DD, DOY, HH, MM, SS)
####################################
# BEGIN: dt2TimeVerify(Which, Parts)
# FUNC:dt2TimeVerify():2015.048
#   This could figure out what to check just by passing [Y,M,D,D,H,M,S], but
#   that means the splitters() in dt2Time would need to work much harder to
#   make sure all of the Parts were there, thus it needs a Which value.


def dt2TimeVerify(Which, Parts):
    if Which.startswith("ydhms"):
        YYYY = intt(Parts[0])
        MMM = -1
        DD = -1
        DOY = intt(Parts[1])
        HH = intt(Parts[2])
        MM = intt(Parts[3])
        SS = floatt(Parts[4])
    elif Which.startswith("ymdhms") or Which.startswith("xymdhms"):
        YYYY = intt(Parts[0])
        MMM = intt(Parts[1])
        DD = intt(Parts[2])
        DOY = -1
        HH = intt(Parts[3])
        MM = intt(Parts[4])
        SS = floatt(Parts[5])
    elif Which.startswith("hms"):
        YYYY = -1
        MMM = -1
        DD = -1
        DOY = -1
        HH = intt(Parts[0])
        MM = intt(Parts[1])
        SS = floatt(Parts[2])
    if YYYY != -1 and (YYYY < 1000 or YYYY > 9999):
        return (1, "RW", "Bad year value: %d" % YYYY, 2, "")
# Normally do the normal checking.
    if Which.startswith("x") is False:
        if MMM != -1 and (MMM < 1 or MMM > 12):
            return (1, "RW", "Bad month value: %d" % MMM, 2, "")
        if DD != -1:
            if DD < 1 or DD > 31:
                return (1, "RW", "Bad day value: %d" % DD, 2, "")
            if YYYY % 4 != 0:
                if DD > PROG_MAXDPMNLY[MMM]:
                    return (1, "RW", "Too many days for month %d: %d" %
                            (MMM, DD), 2, "")
            elif YYYY % 100 != 0 or YYYY % 400 == 0:
                if DD > PROG_MAXDPMLY[MMM]:
                    return (1, "RW", "Too many days for month %d: %d" %
                            (MMM, DD), 2, "")
            else:
                if DD > PROG_MAXDPMNLY[MMM]:
                    return (1, "RW", "Too many days for month %d: %d" %
                            (MMM, DD), 2, "")
        if DOY != -1:
            if DOY < 1 or DOY > 366:
                return (1, "RW", "Bad day of year value: %d" % DOY, 2, "")
            if YYYY % 4 != 0 and DOY > 365:
                return (1, "RW" "Too many days for non-leap year: %d" % DOY,
                        2, "")
        if HH < 0 or HH >= 24:
            return (1, "RW", "Bad hour value: %d" % HH, 2, "")
        if MM < 0 or MM >= 60:
            return (1, "RW", "Bad minute value: %d" % MM, 2, "")
        if SS < 0 or SS >= 60:
            return (1, "RW", "Bad seconds value: %06.3f" % SS, 2, "")
# "x" checks.
# Here if Which starts with "x" then it is OK if month and day values are
# missing. This is for checking an entry like  2013-7  for example. If the
# portion of the date(/time) that was passed looks OK then (0,) will be
# returned. If it is something like  2013-13  then that will be bad.
    else:
        # If the user entered just the year, then we are done, etc.
        if MMM != -1:
            if MMM < 1 or MMM > 12:
                return (1, "RW", "Bad month value: %d" % MMM, 2, "")
            if DD != -1:
                if DD < 1 or DD > 31:
                    return (1, "RW", "Bad day value: %d" % DD, 2, "")
                if YYYY % 4 != 0:
                    if DD > PROG_MAXDPMNLY[MMM]:
                        return (1, "RW", "Too many days for month %d: %d" %
                                (MMM, DD), 2, "")
                elif YYYY % 100 != 0 or YYYY % 400 == 0:
                    if DD > PROG_MAXDPMLY[MMM]:
                        return (1, "RW", "Too many days for month %d: %d" %
                                (MMM, DD), 2, "")
                else:
                    if DD > PROG_MAXDPMNLY[MMM]:
                        return (1, "RW", "Too many days for month %d: %d" %
                                (MMM, DD), 2, "")
                if DOY != -1:
                    if DOY < 1 or DOY > 366:
                        return (1, "RW", "Bad day of year value: %d" % DOY, 2,
                                "")
                    if YYYY % 4 != 0 and DOY > 365:
                        return (1, "RW"
                                "Too many days for non-leap year: %d" % DOY,
                                2, "")
                if HH != -1:
                    if HH < 0 or HH >= 24:
                        return (1, "RW", "Bad hour value: %d" % HH, 2, "")
                    if MM != -1:
                        if MM < 0 or MM >= 60:
                            return (1, "RW", "Bad minute value: %d" % MM, 2,
                                    "")
# Checking these special values are usually from user input and are date/time
# values and not timing values, so the seconds part here will just be an int.
                        if SS != -1:
                            if SS < 0 or SS >= 60:
                                return (1, "RW", "Bad seconds value: %d" % SS,
                                        2, "")
    return (0,)
##########################################
# BEGIN: dt2TimeydoyMath(Delta, YYYY, DOY)
# LIB:dt2TimeydoyMath():2013.036
#   Adds or subtracts the passed number of days (Delta) to the passed year and
#   day of year values. Returns YYYY and DOY.
#   Replaces ydoyMath().


def dt2TimeydoyMath(Delta, YYYY, DOY):
    if Delta == 0:
        return YYYY, DOY
    if Delta > 0:
        Forward = 1
    elif Delta < 0:
        Forward = 0
    while True:
        # Speed limit the change to keep things simpler.
        if Delta < -365 or Delta > 365:
            if Forward == 1:
                DOY += 365
                Delta -= 365
            else:
                DOY -= 365
                Delta += 365
        else:
            DOY += Delta
            Delta = 0
# This was isLeap(), but it's such a simple thing I'm not sure it was worth the
# function call, so it became this in all library functions.
        if YYYY % 4 != 0:
            Leap = 0
        elif YYYY % 100 != 0 or YYYY % 400 == 0:
            Leap = 1
        else:
            Leap = 0
        if DOY < 1 or DOY > 365 + Leap:
            if Forward == 1:
                DOY -= 365 + Leap
                YYYY += 1
            else:
                YYYY -= 1
                if YYYY % 4 != 0:
                    Leap = 0
                elif YYYY % 100 != 0 or YYYY % 400 == 0:
                    Leap = 1
                else:
                    Leap = 0
                DOY += 365 + Leap
            break
        if Delta == 0:
            break
    return YYYY, DOY
##################################
# BEGIN: dt2Timeydoy2md(YYYY, DOY)
# FUNC:dt2Timeydoy2md():2013.030
#   Does no values checking, so make sure you stuff is in one sock before
#   coming here.


def dt2Timeydoy2md(YYYY, DOY):
    if DOY < 32:
        return 1, DOY
    elif DOY < 60:
        return 2, DOY - 31
    if YYYY % 4 != 0:
        Leap = 0
    elif YYYY % 100 != 0 or YYYY % 400 == 0:
        Leap = 1
    else:
        Leap = 0
# Check for this special day.
    if Leap == 1 and DOY == 60:
        return 2, 29
# The PROG_FDOM values for Mar-Dec are set up for non-leap years. If it is a
# leap year and the date is going to be Mar-Dec (it is if we have made it this
# far), subtract Leap from the day.
    DOY -= Leap
# We start through PROG_FDOM looking for dates in March.
    Month = 3
    for FDOM in PROG_FDOM[4:]:
        # See if the DOY is less than the first day of next month.
        if DOY <= FDOM:
            # Subtract the DOY for the month that we are in.
            return Month, DOY - PROG_FDOM[Month]
        Month += 1
# If anything goes wrong...
    return 0, 0
######################################
# BEGIN: dt2Timeymd2doy(YYYY, MMM, DD)
# FUNC:dt2Timeymd2doy():2013.030


def dt2Timeymd2doy(YYYY, MMM, DD):
    if YYYY % 4 != 0:
        return (PROG_FDOM[MMM] + DD)
    elif (YYYY % 100 != 0 or YYYY % 400 == 0) and MMM > 2:
        return (PROG_FDOM[MMM] + DD + 1)
    else:
        return (PROG_FDOM[MMM] + DD)
##################################################################
# BEGIN: dt2Timeymddhms(OutFormat, YYYY, MMM, DD, DOY, HH, MM, SS)
# FUNC:dt2Timeymddhms():2018.235
#   The general-purpose time handler for when the time is already split up into
#   it's parts.
#   Make MMM, DD -1 if passing DOY and DOY -1 if passing MMM, DD.


def dt2Timeymddhms(OutFormat, YYYY, MMM, DD, DOY, HH, MM, SS):
    global Y2EPOCH
# Returns a float Epoch is SS is a float, otherwise an int value.
    if OutFormat == -1:
        Epoch = 0
        if YYYY < 70:
            YYYY += 2000
        if YYYY < 100:
            YYYY += 1900
        try:
            Epoch = Y2EPOCH[YYYY]
        except KeyError:
            for YYY in arange(1970, YYYY):
                if YYY % 4 != 0:
                    Epoch += 31536000
                elif YYY % 100 != 0 or YYY % 400 == 0:
                    Epoch += 31622400
                else:
                    Epoch += 31536000
            Y2EPOCH[YYYY] = Epoch
        if DOY == -1:
            DOY = dt2Timeymd2doy(YYYY, MMM, DD)
        return Epoch + ((DOY - 1) * 86400) + (HH * 3600) + (MM * 60) + SS
##########################################
# BEGIN: dt2Timeystr2Epoch(YYYY, DateTime)
# FUNC:dt2Timeystr2Epoch():2018.238
#   A slightly specialized time to Epoch converter where the input can be
#       YYYY, DOY:HH:MM:SS:TTT or
#       None,YYYY:DOY:HH:MM:SS:TTT or
#       None,YYYY-MM-DD:HH:MM:SS:TTT
#   DOY/DD and HH must be separated by a ':' -- no space.
#   The separator between SS and TTT may be : or .
#   Returns 0 if ANYTHING goes wrong.
#   Returns a float if the seconds were a float (SS:TTT or SS.ttt), or an int
#   if the time was incomplete or there was an error. The caller will just
#   have to do an int() if that is all they want.
#   Replaces str2Epoch(). Used a lot in RT130/72A data decoding.


def dt2Timeystr2Epoch(YYYY, DateTime):
    global Y2EPOCH
    Epoch = 0
    try:
        if DateTime.find("-") != -1:
            DT = DateTime.split(":", 1)
            Parts2 = DT[0].split("-")
            Parts = []
            for Parts in Parts2:
                Parts.append(float(Part))
            Parts2 = DT[1].split(":")
            for Parts in Parts2:
                Parts.append(float(Part))
# There "must" have been a :TTT part.
            if len(Parts) == 6:
                Parts[5] += Parts[6] / 1000.0
        else:
            Parts2 = DateTime.split(":")
            Parts = []
            for Part in Parts2:
                Parts.append(float(Part))
# There "must" have been a :TTT part.
            if len(Parts) == 7:
                Parts[5] += Parts[6] / 1000.0
        if YYYY is None:
            # Change this since it gets used in loops and dictionary keys and
            # stuff. If the year is passed (below) then we're already covered.
            Parts[0] = int(Parts[0])
# Just in case someone generates 2-digit years.
            if Parts[0] < 100:
                if Parts[0] < 70:
                    Parts[0] += 2000
                else:
                    Parts[0] += 1900
        else:
            # Check this just in case. The caller will just have to figure this
            # one out.
            if YYYY < 1970:
                YYYY = 1970
            Parts = [YYYY, ] + Parts
        Parts += (5 - len(Parts)) * [0]
        # This makes each year's Epoch get calculated only once.
        try:
            Epoch = Y2EPOCH[Parts[0]]
        except KeyError:
            for YYY in arange(1970, Parts[0]):
                if YYY % 4 != 0:
                    Epoch += 31536000
                elif YYY % 100 != 0 or YYY % 400 == 0:
                    Epoch += 31622400
                else:
                    Epoch += 31536000
            Y2EPOCH[Parts[0]] = Epoch
        # This goes along with forcing YYYY to 1970 if things are bad.
        if Parts[1] < 1:
            Parts[1] = 1
        Epoch += ((int(Parts[1]) - 1) * 86400) + (int(Parts[2]) * 3600) + \
            (int(Parts[3]) * 60) + Parts[4]
    except ValueError:
        Epoch = 0
    return Epoch
##########################################################
# BEGIN: dtHoursFromNow(ToFormat, OutFormat, DateTime, TZ)
# FUNC:dtHoursFromNow():2018.264
#   Takes in a time (presumably in the future) and returns the decimal hours
#   from now until then.
#   ToFormat is the format of the time we are going to.
#   OutFormat is how to format the return time, "H" for decimal hours, or "H:M"
#   for HH:MM (no seconds).
#   TZ is either "GMT" or "LT" pertaining to the input DateTime.
#   The current time it uses (in YYYY:DOY:HH:MM:SS) is also returned.
#       (decimal hours, current time used)


def dtHoursFromNow(ToFormat, OutFormat, DateTime, TZ):
    InEpoch = dt2Time(ToFormat, -1, DateTime)
    if TZ == "GMT":
        NowDateTime = getGMT(0)
    elif TZ == "LT":
        NowDateTime = getGMT(13)
    NowEpoch = dt2Time(11, -1, NowDateTime)
    Diff = InEpoch - NowEpoch
    if OutFormat == "H":
        return ("%.2f hours" % (Diff / 3600.0), NowDateTime)
    elif OutFormat == "H:M":
        H = Diff / 3600.0
        M = int((H - int(H)) * 60.0)
        H = int(H)
# Rounding may cause this.
        if M > 59:
            H += 1
            M = M - 60
        return ("%02d:%02d" % (H, M), NowDateTime)
    return ("0.00", "0000:000:00:00:00")
############################
# BEGIN: dt2DateDiff(D1, D2)
# FUNC:dt2DateDiff():2018.238
#   Takes two dates in YYYY-MM-DD format and returns the number of whole days
#   between them. Dates must be proper format or there will be crashing.
#   Returned is D2-D1, so D2 should be later than D1, but doesn't have to be.


def dt2DateDiff(D1, D2):
    Parts = D1.split("-")
    D1Y = int(Parts[0])
    D1M = int(Parts[1])
    D1D = int(Parts[2])
    Parts = D2.split("-")
    D2Y = int(Parts[0])
    D2M = int(Parts[1])
    D2D = int(Parts[2])
# Pick off the easy one.
    if D1Y == D2Y and D1M == D2M:
        return D2D - D1D
# Almost easy.
    elif D1Y == D2Y:
        D1Doy = dt2Timeymd2doy(D1Y, D1M, D1D)
        D2Doy = dt2Timeymd2doy(D2Y, D2M, D2D)
        return D2Doy - D1Doy
    else:
        # The full monty.
        D1E = dt2Timeymddhms(-1, D1Y, D1M, D1D, -1, 0, 0, 0)
        D2E = dt2Timeymddhms(-1, D2Y, D2M, D2D, -1, 0, 0, 0)
        return int((D2E - D1E) / 86400)
# END: dt2Time


###################
# BEGIN: eatBATMP()
# FUNC:eatBATMP():2013.034
def eatBATMP():
    global TheYear
    global TheLastDOY
    global TimeMax
    global InParts
# 72A's:
# Ex: 186:14:33:58 BATTERY VOLTAGE = 13.6V, TEMPERATURE = 26C
# RT130:
# Ex: 186:14:33:58 BATTERY VOLTAGE = 13.6V, TEMPERATURE = 26C, BACKUP = 3.3V
    InParts = InLine.split()
    ThisDOY = InIntt()
    if ThisDOY == 1 and (TheLastDOY == 365 or TheLastDOY == 366):
        TheYear += 1
    TheLastDOY = ThisDOY
    Time = dt2Timeystr2Epoch(TheYear, InParts[0])
    if Time > TimeMax:
        TimeMax = Time
    Volts = floatt(InParts[4])
    Temp = floatt(InParts[7])
    if RTModel == "RT130":
        BkupV = floatt(InParts[10])
    else:
        BkupV = 0.0
    return (Time, Volts, Temp, BkupV)
# END: eatBATMP


##################
# BEGIN: eatCPUV()
# FUNC:eatCPUV():2011.080
def eatCPUV():
    global TheCPUVers
    global InParts
# The version line changes every week, so if there is no . in what we have
# then we must not have the right thing.
# Possiblities so far:
#    341:22:05:41 CPU SOFTWARE V03.00H  (72A and older 130 FW)
#    341:22:05:41 REF TEK 130  (no version number at all)
#    341:22:05:41 Ref Tek 130  2.8.8S (2007:163)
#    341:22:05:41 CPU SOFTWARE V 3.0.0 (2009:152)
    if TheCPUVers.find(".") == -1:
        InParts = InLine.split()
        if InParts[1].startswith("CPU"):
            TheCPUVers = " ".join(InParts[3:])
        elif InParts[1].startswith("R"):
            # There may not be any version info in this line (REF TEK
            # 130...then nothing) but accessing non-existant InParts doesn't
            # matter. TheCPUVers will just be "".
            TheCPUVers = " ".join(InParts[4:])
    return
# END: eatCPUV


####################
# BEGIN: eatDCDIFF()
# FUNC:eatDCDIFF():2013.034
def eatDCDIFF():
    global TheYear
    global TheLastDOY
    global TimeMax
    global InParts
# Ex: 245:07:41:45 DSP CLOCK DIFFERENCE: 0 SECS AND -989 MSECS
    InParts = InLine.split()
    ThisDOY = InIntt()
    if ThisDOY == 1 and (TheLastDOY == 365 or TheLastDOY == 366):
        TheYear += 1
    TheLastDOY = ThisDOY
    Time = dt2Timeystr2Epoch(TheYear, InParts[0])
    if Time > TimeMax:
        TimeMax = Time
    Secs = floatt(InParts[4])
    MSecs = floatt(InParts[-2])
    Total = abs(Secs) * 1000.0 + abs(MSecs)
    if Secs < 0.0 or MSecs < 0.0:
        Total *= -1.0
    return (Time, Total)
# END: eatDCDIFF


##################
# BEGIN: eatDEFS()
# FUNC:eatDEFS():2016.235
def eatDEFS():
    global InParts
    global LogFile
    global TimeMax
# Ex: Station Channel Definition   01:330:19:24:42:978    ST: 7095
# Ex: Data Stream Definition   01:330:19:24:42:978    ST: 7095
# Ex: Calibration Definition   01:330:19:24:42:978    ST: 7095
    InParts = InLine.split()
# Lines from a .log file may be just the first time the parameters were saved
# until the DAS is reset, so if this is a log file then use the current SOH
# time for plotting the points, instead of what is in the message line.
    if InParts[0] == "STATION" or InParts[0] == "DATA":
        if LogFile is False:
            Year = intt(InParts[3])
            if Year < 70:
                Year += 2000
                DateTime = InParts[3][3:]
# These messages may have 2 or 4 digit years.
            elif Year < 100:
                Year += 1900
                DateTime = InParts[3][3:]
            else:
                DateTime = InParts[3][5:]
            Time = dt2Timeystr2Epoch(Year, DateTime)
        else:
            Time = TimeMax
    elif InParts[0] == "CALIBRATION":
        if LogFile is False:
            Year = intt(InParts[2])
            if Year < 70:
                Year += 2000
                DateTime = InParts[3][3:]
# These messages may have 2 or 4 digit years.
            elif Year < 100:
                Year += 1900
                DateTime = InParts[2][3:]
            else:
                DateTime = InParts[2][5:]
            Time = dt2Timeystr2Epoch(Year, DateTime)
        else:
            Time = TimeMax
    else:
        # This will cause the program to crash.
        stdout.write("eatDEFS error\n")
        return
# These will not be allowed to set TimeMin since the RT130's save multiple
# copies of the parameters in the log files (one per day), but they all have
# the date/timestamp of the first time the parameters were written (unless
# a new copy is written because the parameters were changed, of course).
    return Time
# END: eatDEFS


##################
# BEGIN: eatDISK()
# FUNC:eatDISK():2013.034
#   This will handle entries for both disks and return which in the end.
def eatDISK():
    global TheYear
    global TheLastDOY
    global TimeMax
    global InParts
# RT130:
# Ex: 186:14:33:58 DISK 1: USED: 89744 AVAIL:...
# Ex: 186:14:33:58 DISK 2* USED: 89744 AVAIL:...
    InParts = InLine.split()
    ThisDOY = InIntt()
    if ThisDOY == 1 and (TheLastDOY == 365 or TheLastDOY == 366):
        TheYear += 1
    TheLastDOY = ThisDOY
    Time = dt2Timeystr2Epoch(TheYear, InParts[0])
    if Time > TimeMax:
        TimeMax = Time
    Disk = intt(InParts[2])
    Value = intt(InParts[4])
    return (Time, Disk, Value)
# END: eatDISK


#########################
# BEGIN: eatEVT(DSsButts)
# FUNC:eatEVT():2013.034
#   Eats the event trailer lines that decoding programs (rt2ms, ref2segy, etc.)
#   programs stick into the log lines.
def eatEVT(DSsButts):
    global TimeMin
    global TimeMax
    global InParts
    InParts = InLine.split()
# Check to see if this data stream is even wanted.
    DS = intt(InParts[5])
    if DS not in DSsButts:
        # 0 is we're just not interested.
        return (0, 0)
# Ex: DAS: 0108 EV: 2632 DS: 2 FST = 2001:253:15:13:59:768 TT =
# 2001:253:15:13:59:768 NS: 144005 SPS: 40 ETO: 0
# If the FST time is something like 0000:000:00:00:00:000, which some program
# generated for a TT time when an event ended badly, then try using the TT
# time. If that is also bad pass back (0, 0). If the TT time is 00:000 then
# the FST time may not be correct, but it seems like it will still be something
# sensible and it will at least let the user know things aren't right.
# dt2Timeystr2Epoch() returns 2000:001:00:00:00 for 00:000.
# Too bad for the user if an event actually triggers at 1970:001:00:00:00,
# since that will return an Epoch of 0.
    if InParts[8].startswith("00:000"):
        if InParts[11].startswith("00:000"):
            # -1 is an error code.
            return (-1, 0)
        Time = dt2Timeystr2Epoch(None, InParts[11])
    else:
        Time = dt2Timeystr2Epoch(None, InParts[8])
    if Time > 0:
        if Time < TimeMin:
            TimeMin = Time
        if Time > TimeMax:
            TimeMax = Time
    else:
        return (0, 0)
    return (Time, DS)
# END: eatEVT


##################
# BEGIN: eatGPSV()
# FUNC:eatGPSV():2018.238
def eatGPSV():
    global TheGPSvers
    global InParts
    if len(TheGPSvers) == 0:
        Parts = InLine.split()
        InParts = []
        for Part in Parts:
            InParts.append(Part.strip())
# 130: Ex: 291:19:54:29 GPS FIRMWARE VERSION: GPS 16-HVS Ver. 3.20
        if InLine.find("GPS FIRMWARE VERSION:") != -1:
            TheGPSvers = " ".join(InParts[4:])
        else:
            # 72A: Ex: 159:15:41:05 GPS: V03.30    (17JAN2000)
            TheGPSvers = " ".join(InParts[2:])
    return
# END: eatGPSV


##################
# BEGIN: eatICPE()
# FUNC:eatICPE():2013.034
def eatICPE():
    global TheYear
    global TheLastDOY
    global TimeMax
    global InParts
# Ex: 253:19:41:42 INTERNAL CLOCK PHASE ERROR OF 4823 USECONDS
    InParts = InLine.split()
# Compare ThisDOY with LastDOY.  If LastDOY was 365 or 366 and this one is
# 1 then we must be processing lines from the SOH packet that midnight occured
# in the middle of.  It MUST be the next year, right??
    ThisDOY = InIntt()
    if ThisDOY == 1 and (TheLastDOY == 365 or TheLastDOY == 366):
        TheYear += 1
    TheLastDOY = ThisDOY
    Time = dt2Timeystr2Epoch(TheYear, InParts[0])
    if Time > TimeMax:
        TimeMax = Time
# We always want to return the error in milliseconds.
    Error = floatt(InParts[-2])
    if InParts[-1].startswith("USEC"):
        Error /= 1000.0
    elif InParts[-1].startswith("SEC"):
        Error *= 1000.0
    return (Time, Error)
# END: eatICPE


####################
# BEGIN: eatSIMPLE()
# FUNC:eatSIMPLE():2013.034
def eatSIMPLE():
    global TheYear
    global TheLastDOY
    global TimeMax
# Ex: 186:20:46:41 EXTERNAL CLOCK IS UNLOCKED
# Ex: 186:21:41:35 EXTERNAL CLOCK IS LOCKED
    ThisDOY = InIntt()
    if ThisDOY == 1 and (TheLastDOY == 365 or TheLastDOY == 366):
        TheYear += 1
    TheLastDOY = ThisDOY
# The second item is the DateTime.
    Time = dt2Timeystr2Epoch(TheYear, InLine.split(" ", 1)[0])
    if Time > TimeMax:
        TimeMax = Time
    return Time
# END: eatSIMPLE


#################
# BEGIN: eatSOH()
# FUNC:eatSOH():2018.235
#   The SOH time can be either
#       YYYY:DOY:HH:MM:SS(plus .TTT or :TTT) or
#       YY:DOY:HH:MM:SS(plus .TTT or :TTT)
def eatSOH():
    global TheYear
    global TheLastDOY
    global TimeMin
    global TimeMax
    global TheUnitID
    global TheLastUnitID
    global RTModel
    global InParts
# Ex: State of Health  01:251:09:41:35:656   ST: 0108
    InParts = InLine.split()
# InParts[3] == DateTime
# Set TheYear to this lines year since it is the only place where we can get
# it from.
    TheYear = intt(InParts[3])
    if TheYear < 100:
        if TheYear < 70:
            TheYear += 2000
        else:
            TheYear += 1900
# These log lines set TheYear (above) and TheLastDOY, because we "trust" them.
        TheLastDOY = intt(InParts[3][3:])
        Time = dt2Timeystr2Epoch(TheYear, InParts[3][3:])
    else:
        TheLastDOY = intt(InParts[3][5:])
        Time = dt2Timeystr2Epoch(None, InParts[3])
# The start time of the file will be set by the earliest SOH message.
    if Time < TimeMin:
        TimeMin = Time
    if Time > TimeMax:
        TimeMax = Time
    UnitID = InParts[5].strip()
# The RT130's have hex serial numbers in the range 9000-FFFF. Set this if it
# has not already been done by someone else.
    if UnitID >= "9000":
        RTModel = "RT130"
    else:
        RTModel = "72A"
    if len(TheUnitID) == 0:
        TheUnitID = "%s (%s)" % (UnitID, RTModel)
        TheLastUnitID = UnitID
    if UnitID != TheLastUnitID:
        Answer = formMYD(Root, (("Continue", LEFT, "cont"),
                                ("Stop", LEFT, "stop")),
                         "stop", "YB", "You Knew This, Right?",
                         "The State Of Health messages for DAS %s were being "
                         "read, but now there is information for DAS %s in "
                         "the same State Of Health messages. Do you want to "
                         "continue, or stop and select different files (if "
                         "that is the problem)?" % (TheLastUnitID, UnitID))
        if Answer == "stop":
            progControl("stop")
        TheUnitID = "%s (%s)" % (UnitID, RTModel)
        TheLastUnitID = UnitID
    return Time
# END: eatSOH


###################
# BEGIN: eatTMJMP()
# FUNC:eatTMJMP():2013.035
TMJMP_OTIME = 0
TMJMP_JTIME = 1
TMJMP_CHAN = 2


def eatTMJMP():
    global InParts
# Ex: Time jump: R182.01/06.182.00.46.02.9139.1 obs 00:57:22.800  corr +2680 ms
    InParts = InLine.split()
# Get the start time of the block with the jump.
    Starts = InParts[2].split("/")
# An older version of ref2mseed made lines like:
#    Time jump: DAS9065/R182.01/06.182.00.46.02.9139.1... so watch for it.
# It's all up-in-the-air now, so this might not work anymore with rt2ms.
# (2013feb04)
    if len(Starts) == 2:
        Start = Starts[1].split(".")
    else:
        Start = Starts[2].split(".")
# Year.
    YYYY = int(Start[0])
    if YYYY < 70:
        YYYY += 2000
    else:
        YYYY += 1900
    DOY = int(Start[1])
# Get the time the jump was observed.
    Obed = InParts[4].split(":")
# Since the time of the jump doesn't have any idea of year/doy we'll just
# assume that the year and doy are the same as the event start time.  This,
# of course will possibly be wrong 1/24th and 1/365th of the time.
    OTime = dt2Timeymddhms(-1, YYYY, -1, -1, DOY, int(Obed[0]), int(Obed[1]),
                           float(Obed[2]))
# OTime+"Jsec".
    JTime = OTime + float(InParts[6]) / 1000.0
# the last item is channel number.
    return (OTime, JTime, int(Start[6]))
# END: eatTMJMP


########################
# BEGIN: exportTT(Which)
# FUNC:exportTT():2019.003
#   Goes through the currenty loaded log messages and extracts the event
#   trigger times from the EventTrailer lines and exports them to a selected
#   file in the format:
#      Which = "tspsf" = UTEP's TSP shot file and geometry file format*
#      Which = "depl" = Deployment file SHOT line format
#      Which = "depb" = Deployment file SHOT block format -- key/value pairs
#      Which = "sinfo" = StaID FFID YYYY:DOY:HH:MM:SS.sss Lat Long Elev* (The
#              StaID and FFID are the RT130 event number for the event time)
#   All data streams are extracted and listed separately.
#   * If Options | Add Positions To ET Lines is selected lines showing the
#     position information in event headers will be listed with the first
#     sample time (FST) in the DAS lines. This function will read those and
#     create 'geometry' entries.
ExportTTFilespecVar = StringVar()
PROGSetups += ["ExportTTFilespecVar"]


def exportTT(Which):
    if len(LOGLines) == 0:
        setMsg("MF", "RW", "No SOH lines have been loaded.", 2)
        return
    Datas = []
    for Line in LOGLines:
        if Line.startswith("DAS: "):
            Parts = Line.split()
            if Line.find("ETPOS:") == -1:
                # Data stream, Event number, Trigger time, Lat, Long, Elev.
                Datas.append([Parts[5], Parts[3], Parts[11], "?", "?", "?"])
            else:
                #  0   1  2  3  4  5  6  7 8 9 10 11 12 13  14 15  16 17 18
                #  19  20 21 22
                # DAS: X EV: X DS: X FST = X TT = X  NS: X SPS: X ETO: ?
                # ET ETPOS: X  X  X
                Datas.append([Parts[5], Parts[3], Parts[11], Parts[20],
                              Parts[21], Parts[22]])
    if len(Datas) == 0:
        setMsg("MF", "RW", "No event trigger times found in SOH messages.",
               2)
        return
    if len(ExportTTFilespecVar.get()) == 0 or \
            exists(dirname(ExportTTFilespecVar.get())) is False:
        ExportTTFilespecVar.set(PROGDataDirVar.get() + "events.txt")
    Filespec = formMYDF(Root, 0, "Pick Or Enter An Output File",
                        dirname(ExportTTFilespecVar.get()),
                        basename(ExportTTFilespecVar.get()))
    if len(Filespec) == 0:
        setMsg("MF", "", "Nothing done.")
        return
    Answer = "over"
    if exists(Filespec):
        Answer = formMYD(Root, (("Append", LEFT, "append"),
                                ("Overwrite", LEFT, "over"),
                                ("Cancel", LEFT, "cancel")),
                         "cancel", "YB", "Keep Everything.",
                         "The file\n\n%s\n\nalready exists. Would you like to "
                         "append to that file, overwrite it, or cancel?" %
                         Filespec)
        if Answer == "cancel":
            setMsg("MF", "", "Nothing done.")
            return
    try:
        if Answer == "over":
            Fp = open(Filespec, "w")
        elif Answer == "append":
            Fp = open(Filespec, "a")
    except Exception as e:
        setMsg("MF", "MW", "Error opening file %s\n   %s" % (Filespec, e), 3)
        return
    Datas.sort()
    CurrDS = "0"
    LineCount = 0
#          0    1     2   3     4     5
# Data = [DS, Event, TT, Lat, Long, Elev]
    for Data in Datas:
        if Data[0] != CurrDS:
            if CurrDS != "0":
                Fp.write("\n")
            Fp.write("# Events from data stream %s\n" % Data[0])
            CurrDS = Data[0]
            if Which == "depl":
                Fp.write("# %s\n" % DEPSHOT_FIELDS)
            elif Which == "depb":
                Fp.write("# Key=Value pairs\n")
            elif Which == "tspsf":
                Fp.write("# %s\n" % TSPSHOT_FIELDS)
            elif Which == "sinfo":
                Fp.write("# %s\n" % SINFO_FIELDS)
        Dict = {}
        convertDictDefaults(Dict)
# Use the event number since we have no other idea what they should be.
        Dict["stid"] = Data[1]
        Dict["idid"] = Data[1]
        Parts = Data[2].split(":")
        Parts += (6 - len(Parts)) * ["00"]
        Dict["shtm"] = "%s:%s:%s:%s:%s.%s" % (Parts[0], Parts[1], Parts[2],
                                              Parts[3], Parts[4], Parts[5])
# In case we are make TSP shotfile. We could call the Polish routine, but that
# would be more than we need.
        Dict["tspy"] = int(Parts[0])
        Dict["tspd"] = int(Parts[1])
        Dict["tsph"] = int(Parts[2])
        Dict["tspm"] = int(Parts[3])
        Dict["tsps"] = float("%s.%s" % (Parts[4], Parts[5]))
# We can do these positions now, but will have to go through again below to
# create the 'geometry' info for TSP stuff.
        if Which == "depl" or Which == "depb" or Which == "sinfo":
            # If one is ? they all will be ?.
            if Data[3] != "?":
                LatLL = rt72130Str2Dddd(Data[3])
                LongLL = rt72130Str2Dddd(Data[4])
# rt72130Str2Dddd() returns -1000.0 on error. We'll just not change the default
# values if that happens.
                if LatLL != -1000.0 and LongLL != -1000.0:
                    if LatLL <= 0.0:
                        LatLL = "S%02.6f" % (-LatLL)
                    else:
                        LatLL = "N%02.6f" % LatLL
                    Dict["lati"] = LatLL
                    if LongLL <= 0.0:
                        LongLL = "W%03.6f" % (-LongLL)
                    else:
                        LongLL = "E%03.6f" % LongLL
                    Dict["long"] = LongLL
                    Dict["elev"] = str(intt(Data[5]))
        if Which == "depl":
            Ret = convertDict2DepfileSHOT(Dict, "depl", False, False)
        elif Which == "depb":
            Ret = convertDict2DepfileSHOT(Dict, "depb", False, True)
        elif Which == "tspsf":
            Ret = convertDict2TSPSfEntry(Dict, "space", False)
        elif Which == "sinfo":
            Ret = convertDict2SInfo(Dict, "space", False)
        Fp.write("%s\n" % Ret[1])
        LineCount += 1
# Second round for the geometry info.
    if Which == "tspsf":
        Fp.write("\n")
        CurrDS = "0"
        for Data in Datas:
            if Data[0] != CurrDS:
                if CurrDS != "0":
                    Fp.write("\n")
                Fp.write("# Events from data stream %s\n" % Data[0])
                CurrDS = Data[0]
                Fp.write("# %s\n" % TSPGEOM_FIELDS)
            Dict = {}
            convertDictDefaults(Dict)
# Use the event number since we have no other idea what they should be.
            Dict["stid"] = Data[1]
            if Data[3] != "?":
                LatLL = rt72130Str2Dddd(Data[3])
                LongLL = rt72130Str2Dddd(Data[4])
                if LatLL != -1000.0 and LongLL != -1000.0:
                    if LatLL <= 0.0:
                        LatLL = "S%02.6f" % (-LatLL)
                    else:
                        LatLL = "N%02.6f" % LatLL
                    Dict["lati"] = LatLL
                    if LongLL <= 0.0:
                        LongLL = "W%03.6f" % (-LongLL)
                    else:
                        LongLL = "E%03.6f" % LongLL
                    Dict["long"] = LongLL
                    Dict["elev"] = str(intt(Data[5]))
            Ret = convertDict2TSPGfEntry(Dict, "space")
            Fp.write("%s\n" % Ret[1])
            LineCount += 1
    Fp.close()
    ExportTTFilespecVar.set(Filespec)
    if LineCount == 1:
        setMsg("MF", "", "1 line written to\n   %s" % Filespec)
    else:
        setMsg("MF", "", "%d lines written to\n   %s" % (LineCount, Filespec))
    return
# END: exportTT


###############################
# BEGIN: fileSelected(e = None)
# FUNC:fileSelected():2019.002
#   Does the job of reading and digesting the selected file(s) or directory.
PROGFrm["LOG"] = None
PROGTxt["LOG"] = None
LOGFindLookForVar = StringVar()
PROGSetups += ["LOGFindLookForVar"]
LOGFindIndexVar = IntVar()
LOGFindLastLookForVar = StringVar()
LOGFindLinesVar = StringVar()
LOGFindUseCaseCVar = IntVar()
PROGFrm["ERR"] = None
PROGTxt["ERR"] = None
PROGFrm["RAW"] = None
PROGFrm["TPS"] = None
# PROGCan["MF"] gets defined when the program starts, but these don't until the
# raw data displays come up.
PROGCan["RAW"] = None
PROGCan["TPS"] = None
LOGLines = []
RawData = {}
SampleRates = {}
# Will be True if the file being read is a .log file.
LogFile = False
# For Antelope-produced files that have no 'State of Health' lines in
# them.
LogStartYYYY = 0
# So everyone knows which files were processed.
FilenamesRead = []


def fileSelected(e=None):
    global LOGLines
    global LogFile
    global LogStartYYYY
    global ACQ
    global NET
    global DUMP
    global TEMP
    global VOLT
    global BKUP
    global MP1
    global MP2
    global MP3
    global MP4
    global MP5
    global MP6
    global DCDIFF
    global RESET
    global GPSLK
    global TMJMP
    global EVT
    global DU1
    global DU2
    global GPS
    global GPSERR
    global ICPE
    global JERK
    global PWRUP
    global CLKPWR
    global DEFS
    global MRC
    global SOH
    global DISCREP
    global ERROR
    global WARN
    global FStart
    global FEnd
    global CurMainStart
    global CurMainEnd
    global RTModel
    global MainFilename
    global MainFilespec
    global TimeMin
    global TimeMax
    global TheUnitID
    global TheLastUnitID
    global TheCPUVers
    global TheGPSvers
    global TheLastDOY
    global TheYear
    global InLine
    global StartEndRecord
    global FilenamesRead
    Root.focus_set()
# Clear the screen off and sets things up for plotting.
    plotMFClear("all")
    plotRAWClear()
    plotTPSClear()
# Get rid of any data from a previous raw data file reading.
    RawData.clear()
    SampleRates.clear()
# Get the file from the Listbox or from the command line.
    if len(CLAFile) == 0:
        # Always check the directory entry.
        PROGDataDirVar.set(PROGDataDirVar.get().strip())
        if PROGDataDirVar.get().endswith(sep) is False:
            PROGDataDirVar.set(PROGDataDirVar.get() + sep)
# Just a good place to always keep these in sync.
        PROGMsgsDirVar.set(PROGDataDirVar.get())
        PROGWorkDirVar.set(PROGDataDirVar.get())
        try:
            Indexes = MFFiles.curselection()
            Selected = []
            for ind_ in Indexes:
                File = MFFiles.get(ind_)
                if len(File) == 0:
                    continue
# The entry will probably be something like "Filename (x bytes or lines)" so
# just get the Filename.
                Parts = File.split()
                Selected.append(Parts[0])
            if len(Selected) == 0:
                setMsg("MF", "RW", "You need to select a file name.", 2)
                return
        except TclError:
            beep(1)
            return
    else:
        Selected = [basename(CLAFile)]
# Take whatever the basename of the first file is, add .ps to it and set it as
# the default .ps file names.
    LastMFPSFilespecVar.set(PROGDataDirVar.get() + basename(Selected[0]) +
                            "-main.ps")
    LastRAWPSFilespecVar.set(PROGDataDirVar.get() + basename(Selected[0]) +
                             "-raw.ps")
    LastTPSPSFilespecVar.set(PROGDataDirVar.get() + basename(Selected[0]) +
                             "-tps.ps")
# See if the user wants to restrict the time range.
    FromDateVar.set(FromDateVar.get().strip())
    ToDateVar.set(ToDateVar.get().strip())
# The defaults.
    FromDate = 0
    ToDate = maxInt
    if UseDatesVar.get() == 0:
        UseDates = False
    elif len(FromDateVar.get()) == 0 and len(ToDateVar.get()) == 0:
        UseDates = False
    else:
        # Call this and it will check the values.
        if changeDateFormat() is False:
            return
        for F in ("From", "To"):
            eval("%sDateVar" % F).set(
                eval("%sDateVar" % F).get().upper().strip())
            if len(eval("%sDateVar" % F).get()) != 0:
                Ret = dt2Time(0, -1, eval("%sDateVar" % F).get())
                if F == "From":
                    # From 00:00.
                    FromDate = Ret
                elif F == "To":
                    # To 00:00 the following day (then > and < will be used in
                    # the comparisons).
                    ToDate = Ret + 86400
                UseDates = True
    if FromDate > ToDate:
        setMsg("MF", "RW", "The From date is greater than the To date.", 2)
        return
# Get the rest of these setup items.
    OnlySOH = OPTOnlySOHCVar.get()
# Tells the extractors if the mass positions need to be processed.
    IncDS9 = 0
    DS9s = []
    if DGrf["MP123"].get() == 1:
        DS9s += [1, 2, 3]
        IncDS9 = 1
    if DGrf["MP456"].get() == 1:
        DS9s += [4, 5, 6]
        IncDS9 = 1
# Collect which data streams to collect/plot.
    DSsButts = []
    for i in arange(1, 1 + PROG_DSS):
        if eval("Stream%dVar" % i).get() == 1:
            DSsButts.append(i)
    IncRAW = OPTPlotRAWCVar.get()
    IncTPS = OPTPlotTPSCVar.get()
    IncRAWTPS = 0
    if IncRAW == 1 or IncTPS == 1:
        IncRAWTPS = 1
# Check for this misteak.
    if (IncRAW == 1 or IncTPS == 1) and len(DSsButts) == 0:
        Answer = formMYD(Root, (("You're Right. Stop.", TOP, "stop"),
                                ("Continue Anyway", TOP, "cont")),
                         "stop", "YB", "Really?",
                         "Some Data Stream checkbuttons must be selected if "
                         "you are going to be plotting the RAW data or "
                         "generating TPS plots.")
        if Answer == "stop":
            setMsg("MF", "", "Nothing done.")
            return
# Cover for the user. Don't change the buttons. It will just be our secret.
    if len(DSsButts) == 0:
        IncRAW = 0
        IncTPS = 0
# Ready to roll.
    progControl("go")
    setMsg("MF", "CB", "Working...")
# Create the window that will contain all of the SOH lines.
    if PROGFrm["LOG"] is None:
        LFrm = PROGFrm["LOG"] = Toplevel(Root)
        LFrm.withdraw()
        LFrm.protocol("WM_DELETE_WINDOW", Command(beep, 1))
        Sub = Frame(LFrm)
# Since the log lines do not need to be a fixed-width font then just make the
# font the Msg font.
        LTxt = PROGTxt["LOG"] = Text(Sub, bg=Clr["B"], cursor="",
                                     fg=Clr["W"], font=PROGPropFont, height=30,
                                     width=90, relief=SUNKEN, wrap=NONE)
        LTxt.pack(side=LEFT, expand=YES, fill=BOTH)
        LSb = Scrollbar(Sub, orient=VERTICAL, command=LTxt.yview)
        LSb.pack(side=RIGHT, fill=Y)
        LTxt.configure(yscrollcommand=LSb.set)
        Sub.pack(side=TOP, expand=YES, fill=BOTH)
        LSb = Scrollbar(LFrm, orient=HORIZONTAL, command=LTxt.xview)
        LSb.pack(side=TOP, fill=X)
        LTxt.configure(xscrollcommand=LSb.set)
        Sub = Frame(LFrm)
        labelTip(Sub, "Find:=", LEFT, 30, "[Find]")
        LEnt = Entry(Sub, width=20, textvariable=LOGFindLookForVar)
        LEnt.pack(side=LEFT)
        LEnt.bind("<Return>", Command(formFind, "LOG", "LOG"))
        LEnt.bind("<KP_Enter>", Command(formFind, "LOG", "LOG"))
        BButton(Sub, text="Find", command=Command(formFind, "LOG",
                                                  "LOG")).pack(side=LEFT)
        BButton(Sub, text="Next",
                command=Command(formFindNext, "LOG", "LOG")).pack(side=LEFT)
        Label(Sub, text=" ").pack(side=LEFT)
        BButton(Sub, text="Write To File...",
                command=Command(formWrite, "LOG", "LOG", "LOG",
                                "Pick Or Enter An Output File...",
                                LOGLastFilespecVar, True, "", True,
                                False)).pack(side=TOP)
        Sub.pack(side=TOP, padx=3, pady=3)
        PROGMsg["LOG"] = Text(LFrm, cursor="", font=PROGPropFont,
                              height=3, wrap=WORD)
        PROGMsg["LOG"].pack(side=TOP, fill=X)
        center(Root, "LOG", "NW", "O", False)
# Just make it show up. If it gets lifted then fine. This is the only place
# this will be done, so the LOG form doesn't keep jumping on top of the main
# display for each file read.
        LFrm.deiconify()
        LFrm.lift()
    else:
        LFrm = PROGFrm["LOG"]
# All of the "good" lines will be .insert()'ed, garbled lines will be
# txLn()'ed in red, and 'skipped' lines will be txLn()'ed in yellow.
        LTxt = PROGTxt["LOG"]
    LFrm.title("SOH Messages")
# Take a breath and start in.
    updateMe(0)
    del LOGLines[:]
# Where the data for the lines of interest will be kept.
    del ACQ[:]
    del BKUP[:]
    del CLKPWR[:]
    del DEFS[:]
    del DCDIFF[:]
    del DISCREP[:]
    del DU1[:]
    del DU2[:]
    del DUMP[:]
    del ERROR[:]
    del EVT[:]
    del GPS[:]
    del GPSERR[:]
    del GPSLK[:]
    del ICPE[:]
    del JERK[:]
    del MP1[:]
    del MP2[:]
    del MP3[:]
    del MP4[:]
    del MP5[:]
    del MP6[:]
    del MRC[:]
    del NET[:]
    del PWRUP[:]
    del RESET[:]
    del SOH[:]
    del TEMP[:]
    del TMJMP[:]
    del VOLT[:]
    del WARN[:]
# Line counter for the ERR message window. The .err files get read way up here
# since they have to be done as we are reading multiple files.
    ERRLines = 0
# For the info in "INFO".
    FilenamesRead = []
    FilterSOH = OPTFilterSOHCVar.get()
    WarnIfBig = PROGWarnIfBigCVar.get()
# ===== MAIN LOOP =====
# Now that everything is ready to go start looping through the files.
    for MainFilename in Selected:
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            break
        LogFile = False
        MainFilenameLC = MainFilename.lower()
# Build the long source file name.
        MainFilespec = "%s%s" % (PROGDataDirVar.get(), MainFilename)
        FilenamesRead.append(MainFilename)
# Get all of the SOH lines into LOGLines no matter what the source.
# ---- .log source ----
        if MainFilenameLC.endswith(".log") or MainFilenameLC.endswith("_log"):
            # Check this before we go anywhere.
            if exists(MainFilespec) is False:
                setMsg("MF", "RW", "File does not exist:\n   %s" %
                       MainFilespec, 2)
                progControl("stopped")
                return
            if WarnIfBig == 1 and getsize(MainFilespec) > PROGBIG_LOG:
                Answer = formMYD(Root,
                                 (("I'll Keep This In Mind", TOP, "ok"),
                                  ("I Know. Stop Bothering Me.", TOP, "fine"),
                                  ("Skip This File", TOP, "skip")),
                                 "stop", "YB", "Do You Like Coffee?",
                                 "The .log file\n\n%s\n\nis quite large "
                                 "(check the estimate of the number of lines "
                                 "in the files list). LOGPEEK is not very "
                                 "fast. This will take a while to process." %
                                 MainFilespec)
                if Answer == "stop":
                    setMsg("MF", "", "Nothing done.")
                    progControl("stopped")
                    return
                if Answer == "skip":
                    continue
                if Answer == "fine":
                    PROGWarnIfBigCVar.set(0)
                    WarnIfBig = 0
            if UseDates is True:
                formMYD(Root, (("(OK)", TOP, "ok"),), "ok", "YB",
                        "Not Gonna Do It.",
                        "Setting a date range for .log files has no effect.")
            try:
                setMsg("MF", "CB", "Reading " + MainFilespec + "...")
                Ret = rt72130ExtractLogData(MainFilespec, False, FilterSOH)
                if Ret[0] != 0:
                    setMsg("MF", Ret)
                    progControl("stopped")
                    return
                # May have stopped while in there.
                if PROGRunning.get() == 0:
                    setMsg("MF", "YB", "Reading stopped.", 2)
                    setMsg("INFO", "", "")
                    progControl("stopped")
                    return
                LOGLinesOrig = Ret[1]
# Verify that the file lines look a bit like what we are expecting, and then
# strip off the Antelope stuff if we are supposed to and then continue.
                if OPTAntelopeCVar.get() == 1:
                    AntelopeLine = 0
                    setMsg("MF", "CB",
                           "Checking Antelope log file information...")
                    FoundSOH = False
                    if len(LOGLinesOrig) > 1000:
                        MaxLines = 1000
                    else:
                        MaxLines = len(LOGLinesOrig)
# If there are none of these I can't use the file (no matter where it came
# from).
                    for Index in arange(0, MaxLines):
                        # If we find a line that begins with State of Health,
                        # then we are not looking at an Antelope file, but are
                        # looking at a regular .log file.
                        if LOGLinesOrig[Index].startswith("State of Health"):
                            setMsg("MF", "RW",
                                   "I think you are trying to read a regular "
                                   ".log file with the 'Options | Read "
                                   "Antelope-Produced Log Files' menu item "
                                   "selected.", 2)
                            setMsg("INFO", "", "")
                            progControl("stopped")
                            return
                        if LOGLinesOrig[Index].find("State of Health") != -1:
                            FoundSOH = True
                            break
# If no SOH messages were found then we will have to make them up as we go.
                    if FoundSOH is False:
                        Answer = formMYD(Root,
                                         (("Give It A Try", TOP, "cont"),
                                          ("Stop", TOP, "stop")),
                                         "stop", "YB",
                                         "This May Or May Not Work.",
                                         "No 'State of Health' message lines "
                                         "were found in the first %s lines of "
                                         "the file. I can try to make them up "
                                         "as I read through the lines, or "
                                         "stop. What would you like to do?" %
                                         MaxLines)
                        if Answer == "stop":
                            setMsg("INFO", "", "")
                            setMsg("MF", "YB", "Stopped.")
                            progControl("stopped")
                            return
# I'll need to be given a starting year and try to go from there.
                        if LogStartYYYY == 0:
                            MYDAnswerVar.set(str(getGMT(4)[0]))
                        else:
                            MYDAnswerVar.set(str(LogStartYYYY))
                        Answer = formMYD(Root,
                                         (("Input10", TOP, "input"),
                                          ("(OK)", LEFT, "input"),
                                          ("Stop", LEFT, "stop")),
                                         "stop", "", "Start Of Time Is...?",
                                         "Since there is no line to get the "
                                         "year from enter the start year of "
                                         "the log information in the file.")
                        if Answer == "stop":
                            setMsg("INFO", "", "")
                            setMsg("MF", "YB", "Stopped.")
                            progControl("stopped")
                            return
                        LastYYYY = intt(Answer)
                        LogStartYYYY = LastYYYY
# Now read through the lines and only pull out the lines that appear to have
# an RT130/72A time stamp (000:00:00:00) in Part[5]. If this is a sucked-in
# log file from something like rt2ms or ref2segy then this will catch most of
# the lines. If this is real-time info then this may only extract real SOH
# messages.
                    LastDOY = 0
                    for Line in LOGLinesOrig:
                        # If we found SOH lines then be on the lookout for
                        # others and stick them in.
                        # They won't have 000:00:00:00 in Part[5].
                        if FoundSOH is True:
                            if Line.find("State of Health") != -1:
                                LOGLines += "",
                                LOGLines += \
                                    Line[Line.index("State of Health"):],
                                AntelopeLine += 1
                                if AntelopeLine % 20000 == 0:
                                    setMsg("INFO", "",
                                           "Found SOH line %d..."
                                           % AntelopeLine)
                                continue
                        Parts = Line.split()
# In case there are Antelope-info only short lines of some kind.
                        if len(Parts) > 5:
                            if rtnPattern(Parts[5]) == "000:00:00:00":
                                # If we never found the "State of Health" lines
                                # above then we will make them up and add them
                                # each time the DOY changes.
                                if FoundSOH is False:
                                    DOY = intt(Parts[5])
                                    if DOY != LastDOY:
                                        if DOY == 1:
                                            LastYYYY += 1
                                        LOGLines += "",
                                        LOGLines += ("State of Health  %d:%s  "
                                                     " ST: 9000"
                                                     % (LastYYYY, Parts[5]))
                                        LastDOY = DOY
                                LOGLines += " ".join(Parts[5:]),
                                AntelopeLine += 1
                                if AntelopeLine % 20000 == 0:
                                    setMsg("INFO", "",
                                           "Found SOH line %d..."
                                           % AntelopeLine)
                                    PROGStopBut.update()
                                    if PROGRunning.get() == 0:
                                        break
                else:
                    for Line in LOGLinesOrig:
                        LOGLines += Line,
            except Exception as e:
                setMsg("INFO", "", "See status message.")
                setMsg("MF", "MW", e, 3)
                progControl("stopped")
                return
            if DGrf["ERR"].get() == 1:
                # Check to see if there is a matching .err file.
                EFilename = MainFilespec[:-4] + ".err"
                if exists(EFilename):
                    # Once there was an 89MB .err file!
                    Answer = "all"
                    if getsize(EFilename) > 1000000:
                        Answer = formMYD(Root, (("All", LEFT, "all"),
                                                ("10,000", LEFT, "10000"),
                                                ("None", LEFT, "none")),
                                         "none", "YB", "Wow!",
                                         "The .err file is %d bytes long. Do "
                                         "you want to read the whole thing, "
                                         "only about the first 10,000 lines, "
                                         "or none of it?" % getsize(EFilename))
                    if Answer == "all" or Answer == "10000":
                        setMsg("MF", "CB", "Reading " + EFilename + "...")
                        if Answer == "all":
                            Ret = readFileLinesRB(EFilename)
                        elif Answer == "10000":
                            # The err files are about 80 characters per entry.
                            Ret = readFileLinesRB(EFilename, False,
                                                  80 * 10000)
                        if Ret[0] != 0:
                            setMsg("INFO", "", "See status message.")
                            setMsg("MF", Ret)
                            progControl("stopped")
                            return
                        InLines = Ret[1]
# Create the window that will contain all of the lines of the file if it needs
# to be.
                        if PROGFrm["ERR"] is None:
                            EFrm = PROGFrm["ERR"] = Toplevel(Root)
                            EFrm.withdraw()
                            EFrm.protocol("WM_DELETE_WINDOW",
                                          Command(formClose, "ERR"))
                            EFrm.title("Error File Lines")
                            Sub = Frame(EFrm)
                            PROGTxt["ERR"] = Text(Sub, bg=Clr["B"],
                                                  cursor="", fg=Clr["W"],
                                                  font=PROGPropFont, height=15,
                                                  relief=SUNKEN, width=80,
                                                  wrap=NONE)
                            PROGTxt["ERR"].pack(side=LEFT, fill=BOTH,
                                                expand=YES)
                            Scroll = Scrollbar(Sub, orient=VERTICAL,
                                               command=PROGTxt["ERR"].yview)
                            Scroll.pack(side=RIGHT, fill=Y)
                            PROGTxt["ERR"].configure(yscrollcommand=Scroll.set)
                            Sub.pack(side=TOP, expand=YES, fill=BOTH)
                            center(Root, "ERR", "N", "O")
# Read through the .err lines and find the lines of interest.
                        ERRLines += 1
                        PROGTxt["ERR"].insert(END, "File: %s\n" % EFilename)
                        for InLine in InLines:
                            ERRLines += 1
                            PROGTxt["ERR"].insert(END, "%s\n" % InLine)
                            InLine = InLine.upper()
                            if InLine.find("TIME JUMP:") != -1:
                                Info = eatTMJMP()
# Lines, Observed time, Corrected time, Channel.
                                TMJMP.append((ERRLines, Info[TMJMP_OTIME],
                                              Info[TMJMP_JTIME],
                                              Info[TMJMP_CHAN]))
            LogFile = True
# ---- .ref source ----
        elif MainFilenameLC.endswith(".ref"):
            if exists(MainFilespec) is False:
                setMsg("MF", "RW", "File does not exist:\n   %s" %
                       MainFilespec, 2)
                progControl("stopped")
                return
            if WarnIfBig == 1 and getsize(MainFilespec) > PROGBIG_REF:
                Answer = formMYD(Root,
                                 (("I'll Keep This In Mind", TOP, "ok"),
                                  ("I Know. Stop Bothering Me.", TOP, "fine"),
                                  ("Skip This File", TOP, "skip")),
                                 "stop", "YB", "Do You Like Coffee?",
                                 "The .ref file\n\n%s\n\nis quite large "
                                 "(check the size in the files list). LOGPEEK "
                                 "is not very fast. This will take a while "
                                 "to process." % MainFilespec)
                if Answer == "stop":
                    setMsg("MF", "", "Nothing done.")
                    progControl("stopped")
                    return
                if Answer == "skip":
                    continue
                if Answer == "fine":
                    PROGWarnIfBigCVar.set(0)
                    WarnIfBig = 0
            Ret = rt72130ExtractRefData(PROGDataDirVar.get(), MainFilename,
                                        "", PROGStopBut, PROGRunning, "MF",
                                        OnlySOH, IncDS9, IncRAWTPS, DS9s,
                                        DSsButts, FromDate, ToDate)
            if Ret[0] != 0:
                setMsg("MF", Ret)
                setMsg("INFO", "", "See status message.")
                progControl("stopped")
                return
            LOGLines.append("%s:  Version Number  %s  File: %s" %
                            (PROG_NAMELC, PROG_VERSION, MainFilename))
            LOGLines += Ret[1]
# ---- .zip source ----
# Look for these before the .cf files since the .find(".cf") will go off if
# the file is really a .cf.zip file.
        elif MainFilenameLC.endswith(".zip"):
            if exists(MainFilespec) is False:
                setMsg("MF", "RW", "File does not exist:\n   %s" %
                       MainFilespec, 2)
                progControl("stopped")
                return
            if WarnIfBig == 1 and getsize(MainFilespec) > PROGBIG_ZCF:
                Answer = formMYD(Root,
                                 (("I'll Keep This In Mind", TOP, "ok"),
                                  ("I Know. Stop Bothering Me.", TOP, "fine"),
                                  ("Skip This File", TOP, "skip")),
                                 "stop", "YB", "Do You Like Coffee?",
                                 "The .zip file\n\n%s\n\nis quite large "
                                 "(check the size in the files list). LOGPEEK "
                                 "is not very fast. This will take a while to "
                                 "process." % MainFilespec)
                if Answer == "stop":
                    setMsg("MF", "", "Nothing done.")
                    progControl("stopped")
                    return
                if Answer == "skip":
                    continue
                if Answer == "fine":
                    PROGWarnIfBigCVar.set(0)
                    WarnIfBig = 0
            Ret = rt130ExtractZCFData(PROGDataDirVar.get(), MainFilename,
                                      PROGStopBut, PROGRunning, "MF", OnlySOH,
                                      IncDS9, IncRAWTPS, DS9s, DSsButts,
                                      FromDate, ToDate)
            if Ret[0] != 0:
                setMsg("MF", Ret)
                setMsg("INFO", "", "See status message.")
                progControl("stopped")
                return
            LOGLines.append("%s:  Version Number  %s  File: %s" %
                            (PROG_NAMELC, PROG_VERSION, MainFilename))
            LOGLines += Ret[1]
        # ---- Unzipped CF source ----
        elif MainFilenameLC.find(".cf") != -1:
            # The MainFilename will be Cf.cf(sep)DAS, so split those and build
            # the right things to pass. More than one DAS will be allowed in
            # each .cf directory, but only the one selected by the user will
            # be handled.
            Dir = PROGDataDirVar.get() + MainFilename.split(sep)[0] + sep
            if exists(Dir) is False:
                # [:-1] drops the sep.
                setMsg("MF", "RW", "File does not exist:\n   %s" % Dir[:-1], 2)
                progControl("stopped")
                return
            if WarnIfBig == 1 and getFolderSize(Dir) > PROGBIG_UCF:
                Answer = formMYD(Root,
                                 (("I'll Keep This In Mind", TOP, "ok"),
                                  ("I Know. Stop Bothering Me.", TOP, "fine"),
                                  ("Skip This File", TOP, "skip")),
                                 "stop", "YB", "Do You Like Coffee?",
                                 "The .cf directory\n\n%s\n\nis quite large "
                                 "(check the size in the files list). LOGPEEK "
                                 "is not very fast. This will take a while to "
                                 "process." % Dir)
                if Answer == "stop":
                    setMsg("MF", "", "Nothing done.")
                    progControl("stopped")
                    return
                if Answer == "skip":
                    continue
                if Answer == "fine":
                    PROGWarnIfBigCVar.set(0)
                    WarnIfBig = 0
            try:
                DAS = MainFilename.split(sep)[1]
            except Exception:
                DAS = ""
            Ret = rt130ExtractUCFData(Dir, DAS, PROGStopBut, PROGRunning,
                                      "MF", OnlySOH, IncDS9, IncRAWTPS, DS9s,
                                      DSsButts, FromDate, ToDate)
            if Ret[0] != 0:
                setMsg("MF", Ret)
                setMsg("INFO", "", "See status message.")
                progControl("stopped")
                return
            LOGLines.append("%s:  Version Number  %s  File: %s" %
                            (PROG_NAMELC, PROG_VERSION, MainFilename))
            LOGLines += Ret[1]
# ---- .mslogs source ----
# These are the LOG channel miniseed files that you get from the DMC for RT130
# experiments. PASSCAL makes these "miniseed" semi-valid files when the data
# is sent in. These are just chunks of the ASCII text log files with a mseed
# header every 4K bytes.
        elif MainFilenameLC.endswith(".mslogs"):
            if exists(MainFilespec) is False:
                setMsg("MF", "RW", "Folder does not exist:\n   %s" %
                       MainFilespec, 2)
                progControl("stopped")
                return
# The LOG channel files are mostly ASCII text, so PROGBIG_LOG is close enough.
            FolderSize = getFolderSize(MainFilespec)
            if WarnIfBig == 1 and FolderSize > PROGBIG_LOG:
                Answer = formMYD(Root,
                                 (("I'll Keep This In Mind", TOP, "ok"),
                                  ("I Know. Stop Bothering Me.", TOP, "fine"),
                                  ("Skip This File", TOP, "skip")),
                                 "stop", "YB", "Do You Like Coffee?",
                                 "The contents of .mslogs folder\n\n%s\n\nis "
                                 "quite large at %s bytes. LOGPEEK is not "
                                 "very fast. This will take a while to "
                                 "process." % (MainFilespec, fmti(FolderSize)))
                if Answer == "stop":
                    setMsg("MF", "", "Nothing done.")
                    progControl("stopped")
                    return
                if Answer == "skip":
                    continue
                if Answer == "fine":
                    PROGWarnIfBigCVar.set(0)
                    WarnIfBig = 0
            if UseDates is True:
                formMYD(Root, (("(OK)", TOP, "ok"),),
                        "ok", "YB", "Not Gonna Do It.",
                        "Setting a date range for .mslogs folders has no "
                        "effect.")
            setMsg("MF", "CB", "Working...")
            # Get the list of files in the folder and go to town.
            MainFilespec = PROGDataDirVar.get() + MainFilename
            if MainFilespec.endswith(sep) is False:
                MainFilespec += sep
            Ret = readMSLOGSFiles(MainFilespec, PROGStopBut, PROGRunning)
            if Ret[0] != 0:
                setMsg("MF", Ret)
                setMsg("INFO", "", "See status message.")
                progControl("stopped")
                return
            LOGLines = Ret[1]
            LogFile = True
# ---- RT130 DAS ID ----
# The MainFilename is just the DAS ID. The directory to read is the one we are
# currently pointing to (PROGDataDirVar). Pass them together to fool the
# extraction function into thinking this is just a .cf "file" like above.
        elif len(MainFilename) == 4 and isHex(MainFilename):
            Ret = rt130ExtractUCFData(PROGDataDirVar.get(), MainFilename,
                                      PROGStopBut, PROGRunning, "MF", OnlySOH,
                                      IncDS9, IncRAWTPS, DS9s, DSsButts,
                                      FromDate, ToDate)
            if Ret[0] != 0:
                setMsg("MF", Ret)
                setMsg("INFO", "", "See status message.")
                progControl("stopped")
                return
            LOGLines.append("%s:  Version Number  %s  File: %s" %
                            (PROG_NAMELC, PROG_VERSION, MainFilename))
            LOGLines += Ret[1]
    LFrm.title("SOH Messages - %s" % MainFilename)
    setMsg("MF", "CB", "Processing read information...")
# We will skip lines until a "State of Health" line is found.
    Skip = True
# Stick the word "Skipped" in the SOH messages before the beginning of each
# chunk of skipped stuff section so that can be searched for.
# I don't want the first lines of the log lines to be "Skipped", because
# everything up to the first "SOH" line gets skipped, so set this to -1 until
# things get rolling then it will be set to True and False as needed.
    FirstSkip = -1
    TimingProblems = False
    ProblemTimes = []
    Skipped = 0
    Warnings = 0
    Errors = 0
# This is faster than doing the .get()
    OPTIgTEC = OPTIgTECVar.get()
    LastTime = -maxFloat
    TimeMin = maxFloat
    TimeMax = -maxFloat
    TheUnitID = ""
    TheLastUnitID = ""
    TheCPUVers = ""
    TheGPSvers = ""
# For the info in "INFO".
    UnitIDsRead = []
    CPUVersionsRead = []
    GPSVersionsRead = []
    TheLastDOY = 0
    TheYear = 1970
    InLine = ""
    TotalLines = len(LOGLines)
    Lines = 0
# This is the main LOG line reading loop. The lines are processed and put into
# the LOG form in here.
    setMsg("LOG", "CB", "Working on SOH lines...")
    for InLine in LOGLines:
        # Keep the user entertained.
        if Lines % 5000 == 0 and Lines != 0:
            setMsg("INFO", "", "Working...\nLines: %d of %d\nSkipped: %d" %
                   (Lines, TotalLines, Skipped))
# Gives the Stop Reading button a chance to be used.
            PROGStopBut.update()
            if PROGRunning.get() == 0:
                break
# Boy, I hate to spend the CPU cycles doing this, but...
# Any line with "bad" characters will not be processed at all.
        if isprintableInLine() is False:
            if FirstSkip is True:
                txLn("LOG", "", "Skipped")
                Lines += 1
                FirstSkip = False
            txLn("LOG", "R", InLine)
            Lines += 1
            Skipped += 1
            if Skipped % 100000 == 0:
                Answer = formMYD(Root,
                                 (("No, Stop", LEFT, "stop"),
                                  ("Yes, Continue", LEFT, "cont")),
                                 "cont", "YB", "What's Wrong With This File?",
                                 "%d lines have been skipped so far. Are you "
                                 "sure this is a valid information source "
                                 "file?" % Skipped)
                if Answer == "stop":
                    setMsg("INFO", "", "See status message.")
                    setMsg("MF", "YB", "Stopped.", 2)
                    setMsg("LOG", "YB", "Stopped.", 2)
                    progControl("stopped")
                    return
            continue
# SOH lines will be used to get the year of the data, and will be used to
# set a flag that will determine if anything will be processed or not:
#   - Nothing will be processed until one of these lines are found
#   - If the time in any line jumps backwards (enough) then no lines will be
#     processed until a 'good' SOH line is found whose time is later than
#     the last time the loop remembers, UNLESS OPTIgTEC != 0 then all lines
#     will be processed no matter how messed up the times are.
# If the first SOH line is really screwed up (like with a large year value)
# then the file may not even be able to be processed without turning on the
# Plot Timing Problems option.
        if InLine.startswith("State of Health"):
            Time = eatSOH()
            if Time >= LastTime or OPTIgTEC == 1:
                # DEBUG
                #                LTxt.insert(END, "%d: %s\n"%(Lines, InLine))
                LTxt.insert(END, "%s\n" % InLine)
                Lines += 1
                SOH.append((Lines, Time))
                if Time < LastTime:
                    TimingProblems = True
                    ProblemTimes.append(dt2Time(-1, 11, LastTime)[:-4])
                LastTime = Time
                Skip = False
                FirstSkip = True
                if TheUnitID not in UnitIDsRead:
                    UnitIDsRead.append(TheUnitID)
                continue
            else:
                if OPTIgTEC == 0:
                    Skip = True
# Event lines are not mixed into the SOH messages in the right place in time
# depending on the information source, so they can get skipped when we are in
# the process of skipping regular SOH messages. Treat them special (basically
# always plot them). This looks for FST, but eatEVT() actually reads the TT
# time.
        elif InLine.find("FST =") != -1:
            Info = eatEVT(DSsButts)
# Item 0 should be the time, so it should always be more than zero unless we
# get some really old data.
            if Info[0] > 0:
                Lines += 1
                EVT.append((Lines, Info[0], Info[1]))
# DEBUG
#                LTxt.insert(END, "%d: %s\n"%(Lines, InLine))
                LTxt.insert(END, "%s\n" % InLine)
# These are event lines that we are not interested in (the DS checkbuttons).
            elif Info[0] == 0:
                txLn("LOG", "Y", InLine)
                Lines += 1
            else:
                # Bad EVT lines wil probably only be one or so in a row, so
                # we'll just handle everything here and not worry about the
                # Skip and FirstSkip flags.
                # DEBUG
                #                LTxt.insert(END, "%d: Skipped\n"%Lines)
                LTxt.insert(END, "Skipped\n")
# DEBUG
#                txLn("LOG", "Y", "%d: %s"%(Lines, InLine))
                txLn("LOG", "Y", InLine)
                Lines += 1
                Skipped += 1
            continue
# LPMP lines are special and always at the end of the message lines in a .log
# file.
        if Skip is False or InLine.startswith("LPMP"):
            # DEBUG
            #            LTxt.insert(END, "%d: %s\n"%(Lines, InLine))
            LTxt.insert(END, "%s\n" % InLine)
            Lines += 1
        else:
            if FirstSkip is True:
                # DEBUG
                #                LTxt.insert(END, "%d: Skipped\n"%Lines)
                LTxt.insert(END, "Skipped\n")
                Lines += 1
                FirstSkip = False
# This may or may not be true, but it's the best we can do.
                TimingProblems = True
                ProblemTimes.append(dt2Time(-1, 11, LastTime)[:-4])
# DEBUG
#            txLn("LOG", "Y", "%d: %s"%(Lines, InLine))
            txLn("LOG", "Y", InLine)
            Lines += 1
            Skipped += 1
            if Skipped % 100000 == 0:
                Answer = formMYD(Root, (("No, Stop", LEFT, "stop"),
                                        ("Yes, Continue", LEFT, "cont")),
                                 "cont", "YB", "What's Wrong With This File?",
                                 "%d lines have been skipped so far. Are you "
                                 "sure this is a valid information source "
                                 "file?" % Skipped)
                if Answer == "stop":
                    setMsg("INFO", "", "See status message.")
                    setMsg("MF", "YB", "Stopped.", 2)
                    progControl("stopped")
                    return
            continue
# Now that InLine has been written to where ever it needs to be displayed upper
# it so we are immune to Reftek's grammer changes.
        InLine = InLine.upper()
# 72, 130.
        if InLine.find("INTERNAL CLOCK PHASE ERROR") != -1:
            Info = eatICPE()
            ICPE.append((Lines, Info[0], Info[1]))
# 72, 130.
        elif InLine.find("EXTERNAL CLOCK IS UNLOCKED") != -1:
            GPSLK.append((Lines, eatSIMPLE(), 0))
# 72, 130.
        elif InLine.find("EXTERNAL CLOCK IS LOCKED") != -1:
            GPSLK.append((Lines, eatSIMPLE(), 1))
# 72, 130.
        elif InLine.find("POSSIBLE DISCREPANCY") != -1:
            DISCREP.append((Lines, eatSIMPLE(), 1))
# 130 - started somewhere around v2.3.1.
        elif InLine.find("EXTERNAL CLOCK POWER IS TURNED ON") != -1:
            GPS.append((Lines, eatSIMPLE(), 1))
# 130 - started somewhere around v2.3.1.
        elif InLine.find("EXTERNAL CLOCK POWER IS TURNED OFF") != -1:
            GPS.append((Lines, eatSIMPLE(), 0))
# 72, 130.
        elif InLine.find("BATTERY VOLTAGE") != -1:
            Info = eatBATMP()
            VOLT.append((Lines, Info[0], Info[1]))
            TEMP.append((Lines, Info[0], Info[2]))
            if Info[3] == 3.3:
                BKUP.append((Lines, Info[0], Info[3], 1))
            elif Info[3] >= 3.0:
                BKUP.append((Lines, Info[0], Info[3], 2))
            else:
                BKUP.append((Lines, Info[0], Info[3], 3))
# 130.
        elif InLine.find("DISK") != -1:
            # There are several lines with "DISK" in them.
            if InLine.find("USED:") != -1:
                Info = eatDISK()
                if Info[1] == 1:
                    DU1.append((Lines, Info[0], Info[2]))
                elif Info[1] == 2:
                    DU2.append((Lines, Info[0], Info[2]))
# 130 ~3.4.5
        elif InLine.find("EXTERNAL CLOCK ERROR") != -1:
            GPSERR.append((Lines, eatSIMPLE(), 0))
# 72, 130.
        elif InLine.find("ERROR:") != -1:
            # These lines are generated by programs like ref2segy and do not
            # have any time associated with them so just use whatever time was
            # last in Time.
            ERROR.append((Lines, Time, 1))
            Errors += 1
# 72, 130. Warings come in lines with the time from the RT130 and lines
# created by programs without times.
        elif InLine.find("WARNING") != -1:
            if rtnPattern(InLine[:12]) == "000:00:00:00":
                WARN.append((Lines, eatSIMPLE(), 1))
            else:
                # Just use whatever time was last in Time. The 2 means that the
                # dots will be a little different.
                WARN.append((Lines, Time, 2))
            Warnings += 1
# 130.
# Has always been upper case.
        elif InLine.find("BAD") != -1:
            # Different case in different versions. All upper is the latest.
            if InLine.find("MARKER") != -1 or InLine.find("Marker") != -1:
                WARN.append((Lines, eatSIMPLE(), 1))
                Warnings += 1
# 72, 130.
        elif InLine.find("AUTO DUMP CALLED") != -1:
            DUMP.append((Lines, eatSIMPLE(), 1))
# 72, 130.
        elif InLine.find("AUTO DUMP COMPLETE") != -1:
            DUMP.append((Lines, eatSIMPLE(), 0))
# 72, 130.
        elif InLine.find("DSP CLOCK SET") != -1:
            JERK.append((Lines, eatSIMPLE(), 1))
# 72, 130.
        elif InLine.find("DSP CLOCK DIFFERENCE") != -1:
            Info = eatDCDIFF()
            DCDIFF.append((Lines, Info[0], Info[1]))
# 72, 130.
        elif InLine.find("TIME JERK") != -1 and InLine.find("OCCURRED") != -1:
            JERK.append((Lines, eatSIMPLE(), 0))
# 130.
        elif InLine.find("JERK") != -1:
            JERK.append((Lines, eatSIMPLE(), 0))
# 72, 130.
        elif InLine.find("ACQUISITION STARTED") != -1:
            ACQ.append((Lines, eatSIMPLE(), 1))
# 72, 130.
        elif InLine.find("ACQUISITION STOPPED") != -1:
            ACQ.append((Lines, eatSIMPLE(), 0))
# 72, 130.
# 2010-Uppering InLine started here. Version 3.0.0 went to all uppercase msgs.
        elif InLine.find("NETWORK LAYER IS UP") != -1:
            NET.append((Lines, eatSIMPLE(), 1))
# 72, 130.
        elif InLine.find("NETWORK LAYER IS DOWN") != -1:
            NET.append((Lines, eatSIMPLE(), 0))
# 72, 130.
        elif InLine.find("STATION CHANNEL DEFINITION") != -1:
            DEFS.append((Lines, eatDEFS()))
# 72?, 130.
        elif InLine.find("MASS RE-CENTER") != -1:
            MRC.append((Lines, eatSIMPLE()))
# FINISHME - mass position kludge stuff. Will need another one of these to read
# whatever Reftek ends up putting in. (If they ever do. 2013FEB18) (If they
# ever do. 2018DEC14)
# 130.
# The lines are: LPMP YYYY:DOY:HH:MM:SS Chan Value
        elif InLine.startswith("LPMP"):
            Parts = InLine.split()
            Epoch = dt2Time(11, -1, Parts[1])
            Chan = intt(Parts[2])
            Value = floatt(Parts[3])
            ColorCode = rt130MPDecodeGetColorCode(Value)
            eval("MP%d" % Chan).append((Epoch, Value, ColorCode))
# 72?, 130 - system reset command.
        elif InLine.find("SYSTEM RESET") != -1:
            RESET.append((Lines, eatSIMPLE()))
# Just 130, I think.
        elif InLine.find("FORCE RESET") != -1:
            RESET.append((Lines, eatSIMPLE()))
        elif InLine.find("SYSTEM POWERUP") != -1:
            PWRUP.append((Lines, eatSIMPLE()))
# The firmware version info can be in either of these two lines depending on
# the model and the version of firmware and the day of the week (it changed
# several times on RT130s).
        elif InLine.find("REF TEK") != -1:
            # This will set TheCPUVers.
            eatCPUV()
            if TheCPUVers not in CPUVersionsRead:
                CPUVersionsRead.append(TheCPUVers)
        elif InLine.find("CPU SOFTWARE") != -1:
            eatCPUV()
            if TheCPUVers not in CPUVersionsRead:
                CPUVersionsRead.append(TheCPUVers)
        elif InLine.find("EXTERNAL CLOCK CYCLE") != -1:
            CLKPWR.append((Lines, eatSIMPLE(), 1))
# 130. Used to just be AUTO DUMP FAILED.
        elif InLine.find("FAILED") != -1:
            ERROR.append((Lines, eatSIMPLE(), 1))
# 130.
        elif InLine.find("ERTFS") != -1:
            WARN.append((Lines, eatSIMPLE(), 1))
# 130.
        elif InLine.find("IDE BUSY") != -1:
            WARN.append((Lines, eatSIMPLE(), 1))
# 130.
        elif InLine.find("NO VALID DISK") != -1:
            ERROR.append((Lines, eatSIMPLE(), 1))
# 130 ~>vers 3.4.4.
        elif InLine.find("GPS FIRMWARE VERSION:") != -1:
            eatGPSV()
            if TheGPSvers not in GPSVersionsRead:
                GPSVersionsRead.append(TheGPSvers)
# 130-before v2.3.1 to 3.2.2. Gone in 3.4.5.
        elif InLine.find("EXTERNAL CLOCK WAKEUP") != -1:
            GPS.append((Lines, eatSIMPLE(), 1))
# 130-before v2.3.1 to 3.2.2. Gone in 3.4.5.
        elif InLine.find("EXTERNAL CLOCK SLEEP") != -1:
            GPS.append((Lines, eatSIMPLE(), 0))
# 72.
        elif InLine.find("GPS: POWER IS TURNED ON") != -1:
            GPS.append((Lines, eatSIMPLE(), 1))
# 72.
        elif InLine.find("GPS: POWER IS TURNED OFF") != -1:
            GPS.append((Lines, eatSIMPLE(), 0))
# 72.
        elif InLine.find("GPS: POWER IS CONTINUOUS") != -1:
            CLKPWR.append((Lines, eatSIMPLE(), 1))
# 72.
        elif InLine.find("NO EXTERNAL CLOCK") != -1:
            GPSERR.append((Lines, eatSIMPLE(), 0))
# 72.
        elif InLine.find("GPS: V") != -1:
            eatGPSV()
            if TheGPSvers not in GPSVersionsRead:
                GPSVersionsRead.append(TheGPSvers)
# FINISHME - 2010SEP07, 2018DEC14 (still the same)
# This whole mass position thing is really kludgy, but here goes...
# If the mass positions were extracted from the binary data then len(MPx) will
# not be 0. In that case add strange lines to the end of the regular log lines
# that will not get plotted in the usual way, but will get plotted if the
# user saves the log lines to a .log file and sends it in to PIC. Once Reftek
# starts putting the mass positions into the regular SOH messages then LOGPEEK
# will be changed to start plotting them, instead.
# Now if a .log file was read then there may or may not be any mass positions
# (yes if Reftek puts them in, no if not), so don't add them again (MPx will
# contain the info taken from them and not from the DS9 packets).
    if LogFile is False and OPTAddMPMsgsCVar.get() == 1:
        for i in arange(1, 7):
            MPData = eval("MP%d" % i)
            if len(MPData) > 0:
                for Point in MPData:
                    LTxt.insert(END, "LPMP %s %d %.1f\n" %
                                (dt2Time(-1, 11, Point[0]), i, Point[1]))
    setMsg("LOG", "")
    PROGStopBut.update()
    if PROGRunning.get() == 0:
        setMsg("MF", "YB", "Reading stopped.", 2)
        setMsg("INFO", "", "")
        progControl("stopped")
        return
# Get the max time range of the plots, and the other info to display if we
# finished the file.
    FStart = TimeMin
    CurMainStart = FStart
    FEnd = TimeMax
    CurMainEnd = FEnd
# Clear out any previously saved time ranges.
    del StartEndRecord[:]
# If there are no SOH messages then there isn't anything to plot.
    if len(SOH) == 0:
        if UseDates is False:
            setMsg("MF", "YB", "No SOH messages found.", 2)
        else:
            setMsg("MF", "YB",
                   "No SOH messages found. Could be the date range.", 2)
# This is a weird one, and this is the only bad way to fix it. If the very
# first file the user reads after starting the program is garbage, and no
# SOH lines are found they will get an error, but the LOG window will not
# show up, but the garbage will have been loaded into the LOG Text(). Then
# if they read a good file next the garbage will still be in there, so just
# try and clear the LOG Text() no matter what, and don't first check to see
# if the LOG form exists (is not None).
        txLn("LOG", "", "", False)
        setMsg("INFO", "", "File: " + MainFilename)
    else:
        # Fill in the Info box with info about the file.
        FS = dt2Time(-1, 80, FStart)[:-4]
        FE = dt2Time(-1, 80, FEnd)[:-4]
# This will only be the last sample rate for each stream which will be OK for
# the most part.
        DSMsg = "DS Sample Rates:\n"
        DSMsg2 = ""
        for DS in arange(1, PROG_DSS + 1):
            try:
                SR = SampleRates[DS]
                DSMsg2 += "  DS%d: %dsps\n" % (DS, SR)
            except KeyError:
                pass
        if len(DSMsg2) == 0:
            DSMsg2 = "  See SOH messages."
# Remove possible extra \n with rstrip().
        DSMsg += DSMsg2.rstrip()
        if RTModel == "RT130":
            setMsg("INFO", "", "File(s):\n  " + list2Str(FilenamesRead,
                                                         "\n  ") +
                   "\nDAS(s):\n  " + list2Str(UnitIDsRead, "\n  ") +
                   "\nCPU FW Version(s):\n  " + list2Str(CPUVersionsRead,
                                                         "\n  ", False, True) +
                   "\nGPS FW Version(s):\n  " + list2Str(GPSVersionsRead,
                                                         "\n  ", False, True) +
                   "\n%s" % DSMsg +
                   "\nSOH lines: " + str(Lines) +
                   "\nERROR lines: " + str(Errors) +
                   "\nWARNING lines: " + str(Warnings) +
                   "\nLines skipped: " + str(Skipped) +
                   "\nFile start: " + FS +
                   "\nFile end: " + FE)
        elif RTModel == "72A":
            setMsg("INFO", "", "File(s):\n  " + list2Str(FilenamesRead,
                                                         "\n  ") +
                   "\nDAS(s):\n  " + list2Str(UnitIDsRead, "\n  ") +
                   "\nCPU FW Version(s):\n  " + list2Str(CPUVersionsRead,
                                                         "\n  ", False, True) +
                   "\nGPS FW Version(s):\n  " + list2Str(GPSVersionsRead,
                                                         "\n  ", False, True) +
                   "\n%s" % DSMsg +
                   "\nSOH lines: " + str(Lines) +
                   "\nERROR lines: " + str(Errors) +
                   "\nWARNING lines: " + str(Warnings) +
                   "\nLines skipped: " + str(Skipped) +
                   "\nFile start: " + FS +
                   "\nFile end: " + FE)
# Make the calls to draw all of the plots. Keep the message from plotMF()
# until we are finished with the raw plots.
        Msge = plotMF(None)
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            setMsg("INFO", "", "")
            setMsg("MF", "YB", "Stopped.", 2)
            progControl("stopped")
            return
        plotRAW()
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            setMsg("RAW", "YB", "Stopped.")
            setMsg("INFO", "", "")
            setMsg("MF", "YB", "Stopped.", 2)
            progControl("stopped")
            return
        plotTPS()
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            setMsg("TPS", "YB", "Stopped.")
            setMsg("INFO", "", "")
            setMsg("MF", "YB", "Stopped.", 2)
            progControl("stopped")
            return
        if TimingProblems is False:
            setMsg("MF", "WB", Msge)
        else:
            ProblemTimes = list2Str(ProblemTimes[:5])
            Msge += \
                "\nPossible timing problems at approx (up to 5 listed): %s" % \
                ProblemTimes
            setMsg("MF", "YB", Msge)
    progControl("stopped")
    return
# END: fileSelected


###################
# BEGIN: floatt(In)
# LIB:floatt():2018.256logpeek
# FOR LOGPEEK: No .strip() check. Skips over " " in In.
#    Handles all of the annoying shortfalls of the float() function (vs C).
#    Does not handle scientific notation numbers.
def floatt(In):
    In = str(In)
# At least let the system give it the ol' college try.
    try:
        return float(In)
    except Exception:
        Number = ""
        for c in In:
            if c.isdigit() or c == ".":
                Number += c
            elif (c == "-" or c == "+") and len(Number) == 0:
                Number += c
            elif c == "," or c == " ":
                continue
            else:
                break
        try:
            return float(Number)
        except ValueError:
            return 0.0
# END: floatt


####################
# BEGIN: fmti(Value)
# LIB:fmti():2018.235
#   Just couldn't rely on the system to do this, although this won't handle
#   European-style numbers.
def fmti(Value):
    Value = int(Value)
    if Value > -1000 and Value < 1000:
        return str(Value)
    Value = str(Value)
    NewValue = ""
# There'll never be a + sign.
    if Value[0] == "-":
        Offset = 1
    else:
        Offset = 0
    CountDigits = 0
    for i in arange(len(Value) - 1, -1 + Offset, -1):
        NewValue = Value[i] + NewValue
        CountDigits += 1
        if CountDigits == 3 and i != 0:
            NewValue = "," + NewValue
            CountDigits = 0
    if Offset != 0:
        if NewValue.startswith(","):
            NewValue = NewValue[1:]
        NewValue = Value[0] + NewValue
    return NewValue
# END: fmti


#################################
# BEGIN: formABOUT(Parent = Root)
# LIB:formABOUT():2018.324
def formABOUT(Parent=Root):
    # For the versions info and calling __X functions.
    import tkinter as tkntr
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    Message = "%s (%s)\n   " % (PROG_LONGNAME, PROG_NAME)
    Message += "Version %s\n" % PROG_VERSION
    Message += "%s\n" % "PASSCAL Instrument Center"
    Message += "%s\n\n" % "Socorro, New Mexico USA"
    Message += "%s\n" % "Email: passcal@passcal.nmt.edu"
    Message += "%s\n" % "Phone: 575-835-5070"
    Message += "\n"
    Message += "--- Configuration ---\n"
    Message += \
        "%s %dx%d\nProportional font: %s (%d)\nFixed font: %s (%d)\n" % \
        (PROGSystemName, PROGScreenWidthNow, PROGScreenHeightNow,
         PROGPropFont["family"], PROGPropFont["size"],
         PROGMonoFont["family"], PROGMonoFont["size"])
    # Some programs don't care about the geometry of the main display.
    try:
        # If the variable has something in it get the current geometry. The
        # variable value may still be the initial value from when the program
        # was started.
        if len(PROGGeometryVar.get()) != 0:
            Message += "\n"
            Message += "Geometry: %s\n" % Root.geometry()
    except Exception:
        pass
    # Some don't have setup files.
    try:
        if len(PROGSetupsFilespec) != 0:
            Message += "\n"
            Message += "--- Setups File ---\n"
            Message += "%s\n" % PROGSetupsFilespec
    except Exception:
        pass
    # This is only for QPEEK.
    try:
        if len(ChanPrefsFilespec) != 0:
            Message += "\n"
            Message += "--- Channel Preferences File ---\n"
            Message += "%s\n" % ChanPrefsFilespec
    except Exception:
        pass
    Message += "\n"
    Message += "--- Versions ---\n"
    Message += "Python: %s\n" % PROG_PYVERSION
    Message += "Tcl/Tk: %s/%s\n" % (tkntr.TclVersion, tkntr.TkVersion)
    try:
        Message += "pySerial: %s" % SerialVERSION
    except Exception:
        pass
# Some do, some don't have this.
    try:
        if len(PROG_LASTWORK) != 0:
            Message += "\n"
            Message += "--- Last Worked On ---\n"
            Message += "%s\n" % PROG_LASTWORK
    except Exception:
        pass
    formMYD(Parent, (("(OK)", LEFT, "ok"), ), "ok", "WB", "About",
            Message.strip())
    return
# END: formABOUT


################################################################
# BEGIN: formCAL(Parent, Months = 3, Show = True, AllowX = True,
#                OrigFont = False, CloseQuit = "close")
# LIB:formCAL():2019.058
#   Displays a 1 or 3-month calendar with dates and day-of-year numbers.
#   Pass in 1 or 3 as desired.
#   Show = Should the form show itself or no? (True/False)
#   AllowX = Should the [X] button be allowed for closing? (True/False)
#   OrigFont = False allows the font to change if the program has Font BIGGER,
#              and Font smaller functionality, otherwise the font is stuck
#              with the original font in use when the program started.
#   CloseQuit = "close" shows a Close button for the form, "quit" shows a
#               Quit button and calls progQuitter(True) when pressed.
PROGFrm["CAL"] = None
CALTmModeRVar = StringVar()
CALTmModeRVar.set("lt")
CALDtModeRVar = StringVar()
CALDtModeRVar.set("dates")
PROGSetups += ["CALTmModeRVar", "CALDtModeRVar"]
CALYear = 0
CALMonth = 0
CALText1 = None
CALText2 = None
CALText3 = None
CALMonths = 3
CALTipLastValue = ""


def formCAL(Parent, Months=3, Show=True, AllowX=True, OrigFont=False,
            CloseQuit="close"):
    global CALText1
    global CALText2
    global CALText3
    global CALMonths
    global CALTipLastValue
    CALTipLastValue = ""
    if showUp("CAL"):
        return
    CALMonths = Months
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    LFrm = PROGFrm["CAL"] = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    if AllowX is True:
        if CloseQuit == "close":
            LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "CAL"))
        elif CloseQuit == "quit":
            LFrm.protocol("WM_DELETE_WINDOW", Command(progQuitter, True))
    if CALTmModeRVar.get() == "gmt":
        LFrm.title("Calendar (GMT)")
    elif CALTmModeRVar.get() == "lt":
        GMTDiff = getGMT(20)
        LFrm.title("Calendar (LT: GMT%+.2f hours)" % (float(GMTDiff) / 3600.0))
    LFrm.iconname("Cal")
    Sub = Frame(LFrm)
    if CALMonths == 3:
        if OrigFont is False:
            CALText1 = Text(Sub, bg=Clr["D"], fg=Clr["B"], height=11,
                            width=29, relief=SUNKEN,
                            cursor=Button().cget("cursor"), state=DISABLED)
        else:
            CALText1 = Text(Sub, bg=Clr["D"], fg=Clr["B"], height=11,
                            width=29, relief=SUNKEN, font=PROGOrigMonoFont,
                            cursor=Button().cget("cursor"), state=DISABLED)
        CALText1.pack(side=LEFT, padx=7)
    if OrigFont is False:
        CALText2 = Text(Sub, bg=Clr["W"], fg=Clr["B"], height=11,
                        width=29, relief=SUNKEN,
                        cursor=Button().cget("cursor"), state=DISABLED)
    else:
        CALText2 = Text(Sub, bg=Clr["W"], fg=Clr["B"], height=11,
                        width=29, relief=SUNKEN, font=PROGOrigMonoFont,
                        cursor=Button().cget("cursor"), state=DISABLED)
    CALText2.pack(side=LEFT, padx=7)
    if CALMonths == 3:
        if OrigFont is False:
            CALText3 = Text(Sub, bg=Clr["D"], fg=Clr["B"], height=11,
                            width=29, relief=SUNKEN,
                            cursor=Button().cget("cursor"), state=DISABLED)
        else:
            CALText3 = Text(Sub, bg=Clr["D"], fg=Clr["B"], height=11,
                            width=29, relief=SUNKEN, font=PROGOrigMonoFont,
                            cursor=Button().cget("cursor"), state=DISABLED)
        CALText3.pack(side=LEFT, padx=7)
    Sub.pack(side=TOP, padx=3, pady=3)
    if CALYear != 0:
        formCALMove("c")
    else:
        formCALMove("n")
    Sub = Frame(LFrm)
    BButton(Sub, text="<<", command=Command(formCALMove,
                                            "-y")).pack(side=LEFT)
    BButton(Sub, text="<", command=Command(formCALMove,
                                           "-m")).pack(side=LEFT)
    BButton(Sub, text="Today", command=Command(formCALMove,
                                               "n")).pack(side=LEFT)
    BButton(Sub, text=">", command=Command(formCALMove,
                                           "+m")).pack(side=LEFT)
    BButton(Sub, text=">>", command=Command(formCALMove,
                                            "+y")).pack(side=LEFT)
    Sub.pack(side=TOP, padx=3, pady=3)
    Sub = Frame(LFrm)
    SSub = Frame(Sub)
    SSSub = Frame(SSub)
    LRb = Radiobutton(SSSub, text="GMT", value="gmt",
                      variable=CALTmModeRVar,
                      command=Command(formCALMove, "c"))
    LRb.pack(side=LEFT)
    ToolTip(LRb, 35, "Use what appears to be this computer's GMT time.")
    LRb = Radiobutton(SSSub, text="LT", value="lt",
                      variable=CALTmModeRVar,
                      command=Command(formCALMove, "c"))
    LRb.pack(side=LEFT)
    ToolTip(LRb, 35, "Use what appears to be this computer's time and time "
            "zone setting.")
    SSSub.pack(side=TOP, anchor="w")
    SSub.pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    SSub = Frame(Sub)
    LRb = Radiobutton(SSub, text="Dates", value="dates",
                      variable=CALDtModeRVar,
                      command=Command(formCALMove, "c"))
    LRb.pack(side=TOP, anchor="w")
    ToolTip(LRb, 35, "Show the calendar dates.")
    LRb = Radiobutton(SSub, text="DOY", value="doy",
                      variable=CALDtModeRVar,
                      command=Command(formCALMove, "c"))
    LRb.pack(side=TOP, anchor="w")
    ToolTip(LRb, 35, "Show the day-of-year numbers.")
    SSub.pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    if CloseQuit == "close":
        BButton(Sub, text="Close", fg=Clr["R"],
                command=Command(formClose, "CAL")).pack(side=LEFT)
    elif CloseQuit == "quit":
        BButton(Sub, text="Quit", fg=Clr["R"],
                command=Command(progQuitter, True)).pack(side=LEFT)
    Sub.pack(side=TOP, padx=3, pady=3)
    if Show is True:
        center(Parent, LFrm, "CX", "I", True)
    return
####################################
# BEGIN: formCALMove(What, e = None)
# FUNC:formCALMove():2018.235
#   Handles changing the calendar form's display.


def formCALMove(What, e=None):
    global CALYear
    global CALMonth
    global CALText1
    global CALText2
    global CALText3
    PROGFrm["CAL"].focus_set()
    DtMode = CALDtModeRVar.get()
    Year = CALYear
    Month = CALMonth
    if What == "-y":
        Year -= 1
    elif What == "-m":
        Month -= 1
    elif What == "n":
        if CALTmModeRVar.get() == "gmt":
            Year, Month, Day = getGMT(4)
        elif CALTmModeRVar.get() == "lt":
            Year, DOY, HH, MM, SS = getGMT(11)
            GMTDiff = getGMT(20)
            if GMTDiff != 0:
                Year, Month, Day, DOY, HH, MM, SS = dt2TimeMath(0, GMTDiff,
                                                                Year, 0, 0,
                                                                DOY, HH, MM,
                                                                SS)
    elif What == "+m":
        Month += 1
    elif What == "+y":
        Year += 1
    elif What == "c":
        if CALTmModeRVar.get() == "gmt":
            PROGFrm["CAL"].title("Calendar (GMT)")
        elif CALTmModeRVar.get() == "lt":
            GMTDiff = getGMT(20)
            PROGFrm["CAL"].title("Calendar (LT: GMT%+.2f hours)" %
                                 (float(GMTDiff) / 3600.0))
    if Year < 1971:
        beep(1)
        return
    elif Year > 2050:
        beep(1)
        return
    if Month > 12:
        Year += 1
        Month = 1
    elif Month < 1:
        Year -= 1
        Month = 12
    CALYear = Year
    CALMonth = Month
# Only adjust this back one month if we are showing all three months.
    if CALMonths == 3:
        Month -= 1
    if Month < 1:
        Year -= 1
        Month = 12
    for i in arange(0, 0 + 3):
        # Skip the first and last months if we are only showing one month.
        if CALMonths == 1 and (i == 0 or i == 2):
            continue
        LTxt = eval("CALText%d" % (i + 1))
        LTxt.configure(state=NORMAL)
        LTxt.delete("0.0", END)
        LTxt.tag_delete(*LTxt.tag_names())
        DOM1date = 0
        DOM1doy = PROG_FDOM[Month]
        if (Year % 4 == 0 and Year % 100 != 0) or Year % 400 == 0:
            if Month > 2:
                DOM1doy += 1
        if i == 1:
            LTxt.insert(END, "\n")
            LTxt.insert(END, "%s" % (PROG_CALMON[Month] + " " +
                                     str(Year)).center(29))
            LTxt.insert(END, "\n\n")
            IdxS = LTxt.index(CURRENT)
            LTxt.tag_config(IdxS, background=Clr["W"], foreground=Clr["R"])
            LTxt.insert(END, " Sun ", IdxS)
            IdxS = LTxt.index(CURRENT)
            LTxt.tag_config(IdxS, background=Clr["W"], foreground=Clr["U"])
            LTxt.insert(END, "Mon Tue Wed Thu Fri", IdxS)
            IdxS = LTxt.index(CURRENT)
            LTxt.tag_config(IdxS, background=Clr["W"], foreground=Clr["R"])
            LTxt.insert(END, " Sat", IdxS)
            LTxt.insert(END, "\n")
        else:
            LTxt.insert(END, "\n")
            LTxt.insert(END, "%s" % (PROG_CALMON[Month] + " " +
                                     str(Year)).center(29))
            LTxt.insert(END, "\n\n")
            LTxt.insert(END, " Sun Mon Tue Wed Thu Fri Sat")
            LTxt.insert(END, "\n")
        All = monthcalendar(Year, Month)
        if CALTmModeRVar.get() == "gmt":
            NowYear, NowMonth, NowDay = getGMT(4)
        elif CALTmModeRVar.get() == "lt":
            # Do this so NowDay gets set. It may not get altered below.
            NowYear, NowMonth, NowDay = getGMT(18)
            NowYear, DOY, HH, MM, SS = getGMT(11)
            GMTDiff = getGMT(20)
            NowDay = 0
            if GMTDiff != 0:
                NowYear, NowMonth, NowDay, DOY, HH, MM, \
                    SS = dt2TimeMath(0, GMTDiff, NowYear, 0, 0, DOY, HH, MM,
                                     SS)
        if DtMode == "dates":
            TargetDay = DOM1date + NowDay
        elif DtMode == "doy":
            TargetDay = DOM1doy + NowDay
        for Week in All:
            LTxt.insert(END, " ")
            for DD in Week:
                if DD != 0:
                    if DtMode == "dates":
                        ThisDay = DOM1date + DD
                        ThisDay1 = DOM1date + DD
                        ThisDay2 = DOM1doy + DD
                    elif DtMode == "doy":
                        ThisDay = DOM1doy + DD
                        ThisDay1 = DOM1doy + DD
                        ThisDay2 = DOM1date + DD
                    IdxS = LTxt.index(CURRENT)
                    if ThisDay == TargetDay and Month == NowMonth and \
                            Year == NowYear:
                        LTxt.tag_config(IdxS, background=Clr["C"],
                                        foreground=Clr["B"])
                    if DtMode == "dates":
                        LTxt.tag_bind(IdxS, "<Enter>",
                                      Command(formCALStartTip, LTxt,
                                              "%s/%03d" %
                                              (ThisDay1, ThisDay2)))
                    elif DtMode == "doy":
                        LTxt.tag_bind(IdxS, "<Enter>",
                                      Command(formCALStartTip, LTxt,
                                              "%03d/%s" %
                                              (ThisDay1, ThisDay2)))
                    LTxt.tag_bind(IdxS, "<Leave>", formCALHideTip)
                    LTxt.tag_bind(IdxS, "<ButtonPress>", formCALHideTip)
                    if DtMode == "dates":
                        if ThisDay < 10:
                            LTxt.insert(END, "  ")
                        else:
                            LTxt.insert(END, " ")
                        LTxt.insert(END, "%d" % ThisDay, IdxS)
                    elif DtMode == "doy":
                        LTxt.insert(END, "%03d" % ThisDay, IdxS)
                    LTxt.insert(END, " ")
                else:
                    LTxt.insert(END, "    ")
            LTxt.insert(END, "\n")
        LTxt.configure(state=DISABLED)
        Month += 1
        if Month > 12:
            Year += 1
            Month = 1
    return


#################################################
# BEGIN: formCALStartTip(Parent, Value, e = None)
# FUNC:formCALStartTip():2011.110
#   Pops up a "tool tip" when mousing over the dates of Date/DOY.
CALTip = None


def formCALStartTip(Parent, Value, e=None):
    # Multiple <Entry> events can be generated just by moving the cursor
    # around.
    # If we are still in the same date number just return.
    if Value == CALTipLastValue:
        return
    formCALHideTip()
    formCALShowTip(Parent, Value)
    return
######################################
# BEGIN: formCALShowTip(Parent, Value)
# FUNC:formCALShowTip():2019.058


def formCALShowTip(Parent, Value):
    global CALTip
    global CALTipLastValue
# The tooltips were not working under Python 3 and lesser versions of Tkinter
# until I added the .update(). Then AttributeErrors started showing up. So
# just try everything and hid the tip on any error.
    try:
        CALTip = Toplevel(Parent)
        CALTip.withdraw()
        CALTip.wm_overrideredirect(1)
        LLb = Label(CALTip, text=Value, bg=Clr["Y"], bd=1,
                    fg=Clr["B"], relief=SOLID, padx=3, pady=3)
        LLb.pack()
        x = Parent.winfo_pointerx() + 5
        y = Parent.winfo_pointery() + 5
        CALTip.wm_geometry("+%d+%d" % (x, y))
        CALTip.update()
        CALTip.deiconify()
        CALTip.lift()
    except Exception:
        formCALHideTip()
    CALTipLastValue = Value
    return
#################################
# BEGIN: formCALHideTip(e = None)
# FUNC:formCALHideTip():2011.110


def formCALHideTip(e=None):
    global CALTip
    global CALTipLastValue
    try:
        CALTip.destroy()
    except Exception:
        pass
    CALTip = None
    CALTipLastValue = ""
    return
# END: formCAL


##########################################
# BEGIN: formFind(Who, WhereMsg, e = None)
# LIB:formFind():2018.236
#   Implements a "find" function in a form's Text() field.
#   The caller must set up the global Vars:
#      <Who>FindLookForVar = StringVar()
#      <Who>FindLastLookForVar = StringVar()
#      <Who>FindLinesVar = StringVar()
#      <Who>FindIndexVar = IntVar()
#      <Who>FindUseCaseCVar = IntVar()
#      <Who>FindReplaceWithVar = StringVar()
#   <Who> must be the string "INE", "CKTRD", etc.
#   Then on the form set up an Entry field and two Buttons like:
#
#   LEnt = Entry(Sub, width = 20, textvariable = <Who>FindLookForVar)
#   LEnt.pack(side = LEFT)
#   LEnt.bind("<Return>", Command(formFind, "<Who>", "<Who>"))
#   LEnt.bind("<KP_Enter>", Command(formFind, "<Who>", "<Who>"))
#   BButton(Sub, text = "Find", command = Command(formFind, "<Who>", \
#           "<Who>")).pack(side = LEFT)
#   BButton(Sub, text = "Next", command = Command(formFindNext, "<Who>", \
#           "<Who>")).pack(side = LEFT)
#   Checkbutton(Sub, text = "Use Case", \
#           variable = <Who>FindUseCaseCVar).pack(side = LEFT)
#   Entry(Sub, width = 20, \
#           textvariable = <Who>FindReplaceWithVar).pack(side = LEFT)
#   BButton(SSSub, text = "<Replace With", command = Command(formFindReplace, \
#           <Who>, <Who>)).pack(side = LEFT)
#
#   Must be defined even if it not used.
#      <Who>FindUseCaseCVar = IntVar()
#   Does not need to be defined if there is no replacing going on.
#      <Who>FindReplaceWithVar = StringVar()
#
def formFind(Who, WhereMsg, e=None):
    if WhereMsg is not None:
        setMsg(WhereMsg, "CB", "Finding...")
    LTxt = PROGTxt[Who]
    Case = eval("%sFindUseCaseCVar" % Who).get()
    if Case == 0:
        LookFor = eval("%sFindLookForVar" % Who).get().lower()
    else:
        LookFor = eval("%sFindLookForVar" % Who).get()
    LTxt.tag_delete("Find%s" % Who)
    LTxt.tag_delete("FindN%s" % Who)
    if len(LookFor) == 0:
        eval("%sFindLinesVar" % Who).set("")
        eval("%sFindIndexVar" % Who).set(-1)
        if WhereMsg is not None:
            setMsg(WhereMsg)
        return 0
    Found = 0
# Do this in case there is a lot of text from any previous find, otherwise
# the display just sits there.
    updateMe(0)
    eval("%sFindLastLookForVar" % Who).set(LookFor)
    eval("%sFindLinesVar" % Who).set("")
    eval("%sFindIndexVar" % Who).set(-1)
    FindLines = ""
    N = 1
    while True:
        if len(LTxt.get("%d.0" % N)) == 0:
            break
        if Case == 0:
            Line = LTxt.get("%d.0" % N, "%d.0" % (N + 1)).lower()
        else:
            Line = LTxt.get("%d.0" % N, "%d.0" % (N + 1))
        C = 0
        try:
            # Keep going through here until we run out of Line to search.
            while True:
                Index = Line.index(LookFor, C)
                TagStart = "%d.%d" % (N, Index)
                C = Index + len(LookFor)
                TagEnd = "%d.%d" % (N, C)
                LTxt.tag_add("Find%s" % Who, TagStart, TagEnd)
                LTxt.tag_config("Find%s" % Who, background=Clr["U"],
                                foreground=Clr["W"])
                FindLines += " %s,%s" % (TagStart, TagEnd)
                Found += 1
        except Exception:
            pass
        N += 1
    if Found == 0:
        if WhereMsg is not None:
            setMsg(WhereMsg, "", "No matches found.")
    else:
        eval("%sFindLinesVar" % Who).set(FindLines)
        formFindNext(Who, WhereMsg, True, True)
        if WhereMsg is not None:
            setMsg(WhereMsg, "", "Matches found: %d" % Found)
    return Found
#################################################
# BEGIN: formFindReplace(Who, WhereMsg, e = None)
# FUNC:formFindReplace():2018.236
#   A Simple-minded find and replace function.
#   If WhereMsg is None then the caller is responsible for displying all
#   messages.


def formFindReplace(Who, WhereMsg, e=None):
    if WhereMsg is not None:
        setMsg(WhereMsg, "CB", "Replacing...")
    LTxt = PROGTxt[Who]
    LookFor = eval("%sFindLookForVar" % Who).get()
    ReplaceWith = eval("%sFindReplaceWithVar" % Who).get()
# These come in as straight chars ("\" + "n"). Convert them to real \n's.
    if ReplaceWith.endswith("\\n"):
        ReplaceWith = "%s\n" % ReplaceWith[:-2]
    LTxt.tag_delete("Find%s" % Who)
    LTxt.tag_delete("FindN%s" % Who)
    updateMe(0)
    FindLines = ""
    N = 1
    Found = 0
    while True:
        if len(LTxt.get("%d.0" % N)) == 0:
            break
        Line = LTxt.get("%d.0" % N, "%d.0" % (N + 1))
        if Line.find(LookFor) != -1:
            Line = Line.replace(LookFor, ReplaceWith)
            LTxt.delete("%d.0" % N, "%d.0" % (N + 1))
            LTxt.insert("%d.0" % N, Line)
            Line = LTxt.get("%d.0" % N, "%d.0" % (N + 1))
            TagStart = "%d.%d" % (N, Line.find(ReplaceWith))
            TagEnd = "%d.%d" % (N, Line.find(ReplaceWith) + len(ReplaceWith))
            LTxt.tag_add("Find%s" % Who, TagStart, TagEnd)
            LTxt.tag_config("Find%s" % Who, background=Clr["U"],
                            foreground=Clr["W"])
            Found += 1
        N += 1
    if Found > 0:
        chgPROGChgBar(WhereMsg, 1)
    if WhereMsg is not None:
        setMsg(WhereMsg, "GB", "Done. Replaced: %d" % Found)
    return Found
######################################################
# BEGIN: formFindReplaceTrunc(Who, WhereMsg, e = None)
# FUNC:formFindReplaceTrunc():2018.236
#   A Simple-minded find and replace function that finds LookFor, truncates
#   that line, then appends ReplaceWith in its place.


def formFindReplaceTrunc(Who, WhereMsg, e=None):
    if WhereMsg is not None:
        setMsg(WhereMsg, "CB", "Replacing...")
    LTxt = PROGTxt[Who]
    LookFor = eval("%sFindLookForVar" % Who).get()
    ReplaceWith = eval("%sFindReplaceWithVar" % Who).get()
# These come in as straight chars ("\" + "n"). Convert them to real \n's.
    if ReplaceWith.endswith("\\n"):
        ReplaceWith = "%s\n" % ReplaceWith[:-2]
    LTxt.tag_delete("Find%s" % Who)
    LTxt.tag_delete("FindN%s" % Who)
    updateMe(0)
    FindLines = ""
    N = 1
    Found = 0
    while True:
        if len(LTxt.get("%d.0" % N)) == 0:
            break
        Line = LTxt.get("%d.0" % N, "%d.0" % (N + 1))
        if Line.find(LookFor) != -1:
            Index = Line.index(LookFor)
            Line = Line[:Index] + ReplaceWith
            LTxt.delete("%d.0" % N, "%d.0" % (N + 1))
            LTxt.insert("%d.0" % N, Line)
            Line = LTxt.get("%d.0" % N, "%d.0" % (N + 1))
            TagStart = "%d.%d" % (N, Line.find(ReplaceWith))
            TagEnd = "%d.%d" % (N, Line.find(ReplaceWith) + len(ReplaceWith))
            LTxt.tag_add("Find%s" % Who, TagStart, TagEnd)
            LTxt.tag_config("Find%s" % Who, background=Clr["U"],
                            foreground=Clr["W"])
            Found += 1
        N += 1
    if Found > 0:
        chgPROGChgBar(WhereMsg, 1)
    if WhereMsg is not None:
        setMsg(WhereMsg, "GB", "Done. Replaced: %d" % Found)
    return Found
###########################################################################
# BEGIN: formFindNext(Who, WhereMsg, Find = False, First = False, e = None)
# FUNC:formFindNext():2014.069


def formFindNext(Who, WhereMsg, Find=False, First=False, e=None):
    LTxt = PROGTxt[Who]
    LTxt.tag_delete("FindN%s" % Who)
    FindLines = eval("%sFindLinesVar" % Who).get().split()
    if len(FindLines) == 0:
        beep(1)
        return
# Figure out which line we are at (at the top of the Text()) and then go
# through the found lines and find the closest one. Only do this on the first
# go of a search and not on "next" finds.
    if First is True:
        Y0, Dummy = LTxt.yview()
        AtLine = LTxt.index("@0,%d" % Y0)
# If we are at the top of the scroll then just go on normally.
        if AtLine > "1.0":
            AtLine = intt(AtLine)
            Index = -1
            Found = False
            for Line in FindLines:
                Index += 1
                Line = intt(Line)
                if Line >= AtLine:
                    Found = True
                    break
# If the current position is past the last found item just let things happen
# normally (i.e. jump to the first item found).
            if Found is True:
                eval("%sFindIndexVar" % Who).set(Index - 1)
    Index = eval("%sFindIndexVar" % Who).get()
    Index += 1
    try:
        Line = FindLines[Index]
        eval("%sFindIndexVar" % Who).set(Index)
    except IndexError:
        Index = 0
        Line = FindLines[Index]
        eval("%sFindIndexVar" % Who).set(0)
# Make the "current find" red.
    TagStart, TagEnd = Line.split(",")
    LTxt.tag_add("FindN%s" % Who, TagStart, TagEnd)
    LTxt.tag_config("FindN%s" % Who, background=Clr["R"],
                    foreground=Clr["W"])
    LTxt.see(TagStart)
# If this is the first find just let the caller set a message.
    if Find is False:
        setMsg(WhereMsg, "", "Match %d of %d." % (Index + 1, len(FindLines)))
    return
# END: formFind


###################################
# BEGIN: formFONTSZ(Parent, Format)
# LIB:formFONTSZ():2019.051
#   Displays the current font sizes and allows the user to enter new ones.
#   Includes the BIGGER and smaller font stepper functions.
PROGFrm["FONTSZ"] = None
FONTSZPropVar = StringVar()
FONTSZMonoVar = StringVar()
PROGSetups += ["PROGPropFontSize", "PROGMonoFontSize"]


def formFONTSZ(Parent, Format):
    if showUp("FONTSZ"):
        return
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    LFrm = PROGFrm["FONTSZ"] = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "CAL"))
    LFrm.title("Set Font Sizes")
    Label(LFrm, text="The current font sizes are shown.\nEnter new values and "
          "click the Set button.\nSome systems may use positive integers, "
          "and\nsome may use negative integers. Experiment.\n(Some systems "
          "may not have some font sizes.)").pack(side=TOP)
    FONTSZPropVar.set("%s" % PROGPropFont["size"])
    FONTSZMonoVar.set("%s" % PROGMonoFont["size"])
# In case different programs have more or fewer fonts to control.
    if Format == 0:
        Sub = Frame(LFrm)
        labelEntry2(Sub, 11, "Proportional Font: ", 35,
                    "Generally for buttons and labels and dialog box "
                    "messages.", FONTSZPropVar, 6)
        Sub.pack(side=TOP)
        Sub = Frame(LFrm)
        labelEntry2(Sub, 11, "Mono-Spaced Font: ", 35,
                    "Generally for text display fields.", FONTSZMonoVar, 6)
        Sub.pack(side=TOP)
    Sub = Frame(LFrm)
    BButton(Sub, text="Set", command=formFONTSZGo).pack(side=LEFT)
    BButton(Sub, text="Close", fg=Clr["R"],
            command=Command(formClose, "FONTSZ")).pack(side=LEFT)
    Sub.pack(side=TOP, padx=3, pady=3)
    center(Parent, LFrm, "C", "I", True)
    return
#######################
# BEGIN: formFONTSZGo()
# FUNC:formFONTSZGo():2019.049


def formFONTSZGo():
    NewProp = intt(FONTSZPropVar.get())
    NewMono = intt(FONTSZMonoVar.get())
    if NewProp != PROGPropFont["size"]:
        PROGPropFontSize.set(NewProp)
        fontSetSize()
    if NewMono != PROGMonoFont["size"]:
        PROGMonoFontSize.set(NewMono)
        fontSetSize()
    return
#####################
# BEGIN: fontBigger()
# FUNC:fontBigger():2012.069


def fontBigger():
    PSize = PROGPropFont["size"]
    if PSize < 0:
        PSize -= 1
    else:
        PSize += 1
    MSize = PROGMonoFont["size"]
    if MSize < 0:
        MSize -= 1
    else:
        MSize += 1
# If either font is at the limit don't change the other one or they will get
# 'out of synch' with each other (like if the original prop font size was 12
# and the mono font size was 10).
    if abs(PSize) > 16 or abs(MSize) > 16:
        beep(1)
        return
    PROGPropFontSize.set(PSize)
    PROGMonoFontSize.set(MSize)
    fontSetSize()
    return
######################
# BEGIN: fontSmaller()
# FUNC:fontSmaller():2012.067


def fontSmaller():
    PSize = PROGPropFont["size"]
    if PSize < 0:
        PSize += 1
    else:
        PSize -= 1
    MSize = PROGMonoFont["size"]
    if MSize < 0:
        MSize += 1
    else:
        MSize -= 1
    if abs(PSize) < 6 or abs(MSize) < 6:
        beep(1)
        return
    PROGPropFontSize.set(PSize)
    PROGMonoFontSize.set(MSize)
    fontSetSize()
    return
######################
# BEGIN: fontSetSize()
# FUNC:fontSetSize():2019.049


def fontSetSize():
    global PROGPropFontHeight
    PROGPropFont["size"] = PROGPropFontSize.get()
    PROGMonoFont["size"] = PROGMonoFontSize.get()
    PROGPropFontHeight = PROGPropFont.metrics("ascent") + \
        PROGPropFont.metrics("descent")
# For any additional stuff a program may need to do.
    if "formFONTSZGoLocal" in globals():
        formFONTSZGoLocal()
    return
# END: formFONTSZ


##################
# BEGIN: formGPS()
# FUNC:formGPS():2016.194
PROGFrm["GPS"] = None
GPSPoints = []
GPSPOINTS_DT = 0
GPSPOINTS_LATI = 1
GPSPOINTS_LONG = 2
GPSPOINTS_ELEV = 3
GPSPOINTS_ADJLATI = 4
GPSPOINTS_ADJLONG = 5
GPSRunning = False
GPSSlowerCVar = IntVar()
PROGSetups += ["GPSSlowerCVar"]


def formGPS():
    global GPSRunning
    global GPSSlower
# If no file has been read yet then don't do any of this.
    if MFPlotted is False:
        beep(1)
        return
    if showUp("GPS") is True:
        return
    LFrm = PROGFrm["GPS"] = Toplevel(Root)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    LFrm.protocol("WM_DELETE_WINDOW", Command(formGPSControl, "close"))
    LFrm.title("GPS Data Plotter")
    LFrm.iconname("GPS")
    Sub = Frame(LFrm)
    LCan = PROGCan["GPS"] = Canvas(LFrm, height=400, width=400,
                                   bg=DClr["GPSCanvas"])
    LCan.pack(side=TOP)
    Sub = Frame(LFrm)
    SSub = Frame(Sub)
    BButton(SSub, text="Read/Plot", command=Command(formGPSRead,
                                                    "read")).pack(side=TOP)
    LCb = Checkbutton(SSub, text="Slower", variable=GPSSlowerCVar)
    LCb.pack(side=TOP)
    ToolTip(LCb, 35,
            "Select this to slow down plotting of the GPS points if needed.")
    SSub.pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    BButton(Sub, text="Export", command=formGPSExport).pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    BButton(Sub, text="Close", fg=Clr["R"],
            command=Command(formClose, "GPS")).pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    LLb = Label(Sub, text="Hints ")
    LLb.pack(side=LEFT)
    ToolTip(LLb, 30, "--Normal/Left-click on dot: Clicking on a dot shows "
            "the information for that dot.\n--Other/Right-click on dot: "
            "Right-clicking on a dot removes that dot from the plot and "
            "replots all of the remaining points. Use this to remove "
            "outliers.")
    Sub.pack(side=TOP, pady=3)
    PROGMsg["GPS"] = Text(LFrm, font=PROGPropFont, height=2, wrap=WORD)
    PROGMsg["GPS"].pack(side=TOP, fill=X)
    center(Root, LFrm, "CX", "I", True)
    GPSRunning = False
    return
#############################
# BEGIN: formGPSClear(Action)
# FUNC:formGPSClear():2014.239


def formGPSClear(Action):
    global GPSPoints
    LCan = PROGCan["GPS"]
    LCan.delete(ALL)
    LCan.configure(bg=DClr["GPSCanvas"])
    if Action == "read":
        del GPSPoints[:]
    setMsg("GPS", "", "")
    return
########################
# BEGIN: formGPSExport()
# FUNC:formGPSExport():2019.003


def formGPSExport():
    if GPSRunning is True:
        beep(1)
        return
    if len(GPSPoints) == 0:
        setMsg("GPS", "RW", "There are no points to export.", 2)
        return
    setMsg("GPS", "CB", "Exporting...")
# The .cf sources and such will have / in them.
    File = cleanAFilename(FilenamesRead[0] + "gps.dat")
    try:
        Fp = open(PROGDataDirVar.get() + File, "w")
    except Exception:
        setMsg("GPS", "MW", "Error opening file\n   %s" % File, 3)
        return
    try:
        Fp.write("# GPS data points for %s\n" % list2Str(FilenamesRead))
        for Point in GPSPoints:
            Fp.write("%s\t%+f\t%+f\t%f\n" % (Point[GPSPOINTS_DT],
                                             Point[GPSPOINTS_LATI],
                                             Point[GPSPOINTS_LONG],
                                             Point[GPSPOINTS_ELEV]))
        Fp.close()
    except Exception:
        setMsg("GPS", "MW", "Error writing to file\n   %s" % File, 3)
        return
    setMsg("GPS", "GB", "GPS data exported to Main Data Dir\n   %s" % File, 1)
    return
############################
# BEGIN: formGPSRead(Action)
# FUNC:formGPSRead():2019.023


def formGPSRead(Action):
    global GPSPoints
    global GPSRunning
    if GPSRunning is True:
        beep(1)
        return
# Check again.
    if MFPlotted is False:
        beep(1)
        return
    setMsg("GPS", "CB", "Plotting...")
    LCan = PROGCan["GPS"]
    formGPSClear(Action)
    GPSRunning = True
    Slower = GPSSlowerCVar.get()
    Found = 0
    MinLati = maxFloat
    MaxLati = -maxFloat
    MinLong = maxFloat
    MaxLong = -maxFloat
    if Action == "read":
        if len(LOGLines) == 0:
            setMsg("GPS", "RW", "There are no LOG messages to read.", 2)
            GPSRunning = False
            return
        for Line in LOGLines:
            # Ex: 253:22:41:25 GPS: POSITION: N43:44:17.12 W096:37:25.27
            # +00456M
            if Line.find("GPS: POSITION") != -1:
                Found += 1
                InParts = Line.split()
# Any number of scrambled data problems could set this off.
                try:
                    Lati = rt72130Str2Dddd(InParts[3])
                    Long = rt72130Str2Dddd(InParts[4])
                    Elev = floatt(InParts[5])
                    if Lati != -1000.0 and Long != -1000.0:
                        # The +90 and +180 below gets rid of the negative
                        # numbers so latitudes will always be 0 at south pole
                        # to 180 at north pole and longitudes will always be 0
                        # at Greenwich to 360 at Greenwich. Much easier to
                        # plot.
                        AdjLati = Lati + 90.0
                        AdjLong = Long + 180.0
                        # Might as well determine these as we go.
                        if AdjLati < MinLati:
                            MinLati = AdjLati
                        if AdjLati > MaxLati:
                            MaxLati = AdjLati
                        if AdjLong < MinLong:
                            MinLong = AdjLong
                        if AdjLong > MaxLong:
                            MaxLong = AdjLong
                        GPSPoints.append([InParts[0], Lati, Long, Elev,
                                          AdjLati, AdjLong])
                except Exception:
                    pass
    elif Action == "replot":
        for Point in GPSPoints:
            #              0      1      2     3        4        5
            # Point = [DateTime, Lati, Long, Height, AdjLati, AdjLong]
            if Point[GPSPOINTS_ADJLATI] < MinLati:
                MinLati = Point[GPSPOINTS_ADJLATI]
            if Point[GPSPOINTS_ADJLATI] > MaxLati:
                MaxLati = Point[GPSPOINTS_ADJLATI]
            if Point[GPSPOINTS_ADJLONG] < MinLong:
                MinLong = Point[GPSPOINTS_ADJLONG]
            if Point[GPSPOINTS_ADJLONG] > MaxLong:
                MaxLong = Point[GPSPOINTS_ADJLONG]
            Found += 1
    if Found == 0:
        setMsg("GPS", "YB", "No GPS information was found.", 1)
        GPSRunning = False
        return
# The MaxRange is going to be plotted in a square 75% of the height of the
# canvas (which should be a square).
    PY = LCan.winfo_height()
    Buff = PY * .25 / 2.0
    PlotW = PY * .75
    Index = 0
    Plotted = 0
# Special case, otherwise the point ends up in the lower left-hand corner.
    if Found == 1:
        XY = Buff + PlotW / 2.0
        ID = LCan.create_rectangle(XY - 2, XY - 2, XY + 2, XY + 2,
                                   fill=DClr["GPSDots"])
        LCan.tag_bind(ID, "<Button-1>", Command(formGPSShowPoint, Index))
    else:
        # The plot area (no matter what shape the canvas is) is just going
        # to be the greater of maxlat x maxlat, or maxlong x maxlong for
        # simplicity, so figure out the max size in degrees.
        LatiRange = getRange(MinLati, MaxLati)
# Just in case.
        if LatiRange == 0.0:
            LatiRange = 1.0
# Get this back into -90 0 +90 range for the cos().
        LatiAve = (MinLati + MaxLati) / 2.0 - 90.0
        LongRange = getRange(MinLong, MaxLong)
        if LongRange == 0.0:
            LongRange = 1.0
        if LatiRange == 1.0 and LongRange == 1.0:
            canText(LCan, 10, PROGPropFontHeight, DClr["CanText"], "No range.")
        else:
            canText(LCan, 10, PROGPropFontHeight, DClr["CanText"],
                    "LatRange: %.2fm    LongRange: %.2fm" %
                    (LatiRange * 110574.27, LongRange * 111319.458 *
                     cos(LatiAve / 360.0 * 3.14159 * 2.0)))
# Skip plotting points that don't change. That's suspicious.
        LastLati = -0.0
        LastLong = -0.0
        for Point in GPSPoints:
            Lati = Point[GPSPOINTS_ADJLATI]
            Long = Point[GPSPOINTS_ADJLONG]
            if Lati == LastLati and Long == LastLong:
                Index += 1
                continue
            LastLati = Lati
            LastLong = Long
# We have to switch the Y zero point to the bottom of the canvas (the plot
# area, actually) and then come back up the amount of the latitude.
            YY = (Buff + PlotW) - ((Lati - MinLati) / LatiRange * PlotW)
# These just plot in from the edge.
            XX = Buff + (Long - MinLong) / LongRange * PlotW
# Don't make an outline so the boxes have a black border which will separate
# them a little since they will mostly be on top of each other (on black bg).
            ID = LCan.create_rectangle(XX - 2, YY - 2, XX + 2, YY + 2,
                                       fill=DClr["GPSDots"])
            LCan.tag_bind(ID, "<Button-1>", Command(formGPSShowPoint, Index))
            if B2Glitch is True:
                LCan.tag_bind(ID, "<Button-2>", Command(formGPSZapPoint,
                                                        Index))
            LCan.tag_bind(ID, "<Button-3>", Command(formGPSZapPoint, Index))
            if Slower == 1:
                updateMe(0)
            Index += 1
            Plotted += 1
    setMsg("GPS", "", "Done. Plotted %d of %d." % (Plotted, Found))
    GPSRunning = False
    return
##########################################
# BEGIN: formGPSShowPoint(Index, e = None)
# FUNC:formGPSShowPoint():2014.239


def formGPSShowPoint(Index, e=None):
    if GPSRunning is True:
        beep(1)
        return
    setMsg("GPS", "",
           "Time: %s\nLat: %s   Long: %s   Elev: %gm" %
           (GPSPoints[Index][GPSPOINTS_DT],
            pm2nsew("lat", GPSPoints[Index][GPSPOINTS_LATI]),
            pm2nsew("lon", GPSPoints[Index][GPSPOINTS_LONG]),
            GPSPoints[Index][GPSPOINTS_ELEV]))
    return
#########################################
# BEGIN: formGPSZapPoint(Index, e = None)
# FUNC:formGPSZapPoint():2014.239
#   Remove points from GPSPoints and calls for a replot. Searches for all
#   points that have the same lat/long as the one that was clicked on and
#   throws them out too.


def formGPSZapPoint(Index, e=None):
    global GPSPoints
    global GPSRunning
# %%%%%
    print("here")
    if GPSRunning is True:
        beep(1)
        return
    GPSRunning = True
# This might take a while.
    setMsg("GPS", "CB", "Plotting...")
    Lati = GPSPoints[Index][GPSPOINTS_LATI]
    Long = GPSPoints[Index][GPSPOINTS_LONG]
    NewGPSPoints = []
    for Point in GPSPoints:
        if Point[GPSPOINTS_LATI] != Lati and Point[GPSPOINTS_LONG] != Long:
            NewGPSPoints.append(Point)
    GPSPoints = deepcopy(NewGPSPoints)
# Otherwise formGPSRead() won't go.
    GPSRunning = False
    formGPSRead("replot")
    return
###############################
# BEGIN: formGPSControl(Action)
# FUNC:formGPSControl():2014.239
#   Just for freeing up the GPSPoints.


def formGPSControl(Action):
    global GPSPoints
    global GPSRunning
    if Action == "close":
        if GPSRunning is True:
            beep(1)
            return
        del GPSPoints[:]
        formClose("GPS")
        GPSRunning = False
        return
# END: formGPS


#################
# BEGIN: HELPText
#   The text of the help form.
HELPText = "QUICK START\n\
===========\n\
1. Enter  logpeek  or  logpeek.py  or  python logpeek.py  on the command \
line, or click on the provided icon (it all depends on which operating \
system is in use and how things have been set up).\n\
\n\
2. The initial directory where LOGPEEK will look for files to read will \
be read from a setups file, setlogpeek.set, that was saved to the \"home\" \
directory (the specific direcotry will depend on the operating system in \
use) when the program was last used. If a setups file cannot be found a \
file selection dialog box will appear and the user should navigate to a \
directory containing information source files to read. This directory may be \
changed at any time using Main Data Dir button.\n\
\n\
Use the List checkboxes (.log, .ref, .cf, .zip, .mslogs) below the file \
selection list to control the types of files that are listed from the \
current data directory.\n\
\n\
The Reload button should be used to refresh the list of files displayed \
if the selected List checkbuttons are changed.\n\
\n\
3. Information selection and control of the plotting may be done using \
the Only SOH Items, Mass Pos, and DSs checkbuttons and all of the items \
in the menu item Plots.\n\
\n\
If it is desired to also look at a simple plot of the seismic data any of \
the DSs (data streams) checkboxes and the RAW checkbox must be selected. \
For TPS (Time-Power-Squared) plots of the same data stream information select \
the TPS checkbutton. There will only be seismic data to plot in the .zip, \
.ref and .cf information sources, of course.\n\
\n\
To read an information source double-click on the file/folder in the \
file list or select the item in the list and click the Read button. \
Multiple files in the list can be selected by using Shipt-Click and \
Control-click actions and then clicking the Read button. The files will \
be read in the order that they appear in the files selection list. \
LOGPEEK does no checking of the files before plotting to see if they \
are in time order or even if they are from the same DAS or station, so \
use this feature carefully. Files of different types, for example, \
a .log file from a previous service run and a .cf folder from this \
service run, may be plotted together if the file names sort correctly \
alphabetically. The Options menu item Sort Files List Alphabetically \
can be used to help make that happen if the file naming requires it.\n\
\n\
\n\
DESCRIPTION\n\
===========\n\
LOGPEEK allows reading and examining in a graphical manner the State \
of Health messages and other data from a Reftek log file produced by \
PASSCAL programs such as the ref2log/ref2segy group of programs, rt2ms, \
as well as the State of Health messages and seismic data information \
contained in all-in-one '.ref' files (normally from Reftek 72A recorders), \
and zipped or unzipped CompactFlash card images from Reftek 130 recorders. \
When requested from the DMC, \"LOG\" channel miniseed files may also be \
read and the information plotted.\n\
\n\
LOGPEEK is started by entering its name on the command line, \
double-clicking on an icon, or selecting an icon from a menu (it depends \
on the operating system and how things are set up). When started LOGPEEK \
will load the contents of a setups file, setlogpeek.set, in the directory \
specified by the HOME environment variable for Linux, macOS and Sun systems, \
or the combination of the HOMEDRIVE and HOMEPATH environment variables \
on Windows. The initial working directory and all of the button and \
option settings will also be read from that file.\n\
\n\
If the setlogpeek.set file is not found a file selection dialog box will \
appear when LOGPEEK is started. This should be used to navigate to an \
initial starting directory where some source information files are located. \
This will normally only be required the first time LOGPEEK is started on \
a machine or in an account.\n\
\n\
The selection list of source information files shown from the data \
directory is controlled by the selection of the List checkbuttons (.log, \
.ref, .cf, .zip, .mslogs) below the file selection listbox. The Reload \
button should be used to reload the files list if the checkbuttons are \
changed or the Find field value is changed.\n\
\n\
The Find field below the file selection listbox can be used to bring files \
containing the value typed into the Find field to the top of the file \
selection list. For example, if the user wanted to examine the data from \
DAS 90A4 they could enter \"90a4\" into the field and press the Return \
key (or use the Reload button). This would place any file containing 90A4 \
at the top of the list followed by all of the files previously listed. \
The Clear button will remove 90a4 from the field and reload all of the \
file names. The search for matching file names is not case sensitive.\n\
\n\
The source to be examined is selected from the list by double-clicking \
on its name or by selecting the name and clicking the Read button.\n\
\n\
The approximate number of lines in a .log file or .mslogs folder, or the \
number of bytes in a binary data source file is displayed to the right \
of each name in the list when the Options menu Calculate File Sizes \
item is selected. The number of lines value is just an estimate (it \
assumes about 48 characters per SOH line).\n\
\n\
Selecting one or more .log files in the file selection, and then \
control-shift-clicking on the file name(s) will bring up a display that \
will load and show the SOH messages from the selected file(s) similar \
to the SOH Messages display, but without plotting the messages.\n\
\n\
Changing the data directory is done by editing the path in the current \
working directory field near the top of the main window and pressing \
the Return key, or by using the Main Data Dir button and navigating to \
the directory of interest using the file navigation dialog box that \
will appear.\n\
\n\
When a source has been selected it is opened and read. Various lines of \
interest in the file's SOH information are parsed, and their information \
saved. When file reading is finished a smaller second window will then \
be created. All of the SOH information extracted from the source will \
be written into that \"SOH Messages\" window. The information of interest \
in those messages will then be plotted in the plot area, and some \
information about what was read will be placed into the information \
area below the file selection listbox. Status messages will be displayed \
in the status area at the bottom of the main display as the reading and \
plotting processes are performed.\n\
\n\
If there is an error file (files ending in \".err\") with the same \
basename as a selected .log file that file will also be opened and \
read. These files are produced by some post-offload processing programs. \
If there are any lines of interest in the .err file another small \
window \"Error File Lines\" will be created and the contents of the file \
will be displayed there. Data from the .err file will be plotted in the \
main plot area along with the data from the main .log file.\n\
\n\
If the .err file is large (over 1,000,000 bytes) a dialog box will appear \
asking if the user wants to read the whole file, just the first 10,000 \
lines of the .err file, or not read the .err file at all. Error files \
that large generally point to equipment failure, which makes plotting \
the .err file information worthless.\n\
\n\
An additional window can be requested from the menu item Commands | GPS \
Information, that will display information about the DAS's recorded \
position (latatude and longitude) based on the 'GPS: POSITION' messages \
found in the SOH information along with a plot of the data points found.\n\
\n\
Another window can be requested from the menu item Commands | Log \
Search, that will allow searching through the current SOH messages for \
an entered string.\n\
\n\
The menu command Commands | Plot Time Ranges Of Files is a function that \
will graphically display the time range covered by each selected source \
information file.\n\
\n\
Menu item Commands | Plot Positions From Files will produce a simple map \
of where the GPS position information says each selected file is in \
relation to the other files selected.\n\
\n\
LOGPEEK examines the DAS serial number that it gleans from the source \
file it is requested to read. If the DAS serial number is in the \
range 9000 to FFFF then LOGPEEK guesses that the DAS is an RT130 model \
recorder. Any other number and LOGPEEK will assume the information is \
from a Reftek 72A series DAS. There are just a few SOH messages that \
are worded differently between the different DOS models.\n\
\n\
All of the program settings are saved to the file \"setlogpeek.set\" in \
the \"HOME\" directory that was determined when the program was started. \
See the General Information section below for more information about the \
\"HOME\" directory.\n\
\n\
\n\
General Information\n\
===================\n\
Check the Options | Plot Timing Problems menu selection to force \
LOGPEEK to plot all of the lines in the SOH messages. Normally, when the \
option is not checked, LOGPEEK will ignore lines that are timestamped \
earlier in time than the time for the current line being processed for \
plotting. The lines that LOGPEEK skips will be displayed in yellow in \
the SOH Messages display. The word \"skipped\" will also be inserted \
into the displayed log lines so the skipped sections may be searched \
for if there are a large number of log lines.\n\
\n\
When the program is fist started on a machine it will attempt to determine \
the \"HOME\" directory for the current user. This will be done using the \
HOME environment variable on macOS, Linux and Sun operating systems, \
and the combination of HOMEDRIVE+HOMEPATH environment variables on \
Windows. If this directory can be written to all will be well and a \
dialog box will appear indicating that the file \"setlogpeek.set\" could \
not be found. That is OK. When the user quits the program all of the \
current settings (checkbuttons, menu options, the current working directory, \
etc.) will be saved to the setups file. When the program is started again \
all of that information will be restored.\n\
\n\
If the \"HOME\" directory cannot be written to then the program will \
quit. This will have to be resolved before LOGPEEK may be used.\n\
\n\
\n\
MAIN PLOT AREA OPTIONS\n\
======================\n\
The main plot area supports several clicking options to allow detailed \
examination of the plotted information. These are summarized in the \
tooltip for the \"Hints\" lable in the upper right corner of the display.\n\
\n\
\n\
- Regular click on a Point\n\
Clicking on or near a plotted point (or line if the points are close \
together) will direct the program to display in the \"SOH Messages\" \
window the line in the messages that corresponds to the point clicked on. \
This does not work with Mass Position points since these are not normally \
generated from lines in the SOH messages. Clicking on Mass Pos X points \
will display information about the point in the messages area at the \
bottom of the main display.\n\
\n\
Clicking on points plotted on the ERR DSx lines will show the \
corresponding line in the Error File Lines window.\n\
\n\
If there are a lot of Phase Error points the plot will not be dots, but, \
instead just be a line. Clicking on the line will select a log message \
roughly corresponding area of the log messages. To see individual points \
you must zoom in. Just drawing the plot as a line is done to speed up \
plotting 1000's of points. When a line is drawn is also related to the \
width of the plotting area. There is no point in trying to plot 10,000 \
points where there is only 700 pixels to plot them in.\n\
\n\
- Clicking below the time tick marks at the bottom of the plot area \
will scroll the display ahead or backward in time. Clicking to the \
right of center of the plots will scroll ahead in time, and to the \
left of center backward in time. The amount of scroll is 90% of the \
currently displayed time range so there will be some overlap between \
subsequent refreshes.\n\
\n\
- Right-click (Zapping)\n\
Clicking the right mouse button on a point will remove that point from \
the data set. \"Points zapped: x\" will be displayed in the status \
area after this function has been used. The file must be reloaded to \
recover and re-plot the zapped points. This only applies to the plots \
which are ranges of points (DSP-Clk Diff, Phase Error, DAS Temp, etc.), \
and not plots that are just linear sets of points like Jerks/DSP Sets.\n\
\n\
- Shift-click (\"S\"election click)\n\
Clicking on the plot area while holding down the Shift key will \
allow the user to select an area of the plots to be zoomed in on. \
The first shift-click will display a vertical rule marking one boundary \
of the area to be selected. The second shift-click will select the other \
boundry. The plot area will be cleared and the selected area \
redrawn. \"(zoomed)\" will be displayed in the status area until the \
plots return to the original scale.\n\
\n\
Shift-clicking just to the left of the beginning of the plots will place \
the selection rule at the starting pixel of the plots. Likewise, \
shift-clicking just to the right of the end of the plots will place \
the selection rule at the last pixel of the plots.\n\
\n\
To return to the original scale of the plotted information perform a \
shift-click in the plot labels on the left side of the plot area. The \
program will step back out through each of the previous selection \
ranges until the original time range is reached.\n\
\n\
The first selection boundry rule can be cancelled by shift-clicking \
in the label area on the left side of the plot area before \
shift-clicking at the second selection rule's position.\n\
\n\
- Control-click (\"C\"lock click)\n\
Clicking on the plot area while holding down the Control key will display \
a vertical rule with the time corresponding to the X-axis position of \
the click displayed above the rule. The time above the rule is \
computed based on the start and end times of the displayed plots, and \
the pixel position of the mouse cursor. This will undoubtedly cause \
some rounding errors since the number of seconds covered by the plots \
will usually be much larger than the number of display pixels.\n\
\n\
- Clicking on times\n\
If the display is zoomed in at all clicking in the left side of the area \
at the bottom of the plotting area where the X-axis dates/times are \
located will 'scroll' the display earlier in time. Clicking on the \
right side will 'scroll' the display forward in time.\n\
\n\
- Control-clicking on times\n\
Control-clicking in the dates/times area at the bottom of the plots \
will extend the X-axis tick marks to the top of the plotting area.\n\
\n\
To remove the rule and the time, and/or the extended tick marks, \
perform a control-click in the area of the plot labels on the left \
side.\n\
\n\
-Shift-Control-clicking on .log files\n\
Selecting and then shift-control-clicking on a .log data source file \
name in the files list will read in and display that file in a separate \
window without plotting the contents.\n\
\n\
\n\
TIME DETAILS\n\
============\n\
The time quality of SOH messages can range from perfect to ridiculous. \
Most SOH messages do not know what year it is. The program begins reading \
lines and ignores all of them that it comes to until the first \"State \
of Health\" line is reached (which, thechnically, is a line generated \
by reading a SH -- State of Health -- packet header, and is not written by \
the RT130s). The program uses the timestamp from that line, which does \
contain the year, as the beginning time of the file. Lines timestamped \
before that time, either because of a file problem, or a real DAS \
clock/timing problem of some sort, will be ignored. These lines will \
be counted as \"skipped\" lines. The number of lines skipped while \
reading the file will be displayed in the information area of the \
display (lower left-hand corner). The skipped lines will also be \
displayed in yellow color in the \"SOH Messages\" display. For a \
well-behaved DAS/normally produced log file the number of skipped lines \
will be 2 -- PASSCAL programs that produce log files (and LOGPEEK when \
reading raw data source information files) place a line with their name, \
and a blank line, before the first \"State of Health\" line. If the \
number of skipped lines is large then the file may have to be looked \
at manually with an editor, or see the Options | Plot Timing Problems \
option below.\n\
\n\
In addition to individual lines timestamped before the start time of \
the file being rejected, subsequent \"State of Health\" lines are also \
checked against each other. If a \"State of Health\" line with time X \
is found, and the next \"State of Health\" line has a time before X all \
lines will be skipped until the next \"State of Health\" line with a \
time after X is found. If a DAS generates timestamps way in the \
future, and then at some point jumps back to the correct time, then it \
is possible that the program may skip all lines from the wrong time \
point on. The file may need to be manually edited to get the program \
to plot anything or timing problems may have to be ignored with the \
Options | Plot Timeing Problems menu item.\n\
\n\
Because of the way lines are rejected above, it is possible that a few \
data points from the beginning of a new year may be lost. Most lines \
do not know what year it is, and the first lines of a new year will be \
evaluated as being recorded on day 001 of the 'old' year until the next \
\"State of Health\" line is found (usually no more than every 20th \
line of SOH messages) which will then set the internal year value to the \
new year. Only three lines were skipped in a test log file that was \
produced during the 2001-2002 year change, and of the three lines only \
one contained information that would have been recorded and plotted. \
Most SOH message lines contain information that is not plotted.\n\
\n\
Log file lines that ignore the skipping convention are the lines that \
signifiy the start of an event. Basically, the information from these \
lines is always plotted. No matter how the state of health messages are \
produced these lines will always not quite be in the right place in time \
with respect to the normal SOH messages. The worst case is when reading \
.cf or .cf.zip files when these lines will always follow ALL of the SOH \
messages for a given UT day, because of the way the raw data is stored \
by the RT130s. For this reason they will always be susceptible to being \
skipped under certain conditions, because their 'time' will be less than \
the last good time from a \"State of Health\" line. To keep that from \
happening they are just always plotted. Any actual timing errors will \
show up in other places.\n\
\n\
Internally all dates/times are converted to the number of seconds \
since Jan 1, 1970. If you have information source files with times before \
1970 there's no telling what will happen.\n\
\n\
The Options | Plot Timing Problems checkbutton may be checked \
which will plot everything no matter how bad the times are in the \
timestamps.\n\
\n\
When LOGPEEK determines that the timing may be bad the status message \
area at the bottom of the display will be yellow, and the message \
\"Possible timing problems\" will appear. This may or may not be true. \
It is just a guess. The message will not be displayed again when zooming \
in or out.\n\
\n\
\n\
OTHER STUFF\n\
===========\n\
SOH message lines that are \"scrambled\" in some way and that contain \
non-printable characters will be displayed in red, and the bad characters \
will be replaced with a _ (underscore) character. These lines will also \
not be processed in any way, but will just be skipped.\n\
\n\
Bad files found when reading zipped or uncompressed CF card source files \
will be listed at the top of the listing of the SOH Messages. They will \
be yellow, since they will have been skipped. They will usually start \
with the word \"WARNING\". They will be included in the number of lines \
skipped count in the information area, but will not be included in the \
'WARNING lines' count (that is reserved for RT130 and processing program \
-- whatever produced the .log file -- messages).\n\
\n\
The messages may contain \"Not1024\" or \"IOError\". Not1024 means \
that bytes from a file were missing. All files created by RT130s and \
72As should be some multiple of 1024 bytes (the packet size), unless \
a file is corrupted or recording stopped early because of something \
like a power outage. Under some circumstances, so far it has only been \
seen on CF cards from stations that had severe power problems, individual \
files on CF cards may generate read errors. Those warning messages \
will have IOError in them.\n\
\n\
Any file listed in these warning messages will have been partially or \
completely skipped.\n\
\n\
\n\
SOURCE FILE FORMATS\n\
===================\n\
LOGPEEK will read several different types of sources looking for SOH \
information. The program is not very smart, so it decides what type of \
file it is reading by the file name extension of the selected file.\n\
\n\
.log\n\
----\n\
These are ASCII text files of the State Of Health messages extracted \
by PASSCAL programs such as ref2segy or ref2log. Each .log file can only \
contain the SOH messages from one recorder, which is normally the case.\n\
\n\
.ref\n\
----\n\
These are binary files created by PASSCAL programs such as Neo or \
rt130Cut from the contents of RT130 recorder CompactFlash cards. These \
are also the native file format of Reftek 72A recorders. Each .ref \
file can only contain the data from one recorder.\n\
\n\
.zip\n\
----\n\
The RT130 recorders record their data to CompactFlash cards in a \
Reftek-created format of directories and data files (one file for each \
event and data stream in about the same packet-format as the old .ref \
files). This whole directory and file structure can be copied by \
Neo/rt130Cut and placed into one zipped file. LOGPEEK can read these \
.zip files if the first directories in the archive are the day \
directories named something like \"2008145\" (day 145 of year 2008), \
which is usually the case. Each .zip file may only contain the data from \
one recorder. These files can also be created by making a folder, \
dragging the contents of a mounted CF card to the folder, and then \
zipping that folder.\n\
\n\
.zip files should have the data from only one DAS in them. If not the \
information from all of the DASs will be \"combined\", and probably \
not in a good way.\n\
\n\
.cf\n\
---\n\
Sources ending in .cf are really (read: need to be) directories that \
are copies of the directories, files and the structure of an RT130 \
CompactFlash card. Like the .zip files the first sub-directory in the \
\"file\"/folder/directory must be any day directories. Because of the \
structure of the directories data from more than one RT130 in may be in \
a .cf \"file\". LOGPEEK lists these file names as '<name>.cf/<RT130 ID>' \
in the file selection list.\n\
\n\
The current working directory may be changed (using the Main data Dir \
button, for example) to the 'root' directory of a \"file\"/folder/directory \
that has the same structure as an RT130 CompactFlash card. If this \
directory only contains YYYYDOY-named day directories LOGPEEK will consider \
it a .cf-like directory and display all of the DAS IDs found in the \
directory. This could be used, for example, to look at the archive \
directory of an RTCC installation, or to direct LOGPEEK to read a mounted \
RT130 CF card directly.\n\
\n\
.mslogs\n\
-------\n\
This extension should be added to a folder/directory that contains the \
semi-miniseed valid files of the text lines from SOH messages from RT130 \
recorders. The log lines are packaged this way when the data from an \
experiment is sent to the DMC. It is the way the data will be returned \
when requested. There will usually be one day of log lines per file.\n\
\n\
\n\
COMMAND LINE ARGUMENTS\n\
======================\n\
-c\n\
--\n\
Because of the irregularities caused when the program is started by \
clicking on or selecting some kind of icon the program will either ask \
for an initial directory where the log source information files are \
located, or directory paths saved in the setups file will be used. If \
LOGPEEK is started from the command line the command line argument -c \
may be used to set the directory paths of the program to the current \
working directory of the shell window where the program is being started.\n\
\n\
-g\n\
--\n\
LOGPEEK attempts to remember its last screen position and size when it \
quits. If the last position it remembers is for a laptop connected to an \
external monitor, and it is then started with the external monitor \
disconnected it should at least show up such that the window can be \
dragged around and resized, but it might not be possible to get at \
the resize controls. If this happens quit LOGPEEK and restart it with \
the -g command line option. LOGPEEK will then restart with the main \
program window fully displayed within the laptop display.\n\
\n\
-x\n\
--\n\
LOGPEEK writes all program button and field values to a file when it \
quits. Starting it with this command line option will cause it to not \
read that file a start as if it were the first time on the system.\n\
\n\
\n\
MENU COMMANDS\n\
=============\n\
FILE MENU\n\
---------\n\
----- Delete Setups File -----\n\
Sometimes a bad setups file can cause the program to misbehave. This can \
be caused by upgrading the program, but reading the setups file created \
by an earlier version of the program, or trying to read a setups file that \
was created by running LOGPEEK on a different operating system. This \
function will delete the current setups file (whose location can be found \
in the About box) and then quit the program without saving the current \
parameters. This will reset all parameters to their default values. When \
the program is restarted a dialog box should appear saying that the setups \
file could not be found. If this does not happen the Delete Setups File \
function may possibly not have permission to delete the setups file.\n\
\n\
\n\
COMMANDS MENU\n\
-------------\n\
----- GPS Data Plotter -----\n\
This display plots all of the Latitude and Longitude values found in \
the \"GPS: POSITION\" SOH messages in the SOH Messages lines. Clicking \
the Read/Plot button will tell the function to read through all of the \
SOH messages that were read from the data source and plot the results. \
A point can be clicked on to display the time, latitude, longitude, and \
elevation values for that point. The range of the latitude and longitude \
plotted points in meters are displayed at the top of the plot. These are \
just a simple max/min subtraction with the longitude corrected for \
latitude.\n\
\n\
The function filters out positions that are the same more than once in \
a row. This can happen for various reasons, not the least of which being \
a non-functioning GPS module. This is why the display may show something \
like 'Plotted 4 of 1235.' That means there there were only 4 unique \
position measurements.\n\
\n\
Right-clicking on a point will delete that point from the plot to allow \
throwing out \"bad\" outlying points. If multiple data points are \
plotted on top of each other multiple right-clicks will be required to \
eliminate them, except points with the same lat/long as the point \
clicked on will also be removed. Re-reading the data source is required \
to recover the deleted points.\n\
\n\
The Export button writes information for the currently plotted data \
points to a tab-delimited file. The lines have the format\n\
\n\
    # Comment about the source information file(s)\n\
    Time  Latitude  Longitude  Elevation\n\
    Time  Latitude  Longitude  Elevation\n\
    ...\n\
The file will be in the current data directory and will be named the \
same as the source information file with \"gps.dat\" appended to the \
end.\n\
\n\
----- Log Search -----\n\
Displays a window where a string of text may be entered. The currently \
plotted SOH messages will be searched for lines containing the string. \
The search is not case-sensitive. Just the lines matching the search \
string will be displayed. This is different from the search field at \
the bottom of the SOH Messages window.\n\
\n\
----- Plot Time Ranges Of Files -----\n\
This function allows the user to select any combination of the information \
source files and produce a plot showing the time range covered by each \
file. This can be useful when sorting files to, for example, find which \
files belong to which service run. To keep the function fast it only \
looks at the SOH information in the information source files. In general \
the function plots a 1 pixel wide line for each SOH packet in any source \
file, and for each event header line of information in .log files. This \
might allow the function to show gaps where the recorder stopped recording, \
but it all depends on how much time is covered by the information source \
file. For example, if a .cf source is a year long, then a gap of 1 hour \
probably won't show up on the plot unless the form is stretched very long.\n\
\n\
As the function reads the files it also collects ACQUISITION STARTED and \
ACQUSITION STOPPED message times. These will show up in the information \
flag shown by clicking on the information source file name, and as green \
and red lines in the plots. The number of start/stop events will be \
limited in the information flags to 15.\n\
\n\
Date Limit fields -----\n\
The Date Limits fields can be used to limit the starting and ending times \
of all of the plots. This can be used in cases where one or more of the \
plotted information sorce files start way before, or extend way after \
the other files, because of something like an internal clock timing \
error.\n\
\n\
Sort button -----\n\
The Sort button rearranges the listed files in alphabetical order. \
This is not an ASCII text order where \"a\" would follow \"Z\".\n\
\n\
Warn If Big -----\n\
When this checkbutton is selected plotting the time ranges and the \
rename function will wan the user before it tries to process large \
source files. What size is considered \"large\" is still being worked \
out.\n\
\n\
All and Each radiobuttons -----\n\
Normally the plot mode (radiobuttons All and Each) will be All for checking \
files against each other in time. Selecting the Each mode will just plot \
the points for each file all of the way across the plot area. This will, \
in effect, spread out the points so gaps may be more easily detected.\n\
\n\
----- Plot Positions From Files -----\n\
This function allows the user to select any combination of the information \
source files and produce a map plot showing positions of the GPS information \
from each file. The plot can also show the relative elevations of each \
station.\n\
\n\
The function reads the SOH information from each source file selected and \
calulates a simple average of the second set of 5 GPS positions it comes \
across. It does this to try and skip over initial positions that might \
be from when the GPS was still getting a fix. If there are fewer than \
10 position measurements in a file then it does the best it can.\n\
\n\
----- Export TT Times As Deployment File (Line Format) -----\n\
----- Export TT Times As Deployment File (Block Format) -----\n\
This command will scan through the current log messages looking for the \
lines generated by programs like ref2log that indicate the beginning of \
each event. These lines contain the string \"TT = yyyy:ddd:...\" which \
is the time of the start of the event. These are collected for each \
data stream and written to a user-selected file in the PASSCAL \
Deployment File Shot Line or Block information format. The Station ID and \
Shot ID/FFID will be the DAS event number.\n\
\n\
The Only SOH Items checkbutton cannot be selected when reading \
information source files .cf, .zip, .ref or .mslogs when an export \
function is going to be used for obvious reasons: The SOH Messages will \
not contain any event information. At least one of the DSs checkbuttons \
will also need to be selected for the same reason.\n\
\n\
The position information is obtained from the Event Trailer packet of \
binary data sources. There will be no position information when reading \
.log files. Of course the GPS should have been on continuously for the \
positions to be good.\n\
\n\
----- Export TT Times As TSP Shotfile/Geometry -----\n\
This command performs the same action as the Extract TT Times As \
Deployment File options, except the output format is close to the shot \
file and geometry file formats required by UTEP's TSP program. The file \
must have the FFID and length (of time) fields edited by the user before \
TSP will accept the file. The Station ID and Shot ID/FFID will be the \
DAS event number.\n\
\n\
The Only SOH Items checkbutton cannot be selected when reading information \
source files .cf, .zip, .ref or .mslogs when this function is going \
to be used for obvious reasons: The SOH Messages will not contain any \
event information. At least one of the DSs checkbuttons will also need \
to be selected for the same reason.\n\
\n\
The position information is obtained from the Event Trailer packet of \
binary data sources. There will be no position information when reading \
.log files. Of course the GPS should have been on continuously for the \
positions to be good.\n\
\n\
----- Export TT Times As Shot Info -----\n\
Same as above options, but a single event time is listed on each line \
with the format\n\
\n\
    StaID ShotID YYYY:DOY:HH:MM:SS.sss Lat Long Elev\n\
\n\
The Station ID and Shot ID/FFID will be the DAS event number.\n\
\n\
The Only SOH Items checkbutton cannot be selected when reading information \
source files .cf, .zip, .ref and .mslogs when this function is going \
to be used for obvious reasons: The SOH Messages will not contain any \
event information. At least one of the DSs checkbuttons will also need \
to be selected for the same reason.\n\
\n\
The position information is obtained from the Event Trailer packet of \
binary data sources. There will be no position information when reading \
.log files. Of course the GPS should have been on continuously for the \
positions to be good.\n\
\n\
\n\
PLOTS MENU\n\
----------\n\
----- All of those items -----\n\
Use these items to turn individual item plots on and off.\n\
\n\
----- All Plots On -----\n\
Simply selects all of the individual plots in the menu, with the \
exception of the Disk Usage, and Mass Position checkbuttons. They are not \
changed. They can take up a lot of plotting space, so the user must \
specifically select them.\n\
\n\
\n\
OPTIONS MENU\n\
------------\n\
----- Plot Timing Problems -----\n\
Normally as the SOH Messages are read after they have been extracted \
from the information source file LOGPEEK will read until it finds the line \
identifying the first State Of Health (SOH) block of messages. It \
will obtain the date and time from that line. All lines read before \
that first SOH block will be skipped. Lines will continue to be read \
as long as a line's time is later than the line preceeding it. If \
there was a timing problem with the DAS, and the timestamps in the log \
file are really messed up (e.g. the time jumps forward several years \
and then jumps back to the current year, thus causing the program to \
not process the remainder of the file after the time jumps back) then \
the program can be forced to process all of the lines in the file and \
plot points in the order it finds them by checking the PlotTiming \
Problems option.\n\
\n\
----- Filter Out Non-SOH Lines -----\n\
Some programs add lines to .log files (not the SOH messages in .cf \
or other sources). This will try to filter most of them out and leave \
just the proper SOH messages. Some lines will still slip through, because \
there is nothing wrong with them and they look line normal information \
lines. This should only be needed if a .log file is causing problems \
for LOGPEEK.\n\
\n\
----- Sort Files List By Type -----\n\
----- Sort Files List Alphabetically -----\n\
These two options control how the files will be listed in the file \
selection list. Normally they should be grouped by type (.log, .ref, \
.cf, .zip, .mslogs), but when plotting more than one source file together \
it may be convenient to have them sorted alphabetically so the file \
earlier in time is listed before the later file (if they have been named \
wisely).\n\
\n\
The alphabetical sorting is not by ASCII character where \"a\" would \
follow \"Z\". All \"A\"'s and \"a\"'s will be sorted together, then \"B\"'s \
and \"b\"'s, etc.\n\
\n\
After changing these selections use the Reload button to reload the \
file selection list.\n\
\n\
----- Calculate File Sizes -----\n\
The sizes of the files and folders/directories listed in the file \
selection list can have their sizes displayed. This can be turned on \
and off, because calculating the sizes of all of the files listed can \
take a significant amount of time if there are a lot of files. Each \
listed \"file\" can contain 1000's of files depending on the type.\n\
\n\
Some items list an estimate of the number of log message lines that an \
item contains. This is done by assuming 48 characters per log line. It's \
usually within a few thousand lines of the correct number with large \
log file listings.\n\
\n\
After changing this option you must click the Reload button below the \
file selection list.\n\
\n\
----- Warn If Big -----\n\
LOGPEEK is not particularly speedy (but it is very portable between \
operating systems). It will choke/grind to a halt on large source files \
or folders. If the file LOGPEEK is about to read is above a certain \
size there will be a warning message if this item is selected. What \
constitutes a large size is still being determined.\n\
\n\
----- Add Mass Positions To SOH Messages -----\n\
The mass position voltages are recorded in data stream 9 of the RT130 \
raw data if reqested in the RT130 recording parameters. There is no \
mass position information in the state of health messages. If the \
voltages were being written to data stream 9, either of the Mass Pos \
123 or 456 checkbuttons are selected, this Option menu item is selected, \
and a raw data source file (.ref, .cf or zipped CF file) is read, then \
lines of information about the mass positions will be written to the \
SOH Messages window following all of the normal SOH messages extracted \
from the source file.\n\
\n\
With the mass position messages being in with the rest of the SOH messages \
writing the SOH messages to a .log file using the SOH Messages window's \
Write To File button, will make it so that when the saved file is read \
by LOGPEEK the mass position voltages will be plotted if one of the Mass \
Pos checkbuttons (123 or 456) are selected. The mass position messages \
start with \"LPMP\" (LOGPEEK Mass Position) and will be the last items \
listed. The information in each line will be the date/time, the channel \
and the mass position value.\n\
\n\
These messages are not subject to the usual timing checks, so for files \
with severe timing problems it is possible that the points may be plotted \
no where near where they should be. Be careful out there.\n\
\n\
This is all a small kludge until the mass position information is \
written to the SOH messages by the RT130. (Still not done as of 2018.)\n\
\n\
----- MP Coloring: x.x, x.x, x.x, x.x -----\n\
These items are used to set the voltage ranges and values for coloring \
mass position plots. The x.x values are in volts as read through the \
RT130. The first range of settings are for \"regular\" broadband sensors \
and the second set is for Trillium sensors. They are the ones that \
started the need for more than one range.\n\
\n\
----- Add Positions To ET Lines ----\n\
When selected the Latitude and Longitude recorded by the DAS will be added \
to the end of the \"DAS:\" lines that are made from the Event Trailer (ET) \
packets in binary data files. The positions can be extracted by the Export \
TT Times routine and included in its output files. This may make the DAS: \
lines unreadable by other programs using the SOH messages, so it is \
optional. The positions will follow the tag ETPOS.\n\
\n\
----- Read Antelope-Produced Log Files ----\n\
SOH messages extracted from an Antelope database using the Antelope qlog \
program which were originally the .log files produced by the programs \
like ref2mseed, ref2log, rt2ms, and others, that were imported into the \
database, or the log files written to the station directories in the db \
directory by Antelope, may be read by selecting this option. These files \
basically just have information from Antelope placed at the beginning of \
each SOH message line. This simply tells LOGPEEK to strip that information \
off before beginning to process the SOH message lines.\n\
\n\
This option will also work with files of information extracted with qlog \
from real-time rt2orb log recording. Real-time logs will not have the \
basic information that LOGPEEK needs (specifically, the \"State of \
Health\" lines), so LOGPEEK will ask for the start year of the log \
information.\n\
\n\
.log files placed into Antelope will come back out with all of the usual \
RT130 log messages intact, however, SOH logs produced by Antelope from \
realtime data will be missing quite a few things. LOGPEEK does not know \
how to decode the Antelope versions of ET, EH, ST, etc. packets.\n\
\n\
The option must be unselected when not reading log files produced by \
Antelope.\n\
\n\
----- Show YYYY:DOY Dates ----\n\
----- Show YYYY-MM-DD Dates -----\n\
----- Show YYYYMMMDD Dates -----\n\
Selecting one of these menu items will tell LOGPEEK to display all of \
the dates in the selected format, except for the actual SOH messages. \
They will always be in the DOY:HH:MM:SS format.\n\
\n\
----- Set Font Sizes -----\n\
Brings up a form so the proportional and mono-spaced font sizes can be \
changed. Some systems use positive integers and some use negative numbers. \
The user will need to experiment. A larger number value is always a \
larger font.\n\
\n\
Most systems do not support all numerical values, so a value may be \
changed with no resulting change in size of the displayed fonts.\n\
\n\
\n\
FORMS MENU\n\
----------\n\
The currently open forms of the program will show up here. Selecting one \
will bring that form to the foreground.\n\
\n\
\n\
HELP MENU\n\
---------\n\
----- Calendar -----\n\
Just a built-in calendar that shows dates or DOY values for three months \
in a row. Of course the clock of the computer that is running LOGPEEK must \
be correct for this to show the right current date.\n\
\n\
----- Check For Updates -----\n\
If the computer is connected to the Internet selecting this function will \
contact PASSCAL's website and check the version of the program against \
a copy of the program that is on the website. Appropriate dialog boxes \
will be shown depending on the result of the check.\n\
\n\
If the current version is old the Download button in the dialog box that \
will appear may be clicked to obtain the new version. The new version will \
be delivered in a zipped file/folder and the name of the program will be \
preceeded by \"new\". A dialog box indicating the location of the \
downloaded file will be shown after the downloading finishes. Once it has \
been confirmed by the user that the \"new\" program file is OK, it should \
be renamed (remove \"new\") and placed in the proper location which \
depends on the operating system of the computer.\n\
\n\
\n\
BUTTONS AND FIELDS\n\
==================\n\
----- Main Data Dir button -----\n\
This button may be used to navigate to the directory in which to look \
for source information files. When the OK button is selected in the file \
picker that directory will be read.\n\
\n\
---- The Current Working Directory field ----\n\
The directory to look for information in may be entered directly into \
this field. Pressing the Return key afterwards will read the entered \
directory. All directory paths are absolute verses relative.\n\
\n\
----- From/To fields and C buttons -----\n\
When reading a large source of SOH information, like a .ref file that \
spans months or more, the from and to date range may be entered into \
these fields to restrict how much of the SOH information gets plotted. \
The date format may be YYYY:DOY, YYYY-MM-DD or YYYYMMMDD. To read and \
plot everything in the selected source just clear the date from both \
fields.\n\
\n\
Entering just a From date will tell LOGPEEK to plot from that date \
to the end of the selected source, and likewise, just entering a To \
date will plot from the beginning of the selected source to the To date. \
The plotted times are from 00:00 on the From date to 24:00 on the To \
date.\n\
\n\
The C buttons bring up date pickers that fill in their associated date \
field by clicking on the desired date.\n\
\n\
The plotted information will generally not start and end exactly at \
00:00 and 24:00. The plotted points may start after 00:00 and run past \
midnight.\n\
\n\
The checkbutton left of the From field may be used to turn using the \
date limits on and off.\n\
\n\
----- Find field and Clear button ----\n\
A word may be entered into the Find field, the Return key pressed, \
and any of the file names in the files list containing that word will \
displayed at the top of the list. The Clear button just clears the Find \
field.\n\
\n\
----- .log .ref. .zip .cf .mslogs checkbuttons -----\n\
These control the different types of state of heath information sources \
that will show up in the list of data source files. The Reload button just \
refreshes the list.\n\
\n\
The .cf checkbutton must be selected when trying to read directly from a \
mounted CF card.\n\
\n\
----- Background B and W radiobuttons -----\n\
The B button will make the background of the main plotting area black \
and the W button white. The white background should be used if the plots \
are going to be printed to use less ink/toner.\n\
\n\
----- Replot button -----\n\
This will clear and redraw all of the current plots. This will only work \
for bringing up the RAW or TPS plot windows if they were not previously \
created, or for changing background colors. It will not, for example, \
make bringing up and plotting the raw data if the one of the DS \
checkbuttons were not initially selected. In that case the source file \
will have to be re-read using the Read button.\n\
\n\
----- Only SOH Items checkbutton -----\n\
Normally LOGPEEK will try to read as much information as it can. Selecting \
this checkbutton will tell LOGPEEK to only extract and plot information \
found in the SOH log file lines. This will greatly speed up reading data \
sources, but the amonut of information plotted will be less.\n\
\n\
----- Mass Pos 123 and 456 checkbuttons -----\n\
When selected the mass position readings recorded in the AUX Data \
will be plotted for the selected channel group. The color scheme is\n\
   +/-.5V will be cyan (black background) or light\n\
           grey (white background)\n\
   +/-2.0V green or grey\n\
   +/-4.0V yellow or dark grey\n\
   +/-7.0V red or dark blue\n\
   >8.0V magenta or black\n\
\n\
Currently only about every 500th data point is plotted since the position \
is normally recorded every 10 seconds as data stream 9 raw data, which \
is way too often.\n\
\n\
----- DSs checkbuttons -----\n\
Checking any of these checkbuttons, along with the RAW checkbutton will \
tell LOGPEEK to additionally decode raw seismic data and display a plot \
for all channels in each data stream selected in an additional window. \
See RAW checkbutton below.\n\
\n\
----- RAW checkbutton -----\n\
This will control whether or not the RAW Data Plot window appears. The \
data displayed will cover the same time range as the main display plots. \
Nothing beyond just displying the data is supported (no zooming or \
picking times). The quality of the plots will vary depending on the \
data format/recording mode, and the amount of compression when \
recording using compressed modes. LOGPEEK does not read all of the data \
points, but only a subset of them, otherwise it would take way too long to \
read large data files.\n\
\n\
For compressed data formats only the first value from each 1024-byte DT \
packet is plotted. Depending on the compression factor that could mean \
that as few as one of every 892 samples will be plotted, which could \
mean that whole events may be missed (though the theory is that if there \
is a seismic event then the compression factor will go down and more \
points from the event will be plotted). The raw data display function is \
not for analyzing data, but just for checking to see if the station is \
functioning.\n\
\n\
The amount of seismic data plotted can be increased using the All \
checkbutton (see below).\n\
\n\
If the Mass Pos 123 or 456 checkbutton is selected then the mass position \
information for each channel will also be plotted on the RAW Data Plot \
display below each channel's raw data.\n\
\n\
----- All checkbutton -----\n\
When selected this will tell LOGPEEK to decode all data points in each \
data packet, instead of the normal, highly-decimated way it normally \
decodes the data (only collecting roughly one data point per packet). \
This should only be used when examining very small data sets. 3 channels, \
100sps, for 24 hours is about the limit. Anything more than that and \
you run the risk of a memory error and/or growing old before the reading \
finishes.\n\
\n\
Using the From and To dates may be used to look at a day's worth of \
data when using this option.\n\
\n\
This option does not work if the data were recorded using Steim2 \
compression (C2 on the RT130). Python is not good at bit-fiddling, and \
decoding C2 data would take forever. C1 is bad enough.\n\
\n\
----- TPS checkbutton -----\n\
This will control whether or not the Time-Power-Squared window appears. \
The Time-Power-Squared plot displays the average of the squared amplitudes \
over every 5-minute period covering the time range of the main plot. Each \
horizontal line of the display contains one whole day of data from 0000 to \
2400 GMT. Each 5-minute block is color-coded according to the average \
amplitude during the period. There are four ranges, Antarctica, Low, \
Medium and High.\n\
Antarctica:\n\
    Dark Grey/Black - there was no data for that period or the\n\
                      average really was 0.0 (not likely)\n\
          Dark blue - the average was +/-10 counts\n\
               Cyan - +/-100 counts\n\
              Green - +/-1,000 counts\n\
             Yellow - +/-10,000 counts\n\
                Red - +/-100,000 counts\n\
            Magenta - +/-1,000,000 counts\n\
   Light Grey/White - > +/-1,000,000 counts\n\
Low:\n\
    Dark Grey/Black - there was no data for that period or the\n\
                      average really was 0.0 (not likely)\n\
          Dark blue - the average was +/-20 counts\n\
               Cyan - +/-200 counts\n\
              Green - +/-2,000 counts\n\
             Yellow - +/-20,000 counts\n\
                Red - +/-200,000 counts\n\
            Magenta - +/-2,000,000 counts\n\
   Light Grey/White - > +/-2,000,000 counts\n\
Medium:\n\
    Dark Grey/Black - there was no data for that period or the\n\
                      average really was 0.0 (not likely)\n\
          Dark blue - the average was +/-50 counts\n\
               Cyan - +/-500 counts\n\
              Green - +/-5,000 counts\n\
             Yellow - +/-50,000 counts\n\
                Red - +/-500,000 counts\n\
            Magenta - +/-5,000,000 counts\n\
   Light Grey/White - > +/-5,000,000 counts\n\
High:\n\
    Dark Grey/Black - there was no data for that period or the\n\
                      average really was 0.0 (not likely)\n\
          Dark blue - the average was +/-80 counts\n\
               Cyan - +/-800 counts\n\
              Green - +/-8,000 counts\n\
             Yellow - +/-80,000 counts\n\
                Red - +/-800,000 counts\n\
            Magenta - +/-8,000,000 counts\n\
   Light Grey/White - > +/-8,000,000 counts\n\
\n\
The ranges are selected by using the \"A\", \"L\", \"M\" and \"H\" \
radiobuttons on the plot window.\n\
\n\
Clicking along a days line will display the counts value during the \
five-minute period for that point on the line.\n\
\n\
A broken dark/orange line of boxes (a dotted line) indicates that there \
were no data for 1 or more days. This is done instead of plotting, possibly \
100's, of lines with nothing in them. The dots can be clicked on to see how \
many days were skipped.\n\
\n\
Control-clicking on any part of the plots will display the 'center' time \
of the 5-minute block that was clicked on. Clock rules and the time will \
also be drawn on the Raw Data and the main plots at the same time point. \
Control-clicking to the left of the plots will remove the times and time \
indicators.\n\
\n\
----- Read/Stop buttons -----\n\
The Read button starts the reading of the selected source file, and \
the Stop button halts the reading of a source file. This may be \
used if it is decided that the file selected is not the correct one, \
or is too long to wait to be read. The button will only stop LOGPEEK \
while reading the SOH information. Once that point has been passed \
LOGPEEK will not stop until after the information has been plotted.\n\
\n\
----- Write .ps button -----\n\
This creates a PostScript file of the the contents of the plot area. \
When selected a dialog box will allow the name of the file to be selected \
or entered.\n\
\n\
----- Redraw button (lower right-hand corner) -----\n\
Use this button after changing the size of the main window to command \
LOGPEEK to redraw the plots in just the main window. There is too much \
inconsistancy between installations and versions of operating systems \
for this to be done automatically. Some systems command LOGPEEK to \
redraw the plots after the resizing of the window is finished, and \
some send the command many times WHILE resizing the window. On some \
systems it is easy to change this behavior, and on some systems it \
isn't. This button makes it the responsibility of the user to click \
the Redraw button when they are finished resizing the window.\n\
\n\
\n\
PLOTTED ITEMS\n\
=============\n\
DSP-CLK Diff\n\
Points on this plot are generated by SOH lines that indicate a \
measurement difference between the DAS DSP time and the GPS 1pps timing \
pulse. This plot will only be displayed if messages about this measurement \
are in the information source SOH messages.\n\
\n\
Phase Error\n\
This is the internal clock phase error of the DASs timing system.\n\
\n\
Jerks/DSP Sets\n\
This plot indicates where the DAS firmware \"jerked\" the DAS's time to \
get it close to the GPS time. This usually only occurs when a DAS is \
initially powered up.\n\
\n\
Err CHx\n\
Data from an error file. Each line corresponds to a data channel. \
Represented are the overlaps and gaps in the timing information. The \
length of the line indicates the amount of the time jump. A red line \
indicates that there was a correction made to the system clock which \
changed the time backwards (overlap), but the value in the .err file \
will be a positive number. A green line indicates that the time was \
jumped forward (gap).\n\
\n\
The error files are created by programs like ref2mseed. LOGPEEK itself \
does not do these calculations.\n\
\n\
GPS On/Off/Err\n\
This plot indicates when the GPS clock was powered up by the DAS. The \
GPS may or may not have actually powered up, or even been connected at the \
time. The green dots are power-ups, and the red dots are power-downs. It \
also shows a magenta dot for any SOH messages with \"EXTERNAL CLOCK ERROR\" \
in them.\n\
\n\
GPS Lk-Unlk\n\
This plot shows when the GPS connected to the RT130 or 72A indicates that \
it is locked and unlocked.\n\
\n\
Clock Power State Messages\n\
Imbedded in the center of the GPS LK-UNLK plot may be yellow dots that \
indicate a change in the power state of the GPS clock. The two usual GPS \
clock states are NORMAL (cycling once per hour) and CONTINUOUS.\n\
\n\
Dump Call\n\
This plot shows when the DAS calls for the dumping of the RAM to the CF or \
external disk drive and when the operation is completed.\n\
\n\
Events DS1\n\
Events DS2\n\
Points on these lines plot the trigger time of each event for each displyed \
data stream. When reading .ref, .zip and .cf information source files \
the lines in the SOH Messages for these points are generated by LOGPEEK.\n\
\n\
Disk 1 Usage\n\
Disk 2 Usage\n\
These show the amount of disk space used for disk 1 and 2. These should \
normally be a smooth ramp up.\n\
\n\
DAS Temp and Battery Volts\n\
These plots are the internal DAS temperature and the external battery \
voltage as maesured by the DASs.\n\
\n\
Reset/Powerup\n\
Red dots on the top of this plot line indicate times when the DAS was \
reset and green dots below the line indicate powerup events.\n\
\n\
Discrepancies\n\
These points are plotted when lines with \"DISCREPANCY\" are found. These \
are some test lines that have been put in by Reftek (2005SEP22) to \
watch for GPS clock problems.\n\
\n\
Error/Warning\n\
This plot shows lines placed into the SOH messages by programs like \
ref2segy that have the word \"ERROR\" or \"WARNING\" in them. The \
position of the dots in time on the plot for these messages may, at \
times, not make any sense. Some of these message lines in the log files \
(mostly WARNING messages) do not have a timestamp at the beginning, so \
LOGPEEK just uses the time from the last line that had a valid timestamp, \
which, since a lot of the warning messages have to do with clock problems, \
means that the dots may show up in the wrong place.\n\
\n\
WARNING messages that do not have a timestamp (and that use the last \
valid timestamp) will be plotted using orange on the ERROR/WARNING plot. \
The dots for them will also be a bit longer and a bit narrower than the \
normal yellow dots for warning messages with timestamps, so that they \
will stick out below yellow dots if one is plotted on top of the orange \
dot, and so the yellow dots will still show up (barely) if the orange \
dot is plotted on top of a yellow one.\n\
\n\
Some SOH messages also have the words ERROR and WARNING in them and \
have normal time stamps. These will also be plotted here.\n\
\n\
Re-center\n\
This plot indicates when the DAS sent mass re-center commands to the \
attached sensor.\n\
\n\
ACQ On-Off\n\
Plots the times when acquistion was started and stopped.\n\
\n\
SOH/Data Defs\n\
This shows a dot for each line in a .log file that starts with \"State \
of Health\", or each SH packet from .ref, .cf and .zip information \
sources.\n\
\n\
This plot lin also shows the beginning of the data definition \
blocks/packets which include Data Stream Definition, Station Channel \
Definition, Calibration Definition, etc. packets.\n\
\n\
Net Up-Down\n\
The plot points out when an RT130 is either first sync'ing up with \
Reftek's RTPD product, re-sync'ing with RTPD, or also if the RT130 is \
having trouble maintaining it's communication with RTPD. These \
messages are not the Ethernet Stack itself going up or down, but \
rather one of the layer's built into the RTP thread of the RT130.\n\
\n\
END\n"


##############################################
# BEGIN: formHELP(Parent, AllowWriting = True)
# LIB:formHELP():2019.042
#   Put the help contents in global variable HELPText somewhere in the
#   program.
PROGFrm["HELP"] = None
HELPFindLookForVar = StringVar()
HELPFilespecVar = StringVar()
PROGSetups += ["HELPFindLookForVar", "HELPFilespecVar"]
HELPFindIndexVar = IntVar()
HELPFindLastLookForVar = StringVar()
HELPFindLinesVar = StringVar()
HELPFindUseCaseCVar = IntVar()
HELPHeight = 25
HELPWidth = 80
HELPFont = PROGOrigMonoFont


def formHELP(Parent, AllowWriting=True):
    if PROGFrm["HELP"] is not None:
        PROGFrm["HELP"].deiconify()
        PROGFrm["HELP"].lift()
        return
    LFrm = PROGFrm["HELP"] = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "HELP"))
    LFrm.title("Help - %s" % PROG_NAME)
    LFrm.iconname("Help")
    Sub = Frame(LFrm)
    LTxt = PROGTxt["HELP"] = Text(Sub, font=HELPFont, height=HELPHeight,
                                  width=HELPWidth, wrap=WORD, relief=SUNKEN)
    LTxt.pack(side=LEFT, expand=YES, fill=BOTH)
    LSb = Scrollbar(Sub, orient=VERTICAL, command=LTxt.yview)
    LSb.pack(side=RIGHT, fill=Y)
    LTxt.configure(yscrollcommand=LSb.set)
    Sub.pack(side=TOP, expand=YES, fill=BOTH)
    Sub = Frame(LFrm)
    labelTip(Sub, "Find:=", LEFT, 30, "[Find]")
    LEnt = Entry(Sub, width=20, textvariable=HELPFindLookForVar)
    LEnt.pack(side=LEFT)
    LEnt.bind("<Return>", Command(formFind, "HELP", "HELP"))
    LEnt.bind("<KP_Enter>", Command(formFind, "HELP", "HELP"))
    BButton(Sub, text="Find", command=Command(formFind,
                                              "HELP", "HELP")).pack(side=LEFT)
    BButton(Sub, text="Next", command=Command(formFindNext,
                                              "HELP", "HELP")).pack(side=LEFT)
    if AllowWriting:
        Label(Sub, text=" ").pack(side=LEFT)
        BButton(Sub, text="Write To File",
                command=Command(formHELPWrite, LTxt)).pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    BButton(Sub, text="Close", fg=Clr["R"],
            command=Command(formClose, "HELP")).pack(side=LEFT)
    Sub.pack(side=TOP, padx=3, pady=3)
    PROGMsg["HELP"] = Text(LFrm, font=PROGPropFont, height=3, wrap=WORD)
    PROGMsg["HELP"].pack(side=TOP, fill=X)
    LTxt.insert(END, HELPText)
# Clear this so the formFind() routine does the right thing if this form was
# brought up previously.
    HELPFindLinesVar.set("")
    center(Parent, LFrm, "CX", "I", True)
    if len(HELPFilespecVar.get()) == 0 or \
            exists(dirname(HELPFilespecVar.get())) is False:
        # Not all programs will have all directory vars. Look for them in this
        # order.
        try:
            HELPFilespecVar.set(PROGWorkDirVar.get() +
                                PROG_NAMELC + "help.txt")
        except NameError:
            try:
                HELPFilespecVar.set(PROGMsgsDirVar.get() + PROG_NAMELC +
                                    "help.txt")
            except NameError:
                # The program HAS to have one of these, so don't try here. Let
                # it crash.
                HELPFilespecVar.set(PROGDataDirVar.get() + PROG_NAMELC +
                                    "help.txt")
    return
#####################################
# BEGIN: formHELPWrite(Who, e = None)
# FUNC:formHELPWrite():2019.042


def formHELPWrite(Who, e=None):
    Dir = dirname(HELPFilespecVar.get())
    if Dir.endswith(sep) is False:
        Dir += sep
    File = basename(HELPFilespecVar.get())
    Filespec = formMYDF("HELP", 3, "Save Help To...", Dir, File)
    if len(Filespec) == 0:
        setMsg("HELP", "", "Nothing done.")
    try:
        Fp = open(Filespec, "w")
    except Exception as e:
        setMsg("HELP", "MW", "Error opening help file\n   %s\n   %s" %
               (Filespec, e), 3)
        return
    Fp.write("Help for %s version %s\n\n" % (PROG_NAME, PROG_VERSION))
    N = 1
    while True:
        if len(Who.get("%d.0" % N)) == 0:
            break
# The lines from the Text field do not come with a \n after each screen line,
# so we'll have to split the lines up ourselves.
        Line = Who.get("%d.0" % N, "%d.0" % (N + 1))
        N += 1
        if len(Line) < 65:
            Fp.write(Line)
            continue
        Out = ""
        for c in Line:
            if c == " " and len(Out) > 60:
                Fp.write(Out + "\n")
                Out = ""
            elif c == "\n":
                Fp.write(Out + "\n")
                Out = ""
            else:
                Out += c
    Fp.close()
    HELPFilespecVar.set(Filespec)
    setMsg("HELP", "", "Help written to\n   %s" % Filespec)
    return
# END: formHELP


############################################################################
# BEGIN: formMYD(Parent, Clicks, Close, C, Title, Msg1, Msg2 = "", Bell = 0,
#                CenterX = 0, CenterY = 0, Width = 0)
# LIB:formMYD():2019.030
#   The built-in dialog boxes cannot be associated with a particular frame, so
#   the Root/Main window kept popping to the front and covering up everything
#   on some systems when they were used, so I wrote this. I'm sure there is a
#   "classy" way of doing this, but I couldn't figure out how to return a
#   value after the window was destroyed from a classy-way.
#
#   The dialog box can contain an input field by using something like:
#
#   Answer = formMYD(Root, (("Input60", TOP, "input"), ("Write", LEFT, \
#           "input"), ("(Cancel)", LEFT, "cancel")), "cancel", "YB"\
#           "Let it be written...", \
#   "Enter a message to write to the Messages section (60 characters max.):")
#   if Answer == "cancel":
#       return
#   What = Answer.strip()
#   if len(What) == 0 or len(What) > 60:
#       Root.bell()
#   stdout.write("%s\n"%What)
#
#   The "Input60" tells the function to make an entry field 60 characters
#   long.  The "input" return value for the "Write" button tells the routine
#   to return whatever was entered into the input field. The "Cancel" button
#   will return "cancel", which, of course, could be confusing if the user
#   entered "cancel" in the input field and hit the Write button. In the case
#   above the Cancel button should probably return something like "!@#$%^&",
#   or something else that the user would never normally enter.
#
#   A "default" button may be designated by enclosing the text in ()'s.
#   Pressing the Return or keypad Enter keys will return that button's value.
#   The Cancel button is the default button in this example.
#
#   A caller must clear, or may set MYDAnswerVar to clear a previous
#   call's entry or to provide a default value for the input field before
#   calling formMYD().
#
#   To bring up a "splash dialog" for messages like "Working..." use
#
#       formMYD(Root, (), "", "", "", "Working...")
#
#   Call formMYDReturn to get rid of the dialog:
#
#       formMYDReturn("")
#
#   CenterX and CenterY can be used to position the dialog box when there is
#   no Parent if they are set to something other than 0 and 0.
#
#   Width may be set to something other than 0 to not use the default message
#   width.
#
#   Adding a length and text to the end of a button definition like below will
#   cause a ToolTip item to be created for that button.
#       ("Stop", LEFT, "stop", 35, "Click this to stop the program.")
#
#   This is REALLY kinda kludgy, but for a good cause, since there's already a
#          lot of arguments...
#      The normal font will be PROGPropFont.
#      If the Msg1 value begins with "|" then PROGMonoFont.
#      Starts with "}" will be PROGOrigPropFont.
#      Starts with "]" will be PROGOrigMonoFont.
#
#   A secret Shift-Control-Button-1 click on the main message Label (the text
#   of Msg1) will return the string "woohoo" which can be used to exit a
#   while-loop of some kind to get the dialog box to keep the program held in
#   place until someone knows the secret to get the program to continue.
#   Passing
#       (("NB", TOP, "NB"),)
#   for the buttons will make no button show up. It's really only useful when
#   using this secret code. The user won't normally know how to break out of
#   a caller's loop.
#   Shift-Control-1 can also be used to indicate some kind of 'override'
#   answer to the caller.
MYDFrame = None
MYDLabel = None
MYDAnswerVar = StringVar()


def formMYD(Parent, Clicks, Close, C, Title, Msg1, Msg2="", Bell=0,
            CenterX=0, CenterY=0, Width=0):
    global MYDFrame
    global MYDLabel
# This Should Never Happen, but I think I saw it once where MYDFrame seemed to
# no longer point to a dialog that was visible. It may have just been a XQuartz
# glitch. The other alternative here is to destroy MYDFrame. We'll see how this
# works.
    if MYDFrame is not None:
        MYDFrame.deiconify()
        MYDFrame.lift()
        beep(2)
        return
    if Parent is not None:
        # Allow either way of passing the parent.
        if isinstance(Parent, astring) is True:
            Parent = PROGFrm[Parent]
# Without this update() sometimes when running a program through ssh everyone
# loses track of where the parent is and the dialog box ends up at 0,0.
# It's possible for this to fail if the program is being stopped in a funny
# way (like with a ^C over an ssh connection, etc.).
        try:
            Parent.update()
        except Exception:
            pass
    TheFont = PROGPropFont
    MonoFont = False
    if Msg1.startswith("|"):
        Msg1 = Msg1[1:]
        TheFont = PROGMonoFont
        MonoFont = True
    if Msg1.startswith("}"):
        Msg1 = Msg1[1:]
        TheFont = PROGOrigPropFont
        MonoFont = False
    if Msg1.startswith("]"):
        Msg1 = Msg1[1:]
        TheFont = PROGOrigMonoFont
        MonoFont = True
    LFrm = MYDFrame = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    LFrm.protocol("WM_DELETE_WINDOW", Command(formMYDReturn, Close))
# A number shows up in the title bar if you do .title(""), and if you don't
# set the title it ends up with the program name in it.
    if len(Title) == 0:
        Title = " "
    LFrm.title(Title)
    LFrm.iconname(Title)
# Gets rid of some of the extra title bar buttons.
    LFrm.transient(Parent)
# If C is X or it ends with X then set this so we get the modality right. Clean
# up C so the rest of the function doesn't need to worry about it.
    GSG = False
    if len(C) != 0:
        if C == "X":
            GSG = True
            C = ""
        else:
            if C.endswith("X"):
                GSG = True
                C = C.replace("X", "")
            LFrm.configure(bg=Clr[C[0]])
# Break up the incoming message about every 50 characters or whatever Width is
# set to.
    if Width == 0:
        Width = 50
    if len(Msg1) > Width:
        Count = 0
        Mssg = ""
        for c in Msg1:
            if Count == 0 and c == " ":
                continue
            if Count > Width and c == " ":
                Mssg += "\n"
                Count = 0
                continue
            if c == "\n":
                Mssg += c
                Count = 0
                continue
            Count += 1
            Mssg += c
        Msg1 = Mssg
# This is an extra line that gets added to the message after a blank line.
    if len(Msg2) != 0:
        if len(Msg2) > Width:
            Count = 0
            Mssg = ""
            for c in Msg2:
                if Count == 0 and c == " ":
                    continue
                if Count > Width and c == " ":
                    Mssg += "\n"
                    Count = 0
                    continue
                if c == "\n":
                    Mssg += c
                    Count = 0
                    continue
                Count += 1
                Mssg += c
            Msg1 += "\n\n" + Mssg
        else:
            Msg1 += "\n\n" + Msg2
    Sub = Frame(LFrm)
    if len(C) == 0:
        if MonoFont is False:
            MYDLabel = Label(Sub, text=Msg1, bd=15, font=TheFont)
        else:
            MYDLabel = Label(Sub, text=Msg1, bd=15, font=TheFont,
                             justify=LEFT)
        MYDLabel.pack(side=LEFT)
        Sub.pack(side=TOP)
        Sub = Frame(LFrm)
    else:
        if MonoFont is False:
            MYDLabel = Label(Sub, text=Msg1, bd=15, bg=Clr[C[0]],
                             fg=Clr[C[1]], font=TheFont)
        else:
            MYDLabel = Label(Sub, text=Msg1, bd=15, bg=Clr[C[0]],
                             fg=Clr[C[1]], font=TheFont, justify=LEFT)
        MYDLabel.pack(side=LEFT)
        Sub.pack(side=TOP)
        Sub = Frame(LFrm, bg=Clr[C[0]])
    MYDLabel.bind("<Shift-Control-Button-1>", Command(formMYDReturn, "woohoo"))
    One = False
    InputField = False
    for Click in Clicks:
        if Click[0] == "NB" and Click[2] == "NB":
            continue
        if Click[0].startswith("Input"):
            InputEnt = Entry(LFrm, textvariable=MYDAnswerVar,
                             width=intt(Click[0][5:]))
            InputEnt.pack(side=TOP, padx=10, pady=10)
# So we know to do the focus_set at the end.
            InputField = True
            continue
        if One is True:
            if len(C) == 0:
                Label(Sub, text=" ").pack(side=LEFT)
            else:
                Label(Sub, text=" ", bg=Clr[C[0]]).pack(side=LEFT)
        But = BButton(Sub, text=Click[0], command=Command(formMYDReturn,
                                                          Click[2]))
        if Click[0].startswith("Clear") or Click[0].startswith("(Clear"):
            But.configure(fg=Clr["U"], activeforeground=Clr["U"])
        elif Click[0].startswith("Close") or Click[0].startswith("(Close"):
            But.configure(fg=Clr["R"], activeforeground=Clr["R"])
        But.pack(side=Click[1])
# Check to see if there is ToolTip text.
        try:
            ToolTip(But, Click[3], Click[4])
        except Exception:
            pass
        if Click[0].startswith("(") and Click[0].endswith(")"):
            LFrm.bind("<Return>", Command(formMYDReturn, Click[2]))
            LFrm.bind("<KP_Enter>", Command(formMYDReturn, Click[2]))
        if Click[1] != TOP:
            One = True
    Sub.pack(side=TOP, padx=3, pady=3)
# CX. Always keep these fully on the display.
    center(Parent, LFrm, "CX", "I", True, CenterX, CenterY)
# If the user clicks to dismiss the window before any one of these things get
# taken care of then there will be touble. This may only happen when the user
# is running the program over a network and not on the local machine. It has
# something to do with changes made to the beep() routine which fixed a
# problem of occasional missing beeps.
    try:
        LFrm.focus_set()
        if GSG is False:
            # This is not working well on macOS with the "stock"
            # Pythons/Tkinters. We'll try this for a while and see how it
            # goes. What a pain. This is why PASSCAL created our own
            # Python/Tkinter package.
            if PROGSystem == "dar":
                pass
            else:
                LFrm.grab_set()
        else:
            if PROGSystem == "dar":
                pass
            else:
                LFrm.grab_set_global()
        if InputField is True:
            InputEnt.focus_set()
            InputEnt.icursor(END)
        if Bell != 0:
            beep(Bell)
# Everything will pause here until one of the buttons are pressed if there are
# any, then the box will be destroyed, but the value will still be returned.
# since it was saved in a "global".
        if len(Clicks) != 0:
            LFrm.wait_window()
    except Exception:
        pass
# At least do this much cleaning for the caller.
    MYDAnswerVar.set(MYDAnswerVar.get().strip())
    return MYDAnswerVar.get()
######################################
# BEGIN: formMYDReturn(What, e = None)
# FUNC:formMYDReturn():2018.256


def formMYDReturn(What, e=None):
    # If What is "input" just leave whatever is in the var in there.
    global MYDFrame
    if What != "input":
        MYDAnswerVar.set(What)
    MYDAnswerVar.set(MYDAnswerVar.get().strip())
# This function may get called more than once if there are errors and stuff,
# especially when the dialog box is just being used for splash messages.
    try:
        MYDFrame.destroy()
    except Exception:
        pass
    MYDFrame = None
    updateMe(0)
    return
############################
# BEGIN: formMYDMsg(Message)
# FUNC:formMYDMsg():2008.012


def formMYDMsg(Message):
    global MYDLabel
    MYDLabel.config(text=Message)
    updateMe(0)
    return
# END: formMYD


######################################################################
# BEGIN: formMYDF(Parent, Mode, Title, StartDir, StartFile, Mssg = "",
#                EndsWith = "", SameCase = True, CompFs = True, \
#                DePrefix = "", ReturnEmpty = False)
# LIB:formMYDF():2019.030
# NEEDS: PROGMsgsDirVar, PROGDataDirVar, PROGWorkDirVar if anyone tries to
#        use the Default codes in Mode.
#   Mode = (These must be the first character in Mode)
#          0 = allow picking a file
#          1 = just allow picking directories
#          2 = allow picking/making directories only
#          3 = pick/save directories and files (the works)
#          4 = pick multiple files and change dirs
#          5 = enter/pick a file, but not change directory
#          6 = enter/pick a file, but not change directory, and don't show the
#              directory
#   Mode = A second character that specifies one of the "main" directories
#          that can be selected using a "Default" button.
#          D = Main Data Directory (PROGDataDirVar.get())
#          W = Main Work Directory (PROGWorkDirVar.get())
#          M = Main Messages Directory (PROGMsgsDirVar.get())
#   Mode = Optional last character, X, that indicates that the form should use
#          grab_set_global(). This gets detected and stripped off right away so
#          the rest of the function doesn't have to worry about it.
#   Mssg = A message that can be displayed at the top of the form. It's a
#          Label, so \n's should be put in the passed Mssg string.
#   EndsWith = The module will only display files ending with EndsWith, the
#              entered filename's case will be matched with the case of
#              EndsWith (IF it is all one case or another). EndsWith may be
#              a passed string of file extensions seperated by commas. EndsWith
#              will be added to the entered filename if it is not there before
#              returning, but only if there is one extension passed.
#   SameCase = If True then the case of the filename must match the case of
#              the EndsWith items.
#   CompFs = If True then directory and file completion with the Tab key are
#            set up.
#   DePrefix = If not "" then only the files starting with DePrefix will be
#              loaded and the DePrefix will be removed.
#   ReturnEmpty = If True it allows clearing the file name field and clicking
#                 the OK button, instead of complaining that no file name was
#                 entered.
#   Tabbing in the directory field is supported.
MYDFFrame = None
MYDFModeVar = StringVar()
MYDFDirVar = StringVar()
MYDFFiles = None
MYDFDirField = None
MYDFFileVar = StringVar()
MYDFEndsWithVar = StringVar()
MYDFAnswerVar = StringVar()
MYDFHiddenCVar = IntVar()
MYDFSameCase = True
MYDFDePrefix = ""
MYDFReturnEmpty = False


def formMYDF(Parent, Mode, Title, StartDir, StartFile, Mssg="",
             EndsWith="", SameCase=True, CompFs=True, DePrefix="",
             ReturnEmpty=False):
    global MYDFFrame
    global MYDFFiles
    global MYDFDirField
    global MYDFSameCase
    global MYDFDePrefix
    global MYDFReturnEmpty
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
# Mode may be passed as an integer 1 or as a string "1X".
    Mode = str(Mode)
    GSG = False
    if Mode.endswith("X"):
        GSG = True
        Mode = Mode.replace("X", "")
# Everyone else looks for the str() version.
    MYDFModeVar.set(Mode)
    MYDFEndsWithVar.set(EndsWith)
    MYDFSameCase = SameCase
    MYDFDePrefix = DePrefix
    MYDFReturnEmpty = ReturnEmpty
# Without this update() sometimes when running a program through ssh everyone
# loses track of where the parent is and the dialog box ends up at 0,0.
# It's possible for this to fail if the program is being stopped in a funny
# way (like with a ^C over an ssh connection, etc.).
    try:
        Parent.update()
    except Exception:
        pass
    LFrm = MYDFFrame = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.protocol("WM_DELETE_WINDOW", Command(formMYDFReturn, None))
    LFrm.bind("<Button-1>", Command(setMsg, "MYDF", "", ""))
    LFrm.title(Title)
    LFrm.iconname("PickDF")
    Sub = Frame(LFrm)
# The incoming Mode may be something like "1W". There are special versions of
# intt() out there that don't convert the passed item to a string first, so
# now we want the beginning number part.
    ModeI = intt(Mode)
    if len(Mssg) != 0:
        Label(Sub, text=Mssg).pack(side=TOP)
    if ModeI != 5 and ModeI != 6:
        BButton(Sub, text="Up", command=formMYDFUp).pack(side=LEFT)
        LLb = Label(Sub, text=":=")
        LLb.pack(side=LEFT)
        ToolTip(LLb, 35, "[List Files] Press the Return key to list the files "
                "in the entered directory after editing the directory name.")
        LEnt = MYDFDirField = Entry(Sub, textvariable=MYDFDirVar, width=65)
        LEnt.pack(side=LEFT, fill=X, expand=YES)
        if CompFs is True:
            LEnt.bind("<FocusIn>", Command(formMYDFCompFsTabOff, LFrm))
            LEnt.bind("<FocusOut>", Command(formMYDFCompFsTabOn, LFrm))
            LEnt.bind("<Key-Tab>", Command(formMYDFCompFs, MYDFDirVar, None))
        LEnt.bind("<Return>", formMYDFFillFiles)
        LEnt.bind("<KP_Enter>", formMYDFFillFiles)
# KeyPress clears the field when typing. KeyRelease clears the field after it
# has been filled in.
        LEnt.bind("<KeyPress>", formMYDFClearFiles)
        if ModeI == 2 or ModeI == 3:
            But = BButton(Sub, text="Mkdir", command=formMYDFNew)
            But.pack(side=LEFT)
            ToolTip(But, 25, "Add the name of the new directory to the end of "
                    "the current contents of the entry field and click this "
                    "button to create the new directory.")
# Just show the current directory.
    elif ModeI == 5:
        MYDFDirField = Entry(Sub, textvariable=MYDFDirVar,
                             state=DISABLED)
        MYDFDirField.pack(side=LEFT, fill=X, expand=YES)
# Just don't show the current directory. Create, but don't pack.
    elif ModeI == 6:
        MYDFDirField = Entry(Sub, textvariable=MYDFDirVar)
    Sub.pack(side=TOP, fill=X)
    Sub = Frame(LFrm)
    if ModeI == 4:
        LLb = MYDFFiles = Listbox(Sub, relief=SUNKEN, bd=2, height=15,
                                  selectmode=EXTENDED, width=65)
    else:
        LLb = MYDFFiles = Listbox(Sub, relief=SUNKEN, bd=2, height=15,
                                  selectmode=SINGLE, width=65)
    LLb.pack(side=LEFT, expand=YES, fill=BOTH)
    LLb.bind("<ButtonRelease-1>", formMYDFPicked)
    Scroll = Scrollbar(Sub, command=LLb.yview)
    Scroll.pack(side=RIGHT, fill=Y)
    LLb.configure(yscrollcommand=Scroll.set)
    Sub.pack(side=TOP, expand=YES, fill=BOTH)
# The user can type in the filename.
    if ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
        Sub = Frame(LFrm)
        Label(Sub, text="Filename:=").pack(side=LEFT)
        LEnt = Entry(Sub, textvariable=MYDFFileVar)
        LEnt.pack(side=LEFT, fill=X, expand=YES)
        if CompFs is True:
            LEnt.bind("<FocusIn>", Command(formMYDFCompFsTabOff, LFrm))
            LEnt.bind("<FocusOut>", Command(formMYDFCompFsTabOn, LFrm))
            LEnt.bind("<Key-Tab>", Command(formMYDFCompFs, None, MYDFFileVar))
        LEnt.bind("<Return>", Command(formMYDFReturn, ""))
        LEnt.bind("<KP_Enter>", Command(formMYDFReturn, ""))
        Sub.pack(side=TOP, fill=X, padx=3)
    Sub = Frame(LFrm)
# Create a Default button and tooltip if the Mode says so.
    if isinstance(Mode, astring):
        if Mode.find("D") != -1:
            LBu = BButton(Sub, text="Default",
                          command=Command(formMYDFDefault, "D"))
            LBu.pack(side=LEFT)
            ToolTip(LBu, 35, "Set to Main Data Directory")
            Label(Sub, text=" ").pack(side=LEFT)
        elif Mode.find("W") != -1:
            LBu = BButton(Sub, text="Default",
                          command=Command(formMYDFDefault, "W"))
            LBu.pack(side=LEFT)
            ToolTip(LBu, 35, "Set to Main Work Directory")
            Label(Sub, text=" ").pack(side=LEFT)
        elif Mode.find("M") != -1:
            LBu = BButton(Sub, text="Default",
                          command=Command(formMYDFDefault, "M"))
            LBu.pack(side=LEFT)
            ToolTip(LBu, 35, "Set to Main Messages Directory")
            Label(Sub, text=" ").pack(side=LEFT)
    BButton(Sub, text="OK", command=Command(formMYDFReturn,
                                            "")).pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    BButton(Sub, text="Cancel",
            command=Command(formMYDFReturn, None)).pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    LCb = Checkbutton(Sub, text="Show hidden\nfiles",
                      variable=MYDFHiddenCVar, command=formMYDFFillFiles)
    LCb.pack(side=LEFT)
    ToolTip(LCb, 35, "Select this to show hidden/system files in the list.")
    Sub.pack(side=TOP, pady=3)
    PROGMsg["MYDF"] = Text(LFrm, font=PROGPropFont, height=1,
                           width=1, highlightthickness=0, insertwidth=0,
                           takefocus=0)
    PROGMsg["MYDF"].pack(side=TOP, expand=YES, fill=X)
    PROGMsg["MYDF"].bind("<Button-1>", formMYDFNullCall)
    if len(StartDir) == 0:
        StartDir = sep
    if StartDir.endswith(sep) is False:
        StartDir += sep
    MYDFDirVar.set(StartDir)
    Ret = formMYDFFillFiles()
    if Ret is True and (ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6):
        MYDFFileVar.set(StartFile)
    center(Parent, LFrm, "CX", "I", True)
    # Set the cursor for the user.
    if ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
        LEnt.focus_set()
        LEnt.icursor(END)
    if GSG is False:
        # See formMYD().
        if PROGSystem == "dar":
            pass
        else:
            MYDFFrame.grab_set()
    else:
        if PROGSystem == "dar":
            pass
        else:
            MYDFFrame.grab_set_global()
# Everything will pause here until one of the buttons are pressed, then the
# box will be destroyed, but the value will still be returned since it was
# saved in a "global".
    MYDFFrame.wait_window()
    return MYDFAnswerVar.get()
#####################################
# BEGIN: formMYDFClearFiles(e = None)
# FUNC:formMYDFClearFiles():2013.207


def formMYDFClearFiles(e=None):
    if MYDFFiles.size() > 0:
        MYDFFiles.delete(0, END)
    return
#####################################################
# BEGIN: formMYDFFillFiles(FocusSet = True, e = None)
# FUNC:formMYDFFillFiles():2018.330
#   Fills the Listbox with a list of directories and files, or with drive
#   letters if in Windows (and the directory field is just sep).


def formMYDFFillFiles(FocusSet=True, e=None):
    # Some directoryies may have a lot of files in them which could make
    # listdir() take a long time, so do this.
    setMsg("MYDF", "CB", "Reading...")
    ModeI = intt(MYDFModeVar.get())
# Make sure whatever is in the directory field ends, or is at least a
# separator.
    if len(MYDFDirVar.get()) == 0:
        MYDFDirVar.set(sep)
    if MYDFDirVar.get().endswith(sep) is False:
        MYDFDirVar.set(MYDFDirVar.get() + sep)
    Dir = MYDFDirVar.get()
    if PROGSystem == "dar" or PROGSystem == "lin" or PROGSystem == "sun" or \
            (PROGSystem == "win" and Dir != sep):
        try:
            Files = listdir(Dir)
        except Exception:
            MYDFFileVar.set("")
            setMsg("MYDF", "YB", " There is no such directory.", 2)
            return False
        MYDFDirField.icursor(END)
        MYDFFiles.delete(0, END)
# This is sortedLow(), but coded in here to reduce dependencies.
        USList = {}
        for Item in Files:
            USList[Item.lower()] = Item
        SKeys = sorted(USList.keys())
        Files = []
        for Key in SKeys:
            Files.append(USList[Key])
# Always add this to the top of the list unless we are not supposed to.
        if ModeI != 5 and ModeI != 6:
            MYDFFiles.insert(END, " .." + sep)
# To show or not to show.
        ShowHidden = MYDFHiddenCVar.get()
# Do the directories first.
        for File in Files:
            # The DePrefix stuff is only applied to the files (below).
            if ShowHidden == 0:
                # This may need/want to be system dependent at some point
                # (i.e. more than just files that start with a . or _ in
                # different OSs).
                if File.startswith(".") or File.startswith("_"):
                    continue
            if isdir(Dir + sep + File):
                MYDFFiles.insert(END, " " + File + sep)
# Check to see if we are going to be filtering.
        EndsWith = ""
        if len(MYDFEndsWithVar.get()) != 0:
            EndsWith = MYDFEndsWithVar.get()
        EndsWithParts = EndsWith.split(",")
        Found = False
        for File in Files:
            # DeFile will be used for what the user ends up seeing, and File
            # will be used internally.
            DeFile = File
            if len(MYDFDePrefix) != 0:
                if File.startswith(MYDFDePrefix) is False:
                    continue
                DeFile = File[len(MYDFDePrefix):]
            if ShowHidden == 0:
                if DeFile.startswith(".") or DeFile.startswith("_"):
                    continue
            if isdir(Dir + sep + File) is False:
                if len(EndsWith) == 0:
                    # We only want to see the file sizes when we are looking
                    # for files.
                    if ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
                        # Trying to follow links will trip this so just show
                        # them.
                        try:
                            MYDFFiles.insert(END, " %s  (bytes: %s)" %
                                             (DeFile,
                                              fmti(getsize(Dir + sep + File))))
                        except OSError:
                            MYDFFiles.insert(END, " %s  (a link?)" % DeFile)
                    else:
                        MYDFFiles.insert(END, " %s" % DeFile)
                    Found += 1
                else:
                    for EndsWithPart in EndsWithParts:
                        if File.endswith(EndsWithPart):
                            if ModeI == 0 or ModeI == 3 or ModeI == 5 or \
                                    ModeI == 6:
                                try:
                                    MYDFFiles.insert(END, " %s  (bytes: %s)" %
                                                     (DeFile, fmti(getsize(Dir + sep + File))))  # noqa: E501
                                except OSError:
                                    MYDFFiles.insert(END, " %s  (a link?)" %
                                                     DeFile)
                            else:
                                MYDFFiles.insert(END, " %s" % DeFile)
                            Found += 1
                            break
        if ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
            setMsg("MYDF", "", "%d %s found." % (Found, sP(Found, ("file",
                                                                   "files"))))
        else:
            setMsg("MYDF")
    elif PROGSystem == "win" and Dir == sep:
        MYDFFiles.delete(0, END)
# This loop takes a while to run.
        updateMe(0)
        Found = 0
        for Drive in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
            Drivespec = "%s:%s" % (Drive, sep)
            if exists(Drivespec):
                MYDFFiles.insert(END, Drivespec)
                Found += 1
        if ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
            setMsg("MYDF", "", "%d %s found." % (Found, sP(Found, ("drive",
                                                                   "drives"))))
        else:
            setMsg("MYDF")
    if FocusSet is True:
        PROGMsg["MYDF"].focus_set()
    return True
###############################
# BEGIN: formMYDFDefault(Which)
# FUNC:formMYDFDefault():2012.343


def formMYDFDefault(Which):
    if Which == "D":
        MYDFDirVar.set(PROGDataDirVar.get())
    elif Which == "W":
        MYDFDirVar.set(PROGWorkDirVar.get())
    elif Which == "M":
        MYDFDirVar.set(PROGMsgsDirVar.get())
    formMYDFFillFiles()
    return
######################
# BEGIN: formMYDFNew()
# FUNC:formMYDFNew():2018.233


def formMYDFNew():
    setMsg("MYDF")
# Make sure whatever is in the directory field ends in a separator.
    if len(MYDFDirVar.get()) == 0:
        MYDFDirVar.set(sep)
    if MYDFDirVar.get().endswith(sep) is False:
        MYDFDirVar.set(MYDFDirVar.get() + sep)
    Dir = MYDFDirVar.get()
    if exists(Dir):
        setMsg("MYDF", "YB", " Directory already exists.", 2)
        return
    try:
        makedirs(Dir)
        formMYDFFillFiles()
        setMsg("MYDF", "GB", " New directory made.", 1)
    except Exception as e:
        # The system messages can be a bit cryptic and/or long, so simplifiy
        # them here.
        if str(e).find("ermission") != -1:
            setMsg("MYDF", "RW", " Permission denied.", 2)
        else:
            setMsg("MYDF", "RW", " " + str(e), 2)
        return
    return
#################################
# BEGIN: formMYDFPicked(e = None)
# FUNC:formMYDFPicked():2018.235


def formMYDFPicked(e=None):
    # This is just to get the focus off of the entry field if it was there
    # since the user is now clicking on things.
    PROGMsg["MYDF"].focus_set()
    setMsg("MYDF")
    ModeI = intt(MYDFModeVar.get())
# Make sure whatever is in the directory field ends with a separator.
    if len(MYDFDirVar.get()) == 0:
        MYDFDirVar.set(sep)
    if MYDFDirVar.get().endswith(sep) is False:
        MYDFDirVar.set(MYDFDirVar.get() + sep)
    Dir = MYDFDirVar.get()
# Otherwise the selected stuff will just keep piling up.
    if ModeI == 4:
        MYDFFileVar.set("")
# There could be multiple files selected. Go through all of them and see if any
# of them will make us change directories.
    Sel = MYDFFiles.curselection()
    SelectedFiles = ""
    for Index in Sel:
        # If the user is not right on the money this will sometimes trip.
        try:
            Selected = MYDFFiles.get(Index).strip()
        except TclError:
            beep(1)
            return
        if Selected == ".." + sep:
            Parts = Dir.split(sep)
# If there are only two Parts then we have hit the top of the directory tree.
            NewDir = ""
            if len(Parts) == 2 and len(Parts[1]) == 0:
                if PROGSystem == "dar" or PROGSystem == "lin" or \
                        PROGSystem == "sun":
                    NewDir = Parts[0] + sep
                elif PROGSystem == "win":
                    NewDir = sep
            else:
                for i in arange(0, len(Parts) - 2):
                    NewDir += Parts[i] + sep
# Insurance.
            if len(NewDir) == 0:
                NewDir = sep
            MYDFDirVar.set(NewDir)
            formMYDFFillFiles()
            return
# We have to do a Texas Two-Step here to get everyone in the right frame of
# mind. If Dir is just sep then we have just clicked on a directory and not
# on a file, so make it look like the former.
        if PROGSystem == "win" and Dir == sep:
            Dir = Selected
            Selected = ""
        if isdir(Dir + Selected):
            MYDFDirVar.set(Dir + Selected)
            formMYDFFillFiles()
            return
        if ModeI == 1 or ModeI == 2:
            setMsg("MYDF", "YB", "That is not a directory.", 2)
            MYDFFiles.selection_clear(0, END)
            return
# Must have clicked on a file with byte size and that must be allowed.
        if Selected.find("  (") != -1:
            Selected = Selected[:Selected.index("  (")]
# Build the whole path for the multiple file mode.
        if ModeI == 4:
            if len(SelectedFiles) == 0:
                SelectedFiles = Dir + Selected
            else:
                # An * should be a safe separator, right?
                SelectedFiles += "*" + Dir + Selected
        else:
            # This should end up the only file.
            SelectedFiles = Selected
    MYDFFileVar.set(SelectedFiles)
    return
#############################
# BEGIN: formMYDFUp(e = None)
# FUNC:formMYDFUp():2018.235


def formMYDFUp(e=None):
    # This is just to get the focus off of the entry field if it was there.
    PROGMsg["MYDF"].focus_set()
    setMsg("MYDF")
    Dir = MYDFDirVar.get()
    Parts = Dir.split(sep)
# If there are only two Parts then we have hit the top of the directory tree.
    NewDir = ""
    if len(Parts) == 2 and len(Parts[1]) == 0:
        if PROGSystem == "dar" or PROGSystem == "lin" or PROGSystem == "sun":
            NewDir = Parts[0] + sep
        elif PROGSystem == "win":
            NewDir = sep
    else:
        for i in arange(0, len(Parts) - 2):
            NewDir += Parts[i] + sep
# Insurance.
    if len(NewDir) == 0:
        NewDir = sep
    MYDFDirVar.set(NewDir)
    formMYDFFillFiles()
    return
#########################################
# BEGIN: formMYDFReturn(Return, e = None)
# FUNC:formMYDFReturn():2018.236


def formMYDFReturn(Return, e=None):
    # This update keeps the "OK" button from staying pushed in when there is a
    # lot to do before returning.
    MYDFFrame.update()
    setMsg("MYDF")
    ModeI = intt(MYDFModeVar.get())
    if Return is None:
        MYDFAnswerVar.set("")
    elif len(Return) != 0:
        MYDFAnswerVar.set(Return)
    elif len(Return) == 0:
        # The programmer is responsible for making sure this is used correctly.
        if len(MYDFDirVar.get()) == 0 and MYDFReturnEmpty is True:
            MYDFAnswerVar.set("")
        else:
            if ModeI == 1 or ModeI == 2:
                if len(MYDFDirVar.get()) == 0:
                    setMsg("MYDF", "YB", "There is no directory name.", 2)
                    return
                if MYDFDirVar.get().endswith(sep) is False:
                    MYDFDirVar.set(MYDFDirVar.get() + sep)
                MYDFAnswerVar.set(MYDFDirVar.get())
            elif ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
                # Just in case the user tries to pull a fast one. Mode 5 above
                # is just for completness since the directory must be right
                # (it can't be changed by the user).
                if len(MYDFDirVar.get()) == 0:
                    setMsg("MYDF", "YB", "There is no directory name.", 2)
                    return
# What was the point of coming here??
                if len(MYDFFileVar.get()) == 0:
                    setMsg("MYDF", "YB", "No filename has been entered.", 2)
                    return
# File names only at this point.
                if MYDFFileVar.get().find(sep) != -1:
                    setMsg("MYDF", "RW",
                           "Character  %s  cannot be in the Filename." %
                           sep, 2)
                    return
                if len(MYDFEndsWithVar.get()) != 0:
                    # I'm sure this may come back to haunt US someday, but make
                    # sure that the case of the entered filename matches the
                    # passed 'extension'.  My programs, as a general rule, are
                    # written to handle all upper or lowercase file names, but
                    # not mixed. Of course, on some operating systems it won't
                    # make any difference.
                    if MYDFSameCase is True:
                        if MYDFEndsWithVar.get().isupper():
                            if MYDFFileVar.get().isupper() is False:
                                setMsg("MYDF", "YB",
                                       "Filename needs to be all uppercase "
                                       "letters.", 2)
                                return
                        elif MYDFEndsWithVar.get().islower():
                            if MYDFFileVar.get().islower() is False:
                                setMsg("MYDF", "YB",
                                       "Filename needs to be all lowercase "
                                       "letters.", 2)
                                return
# If the user didn't put the 'extension' on the file add it so the caller
# won't have to do it, unless the caller passed multiple extensions, then don't
# do anything.
                    if MYDFEndsWithVar.get().find(",") == -1:
                        if MYDFFileVar.get().endswith(
                                MYDFEndsWithVar.get()) is False:
                            MYDFFileVar.set(MYDFFileVar.get() +
                                            MYDFEndsWithVar.get())
                MYDFAnswerVar.set(MYDFDirVar.get() + MYDFFileVar.get())
            elif ModeI == 4:
                MYDFAnswerVar.set(MYDFFileVar.get())
    MYDFFrame.destroy()
    updateMe(0)
    return
##################################################
# BEGIN: formMYDFCompFs(DirVar, FileVar, e = None)
# FUNC:formMYDFCompFs():2018.236
#   Attempts to complete the directory or file name in an Entry field using
#   the Tab key.
#   - If DirVar is set to a field's StringVar and FileVar is None then the
#     routine only looks for directories and leaves completion results in
#     DirVar.
#   - If FileVar is set to a field's StringVar and DirVar is None then the
#     routine tries to complete a file name using the files loaded into the
#     file listbox and leaves the results in FileVar.


def formMYDFCompFs(DirVar, FileVar, e=None):
    setMsg("MYDF")
# ---- Directories...looking to find you.
    if DirVar is not None:
        Dir = dirname(DirVar.get())
# This is a slight gotchya. If the field is empty that might mean that the user
# means "/" should be the starting point. If it is they will have to enter the
# / since if we are on Windows I'd have no idea what the default should be.
        if len(Dir) == 0:
            beep(2)
            return
        if Dir.endswith(sep) is False:
            Dir += sep
# Now get what must be a partial directory name, treat it as a file name, but
# then only allow the result of everything to be a directory.
        PartialFile = basename(DirVar.get())
        if len(PartialFile) == 0:
            beep(2)
            return
        PartialFile += "*"
        Files = listdir(Dir)
        Matched = []
        for File in Files:
            if fnmatch(File, PartialFile):
                Matched.append(File)
        if len(Matched) == 0:
            beep(2)
            return
        elif len(Matched) == 1:
            Dir = Dir + Matched[0]
# If whatever matched is not a directory then just beep and return, otherwise
# make it look like a directory and put it into the field.
            if isdir(Dir) is False:
                beep(2)
                return
            if Dir.endswith(sep) is False:
                Dir += sep
            DirVar.set(Dir)
            e.widget.icursor(END)
            formMYDFFillFiles(False)
            return
        else:
            # Get the max number of characters that matched and put the partial
            # directory path into the Var. If Dir+PartialDir is really the
            # directory the user wants they will have to add the sep themselves
            # since with multiple matches I won't know what to do. Consider DIR
            # DIR2 DIR3 with a formMYDFMaxMatch() return of DIR. The directory
            # DIR would always be selected and set as the path which may not be
            # what the user wanted. Now this could cause trouble downstream
            # since I'm leaving a path in the field without a trailing sep
            # (everything tries to avoid doing that), so the caller will have
            # to worry about that.
            PartialDir = formMYDFMaxMatch(Matched)
            DirVar.set(Dir + PartialDir)
            e.widget.icursor(END)
            beep(1)
    elif FileVar is not None:
        PartialFile = FileVar.get().strip()
        if len(PartialFile) == 0:
            beep(1)
            return
        Files = MYDFFiles.get(0, END)
        Index = 0
        Found = 0
        for Sel in arange(0, len(Files)):
            # .strip() off the leading space.
            File = Files[Sel].strip()
            if File.startswith(PartialFile):
                Index = Sel
                Found += 1
        if Found == 0:
            beep(1)
            return
        elif Found > 1:
            MYDFFiles.see(Index)
            beep(1)
            return
        MYDFFiles.see(Index)
        File = Files[Index].strip()
# File contains the matching line from the Listbox, but it should have an
# (x bytes) message at the end. Get rid of that.
        if File.find("  (") != -1:
            File = File[:File.index("  (")]
        FileVar.set(File)
        e.widget.icursor(END)
    return
#############################################
# BEGIN: formMYDFCompFsTabOff(LFrm, e = None)
# FUNC:formMYDFCompFsTabOff():2010.225


def formMYDFCompFsTabOff(LFrm, e=None):
    LFrm.bind("<Key-Tab>", formMYDFNullCall)
    return
############################################
# BEGIN: formMYDFCompFsTabOn(LFrm, e = None)
# FUNC:formMYDFCompFsTabOn():2010.225


def formMYDFCompFsTabOn(LFrm, e=None):
    LFrm.unbind("<Key-Tab>")
    return
##################################
# BEGIN: formMYDFMaxMatch(TheList)
# FUNC:formMYDFMaxMatch():2018.310
#   Goes through the items in TheList (should be str's) and returns the string
#   that matches the start of all of the items.
#   This is the same as the library function maxMatch().


def formMYDFMaxMatch(TheList):
    # This should be the only special case. What is the sound of one thing
    # matching itself?
    if len(TheList) == 1:
        return TheList[0]
    Accum = ""
    CharIndex = 0
# If anything goes wrong just return whatever we've accumulated. This will end
# by no items being in TheList or one of the items running out of characters
# (the try) or by the TargetChar not matching a character from one of the
# items (the raise).
    try:
        while True:
            TargetChar = TheList[0][CharIndex]
            for ItemIndex in arange(1, len(TheList)):
                if TargetChar != TheList[ItemIndex][CharIndex]:
                    raise Exception
            Accum += TargetChar
            CharIndex += 1
    except Exception:
        pass
    return Accum
###################################
# BEGIN: formMYDFNullCall(e = None)
# FUNC:formMYDFNullCall():2013.037


def formMYDFNullCall(e=None):
    return "break"
# END: formMYDF


###########################
# BEGIN: formLPMAPR(Parent)
# FUNC:formLPMAPR():2019.024
PROGFrm["LPMAPR"] = None
PROGCan["LPMAPR"] = None
LPMAPRRunning = IntVar()
LPMAPRFiles = None
LPMAPRLbxFindVar = StringVar()
LPMAPRShowLogsCVar = IntVar()
LPMAPRShowLogsCVar.set(1)
LPMAPRShowZCFCVar = IntVar()
LPMAPRShowZCFCVar.set(1)
LPMAPRShowRefsCVar = IntVar()
LPMAPRShowRefsCVar.set(1)
LPMAPRShowUCFDCVar = IntVar()
LPMAPRShowUCFDCVar.set(1)
LPMAPRShowMSLOGSCVar = IntVar()
LPMAPRShowMSLOGSCVar.set(1)
LPMAPRMapTypeRVar = StringVar()
LPMAPRMapTypeRVar.set("s")
LPMAPROneSourceCVar = IntVar()
LPMAPRShowIDIDCVar = IntVar()
LPMAPRCorrLatCVar = IntVar()
LPMAPRCorrLatCVar.set(1)
LPMAPRPSFilespecVar = StringVar()
PROGSetups += ["LPMAPRShowLogsCVar", "LPMAPRShowZCFCVar", "LPMAPRLbxFindVar",
               "LPMAPRShowRefsCVar", "LPMAPRShowUCFDCVar",
               "LPMAPRShowMSLOGSCVar", "LPMAPRMapTypeRVar",
               "LPMAPROneSourceCVar", "LPMAPRShowIDIDCVar",
               "LPMAPRCorrLatCVar", "LPMAPRPSFilespecVar"]
LPMAPRLastCanW = 0
LPMAPRLastCanH = 0
LPMAPRReadBut = None
LPMAPRStopBut = None
# Where all of the data to be plotted will be.
LPMAPRData = []
# Color gradient pallet for the elevation map from lowest to highest. The
# >= elevations will replace the 0.0's once we know the range of the data.
MAPRLandHeight = [[0.0, "#003F00"],
                  [0.0, "#007F00"],
                  [0.0, "#00BF00"],
                  [0.0, "#00FF00"],
                  [0.0, "#3FFF00"],
                  [0.0, "#7FFF00"],
                  [0.0, "#BFFF00"],
                  [0.0, "#FFFF00"],
                  [0.0, "#FFBF00"],
                  [0.0, "#FF7F00"],
                  [0.0, "#FF3F00"],
                  [0.0, "#FF0000"],
                  [0.0, "#FF003F"],
                  [0.0, "#FF007F"],
                  [0.0, "#FF00BF"],
                  [0.0, "#FF00FF"],
                  [0.0, "#FF3FFF"],
                  [0.0, "#FF7FFF"],
                  [0.0, "#FFBFFF"],
                  [0.0, "#FFFFFF"]]


def formLPMAPR(Parent):
    global LPMAPRFiles
    global LPMAPRData
    global LPMAPRLastCanW
    global LPMAPRLastCanH
    global LPMAPRReadBut
    global LPMAPRStopBut
    if showUp("LPMAPR"):
        return
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    LFrm = PROGFrm["LPMAPR"] = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.protocol("WM_DELETE_WINDOW", formLPMAPRClose)
    LFrm.title("Plot Positions From Files")
    LFrm.iconname("Mapper")
# List the current directory(s) that apply to this form.
    Sub = Frame(LFrm)
# formLPMAPR() started in LOGPEEK which had different needs for handling
# directories. It only used one.
    if PROG_NAME == "LOGPEEK":
        LBu = BButton(Sub, text="Main Data Directory:", pady="2p",
                      command=formLPMAPRChangeDir)
    else:
        LBu = BButton(Sub, text="Main Data Directory:", pady="2p",
                      command=Command(changeMainDirs, "LAMAPR", 1, "thedata",
                                      None, "", ""))
    LBu.pack(side=LEFT)
    ToolTip(LBu, 30, "Click to change the directory.")
    Entry(Sub, width=1, textvariable=PROGDataDirVar, state=DISABLED,
          relief=FLAT, bd=0, bg=Clr["D"],
          fg=Clr["B"]).pack(side=LEFT, expand=YES, fill=X)
    labelTip(Sub, " Hints ", RIGHT, 50,
             "MAP AREA HINTS:\n--Double-click: Double-clicking on a blank map "
             "area will zoom in.\n--Right-double-click: Right-double-clicking "
             "on a blank map area will zoom out.\n--Shift-click: "
             "Shift-clicking on a blank map area will zoom in faster.\n"
             "--Shift-right-click: Shift-right-clicking on a blank area will "
             "zoom out faster.\nMAP POINT HINTS:\n--Click on point: Clicking "
             "on a map point will display an information flag about that "
             "point.\n--Click on flag: Clicking on an information flag will "
             "make the flag disappear.\n--Right-click on flag: Right-clicking "
             "on an information flag will show a popup menu of options "
             "concerning the information file associated with that flag.")
    Sub.pack(side=TOP, anchor="w", expand=NO, fill=X, padx=3)
# Buttons and canvas area.
    Sub = Frame(LFrm)
# Listbox and controls.
    SSub = Frame(Sub)
    SSSub = Frame(SSub)
    LPMAPRFiles = Listbox(SSSub, relief=SUNKEN, bd=2, height=1,
                          width=25, selectmode=EXTENDED)
    LPMAPRFiles.pack(side=LEFT, expand=YES, fill=Y)
    LPMAPRFiles.bind("<Double-Button-1>", formLPMAPRRead)
    Scroll = Scrollbar(SSSub, command=LPMAPRFiles.yview, orient=VERTICAL)
    Scroll.pack(side=RIGHT, fill=Y)
    LPMAPRFiles.configure(yscrollcommand=Scroll.set)
    SSSub.pack(side=TOP, expand=YES, fill=Y)
    SSSub = Frame(SSub)
    Scroll = Scrollbar(SSSub, command=LPMAPRFiles.xview, orient=HORIZONTAL)
    Scroll.pack(side=BOTTOM, expand=YES, fill=X)
    LPMAPRFiles.configure(xscrollcommand=Scroll.set)
    SSSub.pack(side=TOP, fill=X, expand=NO)
    SSSub = Frame(SSub)
    BButton(SSSub, text="Reload",
            command=Command(loadRTFilesCmd, LPMAPRFiles,
                            "LPMAPR")).pack(side=LEFT)
    BButton(SSSub, text="Select All",
            command=Command(loadRTFilesSelectAll,
                            LPMAPRFiles)).pack(side=LEFT)
    SSSub.pack(side=TOP)
    SSSub = Frame(SSub)
    LEnt = labelEntry2(SSSub, 11, "Find:=", 35,
                       "[Reload] Show file names containing this at the top.",
                       LPMAPRLbxFindVar, 8)
    LEnt.bind("<Return>", Command(loadRTFilesCmd, LPMAPRFiles, "LPMAPR"))
    LEnt.bind("<KP_Enter>", Command(loadRTFilesCmd, LPMAPRFiles, "LPMAPR"))
    BButton(SSSub, text="Clear", fg=Clr["U"],
            command=Command(loadRTFilesClearLbxFindVar, LPMAPRFiles,
                            "LPMAPR", True)).pack(side=LEFT)
    SSSub.pack(side=TOP)
    SSSub = Frame(SSub)
    LLb = Label(SSSub, text="List:")
    LLb.pack(side=LEFT)
    ToolTip(LLb, 35,
            "The types of data sources that should be displayed in the file "
            "list.")
    SSSSub = Frame(SSSub)
    SSSSSub = Frame(SSSSub)
    SSSSSSub = Frame(SSSSSub)
    LCb = Checkbutton(SSSSSSub, text=".log", variable=LPMAPRShowLogsCVar)
    LCb.pack(side=TOP, anchor="w")
    ToolTip(LCb, 30, "RT130/72A text log files.")
    LCb = Checkbutton(SSSSSSub, text=".cf", variable=LPMAPRShowUCFDCVar)
    LCb.pack(side=TOP, anchor="w")
    ToolTip(LCb, 30, "RT130 CompactFlash card images (not zipped).")
    SSSSSSub.pack(side=LEFT)
    SSSSSSub = Frame(SSSSSub)
    LCb = Checkbutton(SSSSSSub, text=".ref", variable=LPMAPRShowRefsCVar)
    LCb.pack(side=TOP, anchor="w")
    ToolTip(LCb, 30, "RT130/72A all-in-one raw data files.")
    LCb = Checkbutton(SSSSSSub, text=".zip", variable=LPMAPRShowZCFCVar)
    LCb.pack(side=TOP, anchor="w")
    ToolTip(LCb, 30, "RT130 zipped CompactFlash card images.")
    SSSSSSub.pack(side=LEFT)
    SSSSSub.pack(side=TOP)
    LCb = Checkbutton(SSSSub, text=".mslogs",
                      variable=LPMAPRShowMSLOGSCVar)
    LCb.pack(side=TOP)
    ToolTip(LCb, 30, "RT130/DMC LOG Channel mseed files.")
    SSSSub.pack(side=LEFT)
    BButton(SSSub, text="Sort", command=formLPMAPRSort).pack(side=LEFT)
    SSSub.pack(side=TOP)
    Frame(SSub, height=1, bg=Clr["B"], relief=GROOVE).pack(side=TOP,
                                                           fill=X, padx=10,
                                                           pady=3)
    SSSub = Frame(SSub)
    SSSSub = Frame(SSSub)
    Rb = Radiobutton(SSSSub, text="Station Map",
                     variable=LPMAPRMapTypeRVar, value="s")
    Rb.pack(side=TOP, anchor="w")
    ToolTip(Rb, 30,
            "Map the points and group them according to the selection below.")
    Rb = Radiobutton(SSSSub, text="Elevation Map",
                     variable=LPMAPRMapTypeRVar, value="e")
    Rb.pack(side=TOP, anchor="w")
    ToolTip(Rb, 30,
            "Map the points using colored dots corresponding to their "
            "relative elevations. Dark green is the lowest, and white is "
            "the highest.")
    Cb = Checkbutton(SSSSub, text="Show DAS ID",
                     variable=LPMAPRShowIDIDCVar)
    Cb.pack(side=TOP, anchor="w")
    Cb = Checkbutton(SSSSub, text="Correct For\nLatitude",
                     variable=LPMAPRCorrLatCVar)
    Cb.pack(side=TOP, anchor="w")
    ToolTip(Cb, 30,
            "When selected the mapping routine will correct for the "
            "convergence of the lines of longitude with an increase in "
            "latitude.")
    SSSSub.pack(side=LEFT)
    BButton(SSSub, text="Replot", command=Command(formLPMAPRMap,
                                                  "replot")).pack(side=LEFT)
    SSSub.pack(side=TOP)
    Frame(SSub, height=1, bg=Clr["B"], relief=GROOVE).pack(side=TOP,
                                                           fill=X, padx=10,
                                                           pady=3)
    SSSub = Frame(SSub)
    Cb = Checkbutton(SSSub, text="Filter Multiple\nDAS Files",
                     variable=LPMAPROneSourceCVar)
    Cb.pack(side=TOP)
    ToolTip(Cb, 40,
            "When selected only one source of information for each DAS will "
            "be plotted, rather than plotting multiple points on top of each "
            "other for the same DAS in the same location.")
    SSSSub = Frame(SSSub)
    LPMAPRReadBut = BButton(SSSSub, text="Read", command=formLPMAPRRead)
    LPMAPRReadBut.pack(side=LEFT)
    LPMAPRStopBut = BButton(SSSSub, text="Stop",
                            command=Command(formLPMAPRControl, "stopping"))
    LPMAPRStopBut.pack(side=LEFT)
    SSSSub.pack(side=TOP)
    BButton(SSSub, text="Zoom Out", command=Command(formLPMAPRMap,
                                                    "zoomout")).pack(side=TOP,
                                                                     fill=X)
    SSSub.pack(side=TOP)
    Frame(SSub, height=1, bg=Clr["B"], relief=GROOVE).pack(side=TOP, fill=X,
                                                           padx=10, pady=3)
    SSSub = Frame(SSub)
    BButton(SSSub, text="Write .ps",
            command=Command(formWritePS, "LPMAPR", "LPMAPR",
                            LPMAPRPSFilespecVar)).pack(side=LEFT, fill=X)
    BButton(SSSub, text="Close", fg=Clr["R"],
            command=formLPMAPRClose).pack(side=LEFT, fill=X)
    SSSub.pack(side=TOP)
    SSub.pack(side=LEFT, fill=Y, expand=NO, padx=3, pady=3)
# Map section.
    SSub = Frame(Sub, bd=2, relief=SUNKEN)
    SSSub = Frame(SSub)
    LCan = PROGCan["LPMAPR"] = Canvas(SSSub, bg=Clr["W"], height=500,
                                      width=700, relief=SUNKEN)
    LCan.pack(side=LEFT, fill=BOTH, expand=YES)
    LCan.bind("<Shift-Button-1>", Command(formLPMAPRMap, "zin2"))
    LCan.bind("<Double-Button-1>", Command(formLPMAPRMap, "zin"))
    if B2Glitch is True:
        LCan.bind("<Shift-Button-2>", Command(formLPMAPRMap, "zout2"))
        LCan.bind("<Double-Button-2>", Command(formLPMAPRMap, "zout"))
    LCan.bind("<Shift-Button-3>", Command(formLPMAPRMap, "zout2"))
    LCan.bind("<Double-Button-3>", Command(formLPMAPRMap, "zout"))
    LCan.bind("<Button-1>", Command(formLPMAPRMark, LCan))
    LCan.bind("<B1-Motion>", Command(formLPMAPRDrag, LCan))
    LSb = Scrollbar(SSSub, orient=VERTICAL, command=LCan.yview)
    LSb.pack(side=RIGHT, fill=Y, expand=NO)
    LCan.configure(yscrollcommand=LSb.set)
    SSSub.pack(side=TOP, fill=BOTH, expand=YES)
    LSb = Scrollbar(SSub, orient=HORIZONTAL, command=LCan.xview)
    LSb.pack(side=BOTTOM, fill=X, expand=NO)
    LCan.configure(xscrollcommand=LSb.set)
    SSub.pack(side=LEFT, fill=BOTH, expand=YES)
    Sub.pack(side=TOP, fill=BOTH, expand=YES)
    PROGMsg["LPMAPR"] = Text(LFrm, font=PROGPropFont, height=2,
                             wrap=WORD)
    PROGMsg["LPMAPR"].pack(side=TOP, fill=X)
    center(Parent, LFrm, "CX", "I", True)
    if len(LPMAPRPSFilespecVar.get()) == 0 or \
            exists(basename(LPMAPRPSFilespecVar.get())) is False:
        LPMAPRPSFilespecVar.set(PROGDataDirVar.get())
    loadRTFilesCmd(LPMAPRFiles, "LPMAPR")
    LPMAPRLastCanW = LCan.winfo_width()
    LPMAPRLastCanH = LCan.winfo_height()
    return
#################################
# BEGIN: formLPMAPRRead(e = None)
# FUNC:formLPMAPRRead():2018.235
#   Reads the selected files and calls to get the positions plotted.


def formLPMAPRRead(e=None):
    global LPMAPRData
    if LPMAPRRunning.get() == 1:
        setMsg("LPMAPR", "YB", "I'm busy...", 2)
        sleep(.5)
        return
    setMsg("LPMAPR", "CB", "Working...")
    Sel = LPMAPRFiles.curselection()
    if len(Sel) == 0:
        setMsg("LPMAPR", "RW", "No files have been selected.", 2)
        return
# Where we'll store the values for each file until all of the files have been
# read.
    del LPMAPRData[:]
    formLPMAPRMap("clear")
# Keep a list of the DASIDs read so we can skip plotting the same DAS more than
# once.
    OneSource = LPMAPROneSourceCVar.get()
    DASIDs = []
# Always check the directory entry in case the user messed with it.
    PROGDataDirVar.set(PROGDataDirVar.get().strip())
    if PROGDataDirVar.get().endswith(sep) is False:
        PROGDataDirVar.set(PROGDataDirVar.get() + sep)
# Keep these in sync for LOGPEEK. It doesn't have them, but other library
# functions use them.
    if PROG_NAME == "LOGPEEK":
        PROGMsgsDirVar.set(PROGDataDirVar.get())
        PROGWorkDirVar.set(PROGDataDirVar.get())
    TheDataDir = PROGDataDirVar.get()
    formLPMAPRControl("go")
    FileCount = 0
    IndexCount = 0
    for Index in Sel:
        if LPMAPRRunning.get() == 0:
            setMsg("LPMAPR", "YB", "Stopped.", 2)
            formLPMAPRControl("stopped")
            return
        Selected = LPMAPRFiles.get(Index)
# Skip over the blank lines.
        if len(Selected) == 0:
            continue
        FileCount += 1
        setMsg("LPMAPR", "CB", "Working on file %d of %d..." % (FileCount,
                                                                len(Sel)))
# The entry will probably be something like "Filename (x bytes or lines)" so
# just get the Filename.
        Parts = Selected.split()
        Filename = Parts[0]
        FilenameLC = Filename.lower()
        Filespec = "%s%s" % (PROGDataDirVar.get(), Filename)
# This check is a little complicated because of the way .cf's are listed in
# the Listbox (e.g. 9BA6.cf(sep)9BA6).
        if FilenameLC.find(".cf" + sep) != -1:
            try:
                ind_ = Filespec.index(".cf" + sep)
            except Exception:
                ind_ = Filespec.index(".CF" + sep)
            Filecheck = Filespec[:ind_ + 3]
            if exists(Filecheck) is False:
                setMsg("LPMAPR", "RW",
                       "%s: .cf directory not found. Use the Reload button?" %
                       Filename, 2)
                formLPMAPRControl("stopped")
                return
        else:
            if exists(Filespec) is False:
                setMsg("LPMAPR", "RW",
                       "%s: File not found. Use the Reload button?" % Filename,
                       2)
                formLPMAPRControl("stopped")
                return
        if FilenameLC.endswith(".log") or FilenameLC.endswith("_log"):
            # Ret is (0, {file:<file>, idid:<DASID>, latf:<lati>, lonf:<long>,
            #                elef:<elev>, ...})
            # or (1, color, msg, beep, filespec)
            Ret = rt72130logQGPSPosition(Filespec, Filespec, LPMAPRStopBut,
                                         LPMAPRRunning)
        elif FilenameLC.endswith(".ref"):
            Ret = rt72130refQGPSPosition(Filespec, LPMAPRStopBut,
                                         LPMAPRRunning)
# Look for these before .cf's since the names may be like 123456.cf.zip.
        elif FilenameLC.endswith(".zip"):
            Ret = rt130zcfQGPSPosition(Filespec, LPMAPRStopBut,
                                       LPMAPRRunning, "LPMAPR")
        elif FilenameLC.find(".cf") != -1:
            Ret = rt130cfQGPSPosition(Filespec, LPMAPRStopBut,
                                      LPMAPRRunning, "LPMAPR")
        elif FilenameLC.find(".mslogs") != -1:
            Ret = rt72130mslogsQGPSPosition(Filespec, LPMAPRStopBut,
                                            LPMAPRRunning)
# Something fatal this way comes (or we are just stopping).
        if Ret[0] == 2:
            setMsg("LPMAPR", Ret)
            formLPMAPRControl("stopped")
            return
# The information flags will need this.
        if Ret[0] == 0:
            # Skip the information from this file if the DAS has already been
            # seen.
            if OneSource == 1:
                if Ret[1]["idid"] in DASIDs:
                    continue
                DASIDs.append(Ret[1]["idid"])
            Ret[1]["file"] = Filename
            Ret[1]["liid"] = IndexCount
        LPMAPRData.append(Ret)
        IndexCount += 1
# I'm not sure how this could ever happen. The user must have selected
# something.
    if len(LPMAPRData) == 0:
        setMsg("LPMAPR", "YB", "No positions found.", 2)
        formLPMAPRControl("stopped")
        return
    formLPMAPRMap("zoomout")
# Count the number of bad files for the message. Their positions will not have
# been plotted in formLPMAPRMap().
    BadFiles = 0
    BadFilenames = []
    for Item in LPMAPRData:
        if Item[0] != 0:
            BadFiles += 1
            BadFilenames.append(basename(Item[4]))
    if BadFiles == 0:
        setMsg("LPMAPR", "", "Done.")
    else:
        setMsg("LPMAPR", "YB",
               "Bad files: %d   Positions may not have been plotted." %
               BadFiles, 2)
        if BadFiles <= 10:
            formMYD("LPMAPR", (("(Thanks)", TOP, "ok"), ), "ok", "YB",
                    "Just Great.", "These files were bad\n\n%s" %
                    list2Str(BadFilenames, "\n", False))
        else:
            BadFilenames = BadFilenames[:10]
            formMYD("LPMAPR", (("(Thanks)", TOP, "ok"), ), "ok", "YB",
                    "Just Great.",
                    "These are the first 10 files that were bad\n\n%s" %
                    list2Str(BadFilenames, "\n", False))
    formLPMAPRControl("stopped")
    return
########################################
# BEGIN: formLPMAPRMap(Action, e = None)
# FUNC:formLPMAPRMap():2018.235


def formLPMAPRMap(Action, e=None):
    global LPMAPRData
    global LPMAPRLastCanW
    global LPMAPRLastCanH
    LCan = PROGCan["LPMAPR"]
    formLPMAPRFlagInfoOff("")
    LCan.delete(ALL)
    if Action == "clear":
        # These are the same as the "zoom[alltheway]out" Action.
        LCanW = LCan.winfo_width()
        LCanH = LCan.winfo_height()
        setMsg("LPMAPR", "", "")
        return
    if len(LPMAPRData) == 0:
        setMsg("LPMAPR", "RW", "You need to read some files first.", 2)
        return
    setMsg("LPMAPR", "CB", "Working...", 0)
    MapType = LPMAPRMapTypeRVar.get()
    if MapType == "s":
        LCan.config(bg=Clr["W"])
        GridTextFg = Clr["B"]
    elif MapType == "e":
        LCan.config(bg=Clr["B"])
        GridTextFg = Clr["W"]
    ShowIDID = LPMAPRShowIDIDCVar.get()
# Mostly so the canvas can update.
    updateMe(0)
# Find the range of the lat/longs.
    LatTop = 180.0
    LatBottom = 0.0
    LatRange = 0.0
    LonLeft = 360.0
    LonRight = 0.0
    LonRange = 0.0
# Just do these even if we are not doing the elevation map.
    MinElev = maxFloat
    MaxElev = -maxFloat
    XView = 0.0
    YView = 0.0
# Find the Lat and Lon ranges of whatever we're about to map, the max and min
# elevation, and all of the different types of things depending on what we will
# be grouping by. Use the "normalized" latn and lonn values here.
# Skip over the return flag value.
    for Point in LPMAPRData:
        # Point will be (0, {dict of values}) for good files.
        # FINISHME - Do something with the "bad" files?
        if Point[0] != 0:
            continue
        Point = Point[1]
        if Point["latn"] < LatTop:
            LatTop = Point["latn"]
        if Point["latn"] > LatBottom:
            LatBottom = Point["latn"]
        if Point["lonn"] < LonLeft:
            LonLeft = Point["lonn"]
        if Point["lonn"] > LonRight:
            LonRight = Point["lonn"]
        if Point["elef"] < MinElev:
            MinElev = Point["elef"]
        if Point["elef"] > MaxElev:
            MaxElev = Point["elef"]
# If these are still the same there must be nothing to map or this is the
# weirdest deployment ever seen. This should never be triggered (tm).
    if LatTop == 180.0 and LatBottom == 0.0 and LonLeft == 360.0 and \
            LonRight == 0.0:
        setMsg("LPMAPR", "YB", "There doesn't seem to be anything to map.", 2)
        return
# Expand the ranges 12% so items at the extremes don't get mapped right at
# the edge of the canvas.
# Remember: Lats 0.0N to 180.0S, Longs 0.0W to 360.0E
    LatRange = LatBottom - LatTop
    if LatRange == 0.0:
        LatRange = 1.0
    LatRange *= 1.12
    LatTop -= LatRange * 0.06
    LatBottom += LatRange * 0.06
    if LatTop < 0.0 or LatBottom > 180.0:
        if LatTop < 0.0 and LatBottom <= 180.0:
            LatTop = 0.0
            LatRange = LatBottom - LatTop
        elif LatTop >= 0.0 and LatBottom > 180.0:
            LatBottom = 180.0
            LatRange = LatBottom - LatTop
# Something must be wrong way before here.
        elif LatTop < 0.0 and LatBottom > 180.0:
            LatTop = 0.0
            LatBottom = 180.0
            LatRange = 180.0
    LonRange = LonRight - LonLeft
    if LonRange == 0.0:
        LonRange = 1.0
    LonRange *= 1.12
    LonLeft -= LonRange * 0.06
    LonRight += LonRange * 0.06
    if LonLeft < 0.0 or LonRight > 360.0:
        if LonLeft < 0.0 and LonRight <= 360.0:
            LonLeft = 0.0
            LonRange = LonRight
        elif LonLeft >= 0.0 and LonRight > 360.0:
            LonRight = 360.0
            LonRange = LonRight - LonLeft
        elif LonLeft < 0.0 and LonRight > 360.0:
            LonLeft = 0.0
            LonRight = 360.0
            LonRange = 360.0
# Redo these since they were altered above.
    LatRange = LatBottom - LatTop
    LonRange = LonRight - LonLeft
    LCanW = LPMAPRLastCanW
    LCanH = LPMAPRLastCanH
# Adjust the display according to the passed Action command.
    if Action == "replot":
        if LCanW < LCan.winfo_width():
            LCanW = LCan.winfo_width()
        if LCanH < LCan.winfo_height():
            LCanH = LCan.winfo_height()
        LCan.configure(scrollregion=(0, 0, LCanW, LCanH))
    else:
        if Action == "zoomout":
            LCanW = LCan.winfo_width()
            LCanH = LCan.winfo_height()
            XView = 0.0
            YView = 0.0
        elif Action.startswith("zin"):
            # Get the point that the user clicked on in the "old" canvas and
            # calculate the point that should be in the center after we've
            # zoomed in.
            if Action == "zin":
                Zoom = 2.0
            elif Action == "zin2":
                Zoom = 4.0
            LCanW *= Zoom
            LCanH *= Zoom
            XView = ((LCan.canvasx(e.x) * Zoom) -
                     LCan.winfo_width() / 2) / LCanW
            YView = ((LCan.canvasy(e.y) * Zoom) -
                     LCan.winfo_height() / 2) / LCanH
        elif Action.startswith("zout"):
            if Action == "zout":
                Zoom = 2.0
            elif Action == "zout2":
                Zoom = 4.0
            LCanW /= Zoom
            LCanH /= Zoom
            XView = ((LCan.canvasx(e.x) / Zoom) -
                     LCan.winfo_width() / 2) / LCanW
            YView = ((LCan.canvasy(e.y) / Zoom) -
                     LCan.winfo_height() / 2) / LCanH
# You can't zoom out farther than the size of the canvas.
            if LCanW < LCan.winfo_width():
                LCanW = LCan.winfo_width()
                XView = 0.0
            if LCanH < LCan.winfo_height():
                LCanH = LCan.winfo_height()
                YView = 0.0
        LCan.configure(scrollregion=(0, 0, LCanW, LCanH))
        LCan.xview_moveto(XView)
        LCan.yview_moveto(YView)
# We need to adjust the longitude range to compensate for the cos(lat) effect
# on the distance between lines of longitude. The center of the map will be
# correct. The top and bottom will be "off" a little depending on how much
# change in LAT there is (I'm only going so far in this correction business).
    if LPMAPRCorrLatCVar.get() == 1:
        LatMid = (LatTop + LatBottom) / 2.0
        if LatMid < 90.0:
            RealLatMid = 90.0 - LatMid
        else:
            RealLatMid = LatMid - 90.0
        CosLat = cos(RealLatMid / 57.29577951)
        W2H = float(LCanW) / float(LCanH)
# Check to see if the map is narrower in Lon than it needs to be for its height
# in Lat at this Lat.
        LonRange2 = LatRange / CosLat * W2H
        if LonRange < LonRange2:
            LonRange = LonRange2
# Find the original middle point.
            LonMid = (LonLeft + LonRight) / 2.0
# Now readjust everything as needed using the new range.
            LonLeft = LonMid - LonRange / 2.0
            if LonLeft < 0.0:
                LonLeft = 0.0
                LonRight = LonRange
            else:
                LonRight = LonMid + LonRange / 2.0
                if LonRight > 360.0:
                    LonRight = 360.0
                    LonLeft = 360.0 - LonRange
# Check to see if the Lat range of the data is smaller than what the Lat range
# should be for the Lon range of the data.
        else:
            LatRange2 = LonRange / W2H * CosLat
            if LatRange < LatRange2:
                LatRange = LatRange2
                LatTop = LatMid - LatRange / 2.0
                if LatTop < 0.0:
                    LatTop = 0.0
                    LatBottom = LatRange
                else:
                    LatBottom = LatMid + LatRange / 2.0
                    if LatBottom > 180.0:
                        LatTop = 180.0 - LatRange
                        LatBottom = 180.0
    elif LPMAPRCorrLatCVar.get() == 0:
        # Just fool everybody.
        CosLat = 1.0
    LPMAPRLastCanW = LCanW
    LPMAPRLastCanH = LCanH
# Set the MAPRLandHeight[] gradient values if we are going to be using them.
    if MapType == "e":
        Bin = (MaxElev - MinElev) / 20.0
        MAPRLandHeight[0][0] = MinElev
        for i in arange(1, 20):
            MAPRLandHeight[i][0] = MinElev + (Bin * i)
# TAGS:
#   gt = grid text
#   gl = grid line
#   D = anything to do with the distance scale
# Now that we have an idea of where everything is going to be throw in some
# grid lines.
# Just keep making lines until the "if yy" figures out it's time to stop.
    Grids = 0
    LastGrid = -9999
    SkipGrid = 3 * PROGPropFontHeight
    for Lat in arange(int(LatTop), 190):
        if Lat <= 90:
            RealLat = "N%d" % (90 - Lat)
        else:
            RealLat = "S%d" % (Lat - 90)
        yy = (float(Lat) - LatTop) / LatRange * LCanH
        if yy < 0:
            continue
        if yy > LCanH:
            break
        if yy > LastGrid + SkipGrid:
            LCan.create_line(0, yy, LCanW, yy, fill=Clr["A"], tags="gl")
            LCan.create_text(5, yy, text=RealLat, anchor="w",
                             font=PROGPropFont, fill=GridTextFg, tags="gt")
            LastGrid = yy
            Grids = 1
# If no whole integer degree lines were mapped try 1/10th degree lines.
    if Grids == 0:
        Lat = float(int(LatTop))
        while True:
            if Lat <= 90.0:
                RealLat = "N%.2f" % (90.0 - Lat)
            else:
                RealLat = "S%.2f" % (Lat - 90.0)
            yy = (Lat - LatTop) / LatRange * LCanH
            if yy < 0:
                Lat += 0.10
                continue
            if yy > LCanH:
                break
            if yy > LastGrid + SkipGrid:
                LCan.create_line(0, yy, LCanW, yy, fill=Clr["A"],
                                 tags="gl")
                LCan.create_text(5, yy, text=RealLat, anchor="w",
                                 font=PROGPropFont, fill=GridTextFg, tags="gt")
                LastGrid = yy
            Lat += 0.10
# Now do the longitude grid lines.
    Grids = 0
    LastGrid = -9999
    SkipGrid = 2 * PROGPropFont.measure("W100")
    for Lon in arange(int(LonLeft), 370):
        if Lon <= 180:
            RealLon = "W%d" % (180 - Lon)
        else:
            RealLon = "E%d" % (Lon - 180)
        xx = (float(Lon) - LonLeft) / LonRange * LCanW
        if xx < 0:
            continue
        if xx > LCanW:
            break
        if xx > LastGrid + SkipGrid:
            LCan.create_line(xx, 0, xx, LCanH, fill=Clr["A"], tags="gl")
            LCan.create_text(xx, LCanH - 2, text=RealLon, anchor="s",
                             font=PROGPropFont, fill=GridTextFg, tags="gt")
            LastGrid = xx
            Grids = 1
# 1/10th degree.
    if Grids == 0:
        Lon = float(int(LonLeft))
        SkipGrid = 2 * PROGPropFont.measure("W100.00")
        while True:
            if Lon <= 180.0:
                RealLon = "W%.2f" % (180.0 - Lon)
            else:
                RealLon = "E%.2f" % (Lon - 180.0)
            xx = (Lon - LonLeft) / LonRange * LCanW
            if xx < 0.0:
                Lon += 0.10
                continue
            if xx > LCanW:
                break
            if xx > LastGrid + SkipGrid:
                LCan.create_line(xx, 0, xx, LCanH, fill=Clr["A"],
                                 tags="gl")
                LCan.create_text(xx, LCanH - 2, text=RealLon, anchor="s",
                                 font=PROGPropFont, fill=GridTextFg, tags="gt")
                LastGrid = xx
            Lon += 0.10
# There may not be any grid lines to rise above, so try.
    try:
        LCan.tag_raise("gt", "gl")
    except TclError:
        pass
    try:
        LCan.tag_raise("D", "gl")
    except TclError:
        pass
# Keep the user entertained through all of this.
    LCan.update()
    EndMessage = ""
# Everyone will need these "contants"
    LonX = LCanW / LonRange
    LatX = LCanH / LatRange
# ----- Regular map -----
    if MapType == "s":
        Plotted = 0
        for Point in LPMAPRData:
            if Point[0] != 0:
                continue
            Point = Point[1]
            Plotted += 1
# The Tag will be the index/ID number of the entry we are mapping so we can
# look up the information for this item in LPMAPRData for when the user clicks
# on the point.
            Tag = "P" + str(Point["liid"])
            xx = (Point["lonn"] - LonLeft) * LonX
            yy = (Point["latn"] - LatTop) * LatX
# There was a Tkinter bug that caused a missing row and colunm of pixels so
# it's -5 and +6, but that was fixed, however using just -5,+5 or -6,+6 makes
# for really bad looking ovals. Same for MapType == "e".
            ID = LCan.create_oval(xx - 5, yy - 5, xx + 6, yy + 6, outline="",
                                  fill=Clr["G"], tags=Tag)
            LCan.tag_bind(ID, "<Button-1>", formLPMAPRFlagInfo)
# These are DISABLED so they don't prevent the dot from being clicked on when
# everyone is laying on top of each other.
            if ShowIDID == 1:
                LCan.create_text(xx + 8, yy, text=Point["idid"],
                                 font=PROGPropFont, fill=GridTextFg,
                                 anchor="nw", state=DISABLED)
        EndMessage += "  Points mapped: %d" % Plotted
# ----- Elevation map -----
    elif MapType == "e":
        Plotted = 0
        for Point in LPMAPRData:
            if Point[0] != 0:
                continue
            Point = Point[1]
            Plotted += 1
            Tag = "P" + str(Point["liid"])
            xx = (Point["lonn"] - LonLeft) * LonX
            yy = (Point["latn"] - LatTop) * LatX
# Look up what color the dot will be.
            for i in arange(19, 0, -1):
                if Point["elef"] >= MAPRLandHeight[i][0]:
                    break
# If we can't find a bin for the elevation the except will make the dot blue.
            try:
                ID = LCan.create_oval(xx - 6, yy - 6, xx + 7, yy + 7,
                                      outline="",
                                      fill=MAPRLandHeight[i][1], tags=Tag)
            except Exception:
                ID = LCan.create_oval(xx - 6, yy - 6, xx + 7, yy + 7,
                                      outline="",
                                      fill=Clr["U"], tags=Tag)
            LCan.tag_bind(ID, "<Button-1>", formLPMAPRFlagInfo)
            if ShowIDID == 1:
                LCan.create_text(xx + 8, yy, text=Point["idid"],
                                 font=PROGPropFont, fill=GridTextFg,
                                 anchor="nw", state=DISABLED)
        EndMessage += "  Elevation points: %d" % Plotted
# Distance Scale. Do this last so it shows up on top of everything.
    KmPerLonPix = 111.325 * LonRange * CosLat / LCanW * 100
    CanX = LCan.canvasx(0)
    CanY = LCan.canvasy(0)
    LCan.create_line(CanX + 50, CanY + 10, CanX + 150, CanY + 10,
                     fill=GridTextFg, tags="D")
    LCan.create_line(CanX + 50, CanY + 5, CanX + 50, CanY + 15,
                     fill=GridTextFg, tags="D")
    LCan.create_line(CanX + 150, CanY + 5, CanX + 150, CanY + 15,
                     fill=GridTextFg, tags="D")
    if KmPerLonPix > 100.0:
        LCan.create_text(CanX + 100, CanY + 10 + PROGPropFontHeight,
                         text="%.0f km" % KmPerLonPix, font=PROGPropFont,
                         fill=GridTextFg, tags="D")
    elif KmPerLonPix > 10.0:
        LCan.create_text(CanX + 100, CanY + 10 + PROGPropFontHeight,
                         text="%.3f km" % KmPerLonPix, font=PROGPropFont,
                         fill=GridTextFg, tags="D")
    else:
        KmPerLonPix *= 1000.0
        LCan.create_text(CanX + 100, CanY + 10 + PROGPropFontHeight,
                         text="%.1f m" % KmPerLonPix, font=PROGPropFont,
                         fill=GridTextFg, tags="D")
    setMsg("LPMAPR", "", "Done." + EndMessage, 0)
    return
######################################
# BEGIN: formLPMAPRDrag(Who, e = None)
# FUNC:formLPMAPRDrag():2008.020


def formLPMAPRDrag(Who, e=None):
    # If there are currently any flags turn them all off.
    if LPMAPRAnyFlags != 0:
        formLPMAPRFlagInfoOff("")
    Who.scan_dragto(e.x, e.y)
    return
######################################
# BEGIN: formLPMAPRMark(Who, e = None)
# FUNC:formLPMAPRMark():2008.020


def formLPMAPRMark(Who, e=None):
    Who.scan_mark(e.x, e.y)
    return


#####################################
# BEGIN: formLPMAPRFlagInfo(e = None)
# FUNC:formLPMAPRFlagInfo():2019.023
LPMAPRAnyFlags = 0


def formLPMAPRFlagInfo(e=None):
    global LPMAPRAnyFlags
    LCan = PROGCan["LPMAPR"]
# Where the user clicked in canvas coords.
    Cx = LCan.canvasx(e.x)
    Cy = LCan.canvasy(e.y)
# The upper left corner of the visible canvas.
    Wx = LCan.canvasx(0)
    Wy = LCan.canvasy(0)
# The width and height of the visible canvas.
    Ww = LCan.winfo_width()
    Wh = LCan.winfo_height()
    MapType = LPMAPRMapTypeRVar.get()
# We can set up some of the colors here (all of them for the elevation map).
    if MapType == "s":
        BoxOutline = Clr["B"]
    elif MapType == "e":
        BoxOutline = Clr["W"]
        BoxBG = Clr["E"]
        BoxFG = Clr["B"]
# Get a list of all the items that were clicked on, go through all of them and
# make a flag for each one.
    Items = LCan.find_overlapping(Cx, Cy, Cx + 1, Cy + 1)
# If there are a billion overlaps just limit it to the first 10.
    if len(Items) > 10:
        Items = Items[:10]
    LastFTag = ""
    Corr = "nw"
    for Item in Items:
        # Some Items don't have tags, so try.
        try:
            Tag = LCan.gettags(Item)[0]
        except IndexError:
            continue
# We only want "Points".
        if Tag.startswith("P") is False:
            continue
        FTag = "F" + Tag
# If there already is a flag for this item then turn it off.
        if LCan.gettags(FTag) != ():
            LCan.delete(FTag)
            continue
# Remember: The Tag is also the index in the LPMAPRData list.
        ind_ = int(Tag[1:])
# Depending on the type of map set the colors we'll be using. We have to do
        if MapType == "s":
            BoxBG = Clr["G"]
            BoxFG = Clr["B"]
# Make the text of the flag.
        ID = LCan.create_text(Cx, Cy,
                              text="  ID: " + LPMAPRData[ind_][1]["idid"] +
                              " \n" + " " +
                              pm2nsew("lat", LPMAPRData[ind_][1]["latf"]) +
                              "  " +
                              pm2nsew("lon", LPMAPRData[ind_][1]["lonf"]) +
                              " \n" + " Elev: %.1f" %
                              LPMAPRData[ind_][1]["elef"] + " \n" +
                              " File: %s " % LPMAPRData[ind_][1]["file"],
                              font=PROGPropFont, anchor=Corr, fill=BoxFG,
                              justify=CENTER, state=DISABLED, tags=("F", FTag))
# Get the coordinates of where the text is going to be and make sure it
# doesn't end up off the right or bottom edges of the map.
        L, T, R, B = LCan.bbox(ID)
# We only want the first tag's position to be corrected, and the rest to just
# follow along.
        if len(LastFTag) == 0:
            Corr = "n"
            if (Cy - Wy) + (B - T) > Wh:
                Corr = "s"
            if (Cx - Wx) + (R - L) > Ww:
                Corr += "e"
            else:
                Corr += "w"
            if Corr != "nw":
                # Move it and get the coordinates again for the box.
                LCan.itemconfigure(ID, anchor=Corr)
                L, T, R, B = LCan.bbox(ID)
# Naturally, the box needs to be ONE PIXEL taller to make it look right. The
# text had to be made before the box so we would know how big to make the box.
# The B+1 makes it a little bit taller to leave some space between the bottom
# and _ characters in things like file names.
        ID2 = LCan.create_rectangle((L, T - 1, R, B + 1), fill=BoxBG,
                                    outline=BoxOutline, tags=("F", FTag))
# Since the text was made first we now have to raise it above the box.
        LCan.tag_raise(ID, ID2)
# Only send the last/previous info flag lower if there is one. There will be
# one only if multiple dots were clicked on.
        if len(LastFTag) != 0:
            LCan.tag_lower(FTag, LastFTag)
        LastFTag = FTag
        LCan.tag_bind(ID2, "<Button-1>", Command(formLPMAPRFlagInfoOff, FTag))
        if B2Glitch is True:
            LCan.tag_bind(ID2, "<Button-2>",
                          Command(formLPMAPRPopup,
                                  LPMAPRData[ind_][1]["file"]))
        LCan.tag_bind(ID2, "<Button-3>", Command(formLPMAPRPopup,
                                                 LPMAPRData[ind_][1]["file"]))
        LCan.tag_bind(ID2, "<Enter>", Command(formLPMAPRCursorControl, LCan,
                                              "right_ptr black white"))
        LCan.tag_bind(ID2, "<Leave>", Command(formLPMAPRCursorControl, LCan,
                                              ""))
        LPMAPRAnyFlags = 1
# Set up for any additional info flags.
        if Corr == "nw":
            Cx += 5
# +1 for the border width of the Labels.
            Cy += PROGPropFontHeight + 1
        elif Corr == "ne":
            Cx -= 5
            Cy += PROGPropFontHeight + 1
        elif Corr == "sw":
            Cx += 5
            Cy -= PROGPropFontHeight + 1
        elif Corr == "se":
            Cx -= 5
            Cy -= PROGPropFontHeight + 1
    return
#############################################
# BEGIN: formLPMAPRFlagInfoOff(Tag, e = None)
# FUNC:formLPMAPRFlagInfoOff():2018.235


def formLPMAPRFlagInfoOff(Tag, e=None):
    global LPMAPRAnyFlags
    LCan = PROGCan["LPMAPR"]
    if len(Tag) == 0:
        # The Canvas may not exist yet or anymore.
        try:
            LCan.tag_unbind(ALL, "<Enter>")
            LCan.tag_unbind(ALL, "<Leave>")
# Delete any info flags.
            LCan.delete("F")
        except Exception:
            pass
        LPMAPRAnyFlags = 0
    else:
        try:
            LCan.tag_unbind(Tag, "<Enter>")
            LCan.tag_unbind(Tag, "<Leave>")
            LCan.delete(Tag)
        except Exception:
            pass
    formLPMAPRCursorControl(LCan, "")
    return
#########################################################
# BEGIN: formLPMAPRCursorControl(Widget, Which, e = None)
# FUNC:formLPMAPRCursorControl():2018.236


def formLPMAPRCursorControl(Widget, Which, e=None):
    if e is None:
        Widget.config(cursor="")
    elif e.type == "7":
        Widget.config(cursor="%s" % Which)
    else:
        Widget.config(cursor="")
    return
######################################
# BEGIN: formLPMAPRChangeDir(e = None)
# FUNC:formLPMAPRChangeDir():2014.062
#   Just for LOGPEEK which always keeps everything set to the same thing.


def formLPMAPRChangeDir(e=None):
    changeMainDirs("LPMAPR", "thedata", 1, None, "", "")
    PROGMsgsDirVar.set(PROGDataDirVar.get())
    PROGWorkDirVar.set(PROGDataDirVar.get())
    return
############################################
# BEGIN: formLPMAPRPopup(Filespec, e = None)
# FUNC:formLPMAPRPopup():2013.282


def formLPMAPRPopup(File, e=None):
    Xx = Root.winfo_pointerx()
    Yy = Root.winfo_pointery()
    PMenu = Menu(PROGCan["LPMAPR"], font=PROGOrigPropFont, tearoff=0,
                 bg=Clr["D"], bd=2, relief=GROOVE)
    PMenu.add_command(label="Plot This File",
                      command=Command(formLPMAPRPlotMain, File))
    PMenu.tk_popup(Xx, Yy)
    return
#####################################
# BEGIN: formLPMAPRPlotMain(Filespec)
# FUNC:formLPMAPRPlotMain():2018.235
#   Lets the user click on a plotted file's name and gets that file read and
#   plotted in the main display.


def formLPMAPRPlotMain(File):
    setMsg("LPMAPR", "", "")
# The file has to appear in the current mail list of files.
    for Index in arange(0, MFFiles.size()):
        MFFile = MFFiles.get(Index)
        if MFFile.startswith(File):
            MFFiles.selection_clear(0, END)
            MFFiles.selection_set(Index)
            fileSelected()
            return
# The two file lists may be different.
    setMsg("LPMAPR", "RW",
           "The file %s must be listed in the main files list for this to "
           "work. Check the List checkbuttons on the main form to see if this "
           "file's type is being listed and use the Reload button if "
           "necessary." % File, 2)
    return
#########################
# BEGIN: formLPMAPRSort()
# FUNC:formLPMAPRSort():2018.235
#   Sorts the entires in the Listbox alphbetically.


def formLPMAPRSort():
    Items = {}
    for ind_ in arange(0, LPMAPRFiles.size()):
        Item = LPMAPRFiles.get(ind_)
        if len(Item) != 0:
            # Lower the keys so we get an alphbetical sort and not an ASCII
            # sort.
            Items[Item.lower()] = Item
    Keys = sorted(Items.keys())
    LPMAPRFiles.delete(0, END)
    for Key in Keys:
        LPMAPRFiles.insert(END, Items[Key])
    setMsg("LPMAPR", "", "Items sorted: %d" % len(Keys))
    return
###########################################
# BEGIN: formLPMAPRControl(Which, e = None)
# FUNC:formLPMAPRControl():2012.089


def formLPMAPRControl(Which, e=None):
    if Which == "go":
        PROGFrm["LPMAPR"].focus_set()
        buttonBG(LPMAPRReadBut, "G")
        buttonBG(LPMAPRStopBut, "R", NORMAL)
        LPMAPRRunning.set(1)
        return (0, )
    elif Which == "stopping":
        buttonBG(LPMAPRStopBut, "Y")
    elif Which == "stopped":
        buttonBG(LPMAPRReadBut, "D")
        buttonBG(LPMAPRStopBut, "D", DISABLED)
    elif Which == "close":
        # In case it's running.
        LPMAPRRunning.set(0)
        del LPMAPRData[:]
        formClose("LPMAPR")
    LPMAPRRunning.set(0)
    return (0, )
##########################
# BEGIN: formLPMAPRClose()
# FUNC:formLPMAPRClose():2012.002


def formLPMAPRClose():
    formClose("LPMAPR")
    return
# END: formLPMAPR


#############################################################################
# BEGIN: formPCAL(Parent, Where, ChgBar, EntryVar, EntryWidget, Wait, Format,
#                OrigFont = False, Title = "")
# LIB:formPCAL():2019.030
#    Fills in EntryVar with a date selected by the user in YYYY:DOY,
#    YYYY-MM-DD, or YYYYMMMDD format. If a time like:
#        YYYY:DOY:HH:MM:SS
#        YYYY-MM-DD HH:MM:SS
#        YYYYMMMDD HH:MM:SS
#    is in the EntryVar value then that time will be preserved and returned
#    appended to the selected date. No error checking of the time is done.
#    Parent = String or PROGFrm[]
#    Where = center() Where value ("C", "N", "W"...)
#    ChgBar = If "" then the calling form's ChgBar will not be messed with.
#    EntryVar = The StringVar() to leave the selection in.
#    EntryWidget = The Entry() field that goes with the EntryVar.
#    Wait = True/False - should the form capture input until it is dissmissed?
#    Format = The format of the date to return. Can also be a StringVar that
#             contains the desired format string (as shown above).
#    OrigFont = True forces the dialog to use the system font that was going
#               to be used by the program before any user font settings were
#               set.
#    Title = An alternate title to use.
PROGFrm["PCAL"] = None
PCALYear = 0
PCALMonth = 0
PCALTime = ""
PCALText = None
PCALDtModeRVar = StringVar()
PCALDtModeRVar.set("dates")
PROGSetups += ["PCALDtModeRVar"]


def formPCAL(Parent, Where, ChgBar, EntryVar, EntryWidget, Wait, Format,
             OrigFont=False, Title=""):
    global PCALYear
    global PCALMonth
    global PCALTime
    global PCALText
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    if isinstance(Format, StringVar):
        Format = Format.get()
# Set the calendar to the month of whatever may already be in the EntryVar and
# seal with any time that was passed.
    PCALTime = ""
    if rtnPattern(EntryVar.get()).startswith("0000-0"):
        PCALYear = intt(EntryVar.get())
        PCALMonth = intt(EntryVar.get()[5:])
        Parts = EntryVar.get().split()
        if len(Parts) == 2:
            PCALTime = Parts[1]
    elif rtnPattern(EntryVar.get()).startswith("0000:0"):
        PCALYear = intt(EntryVar.get())
        DOY = intt(EntryVar.get()[5:])
        PCALMonth, DD = dt2Timeydoy2md(PCALYear, DOY)
        Parts = EntryVar.get().split(":")
        if len(Parts) > 2:
            Parts += (5 - len(Parts)) * ["0"]
            PCALTime = "%02d:%02d:%02d" % (intt(Parts[2]), intt(Parts[3]),
                                           intt(Parts[4]))
    elif rtnPattern(EntryVar.get()).startswith("0000AAA"):
        PCALYear = intt(EntryVar.get())
        MMM = EntryVar.get()[4:4 + 3].upper()
        PCALMonth = PROG_MONNUM[MMM]
        Parts = EntryVar.get().split()
        if len(Parts) == 2:
            PCALTime = Parts[1]
    if PROGFrm["PCAL"] is not None:
        formPCALMove("c", ChgBar, EntryVar, EntryWidget, Format)
        PROGFrm["PCAL"].deiconify()
        PROGFrm["PCAL"].lift()
        PROGFrm["PCAL"].focus_set()
        return
    LFrm = PROGFrm["PCAL"] = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "PCAL"))
    if len(Title) == 0:
        LFrm.title("Pick A Date")
    else:
        LFrm.title(Title)
    LFrm.iconname("PickCal")
    if Wait is True:
        # Gets rid of some of the extra title bar buttons.
        LFrm.transient(Parent)
    if OrigFont is False:
        PCALText = Text(LFrm, bg=Clr["W"], fg=Clr["B"], height=11,
                        width=29, relief=SUNKEN, state=DISABLED)
    else:
        PCALText = Text(LFrm, bg=Clr["W"], fg=Clr["B"],
                        font=PROGOrigMonoFont, height=11, width=29,
                        relief=SUNKEN, state=DISABLED)
    PCALText.pack(side=TOP, padx=3, pady=3)
    if PCALYear != 0:
        formPCALMove("c", ChgBar, EntryVar, EntryWidget, Format)
    else:
        formPCALMove("n", ChgBar, EntryVar, EntryWidget, Format)
    Sub = Frame(LFrm)
    BButton(Sub, text="<<",
            command=Command(formPCALMove, "-y", ChgBar, EntryVar, EntryWidget,
                            Format)).pack(side=LEFT)
    BButton(Sub, text="<",
            command=Command(formPCALMove, "-m", ChgBar, EntryVar, EntryWidget,
                            Format)).pack(side=LEFT)
    BButton(Sub, text="Today",
            command=Command(formPCALMove, "n", ChgBar, EntryVar, EntryWidget,
                            Format)).pack(side=LEFT)
    BButton(Sub, text=">",
            command=Command(formPCALMove, "+m", ChgBar, EntryVar, EntryWidget,
                            Format)).pack(side=LEFT)
    BButton(Sub, text=">>",
            command=Command(formPCALMove, "+y", ChgBar, EntryVar, EntryWidget,
                            Format)).pack(side=LEFT)
    Sub.pack(side=TOP, padx=3, pady=3)
    Sub = Frame(LFrm)
    Radiobutton(Sub, text="Dates", value="dates", variable=PCALDtModeRVar,
                command=Command(formPCALMove, "c", ChgBar, EntryVar,
                                EntryWidget, Format)).pack(side=LEFT)
    Radiobutton(Sub, text="DOY", value="doy", variable=PCALDtModeRVar,
                command=Command(formPCALMove, "c", ChgBar, EntryVar,
                                EntryWidget, Format)).pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    BButton(Sub, text="Close", fg=Clr["R"],
            command=Command(formClose, "PCAL")).pack(side=TOP)
    Sub.pack(side=TOP, padx=3, pady=3)
    center(Parent, LFrm, Where, "I", True)
    if Wait is True:
        LFrm.focus_set()
# See formMYD().
        if PROGSystem == "dar":
            pass
        else:
            LFrm.grab_set()
        LFrm.wait_window()
    return
#######################################################
# BEGIN: formPCALCursorControl(Widget, Which, e = None)
# FUNC:formPCALCursorControl():2018.236


def formPCALCursorControl(Widget, Which, e=None):
    if e is None:
        Widget.config(cursor="")
    elif e.type == "7":
        Widget.config(cursor="%s" % Which)
    else:
        Widget.config(cursor="")
    return
##################################################################
# BEGIN: formPCALMove(What, ChgBar, EntryVar, EntryWidget, Format)
# FUNC:formPCALMove():2018.235
#   Handles changing the calendar form's display.


def formPCALMove(What, ChgBar, EntryVar, EntryWidget, Format):
    global PCALYear
    global PCALMonth
    global PCALText
    Year = PCALYear
    Month = PCALMonth
    if What == "-y":
        Year -= 1
    elif What == "-m":
        Month -= 1
    elif What == "n":
        (Year, Month, Day) = getGMT(4)
    elif What == "+m":
        Month += 1
    elif What == "+y":
        Year += 1
    elif What == "c":
        pass
    if Year < 1971:
        beep(1)
        return
    elif Year > 2050:
        beep(1)
        return
    if Month > 12:
        Year += 1
        Month = 1
    elif Month < 1:
        Year -= 1
        Month = 12
    PCALYear = Year
    PCALMonth = Month
    LTxt = PCALText
    LTxt.configure(state=NORMAL)
    LTxt.delete("0.0", END)
# Otherwise "today" is cyan on every month.
    LTxt.tag_delete(*LTxt.tag_names())
    DtMode = PCALDtModeRVar.get()
    if DtMode == "dates":
        DOM1 = 0
    elif DtMode == "doy":
        DOM1 = PROG_FDOM[Month]
        if (Year % 4 == 0 and Year % 100 != 0) or Year % 400 == 0:
            if Month > 2:
                DOM1 += 1
    LTxt.insert(END, "\n%s\n\n " %
                (PROG_CALMON[Month] + " " + str(Year)).center(29))
    IdxS = LTxt.index(CURRENT)
    LTxt.tag_config(IdxS, background=Clr["W"], foreground=Clr["R"])
    LTxt.insert(END, "Sun", IdxS)
    IdxS = LTxt.index(CURRENT)
    LTxt.tag_config(IdxS, background=Clr["W"], foreground=Clr["U"])
    LTxt.insert(END, " Mon Tue Wed Thu Fri ", IdxS)
    IdxS = LTxt.index(CURRENT)
    LTxt.tag_config(IdxS, background=Clr["W"], foreground=Clr["R"])
    LTxt.insert(END, "Sat ", IdxS)
    LTxt.insert(END, "\n")
    All = monthcalendar(Year, Month)
    NowYear, NowMonth, NowDay = getGMT(4)
    TargetDay = DOM1 + NowDay
    for Week in All:
        LTxt.insert(END, " ")
        for DD in Week:
            if DD != 0:
                ThisDay = DOM1 + DD
                IdxS = LTxt.index(CURRENT)
                if ThisDay == TargetDay and Month == NowMonth and \
                        Year == NowYear:
                    LTxt.tag_config(IdxS, background=Clr["C"],
                                    foreground=Clr["B"])
                    LTxt.tag_bind(IdxS, "<Button-1>",
                                  Command(formPCALPicked, ChgBar, EntryVar,
                                          EntryWidget, (Year, Month, DD),
                                          Format))
                    LTxt.tag_bind(IdxS, "<Enter>",
                                  Command(formPCALCursorControl, LTxt,
                                          "right_ptr black white"))
                    LTxt.tag_bind(IdxS, "<Leave>",
                                  Command(formPCALCursorControl, LTxt, ""))
                else:
                    LTxt.tag_bind(IdxS, "<Button-1>",
                                  Command(formPCALPicked, ChgBar, EntryVar,
                                          EntryWidget, (Year, Month, DD),
                                          Format))
                    LTxt.tag_bind(IdxS, "<Enter>",
                                  Command(formPCALCursorControl, LTxt,
                                          "right_ptr black white"))
                    LTxt.tag_bind(IdxS, "<Leave>",
                                  Command(formPCALCursorControl, LTxt, ""))
                if DtMode == "dates":
                    if ThisDay < 10:
                        LTxt.insert(END, "  ")
                        LTxt.insert(END, "%d" % ThisDay, IdxS)
                        LTxt.insert(END, " ")
                    else:
                        LTxt.insert(END, " ")
                        LTxt.insert(END, "%d" % ThisDay, IdxS)
                        LTxt.insert(END, " ")
                elif DtMode == "doy":
                    LTxt.insert(END, "%03d" % ThisDay, IdxS)
                    LTxt.insert(END, " ")
            else:
                LTxt.insert(END, "    ")
        LTxt.insert(END, "\n")
    LTxt.configure(state=DISABLED)
    return
##############################################################################
# BEGIN: formPCALPicked(ChgBar, EntryVar, EntryWidget, Date, Format, e = None)
# FUNC:formPCALPicked():2018.236


def formPCALPicked(ChgBar, EntryVar, EntryWidget, Date, Format, e=None):
    if Format.startswith("YYYY-MM-DD"):
        if len(PCALTime) == 0:
            EntryVar.set("%d-%02d-%02d" % Date)
        else:
            EntryVar.set("%d-%02d-%02d %s" % (Date, PCALTime))
    elif Format == "YYYY:DOY":
        DOY = dt2Timeymd2doy(Date[0], Date[1], Date[2])
        if len(PCALTime) == 0:
            EntryVar.set("%d:%03d" % (Date[0], DOY))
        else:
            EntryVar.set("%d:%03d:%s" % (Date[0], DOY, PCALTime))
    elif Format == "YYYYMMMDD":
        if Date[1] > 0 and Date[1] < 13:
            MMM = PROG_CALMONS[Date[1]]
        else:
            MMM = "ERR"
        if len(PCALTime) == 0:
            EntryVar.set("%d%s%02d" % (Date[0], MMM, Date[2]))
        else:
            EntryVar.set("%d%s%02d %s" % (Date[0], MMM, Date[2], PCALTime))
    if EntryWidget is not None:
        EntryWidget.icursor(END)
# The program or the ChgBar form may not have any changebars.
    try:
        if len(ChgBar) != 0:
            # This is relevent portion of chgPROGChgBar() just to get rid of
            # the reference.
            try:
                PROGBar[ChgBar].configure(bg=Clr["R"])
                eval("%sBarSVar" % ChgBar).set(1)
            except Exception:
                pass
    except NameError:
        pass
    formClose("PCAL")
    return
# END: formPCAL


#####################
# BEGIN: formSEARCH()
# FUNC:formSEARCH():2018.236
PROGFrm["SEARCH"] = None
PROGTxt["SEARCH"] = None
PROGMsg["SEARCH"] = None


def formSEARCH():
    if MFPlotted is False:
        beep(1)
        return
    if PROGFrm["SEARCH"] is not None:
        PROGFrm["SEARCH"].deiconify()
        PROGFrm["SEARCH"].lift()
        return
# Create the window and put it in the middle of the screen.
    LFrm = PROGFrm["SEARCH"] = Toplevel(Root)
    LFrm.withdraw()
    LFrm.title("Log Search")
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "SEARCH"))
# Where the user will enter the search string.
    Sub = Frame(LFrm)
    Label(Sub, text="Search For:=").pack(side=LEFT)
    LEnt = PROGEnt["SEARCH"] = Entry(Sub, textvariable=SearchVar)
    LEnt.pack(side=LEFT, expand=YES, fill=X)
    LEnt.bind('<Return>', formSEARCHReturn)
    LEnt.focus_set()
    LEnt.icursor(END)
    Sub.pack(side=TOP, fill=X, padx=3, pady=2)
# Where the search results will be.
    Sub = Frame(LFrm)
    LTxt = PROGTxt["SEARCH"] = Text(Sub, width=80, height=20,
                                    wrap=NONE, relief=SUNKEN)
    LTxt.pack(side=LEFT, expand=YES, fill=BOTH)
    LSb = Scrollbar(Sub, orient=VERTICAL, command=LTxt.yview)
    LSb.pack(side=RIGHT, fill=Y)
    LTxt.configure(yscrollcommand=LSb.set)
    Sub.pack(side=TOP, fill=BOTH, expand=YES)
    LSb = Scrollbar(LFrm, orient=HORIZONTAL, command=LTxt.xview)
    LSb.pack(side=TOP, fill=X)
    LTxt.configure(xscrollcommand=LSb.set)
    Sub = Frame(LFrm)
    BButton(Sub, text="Search", command=formSEARCHReturn).pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    BButton(Sub, text="Write To File...",
            command=Command(formWrite, "SEARCH", "SEARCH", "SEARCH", "Pick Or "
                            "Enter An Output File...", SEARCHLastFilespecVar,
                            True, "", True, False)).pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    BButton(Sub, text="Close", fg=Clr["R"],
            command=Command(formClose, "SEARCH")).pack(side=LEFT)
    Sub.pack(side=TOP, padx=3, pady=3)
# Status messages.
    PROGMsg["SEARCH"] = Text(LFrm, font=PROGPropFont, height=3,
                             wrap=WORD)
    PROGMsg["SEARCH"].pack(side=TOP, fill=X)
    center(Root, "SEARCH", "CX", "I", True)
    return
###################################
# BEGIN: formSEARCHReturn(e = None)
# FUNC:formSEARCHReturn():2013.190


def formSEARCHReturn(e=None):
    PROGTxt["SEARCH"].delete(0.0, END)
    updateMe(0)
    LookFor = SearchVar.get().lower()
    setMsg("SEARCH", "CB", "Searching...")
    busyCursor(1)
    Found = 0
    for Line in LOGLines:
        LLine = Line.lower()
        if LLine.find(LookFor) != -1:
            PROGTxt["SEARCH"].insert(END, Line + "\n")
            Found += 1
    setMsg("SEARCH", "WB", "%d matching %s found." %
           (Found, sP(Found, ("line", "lines"))))
    busyCursor(0)
    return
# END: formSEARCH


##########################
# BEGIN: formTMRNG(Parent)
# FUNC:formTMRNG():2018.236
#   Reads the selected data files and plots a rule-character for each
#   Needs an OPTDateFormatRVar with "YYYY:DOY", "YYYY-MM-DD", or "YYYYMMMDD"
#   in it.
PROGFrm["TMRNG"] = None
PROGCan["TMRNG"] = None
TMRNGRunning = IntVar()
TMRNGFiles = None
TMRNGLbxFindVar = StringVar()
TMRNGShowLogsCVar = IntVar()
TMRNGShowLogsCVar.set(1)
TMRNGShowMSLOGSCVar = IntVar()
TMRNGShowMSLOGSCVar.set(1)
TMRNGShowZCFCVar = IntVar()
TMRNGShowZCFCVar.set(1)
TMRNGShowRefsCVar = IntVar()
TMRNGShowRefsCVar.set(1)
TMRNGShowUCFDCVar = IntVar()
TMRNGShowUCFDCVar.set(1)
TMRNGFromVar = StringVar()
TMRNGToVar = StringVar()
TMRNGPlotRVar = StringVar()
TMRNGPlotRVar.set("all")
TMRNGEraseCVar = IntVar()
TMRNGEraseCVar.set(1)
TMRNGPSFilespecVar = StringVar()
PROGSetups += ["TMRNGLbxFindVar", "TMRNGShowLogsCVar", "TMRNGShowMSLOGSCVar",
               "TMRNGShowZCFCVar", "TMRNGShowRefsCVar", "TMRNGShowUCFDCVar",
               "TMRNGFromVar", "TMRNGToVar", "TMRNGPlotRVar", "TMRNGEraseCVar",
               "TMRNGPSFilespecVar"]
# Not in PROGSetups on purpose.
TMRNGUseDatesVar = IntVar()
# Not in PROGSetups, because it's a real pain if the user forgets to check it.
TMRNGWarnIfBigCVar = IntVar()
TMRNGWarnIfBigCVar.set(1)
TMRNGData = []
TMRNGReadBut = None
TMRNGStopBut = None
# The standard error message code.
TMRNG_CODE = 0
# For good file returns.
TMRNG_FILE = 1
TMRNG_DASID = 2
TMRNG_TIMES = 3
TMRNG_SSTIMES = 4
# For bad file returns.
TMRNG_COLOR = 1
TMRNG_MSG = 2
# TMRNG_BEEP = 3
TMRNG_OTHER = 4


def formTMRNG(Parent):
    global TMRNGFiles
    global TMRNGReadBut
    global TMRNGStopBut
    if showUp("TMRNG"):
        return
    LFrm = PROGFrm["TMRNG"] = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.protocol("WM_DELETE_WINDOW", Command(formTMRNGControl, "close"))
    LFrm.title("Plot Time Ranges Of Files")
    LFrm.iconname("TimeRange")
# List the current directory(s) that apply to this form.
    Sub = Frame(LFrm)
# formTMRNG() started in LOGPEEK which had different needs for handling
# directories. It only used one.
    if PROG_NAME == "LOGPEEK":
        LBu = BButton(Sub, text="Main Data Directory:", pady="2p",
                      command=formTMRNGChangeDir)
    else:
        LBu = BButton(Sub, text="Main Data Directory:", pady="2p",
                      command=Command(changeMainDirs, "TMRNG", 1, "thedata",
                                      None, "", ""))
    LBu.pack(side=LEFT)
    ToolTip(LBu, 30, "Click to change the directory.")
    Entry(Sub, width=1, textvariable=PROGDataDirVar, state=DISABLED,
          relief=FLAT, bd=0, bg=Clr["D"],
          fg=Clr["B"]).pack(side=LEFT, expand=YES, fill=X)
    labelTip(Sub, " Hints ", LEFT, 45,
             "PLOT AREA HINTS:\n--Click on file: Clicking on a file name will "
             "display an information flag for that plot.\n--Click on flag: "
             "Clicking on an information flag will make it disappear.\n"
             "--Right-click on file: Right-clicking on a file name will "
             "show a popup menu with additional options.")
    Sub.pack(side=TOP, anchor="w", expand=NO, fill=X, padx=3)
    Sub = Frame(LFrm)
# Listbox and controls.
    SSub = Frame(Sub)
    SSSub = Frame(SSub)
    TMRNGFiles = Listbox(SSSub, relief=SUNKEN, bd=2, height=15,
                         width=25, selectmode=EXTENDED)
    TMRNGFiles.pack(side=LEFT, expand=YES, fill=Y)
    TMRNGFiles.bind("<Double-Button-1>", formTMRNGGo)
    Scroll = Scrollbar(SSSub, command=TMRNGFiles.yview, orient=VERTICAL)
    Scroll.pack(side=RIGHT, fill=Y)
    TMRNGFiles.configure(yscrollcommand=Scroll.set)
    SSSub.pack(side=TOP, expand=YES, fill=Y)
    SSSub = Frame(SSub)
    Scroll = Scrollbar(SSSub, command=TMRNGFiles.xview, orient=HORIZONTAL)
    Scroll.pack(side=BOTTOM, expand=YES, fill=X)
    TMRNGFiles.configure(xscrollcommand=Scroll.set)
    SSSub.pack(side=TOP, fill=X, expand=NO)
    SSSub = Frame(SSub)
    LEnt = labelEntry2(SSSub, 11, "Find:=", 35,
                       "[Reload] Show file names containing this at the top.",
                       TMRNGLbxFindVar, 8)
    LEnt.bind("<Return>", Command(loadRTFilesCmd, TMRNGFiles, "TMRNG"))
    LEnt.bind("<KP_Enter>", Command(loadRTFilesCmd, TMRNGFiles, "TMRNG"))
    BButton(SSSub, text="Clear", fg=Clr["U"],
            command=Command(loadRTFilesClearLbxFindVar, TMRNGFiles,
                            "TMRNG", True)).pack(side=LEFT)
    SSSub.pack(side=TOP)
    SSSub = Frame(SSub)
    BButton(SSSub, text="Reload",
            command=Command(loadRTFilesCmd, TMRNGFiles,
                            "TMRNG")).pack(side=LEFT)
    BButton(SSSub, text="Select All",
            command=Command(loadRTFilesSelectAll,
                            TMRNGFiles)).pack(side=LEFT)
    SSSub.pack(side=TOP)
    SSSub = Frame(SSub)
    LLb = Label(SSSub, text="List:")
    LLb.pack(side=LEFT)
    ToolTip(LLb, 35,
            "The types of data sources that should be displayed in the file "
            "list.")
    SSSSub = Frame(SSSub)
    SSSSSub = Frame(SSSSub)
    SSSSSSub = Frame(SSSSSub)
    LCb = Checkbutton(SSSSSSub, text=".log", variable=TMRNGShowLogsCVar)
    LCb.pack(side=TOP, anchor="w")
    ToolTip(LCb, 30, "RT130/72A text log files.")
    LCb = Checkbutton(SSSSSSub, text=".cf", variable=TMRNGShowUCFDCVar)
    LCb.pack(side=TOP, anchor="w")
    ToolTip(LCb, 30, "RT130 CompactFlash card images (not zipped).")
    SSSSSSub.pack(side=LEFT)
    SSSSSSub = Frame(SSSSSub)
    LCb = Checkbutton(SSSSSSub, text=".ref", variable=TMRNGShowRefsCVar)
    LCb.pack(side=TOP, anchor="w")
    ToolTip(LCb, 30, "RT130/72A all-in-one raw data files.")
    LCb = Checkbutton(SSSSSSub, text=".zip", variable=TMRNGShowZCFCVar)
    LCb.pack(side=TOP, anchor="w")
    ToolTip(LCb, 30, "RT130 zipped CompactFlash card images.")
    SSSSSSub.pack(side=LEFT)
    SSSSSub.pack(side=TOP)
    LCb = Checkbutton(SSSSub, text=".mslogs", variable=TMRNGShowMSLOGSCVar)
    LCb.pack(side=TOP)
    ToolTip(LCb, 30, "RT130 miniseed format LOG day volumns in a folder.")
    SSSSub.pack(side=LEFT)
    SSSub.pack(side=TOP)
    BButton(SSub, text="Sort", command=formTMRNGSort).pack(side=TOP)
    Frame(SSub, height=1, bg=Clr["B"], relief=GROOVE).pack(side=TOP,
                                                           fill=X, padx=10,
                                                           pady=3)
    LCb = Checkbutton(SSub, text="Warn If Big",
                      variable=TMRNGWarnIfBigCVar)
    LCb.pack(side=TOP)
    ToolTip(LCb, 40,
            "If this is selected the program will warn you before reading "
            "files of an abby-normal size.")
    SSSub = Frame(SSub)
    LLb = Label(SSSub, text="Plot:")
    LLb.pack(side=LEFT)
    ToolTip(LLb, 40,
            "All = Plot all of the files using the earliest and latest times "
            "of all of the files.\nEach = Plot each file individually so the "
            "information is spread out the width of the plotting area so gaps "
            "may show up better.")
    Radiobutton(SSSub, text="All", value="all",
                variable=TMRNGPlotRVar).pack(side=LEFT)
    Radiobutton(SSSub, text="Each", value="each",
                variable=TMRNGPlotRVar).pack(side=LEFT)
    SSSub.pack(side=TOP)
    LCb = Checkbutton(SSub, text="Erase?", variable=TMRNGEraseCVar)
    LCb.pack(side=TOP)
    ToolTip(LCb, 30,
            "Should the text area be erased before reading more files or not?")
    SSSub = Frame(SSub)
    TMRNGReadBut = BButton(SSSub, text="Read", command=formTMRNGGo)
    TMRNGReadBut.pack(side=LEFT)
    TMRNGStopBut = BButton(SSSub, text="Stop", state=DISABLED,
                           command=Command(formTMRNGControl, "stopping"))
    TMRNGStopBut.pack(side=LEFT)
    SSSub.pack(side=TOP)
    Frame(SSub, height=1, bg=Clr["B"], relief=GROOVE).pack(side=TOP,
                                                           fill=X, padx=10,
                                                           pady=3)
    SSSub = Frame(SSub)
    BButton(SSSub, text="Write .ps",
            command=Command(formWritePS, "TMRNG", "TMRNG",
                            TMRNGPSFilespecVar)).pack(side=LEFT, fill=X)
    BButton(SSSub, text="Close", fg=Clr["R"],
            command=Command(formTMRNGControl, "close")).pack(side=TOP)
    SSSub.pack(side=TOP)
    SSub.pack(side=LEFT, padx=3, pady=3, anchor="n", expand=NO,
              fill=Y)
    SSub = Frame(Sub)
    SSSub = Frame(SSub)
    LCb = Checkbutton(SSSub, text="Use:", variable=TMRNGUseDatesVar)
    LCb.pack(side=LEFT)
    ToolTip(LCb, 35, "Select this to use the From/To dates, and deselect it "
            "to ignore them.")
    labelTip(SSSub, " From:", LEFT, 30, "The date range to use to limit the "
             "plotting in the event that there is a data source that starts "
             "way earlier or runs way later than the rest. Format can be "
             "YYYY:DOY, YYYY-MM-DD, or YYYYMMMDD.")
    LEnt = Entry(SSSub, width=10, textvariable=TMRNGFromVar)
    LEnt.pack(side=LEFT)
    BButton(SSSub, text="C",
            command=Command(formPCAL, "TMRNG", "CX", "TMRNG", TMRNGFromVar,
                            LEnt, False, OPTDateFormatRVar)).pack(side=LEFT)
    Label(SSSub, text=" to ").pack(side=LEFT)
    LEnt = Entry(SSSub, width=10, textvariable=TMRNGToVar)
    LEnt.pack(side=LEFT)
    BButton(SSSub, text="C",
            command=Command(formPCAL, "TMRNG", "CX", "TMRNG", TMRNGToVar,
                            LEnt, False, OPTDateFormatRVar)).pack(side=LEFT)
    SSSub.pack(side=TOP)
# The plotting area.
    SSSub = Frame(SSub, bd=2, relief=SUNKEN)
    LCan = PROGCan["TMRNG"] = Canvas(SSSub, bg=Clr["W"], height=500,
                                     width=700)
    LCan.pack(side=LEFT, expand=YES, fill=BOTH)
    LSb = Scrollbar(SSSub, orient=VERTICAL, command=LCan.yview)
    LSb.pack(side=RIGHT, fill=Y)
    LCan.configure(yscrollcommand=LSb.set)
    SSSub.pack(side=TOP, expand=YES, fill=BOTH)
    SSub.pack(side=LEFT, expand=YES, fill=BOTH)
    Sub.pack(side=TOP, expand=YES, fill=BOTH)
    PROGMsg["TMRNG"] = Text(LFrm, font=PROGPropFont, height=3, wrap=WORD)
    PROGMsg["TMRNG"].pack(side=TOP, fill=X)
    center(Parent, LFrm, "CX", "I", True)
    if len(TMRNGPSFilespecVar.get()) == 0 or \
            exists(basename(TMRNGPSFilespecVar.get())) is False:
        TMRNGPSFilespecVar.set(PROGDataDirVar.get())
    loadRTFilesCmd(TMRNGFiles, "TMRNG")
# Always set this when the form is new.
    TMRNGWarnIfBigCVar.set(1)
    return
########################################################
# BEGIN: formTMRNGCursorControl(Widget, Which, e = None)
# FUNC:formTMRNGCursorControl():2018.236


def formTMRNGCursorControl(Widget, Which, e=None):
    if e is None:
        Widget.config(cursor="")
    elif e.type == "7":
        Widget.config(cursor="%s" % Which)
    else:
        Widget.config(cursor="")
    return
#####################################
# BEGIN: formTMRNGChangeDir(e = None)
# FUNC:formTMRNGChangeDir():2014.062
#   Just for LOGPEEK which always keeps everything set to the same thing.


def formTMRNGChangeDir(e=None):
    changeMainDirs("TMRNG", "thedata", 1, None, "", "")
    PROGMsgsDirVar.set(PROGDataDirVar.get())
    PROGWorkDirVar.set(PROGDataDirVar.get())
    return
##############################
# BEGIN: formTMRNGGo(e = None)
# FUNC:formTMRNGGo():2019.023


def formTMRNGGo(e=None):
    global TMRNGData
    if TMRNGRunning.get() == 1:
        setMsg("TMRNG", "YB", "I'm busy...", 2)
        sleep(.5)
        return
    setMsg("TMRNG", "", "")
    Sel = TMRNGFiles.curselection()
    if len(Sel) == 0:
        setMsg("TMRNG", "RW", "No files have been selected.", 2)
        return
    Plot = TMRNGPlotRVar.get()
    DateFormat = OPTDateFormatRVar.get()
# Check out any limits the user wants to use.
    FromEpoch = -maxInt
    ToEpoch = maxInt
# Ignore these if we are plotting full-scale.
    if Plot == "all" and TMRNGUseDatesVar.get() == 1:
        if changeDateFormat() is False:
            return
        for F in ("From", "To"):
            if len(eval("TMRNG%sVar" % F).get()) != 0:
                Ret = dt2Time(0, -1, eval("TMRNG%sVar" % F).get())
                if F == "From":
                    FromEpoch = Ret
                elif F == "To":
                    # To get to 24:00 of the entered date.
                    ToEpoch = Ret + 86400
# In case someone tries to be funny.
    if FromEpoch >= ToEpoch:
        setMsg("TMRNG", "RW",
               "From field date is the same as or later than the To field "
               "date.", 2)
    setMsg("TMRNG", "CB", "Working...")
    LCan = PROGCan["TMRNG"]
    CWIDTH = LCan.winfo_width()
    LFH = PROGPropFontHeight
    if TMRNGEraseCVar.get() == 1:
        LCan.delete(ALL)
        LCan.yview_moveto(0.0)
        LCan.update()
        YTop = LFH
    else:
        # There may not be anything plotted, so try.
        try:
            YTop = LCan.bbox(ALL)[3]
            YTop += LFH * 2.0
        except TypeError:
            YTop = LFH
# Always check the directory entry in case the user messed with it.
    PROGDataDirVar.set(PROGDataDirVar.get().strip())
    if PROGDataDirVar.get().endswith(sep) is False:
        PROGDataDirVar.set(PROGDataDirVar.get() + sep)
# Keep these in sync.
        PROGMsgsDirVar.set(PROGDataDirVar.get())
        PROGWorkDirVar.set(PROGDataDirVar.get())
    TheDataDir = PROGDataDirVar.get()
    canText(LCan, 5, YTop, "B", "Plot of selected item times in directory")
    YTop += LFH
    canText(LCan, 5, YTop, "B", "   " + TheDataDir)
    YTop += LFH * 2
    WarnIfBig = TMRNGWarnIfBigCVar.get()
    formTMRNGControl("go")
# Where we'll store the values for each file until all of the files have been
# read.
    del TMRNGData[:]
    FileCount = 0
    for Index in Sel:
        if TMRNGRunning.get() == 0:
            setMsg("TMRNG", "YB", "Stopped.", 2)
            formTMRNGControl("stopped")
            return
        Selected = TMRNGFiles.get(Index)
# Skip over the blank lines.
        if len(Selected) == 0:
            continue
        FileCount += 1
        setMsg("TMRNG", "CB", "Working on file %d of %d..." % (FileCount,
                                                               len(Sel)))
# The entry may be something like "Filename (x bytes or lines)" so just get
# the Filename portion.
        Parts = Selected.split()
        Filename = Parts[0]
        FilenameLC = Filename.lower()
        Filespec = "%s%s" % (PROGDataDirVar.get(), Filename)
# This check is a little complicated because of the way .cf's are listed in
# the Listbox (e.g. 9BA6.cf(sep)9BA6).
        if FilenameLC.find(".cf" + sep) != -1:
            try:
                ind_ = Filespec.index(".cf" + sep)
            except Exception:
                ind_ = Filespec.index(".CF" + sep)
            Filecheck = Filespec[:ind_ + 3]
            if exists(Filecheck) is False:
                setMsg("TMRNG", "RW",
                       "%s: .cf directory not found. Use the Reload button?" %
                       Filename, 2)
                formTMRNGControl("stopped")
                return
        else:
            if exists(Filespec) is False:
                setMsg("TMRNG", "RW",
                       "%s: File not found. Use the Reload button?" % Filename,
                       2)
                formTMRNGControl("stopped")
                return
        if FilenameLC.endswith(".log") or FilenameLC.endswith("_log"):
            if WarnIfBig == 1 and getsize(Filespec) > PROGBIG_LOG:
                Answer = formMYD("TMRNG",
                                 (("I'll Keep This In Mind", TOP, "ok"),
                                  ("I Know. Stop Bothering Me.", TOP, "fine"),
                                  ("Skip This File", TOP, "skip")),
                                 "stop", "YB", "Do You Like Coffee?",
                                 "The .log file\n\n%s\n\nis quite large "
                                 "(check the estimate of the number of lines "
                                 "in the files list). LOGPEEK is not very "
                                 "fast. This will take a while to process." %
                                 Filespec)
                if Answer == "stop":
                    setMsg("TMRNG", "", "Nothing done.")
                    formTMRNGControl("stopped")
                    return
                if Answer == "skip":
                    continue
                if Answer == "fine":
                    TMRNGWarnIfBigCVar.set(0)
                    WarnIfBig = 0
            Ret = rt72130logTimeRange(Filespec, Filespec, TMRNGStopBut,
                                      TMRNGRunning)
        elif FilenameLC.endswith(".ref"):
            if WarnIfBig == 1 and getsize(Filespec) > PROGBIG_REF:
                Answer = formMYD("TMRNG",
                                 (("I'll Keep This In Mind", TOP, "ok"),
                                  ("I Know. Stop Bothering Me.", TOP, "fine"),
                                  ("Skip This File", TOP, "skip")),
                                 "stop", "YB", "Do You Like Coffee?",
                                 "The .ref file\n\n%s\n\nis quite large "
                                 "(check the size in the files list). LOGPEEK "
                                 "is not very fast. This will take a while to "
                                 "process." % Filespec)
                if Answer == "stop":
                    setMsg("TMRNG", "", "Nothing done.")
                    progTMRNGControl("stopped")
                    return
                if Answer == "skip":
                    continue
                if Answer == "fine":
                    TMRNGWarnIfBigCVar.set(0)
                    WarnIfBig = 0
            Ret = rt72130refTimeRange(Filespec, TMRNGStopBut, TMRNGRunning)
# Look for these before .cf's since the names may be like 123456.cf.zip.
        elif FilenameLC.endswith(".zip"):
            if WarnIfBig == 1 and getsize(Filespec) > PROGBIG_ZCF:
                Answer = formMYD("TMRNG",
                                 (("I'll Keep This In Mind", TOP, "ok"),
                                  ("I Know. Stop Bothering Me.", TOP, "fine"),
                                  ("Skip This File", TOP, "skip")),
                                 "stop", "YB", "Do You Like Coffee?",
                                 "The .zip file\n\n%s\n\nis quite large "
                                 "(check the size in the files list). LOGPEEK "
                                 "is not very fast. This will take a while to "
                                 "process." % Filespec)
                if Answer == "stop":
                    setMsg("TMRNG", "", "Nothing done.")
                    progTMRNGControl("stopped")
                    return
                if Answer == "skip":
                    continue
                if Answer == "fine":
                    TMRNGWarnIfBigCVar.set(0)
                    WarnIfBig = 0
            Ret = rt130zcfTimeRange(Filespec, TMRNGStopBut, TMRNGRunning,
                                    "TMRNG")
        elif FilenameLC.find(".cf") != -1:
            Dir = dirname(Filespec)
            if WarnIfBig == 1 and getsize(Dir) > PROGBIG_UCF:
                Answer = formMYD("TMRNG",
                                 (("I'll Keep This In Mind", TOP, "ok"),
                                  ("I Know. Stop Bothering Me.", TOP, "fine"),
                                  ("Skip This File", TOP, "skip")),
                                 "stop", "YB", "Do You Like Coffee?",
                                 "The .cf directory\n\n%s\n\nis quite large "
                                 "(check the size in the files list). LOGPEEK "
                                 "is not very fast. This will take a while to "
                                 "process." % Dir)
                if Answer == "stop":
                    setMsg("TMRNG", "", "Nothing done.")
                    progTMRNGControl("stopped")
                    return
                if Answer == "skip":
                    continue
                if Answer == "fine":
                    TMRNGWarnIfBigCVar.set(0)
                    WarnIfBig = 0
            Ret = rt130cfTimeRange(Filespec, TMRNGStopBut, TMRNGRunning,
                                   "TMRNG")
        elif FilenameLC.endswith(".mslogs"):
            Size = getFolderSize(Filespec)
            if WarnIfBig == 1 and Size > PROGBIG_LOG:
                Answer = formMYD("TMRNG",
                                 (("I'll Keep This In Mind", TOP, "ok"),
                                  ("I Know. Stop Bothering Me.", TOP, "fine"),
                                  ("Skip This File", TOP, "skip")),
                                 "stop", "YB", "Do You Like Coffee?",
                                 "The .mslogs directory\n\n%s\n\nis quite "
                                 "large at ~%d lines. LOGPEEK is not very "
                                 "fast. This will take a while to process." %
                                 (Dir, int(Size / 48)))
                if Answer == "stop":
                    setMsg("TMRNG", "", "Nothing done.")
                    progTMRNGControl("stopped")
                    return
                if Answer == "skip":
                    continue
                if Answer == "fine":
                    TMRNGWarnIfBigCVar.set(0)
                    WarnIfBig = 0
            Ret = rt72130mslogsTimeRange(Filespec, TMRNGStopBut,
                                         TMRNGRunning)
# Something fatal this way comes (or we are just stopping).
        if Ret[0] == 2:
            setMsg("TMRNG", Ret)
            formTMRNGControl("stopped")
            return
# TMRNGData for good files will be:
#    [(0, Filename, DASID, [time, time,...], [("S/E", Time), \
#            ("S/E", Time...)]), ...]
# one Tuple for each file read.
# For files that have trouble the Tuple will be a standard error message:
#    [..., (1, Color, Message, Beep, <something>), ...]
# We'll keep good and bad files in here then do the right thing with each of
# them later.
        TMRNGData.append(Ret)
# I'm not sure how this could ever happen. The user must have selected
# something.
    if len(TMRNGData) == 0:
        setMsg("TMRNG", "YB", "No times found.", 2)
        formTMRNGControl("stopped")
        return
    if Plot == "all":
        # Determine the earliest and latest times and start plotting.
        SOHStart = maxInt
        SOHEnd = -maxInt
        Earliest = maxInt
        Latest = -maxInt
# Get the longest filename of all files and the time range from files that
# were good.
        LongestFileName = ""
        LongestFileNameLen = 0
        for Info in TMRNGData:
            if Info[TMRNG_CODE] == 0:
                Len = len(Info[TMRNG_FILE])
                if Len > LongestFileNameLen:
                    LongestFileName = Info[TMRNG_FILE]
                    LongestFileNameLen = Len
                SOHStart = min(Info[TMRNG_TIMES])
                SOHEnd = max(Info[TMRNG_TIMES])
                if SOHStart != 0 and SOHStart < Earliest:
                    Earliest = SOHStart
                if SOHEnd != 0 and SOHEnd > Latest:
                    Latest = SOHEnd
            else:
                Len = len(Info[TMRNG_OTHER])
                if Len > LongestFileNameLen:
                    LongestFileName = basename(Info[TMRNG_OTHER])
                    LongestFileNameLen = Len
# Now apply any user date limits. This doesn't apply for "each" plots.
        if Earliest < FromEpoch:
            Earliest = FromEpoch
# This may happen since the user may not have known what the time range of the
# files were when the date was entered.
        if FromEpoch > Latest:
            setMsg("TMRNG", "RW",
                   "The From date (%s) is later than the latest file date/"
                   "time (%s)." % (dt2Time(-1, 81, FromEpoch),
                                   dt2Time(-1, 81, Latest)), 2)
            formTMRNGControl("stopped")
            return
        if Latest > ToEpoch:
            Latest = ToEpoch
        if Earliest > Latest:
            setMsg("TMRNG", "RW",
                   "The earliest date/time (%s) is after the latest date/time "
                   "(%s)." % (dt2Time(-1, 81, Earliest),
                              dt2Time(-1, 81, Latest)))
            formTMRNGControl("stopped")
            return
        if Earliest == Latest:
            setMsg("TMRNG", "RW",
                   "The earliest date/time and the latest date/time are both "
                   "the same (%s)." % dt2Time(-1, 81, Earliest), 2)
            formTMRNGControl("stopped")
            return
# We still need to know the longest file name of all of the files.
    elif Plot == "each":
        # Get the longest filename and the time range from files that were
        # good.
        LongestFileName = ""
        LongestFileNameLen = 0
        for Info in TMRNGData:
            if Info[TMRNG_CODE] == 0:
                Len = len(Info[TMRNG_FILE])
                if Len > LongestFileNameLen:
                    LongestFileName = Info[TMRNG_FILE]
                    LongestFileNameLen = Len
            else:
                Len = len(Info[TMRNG_OTHER])
                if Len > LongestFileNameLen:
                    LongestFileName = basename(Info[TMRNG_OTHER])
                    LongestFileNameLen = Len
# Doing a .measure() on each file name and keeping the longest number in the
# above loops was taking a LONG time, so I changed to this slight convolution
# of just keeping the name with the most number of characters and then getting
# the .measure() length here. That might screw up the plotting at some point
# since the individual character widths are different, but it makes things way
# way faster.
    LongestFileNameLen = PROGPropFont.measure(LongestFileName) + 15
    setMsg("TMRNG", "CB", "Plotting...")
# Get the epoch of the earliest day at 00:00 and the epoch of the latest day
# at 23:59:59 for the PlotRange. This will be done for each file for the Plot
# each mode.
    if Plot == "all":
        TheEarliestTime = dt2Time(-1, 11, Earliest)
        TheLatestTime = dt2Time(-1, 11, Latest)
# We need to add one day to the end YYYY:DOY because the loop below will plot
# a FENCE POST at the last day plus 1 second (i.e. then next day).
        Parts2 = TheLatestTime.split(":", 3)
        Parts = []
        for Part in Parts2:
            Parts.append(intt(Part))
        YYYY, DOY = dt2TimeydoyMath(1, Parts[0], Parts[1])
        TheLatestNextTime = "%s:%03d" % (YYYY, DOY)
# Should be at 00h00m
        PlotEarliestEpoch = (Earliest // 86400.0) * 86400.0
# +1 day, -1 second.
        PlotLatestEpoch = (((Latest // 86400.0) + 1.0) * 86400.0) - 1.0
        PlotRange = PlotLatestEpoch - PlotEarliestEpoch
    elif Plot == "each":
        # Just create the variables.
        TheEarliestTime = ""
        PlotEarliestEpoch = 0.0
        TheLatestTime = ""
        PlotLatestEpoch = 0.0
        PlotRange = 0.0
    LFH2 = LFH / 2
    LFH4 = LFH / 4
    PlotML = LongestFileNameLen + 5
    PlotMR = PROGPropFont.measure(DateFormat) / 2 + 5
    PlotW = CWIDTH - PlotML - PlotMR
# Draw the day tick marks (the plot will always be at least 1 day long).
    if Plot == "all":
        Earliest = dt2Time(0, 81, TheEarliestTime)
        canText(LCan, PlotML, YTop, "B", Earliest, "center")
# Remember this so we can decide when it is safe to print the next date.
        DateEndX = CANTEXTLastX
# We'll use the width of the last date, eventhough we really want the width of
# the not yet plotted date. It should be close enough.
        DateWidth2 = CANTEXTLastWidth / 2
        LCan.create_line(PlotML, YTop + LFH - LFH2, PlotML, YTop + LFH + LFH2,
                         fill=Clr["B"])
        YTop += LFH
# +2 to get the next [part of] day to plot.
        LastX1 = PlotML
        for i in arange(int(PlotEarliestEpoch), int(PlotLatestEpoch + 2), 86400):  # noqa: E501
            X1 = PlotML + ((i - PlotEarliestEpoch) / PlotRange * PlotW)
# Draw the line then decide if it is OK to make a date.
            if X1 >= LastX1 + 50:
                LCan.create_line(X1, YTop - LFH2, X1,
                                 YTop + LFH2, fill=Clr["B"])
                LastX1 = X1
# +15 because we don't know how wide this date is going to be.
                if X1 > DateEndX + DateWidth2 + 15:
                    Current = dt2Time(-1, 81, i)
                    canText(LCan, X1, YTop - LFH, "B", Current, "center")
                    DateEndX = CANTEXTLastX
                    DateWidth2 = CANTEXTLastWidth / 2
        YTop += LFH * 2.0
    PlotCount = 0
    StationColor = "U"
    LastStation = ""
# Info = (0, Filename, DAS ID, [time, time,...], [(SE, Time), (SE, Time)...])
# or
# Info = (1, Color, Message, Beep, Filespec)
    for Info in TMRNGData:
        if Info[TMRNG_CODE] == 0:
            Start = min(Info[TMRNG_TIMES])
            End = max(Info[TMRNG_TIMES])
            if Plot == "all":
                # The file ends before the date limit.
                if End < FromEpoch:
                    canText(LCan, 5, YTop, "O",
                            "%s: File ends too early (%s)." %
                            (Info[TMRNG_FILE], dt2Time(-1, 80, End)[:-4]))
# File starts after the date limit.
                elif Start > ToEpoch:
                    canText(LCan, 5, YTop, "O",
                            "%s: File starts too late (%s)." %
                            (Info[TMRNG_FILE], dt2Time(-1, 80, Start)[:-4]))
                else:
                    # File starts before date limits.
                    if Start < FromEpoch:
                        Start = FromEpoch
                    elif End > ToEpoch:
                        End = ToEpoch
                    ID = canText(LCan, 5, YTop, "B", Info[TMRNG_FILE])
                    LCan.tag_bind(ID, "<Button-1>",
                                  Command(formTMRNGShowFlag,
                                          Info[TMRNG_FILE]))
                    if B2Glitch is True:
                        LCan.tag_bind(ID, "<Button-2>",
                                      Command(formTMRNGPopup,
                                              Info[TMRNG_FILE]))
                    LCan.tag_bind(ID, "<Button-3>", Command(formTMRNGPopup,
                                                            Info[TMRNG_FILE]))
                    LCan.tag_bind(ID, "<Enter>",
                                  Command(formTMRNGCursorControl, LCan,
                                          "right_ptr black white"))
                    LCan.tag_bind(ID, "<Leave>",
                                  Command(formTMRNGCursorControl, LCan, ""))
# Alternate plot colors as the station ID changes.
                    if Info[TMRNG_DASID] != LastStation:
                        if StationColor == "N":
                            StationColor = "U"
                        elif StationColor == "U":
                            StationColor = "N"
                        LastStation = Info[TMRNG_DASID]
# Make a 1 pixel wide line for each time found. It will look like a solid line
# most of the time, unless there is a large enough gap.
                    LastX1 = 0
                    for Time in Info[TMRNG_TIMES]:
                        if Time < PlotEarliestEpoch or Time > PlotLatestEpoch:
                            continue
                        X1 = PlotML + \
                            ((Time - PlotEarliestEpoch) / PlotRange * PlotW)
# Don't plot points on top of each other. It just wastes CPU cycles and could
# make things slow way down when there are a lot of DASs to plot. It might also
# make gaps show up better? There is also enough jumping back and forth in time
# between the SOH and EVT messages (like when all of the EVT messages are at
# the end of the SOH messages) that the line should get fully filled in when
# it should be.
                        if round(X1) == LastX1:
                            continue
                        LCan.create_line(X1, YTop - LFH4, X1, YTop + LFH4,
                                         fill=Clr[StationColor], width=1)
                        LastX1 = round(X1)
                    for Time in Info[TMRNG_SSTIMES]:
                        if Time[1] < PlotEarliestEpoch or \
                                Time[1] > PlotLatestEpoch:
                            continue
                        X1 = PlotML + ((Time[1] - PlotEarliestEpoch) / PlotRange * PlotW)  # noqa: E501
                        if Time[0] == "S":
                            LCan.create_line(X1, YTop - LFH4 - 2, X1,
                                             YTop + LFH4 + 2, fill=Clr["G"],
                                             width=1)
                        else:
                            LCan.create_line(X1, YTop - LFH4 - 2, X1,
                                             YTop + LFH4 + 2, fill=Clr["R"],
                                             width=1)
            elif Plot == "each":
                ID = canText(LCan, 5, YTop, "B", Info[TMRNG_FILE])
                LCan.tag_bind(ID, "<Button-1>",
                              Command(formTMRNGShowFlag, Info[TMRNG_FILE]))
                if B2Glitch is True:
                    LCan.tag_bind(ID, "<Button-2>", Command(formTMRNGPopup,
                                                            Info[TMRNG_FILE]))
                LCan.tag_bind(ID, "<Button-3>", Command(formTMRNGPopup,
                                                        Info[TMRNG_FILE]))
                LCan.tag_bind(ID, "<Enter>",
                              Command(formTMRNGCursorControl, LCan,
                                      "right_ptr black white"))
                LCan.tag_bind(ID, "<Leave>",
                              Command(formTMRNGCursorControl, LCan, ""))
# Alternate plot colors as the station ID changes.
                if Info[TMRNG_DASID] != LastStation:
                    if StationColor == "N":
                        StationColor = "U"
                    elif StationColor == "U":
                        StationColor = "N"
                    LastStation = Info[TMRNG_DASID]
# Make a 1 pixel wide line for each time found. It will look like a solid line
# most of the time, unless there is a large enough gap.
                LastX1 = 0
                PlotRange = End - Start
                for Time in Info[TMRNG_TIMES]:
                    X1 = PlotML + (float(Time - Start) / PlotRange * PlotW)
# Don't plot points on top of each other. It just wastes CPU cycles and could
# make things slow way down when there are a lot of DASs to plot. It might also
# make gaps show up better? There is also enough jumping back and forth in time
# between the SOH and EVT messages (like when all of the EVT messages are at
# the end of the SOH messages) that the line should get fully filled in when
# it should be.
                    if round(X1) == LastX1:
                        continue
                    LCan.create_line(X1, YTop - LFH4, X1, YTop + LFH4,
                                     fill=Clr[StationColor], width=1)
                    LastX1 = round(X1)
                for Time in Info[TMRNG_SSTIMES]:
                    X1 = PlotML + ((Time[1] - Start) / PlotRange * PlotW)
                    if Time[0] == "S":
                        LCan.create_line(X1, YTop - LFH4 - 2, X1,
                                         YTop + LFH4 + 2, fill=Clr["G"],
                                         width=1)
                    else:
                        LCan.create_line(X1, YTop - LFH4 - 2, X1,
                                         YTop + LFH4 + 2, fill=Clr["R"],
                                         width=1)
        else:
            C = Info[TMRNG_COLOR]
            if C[0] == "Y":
                C = "O"
            Msg = Info[TMRNG_MSG]
            canText(LCan, 5, YTop, C, Msg)
        YTop += LFH * 2.0
        PlotCount += 1
# Date tick marks scale every 10 plots for "all".
        if Plot == "all" and PlotCount % 10 == 0:
            Earliest = dt2Time(0, 81, TheEarliestTime)
            canText(LCan, PlotML, YTop, "B", Earliest, "center")
# Remember this so we can decide when it is safe to print the next date.
            DateEndX = CANTEXTLastX
# We'll use the width of the last date, eventhough we really want the width of
# the not yet plotted date. It should be close enough.
            DateWidth2 = CANTEXTLastWidth / 2
            LCan.create_line(PlotML, YTop + LFH - LFH2, PlotML,
                             YTop + LFH + LFH2, fill=Clr["B"])
            YTop += LFH
# +2 to get the next [part of] day to plot.
            LastX1 = PlotML
            for i in arange(int(PlotEarliestEpoch), int(PlotLatestEpoch + 2),
                            86400):
                X1 = PlotML + ((i - PlotEarliestEpoch) / PlotRange * PlotW)
# Draw the line then decide if it is OK to make a date.
                if X1 >= LastX1 + 50:
                    LCan.create_line(X1, YTop - LFH2, X1, YTop + LFH2,
                                     fill=Clr["B"])
                    LastX1 = X1
# +15 because we don't know how wide this date is going to be.
                    if X1 > DateEndX + DateWidth2 + 15:
                        Current = dt2Time(-1, 81, i)
                        canText(LCan, X1, YTop - LFH, "B", Current, "center")
                        DateEndX = CANTEXTLastX
                        DateWidth2 = CANTEXTLastWidth / 2
            YTop += LFH * 2.0
# Set the scrollregion to encompass all of the items created.
    LCan.update()
    LCan.configure(scrollregion=(0, 0, CWIDTH, (LCan.bbox(ALL)[3] + 5)))
    setMsg("TMRNG", "", "Done.")
    formTMRNGControl("stopped")
    return
##########################################
# BEGIN: formTMRNGShowFlag(File, e = None)
# FUNC:formTMRNGShowFlag():2013.275


def formTMRNGShowFlag(File, e=None):
    LCan = PROGCan["TMRNG"]
    Cx = LCan.canvasx(e.x)
    Cy = LCan.canvasy(e.y)
# If there already is a flag for this item then turn it off.
    LCan.delete("F" + File)
    Tag = "F" + File
# Make the text of the flag. Just brute force the way through the file info.
    for Info in TMRNGData:
        if Info[TMRNG_FILE].endswith(File):
            # The text has to be made before the box so we know how big to make
            # the box.
            MinTime = min(Info[TMRNG_TIMES])
            MaxTime = max(Info[TMRNG_TIMES])
            TLength = MaxTime - MinTime
            Message = " %s \n Start: %s \n" % (Info[TMRNG_FILE],
                                               dt2Time(-1, 80, MinTime)[:-4])
# Limit the number so the flags don't get huge.
            for SSTime in Info[TMRNG_SSTIMES][:15]:
                if SSTime[0] == "S":
                    Message += " AcqStart: %s \n" % dt2Time(-1, 80,
                                                            SSTime[1])[:-4]
                elif SSTime[0] == "E":
                    Message += " AcqStop: %s \n" % dt2Time(-1, 80,
                                                           SSTime[1])[:-4]
            if len(Info[TMRNG_SSTIMES]) > 15:
                Message += " ...%d start/stops \n" % len(Info[TMRNG_SSTIMES])
            Message += (" End: %s \n (%.2f days, %.2f hours) " %
                        (dt2Time(-1, 80, MaxTime)[:-4],
                         TLength / 86400.0, TLength / 3600.0))
            ID = LCan.create_text(Cx, Cy, text=Message,
                                  font=PROGPropFont, fill=Clr["B"],
                                  justify=CENTER, anchor="s", tags=Tag)
            break
# Check where the text is going to be and move it if it is going to be off the
# top or left edge of the canvas (it can't be off the other two).
    L, T, R, B = LCan.bbox(ID)
    Corr = "s"
    if T < 10:
        Corr = "n"
    if L < 10:
        Corr += "w"
# If it is other than the default move it and get the coordinates again for
# the box.
    if Corr != "s":
        LCan.itemconfigure(ID, anchor=Corr)
        L, T, R, B = LCan.bbox(ID)
# Naturally, the box needs to be ONE PIXEL taller to make it look right and one
# pixel longer to clear the ()'s.
    ID2 = LCan.create_rectangle((L, T - 1, R, B + 1), fill=Clr["E"],
                                outline=Clr["B"], tags=Tag)
# Since the text was made first we now have to raise it above the box.
    LCan.tag_raise(ID, ID2)
# The user might click on the text or the background so do both.
    LCan.tag_bind(ID, "<Button-1>", Command(formTMRNGFlagOff, Tag))
    LCan.tag_bind(ID2, "<Button-1>", Command(formTMRNGFlagOff, Tag))
    return
########################################
# BEGIN: formTMRNGFlagOff(Tag, e = None)
# FUNC:formTMRNGFlagOff():2012.103


def formTMRNGFlagOff(Tag, e=None):
    PROGCan["TMRNG"].delete(Tag)
    return
###########################################
# BEGIN: formTMRNGPopup(Filespec, e = None)
# FUNC:formTMRNGPopup():2014.077


def formTMRNGPopup(File, e=None):
    Xx = Root.winfo_pointerx()
    Yy = Root.winfo_pointery()
    PMenu = Menu(PROGCan["TMRNG"], font=PROGOrigPropFont, tearoff=0,
                 bg=Clr["D"], bd=2, relief=GROOVE)
    PMenu.add_command(label="Plot This File",
                      command=Command(formTMRNGPlotMain, File))
    PMenu.add_command(label="Rename This File",
                      command=Command(formTMRNGRename, File))
    PMenu.tk_popup(Xx, Yy)
    return
####################################
# BEGIN: formTMRNGPlotMain(Filespec)
# FUNC:formTMRNGPlotMain():2018.235
#   Lets the user click on a plotted file's name and gets that file read and
#   plotted in the main display.


def formTMRNGPlotMain(File):
    setMsg("TMRNG", "", "")
# The file has to appear in the current mail list of files.
    for Index in arange(0, MFFiles.size()):
        MFFile = MFFiles.get(Index)
        if MFFile.startswith(File):
            MFFiles.selection_clear(0, END)
            MFFiles.selection_set(Index)
            fileSelected()
            return
# The two file lists may be different.
    setMsg("TMRNG", "RW",
           "The file %s must be listed in the main files list for this to "
           "work. Check the List checkbuttons on the main form to see if this "
           "file's type is being listed and use the Reload button if "
           "necessary." % File, 2)
    return


##################################
# BEGIN: formTMRNGRename(Filespec)
# FUNC:formTMRNGRename():2016.194
#   Throws up a dialog box that can be used to change the name of a file.
#   Only allows the file to be renamed in the same directory.
PROGFrm["TMRNGREN"] = None
TMRNGRENOrigVar = StringVar()
TMRNGRENNewVar = StringVar()


def formTMRNGRename(Filespec):
    Parent = PROGFrm["TMRNG"]
# Our best guess.
    Dir = PROGDataDirVar.get()
# blah.cf/dasid -> We need the blah.cf part. Same for .zfc.
    if Filespec.find(sep) != -1 and (Filespec.upper().find(".CF") != -1 or
                                     Filespec.upper().find(".ZCF") != -1):
        File = dirname(Filespec)
    else:
        File = Filespec
    if exists(Dir + File) is False:
        formMYD(Parent, (("(OK)", TOP, "ok"),), "ok", "YB", "Huh?",
                "The file\n\n%s\n\ndoes not exist." % File)
        return
    TMRNGRENOrigVar.set(File)
    TMRNGRENNewVar.set(File)
    LFrm = PROGFrm["TMRNGREN"] = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "TMRNGREN"))
    LFrm.title("Rename File")
    LFrm.iconname("Rename")
    Sub = Frame(LFrm)
    Label(Sub, text="Current: ").pack(side=LEFT)
    Entry(Sub, textvariable=TMRNGRENOrigVar, width=35,
          state=DISABLED).pack(side=LEFT)
    Sub.pack(side=TOP, padx=3, pady=3)
    Sub = Frame(LFrm)
    Label(Sub, text="New: ").pack(side=LEFT)
    Entry(Sub, textvariable=TMRNGRENNewVar, width=35).pack(side=LEFT)
    Sub.pack(side=TOP, padx=3, pady=3)
    Sub = Frame(LFrm)
    BButton(Sub, text="Rename",
            command=formTMRNGRenameGo).pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    BButton(Sub, text="Cancel", fg=Clr["R"],
            command=Command(formClose, "TMRNGREN")).pack(side=LEFT)
    Sub.pack(side=TOP, pady=3)
    center("TMRNG", LFrm, "CX", "I", True)
    return
############################
# BEGIN: formTMRNGRenameGo()
# FUNC:formTMRNGRenameGo():2018.235


def formTMRNGRenameGo():
    Dir = PROGDataDirVar.get()
    Orig = TMRNGRENOrigVar.get().strip()
    New = TMRNGRENNewVar.get().strip()
    if Orig == New:
        setMsg("TMRNG", "RW",
               "The two file names are the same. Nothing done", 2)
    elif len(New) == 0:
        setMsg("TMRNG", "RW", "No new filename was entered. Nothing done.", 2)
# You never know.
    elif exists(Dir + Orig) is False:
        setMsg("TMRNG", "YB", "The file %s does not exist. Nothing done." %
               Orig, 2)
# I'm not replacing the OS. The user will have to handle this.
    elif exists(Dir + New):
        setMsg("TMRNG", "YB", "File %s already exists. Nothing done." % File,
               2)
    else:
        try:
            rename(Dir + Orig, Dir + New)
            setMsg("TMRNG", "GB",
                   "Renamed: %s -> %s\nRemember to reload all of the file "
                   "lists." % (Orig, New), 1)
        except Exception as e:
            setMsg("TMRNG", "MW", "Oh oh. %s" % e, 3)
    formClose("TMRNGREN")
    return
########################
# BEGIN: formTMRNGSort()
# FUNC:formTMRNGSort():2018.235
#   Sorts the entires in the Listbox alphbetically.


def formTMRNGSort():
    Items = {}
    for ind_ in arange(0, TMRNGFiles.size()):
        Item = TMRNGFiles.get(ind_)
        if len(Item) != 0:
            # Lower the keys so we get an alphbetical sort and not an ASCII
            # sort.
            Items[Item.lower()] = Item
    Keys = sorted(Items.keys())
    TMRNGFiles.delete(0, END)
    for Key in Keys:
        TMRNGFiles.insert(END, Items[Key])
    setMsg("TMRNG", "", "Items sorted: %d" % len(Keys))
    return
##########################################
# BEGIN: formTMRNGControl(Which, e = None)
# FUNC:formTMRNGControl():2012.103


def formTMRNGControl(Which, e=None):
    global TMRNGData
    if Which == "go":
        PROGFrm["TMRNG"].focus_set()
        buttonBG(TMRNGReadBut, "G")
        buttonBG(TMRNGStopBut, "R", NORMAL)
        TMRNGRunning.set(1)
        return (0, )
    elif Which == "stopping":
        buttonBG(TMRNGStopBut, "Y")
    elif Which == "stopped":
        buttonBG(TMRNGReadBut, "D")
        buttonBG(TMRNGStopBut, "D", DISABLED)
    elif Which == "close":
        if TMRNGRunning.get() == 1:
            beep(2)
            return (2, "YB",
                    "Plot Time Ranges Of Sources cannot be closed.", 2)
        del TMRNGData[:]
        formClose("TMRNG")
    TMRNGRunning.set(0)
    return (0, )
# END: formTMRNG


############################################################################
# BEGIN: formWrite(Parent, WhereMsg, TextWho, Title, FilespecVar, AllowPick,
#                Backup, Confirm, Clean)
# LIB:formWrite():2019.037
#   Writes the contents of the passed TextWho Text() widget to a file.
#   FilespecVar can be a StringVar or an astring.
#   If Backup is "" no backup will be made, but if it is not then the passed
#   string, like "-" or "~", will be appended to the original file name, the
#   original file will be renamed to that, and then the new stuff written to
#   the original file name.
#   If Confirm is True then the popup dialog question will be asked. If it is
#   False then no question will be asked, and the file will be overwritten to
#   the passed FilespecVar. A backup will be made depending on the value of
#   Backup.
#   Clean=True will go through each line and make sure it has ASCII characters.
#   Non-ASCII characters will be "?".
#   A List of text lines may be passed as TextWho and those will be written to
#   the file, insted of the contents of a Text().
FW_BACKUPCHAR = "-"


def formWrite(Parent, WhereMsg, TextWho, Title, FilespecVar, AllowPick,
              Backup, Confirm, Clean):
    # Sometimes it can take a while if there is a lot of text.
    updateMe(0)
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
# Can be StringVar or a string.
    if isinstance(FilespecVar, StringVar):
        InFile = FilespecVar.get()
    if isinstance(FilespecVar, astring):
        InFile = FilespecVar
# Set this in case we are skipping the confirmation stuff.
    Filespec = InFile
    if Confirm is True:
        if AllowPick is True:
            setMsg(WhereMsg, "", "")
            Filespec = formMYDF(Parent, 0, Title, dirname(InFile),
                                basename(InFile))
            if len(Filespec) == 0:
                return ""
    setMsg(WhereMsg, "CB", "Working...")
# Default to backing up and overwriting.
    Answer = "over"
    if Confirm is True and exists(Filespec):
        Answer = formMYD(Parent,
                         (("Append", LEFT, "append"),
                          ("Overwrite", LEFT, "over"),
                          ("Cancel", LEFT, "cancel")),
                         "cancel", "YB", "Keep Everything.",
                         "The file\n\n%s\n\nalready exists. Would you like to "
                         "append to that file, overwrite it, or cancel?" %
                         Filespec)
        if Answer == "cancel":
            setMsg(WhereMsg, "", "Nothing done.")
            return ""
    try:
        if Answer == "over":
            if len(Backup) != 0:
                # rename() on some systems, like Windows, get upset if the
                # backup file already exists. No one else seems to care.
                if exists(Filespec + Backup):
                    remove(Filespec + Backup)
                rename(Filespec, Filespec + Backup)
            Fp = open(Filespec, "w")
        elif Answer == "append":
            Fp = open(Filespec, "a")
    except Exception as e:
        setMsg(WhereMsg, "MW", "Error opening file (2)\n   %s\n   %s" %
               (Filespec, e), 3)
        return ""
# Now that the file is OK...
    if isinstance(FilespecVar, StringVar):
        FilespecVar.set(Filespec)
    if isinstance(TextWho, astring):
        LTxt = PROGTxt[TextWho]
        N = 1
# In case the text field is empty.
        Line = "\n"
        while True:
            # This little convolution keeps empty lines from piling up at the
            # end of the file.
            if len(LTxt.get("%d.0" % N)) == 0:
                if Line != "\n":
                    Fp.write(Line)
                break
            if N > 1:
                if Clean is True:
                    Line = line2ASCII(Line)
# The caller may not want the line cleaned, but that's doesn't 'make it so'.
                try:
                    Fp.write(Line)
                except UnicodeEncodeError:
                    setMsg(WhereMsg, "RW", "Unicode in line:\n%s" % Line, 2)
                    return
            Line = LTxt.get("%d.0" % N, "%d.0" % (N + 1))
            N += 1
    elif isinstance(TextWho, list):
        for Line in Lines:
            if Line.endswith("\n"):
                if Clean is True:
                    Line = line2ASCII(Line)
                try:
                    Fp.write(Line)
                except UnicodeEncodeError:
                    setMsg(WhereMsg, "RW", "Unicode in line:\n%s" % Line, 2)
                    return
            else:
                if Clean is True:
                    Line = line2ASCII(Line)
                try:
                    Fp.write(Line + "\n")
                except UnicodeEncodeError:
                    setMsg(WhereMsg, "RW", "Unicode in line:\n%s" % Line, 2)
                    return
    Fp.close()
    if Answer == "append":
        setMsg(WhereMsg, "GB", "Appended text to file\n   %s" % Filespec)
    elif Answer == "over":
        setMsg(WhereMsg, "GB", "Wrote text to file\n   %s" % Filespec)
# Return this in case the caller is interested.
    return Filespec
#################################################################
# BEGIN: formRead(Parent, TextWho, Title, FilespecVar, AllowPick)
# FUNC:formRead():2019.003
#   Same idea as formWrite(), but handles reading a file into a Text().
#   Only appends the text to the END of the passed Text() field.


def formRead(Parent, TextWho, Title, FilespecVar, AllowPick):
    # Sometimes it can take a while if there is a lot of text.
    updateMe(0)
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    Filespec = FilespecVar.get()
    if AllowPick is True:
        Filespec = formMYDF(Parent, 0, Title, dirname(Filespec),
                            basename(Filespec))
        if len(Filespec) == 0:
            return (2, "", "Nothing done.", 0, "")
    setMsg(WhereMsg, "CB", "Working...")
    if exists(Filespec) is False:
        return (2, "RW", "File to read does not exist:\n   %s" % Filespec, 2,
                Filespec)
    if isdir(Filespec):
        return (2, "RW", "Selected file is not a normal file:\n   %s" %
                Filespec, 2, Filespec)
# We don't know who saved the file, what the encoding is, what the line
# delimters are, so use this to get it split up. formRead() is generally just
# for ASCII text files, but you never know.
    Ret = readFileLinesRB(Filespec)
    if Ret[0] != 0:
        return Ret
    Lines = Ret[1]
# Now that the file is OK...
    LTxt = PROGTxt[TextWho]
    if int(LTxt.index('end-1c').split('.')[0]) > 1:
        LTxt.insert(END, "\n")
    for Line in Lines:
        LTxt.insert(END, "%s\n" % Line)
    FilespecVar.set(Filespec)
    if len(Lines) == 1:
        return (0, "", "Read %d line from file\n   %s" %
                (len(Lines), Filespec), 0, Filespec)
    else:
        return (0, "", "Read %d lines from file\n   %s" %
                (len(Lines), Filespec), 0, Filespec)
################################
# BEGIN: formWriteNoUni(TextWho)
# FUNC:formWriteNoUni():2019.037
#   A quick check before going through everything in formWrite().


def formWriteNoUni(TextWho):
    # Sometimes it can take a while if there is a lot of text.
    updateMe(0)
    if isinstance(TextWho, astring):
        LTxt = PROGTxt[TextWho]
    Lines = LTxt.get("0.0", END).split("\n")
    for Line in Lines:
        try:
            Line.encode("ascii")
        except UnicodeEncodeError:
            return (1, "RW", "Unicode in line:\n   %s" % Line, 2)
    return (0,)
# END: formWrite


#################################################
# BEGIN: formWritePS(Parent, VarSet, FilespecVar)
# LIB:formWritePS():2019.003
def formWritePS(Parent, VarSet, FilespecVar):
    setMsg(VarSet, "", "")
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    LCan = PROGCan[VarSet]
    Running = 0
# The form may not have an associated Running var.
    try:
        Running = eval("%sRunning.get()" % VarSet)
    except Exception:
        pass
    if Running != 0:
        setMsg(VarSet, "YB", "I'm busy...", 2)
        sleep(.5)
        return
# This may not make any difference depending on the form.
    try:
        if LCan.cget("bg") != "#FFFFFF":
            Answer = formMYD(Parent,
                             (("Stop And Let Me Redo It", TOP, "stop"),
                              ("Continue anyway", TOP, "cont")),
                             "stop", "YB", "Charcoal Footprint?",
                             "For best results the background color of the "
                             "plot area should be white. In fact, for most "
                             "plots the .ps file's background will be white, "
                             "and so will the text, so you won't be able to "
                             "see anything.  Do you want to stop and replot, "
                             "or continue anyway?")
        if Answer == "stop":
            return
    except Exception:
        pass
# This will cryptically crash if the canvas is empty.
    try:
        L, T, R, B = LCan.bbox(ALL)
# Postscript is so litteral.
        R += 15
        B += 15
    except Exception:
        setMsg(VarSet, "RW", "Is the area to write blank?", 2)
        return
    Dir = dirname(FilespecVar.get())
    File = basename(FilespecVar.get())
    Filespec = formMYDF(Parent, 3, ".ps File To Save To...", Dir, File, "",
                        ".ps", False)
    if len(Filespec) == 0:
        return False
    Answer = overwriteFile(Parent, Filespec)
    if Answer == "stop":
        return
    setMsg(VarSet, "CB", "Working on a %d x %d pixel area..." % (R, B))
# This might crash if the canvas is huge?
    try:
        Ps = LCan.postscript(height=B, width=R, pageheight=B,
                             pagewidth=R, pageanchor="nw", pagex=0, pagey=B,
                             x=0, y=0, colormode="color")
    except Exception as e:
        setMsg(VarSet, "MW", "Error converting canvas to postscript.\n   %s" %
               e, 3)
        return
    try:
        Fp = open(Filespec, "w")
        Fp.write(Ps)
        Fp.close()
    except Exception as e:
        setMsg(VarSet, "MW", "Error saving postscript commands.\n   %s" % e, 3)
        return
    setMsg(VarSet, "", "Wrote %d x %d area (%s bytes) to file\n   %s" %
           (R, B, fmti(len(Ps)), Filespec))
    FilespecVar.set(Filespec)
    return
# END: formWritePS


#################
# BEGIN: getCWD()
# LIB:getCWD():2008.209
def getCWD():
    CWD = getcwd()
    if CWD.endswith(sep) is False:
        CWD += sep
    return CWD
# END: getCWD


##############################
# BEGIN: getFolderSize(Folder)
# LIB:getFolderSize():2011.301
#   Stolen right from the Web from Samual Lampa, Dec 6, 2010.
def getFolderSize(Folder):
    # If a file is passed just return the size of it so the caller doesn't have
    # to keep checking.
    if isfile(Folder):
        return getsize(Folder)
    TotalSize = getsize(Folder)
    for Item in listdir(Folder):
        Itempath = Folder + sep + Item
        if isfile(Itempath):
            TotalSize += getsize(Itempath)
        elif isdir(Itempath):
            TotalSize += getFolderSize(Itempath)
    return TotalSize
# END: getFolderSize


#######################
# BEGIN: getGMT(Format)
# LIB:getGMT():2015.007
#   Gets the time in various forms from the system.
def getGMT(Format):
    # YYYY:DOY:HH:MM:SS (GMT)
    if Format == 0:
        return strftime("%Y:%j:%H:%M:%S", gmtime(time()))
# YYYYDOYHHMMSS (GMT)
    elif Format == 1:
        return strftime("%Y%j%H%M%S", gmtime(time()))
# YYYY-MM-DD (GMT)
    elif Format == 2:
        return strftime("%Y-%m-%d", gmtime(time()))
# YYYY-MM-DD HH:MM:SS (GMT)
    elif Format == 3:
        return strftime("%Y-%m-%d %H:%M:%S", gmtime(time()))
# YYYY, MM and DD (GMT) returned as ints
    elif Format == 4:
        GMT = gmtime(time())
        return (GMT[0], GMT[1], GMT[2])
# YYYY-Jan-01 (GMT)
    elif Format == 5:
        return strftime("%Y-%b-%d", gmtime(time()))
# YYYYMMDDHHMMSS (GMT)
    elif Format == 6:
        return strftime("%Y%m%d%H%M%S", gmtime(time()))
# Reftek Texan (year-1984) time stamp in BBBBBB format (GMT)
    elif Format == 7:
        GMT = gmtime(time())
        return pack(">BBBBBB", (GMT[0] - 1984), 0, 1, GMT[3], GMT[4], GMT[5])
# Number of seconds since Jan 1, 1970 from the system.
    elif Format == 8:
        return time()
# YYYY-MM-DD/DOY HH:MM:SS (GMT)
    elif Format == 9:
        return strftime("%Y-%m-%d/%j %H:%M:%S", gmtime(time()))
# YYYY-MM-DD/DOY (GMT)
    elif Format == 10:
        return strftime("%Y-%m-%d/%j", gmtime(time()))
# YYYY, DOY, HH, MM, SS (GMT) returned as ints
    elif Format == 11:
        GMT = gmtime(time())
        return (GMT[0], GMT[7], GMT[3], GMT[4], GMT[5])
# HH:MM:SS (GMT)
    elif Format == 12:
        return strftime("%H:%M:%S", gmtime(time()))
# YYYY:DOY:HH:MM:SS (LT)
    elif Format == 13:
        return strftime("%Y:%j:%H:%M:%S", localtime(time()))
# HHMMSS (GMT)
    elif Format == 14:
        return strftime("%H%M%S", gmtime(time()))
# YYYY-MM-DD (LT)
    elif Format == 15:
        return strftime("%Y-%m-%d", localtime(time()))
# YYYY-MM-DD/DOY Day (LT)
    elif Format == 16:
        return strftime("%Y-%m-%d/%j %A", localtime(time()))
# MM-DD (LT)
    elif Format == 17:
        return strftime("%m-%d", localtime(time()))
# YYYY, MM and DD (LT) returned as ints
    elif Format == 18:
        LT = localtime(time())
        return (LT[0], LT[1], LT[2])
# YYYY-MM-DD/DOY HH:MM:SS Day (LT)
    elif Format == 19:
        return strftime("%Y-%m-%d/%j %H:%M:%S %A", localtime(time()))
# Return GMT-LT difference.
    elif Format == 20:
        Secs = time()
        LT = localtime(Secs)
        GMT = gmtime(Secs)
        return dt2Timeymddhms(-1, LT[0], -1, -1, LT[7], LT[3], LT[4],
                              LT[5]) - dt2Timeymddhms(-1, GMT[0], -1, -1,
                                                      GMT[7], GMT[3], GMT[4],
                                                      GMT[5])
# YYYY-MM-DD/DOY HH:MM:SS (LT)
    elif Format == 21:
        return strftime("%Y-%m-%d/%j %H:%M:%S", localtime(time()))
# YYYY-MM-DD HH:MM:SS (LT)
    elif Format == 22:
        return strftime("%Y-%m-%d %H:%M:%S", localtime(time()))
    return ""
# END: getGMT


###########################
# BEGIN: getRange(Min, Max)
# LIB:getRange():2008.360
#   Returns the absolute value of the difference between Min and Max.
def getRange(Min, Max):
    if Min <= 0 and Max >= 0:
        return Max + abs(Min)
    elif Min <= 0 and Max <= 0:
        return abs(Min - Max)
    elif Max >= 0 and Min >= 0:
        return Max - Min
# END: getRange


#################
# BEGIN: InIntt()
# FUNC:InIntt():2008.360
#   intt() that just works on the InLine global, instead of passing big srtings
#   back and forth.
def InIntt():
    global InLine
    Number = ""
    for c in InLine.lstrip():
        if c.isdigit() or c == "-" or c == "+":
            Number += c
        elif c == ",":
            continue
        else:
            break
    try:
        return int(Number)
    except ValueError:
        return 0
# END: InIntt


#################
# BEGIN: intt(In)
# LIB:intt():2018.257logpeek
# FOR LOGPEEK: No .strip() check. Skips over " " in In.
#   This is a special version of intt() for the stuff we run into in LOGPEEK.
def intt(In):
    In = str(In)
    Number = ""
    for c in In:
        if c.isdigit():
            Number += c
        elif (c == "-" or c == "+") and len(Number) == 0:
            Number += c
        elif c == "," or c == " ":
            continue
        else:
            break
    try:
        return int(Number)
    except ValueError:
        return 0
# END: intt


###################
# BEGIN: isHex(Str)
# LIB:isHex():2009.026
def isHex(Hex):
    Hex = str(Hex).strip()
    for Char in Hex:
        if Char not in "0123456789abcdefABCDEF":
            return False
    return True
# END: isHex


############################
# BEGIN: isprintableInLine()
# FUNC:isprintableInLine():2018.238
#   I hate to do this, but pass all of the lines through here to make sure they
#   don't contain non-printable characters that could create havoc in many
#   places.
def isprintableInLine():
    global InLine
    ind_ = 0
    AllGood = True
    for C in InLine:
        if C < " " or C > "~":
            # Why arent strings mutable? Why aren't MutableStrings smarter?
            # I'm not changing the length of the line so the for-loop should
            # still be OK, right?
            InLine = InLine[:ind_] + "_" + InLine[ind_ + 1:]
            AllGood = False
        ind_ += 1
    return AllGood
# END: isprintableInLine


###################################################################
# BEGIN: labelEntry2(Sub, Format, LabTx, TTLen, TTTx, TxVar, Width)
# LIB:labelEntry2():2018.236
#   For making simple  Label(): Entry() pairs.
#   Format: 10 = Aligned LEFT-RIGHT, entry field is disabled
#           11 = Aligned LEFT-RIGHT, entry field is normal
#           20 = Aligned TOP-BOTTOM, entry field disabled
#           21 = Aligned TOP-BOTTOM, entry field is normal
#    LabTx = the text of the label. "" LabTx = no label
#    TTLen = The length of the tooltip text
#     TTTx = The text of the label's tooltip. "" TTTx = no tooltip
#    TxVar = Var for the entry field
#    Width = Width to make the field, 0 = .pack(side=LEFT, expand=YES, fill=X)
#            Width should only be 0 if Format is 10 or 11.
def labelEntry2(Sub, Format, LabTx, TTLen, TTTx, TxVar, Width):
    if len(LabTx) != 0:
        if len(TTTx) != 0:
            Lab = Label(Sub, text=LabTx)
            if Format == 11 or Format == 10:
                Lab.pack(side=LEFT)
            elif Format == 21 or Format == 20:
                Lab.pack(side=TOP)
            ToolTip(Lab, TTLen, TTTx)
        else:
            # Don't create the extra object if we don't have to.
            if Format == 11 or Format == 10:
                Label(Sub, text=LabTx).pack(side=LEFT)
            elif Format == 21 or Format == 20:
                Label(Sub, text=LabTx).pack(side=TOP)
    if Width == 0:
        Ent = Entry(Sub, textvariable=TxVar)
    else:
        Ent = Entry(Sub, textvariable=TxVar, width=Width + 1)
    if Format == 11 or Format == 10:
        if Width != 0:
            Ent.pack(side=LEFT)
        else:
            Ent.pack(side=LEFT, expand=YES, fill=X)
    elif Format == 21 or Format == 20:
        Ent.pack(side=TOP)
    if Format == 10 or Format == 20:
        Ent.configure(state=DISABLED, bg=Clr["D"])
    return Ent
# END: labelEntry2


####################################################
# BEGIN: labelTip(Sub, LText, Side, TTWidth, TTText)
# LIB:labelTip():2006.262
#   Creates a label and assignes the passed ToolTip to it. Returns the Label()
#   widget.
def labelTip(Sub, LText, Side, TTWidth, TTText):
    Lab = Label(Sub, text=LText)
    Lab.pack(side=Side)
    ToolTip(Lab, TTWidth, TTText)
    return Lab
# END: labelTip


###########################
# BEGIN: line2ASCII(InLine)
# LIB:line2ASCII():2019.014
#   Returns the passed line with anything not an ASCII character as a ?.
def line2ASCII(InLine):
    try:
        InLine.encode("ascii")
        return InLine
    except Exception:
        OutLine = ""
        for C in InLine:
            try:
                Value = ord(C)
                OutLine += C
            except Exception:
                OutLine += "?"
        return OutLine
# END: line2ASCII


########################################################################
# BEGIN: list2Str(TheList, Delim = ", ", Sort = True, DelBlanks = False)
# LIB:list2Str():2018.235
def list2Str(TheList, Delim=", ", Sort=True, DelBlanks=False):
    if isinstance(TheList, list) is False:
        return TheList
    if Sort is True:
        TheList.sort()
    Ret = ""
# If there is any funny-business (which has been seen).
    for Item in TheList:
        try:
            if DelBlanks is True:
                if len(str(Item)) == 0:
                    continue
            Ret += str(Item) + Delim
        except UnicodeEncodeError:
            Ret += "Error" + Delim
    return Ret[:-(len(Delim))]
# END: list2Str


##################################
# BEGIN: loadPROGSetupsLocal(Line)
# FUNC:loadPROGSetupsLocal():2018.240
def loadPROGSetupsLocal(Line):
    try:
        Parts2 = Line.split(";", 1)
        Parts = []
        for Part in Parts2:
            Parts.append(Part.strip())
        if Parts[0] == "DGrf":
            Parts3 = Parts[1].split(";")
            Parts2 = []
            for Part in Parts3:
                Parts2.append(Part.strip())
            DGrf[Parts2[0]].set(intt(Parts2[1]))
            return (1,)
    except Exception as e:
        # Return the same thing as the LIB version.
        return (5, "MW",
                "Error loading setups from\n   %s\n   %s\n   Was loading line "
                "%s" % (PROGSetupsFilespec, e, Line), 3, "")
    return (0,)
################################
# BEGIN: savePROGSetupsLocal(Fp)
# FUNC:savePROGSetupsLocal():2018.236


def savePROGSetupsLocal(Fp):
    try:
        for Item in list(DGrf.keys()):
            Fp.write("DGrf; %s; %d\n" %
                     (Item, eval("DGrf[\"%s\"]" % Item).get()))
    except Exception as e:
        # Return the same thing as the LIB version.
        return (2, "MW", "(LOC)Error saving setups to\n   %s\n   %s" %
                (PROGSetupsFilespec, e), 3, "")
    return (0,)
# END: loadPROGSetupsLocal


#####################################
# BEGIN: loadRTFiles(Filebox, VarSet)
# FUNC:loadRTFiles():2018.235
#   Looks through the directory specified in the PROGDataDirVar and creates a
#   list of the files that it comes across which it places in Filebox.
def loadRTFiles(Filebox, VarSet):
    CalcFileSizes = OPTCalcFileSizesCVar.get()
    Filebox.delete(0, END)
    setMsg(VarSet, "CB", "Working...")
    Dir = PROGDataDirVar.get()
# You never know about those users.
    if Dir.endswith(sep) is False:
        PROGDataDirVar.set(Dir + sep)
        Dir += sep
    SortBy = OPTSortFilesByRVar.get()
# Not all listboxes will have a find field.
    try:
        FileFind = eval("%sLbxFindVar" % VarSet).get().strip().upper()
    except Exception:
        FileFind = ""
    if exists(Dir) is False:
        setMsg(VarSet, "RW", "Entered data directory does not exist.", 2)
        return
# Either get the list of files in the directory or get the filename portion
# of the passed path/filename.
    if len(CLAFile) == 0:
        # The user needs to select somebody.
        if eval("%sShowLogsCVar" % VarSet).get() == 0 and \
                eval("%sShowRefsCVar" % VarSet).get() == 0 and \
                eval("%sShowUCFDCVar" % VarSet).get() == 0 and \
                eval("%sShowZCFCVar" % VarSet).get() == 0 and \
                eval("%sShowMSLOGSCVar" % VarSet).get() == 0:
            setMsg(VarSet, "RW",
                   "No file type checkboxes have been selected.", 2)
            return
        Files = sorted(listdir(Dir))
        LOGSFound = 0
        REFSFound = 0
        ZIPSFound = 0
        UCFDFound = 0
        MSLOGSFound = 0
        FilesAlpha = []
# Go through these this way so all of the .logs are listed together, all of the
# .refs are listed together, etc., unless SortBy is "alpha" then everything
# will get collected in FilesAlpha and get sorted at the end.
        if eval("%sShowLogsCVar" % VarSet).get() == 1:
            for File in Files:
                if File.startswith(".") or File.startswith("_"):
                    continue
                FileLC = File.lower()
                if FileLC.endswith(".log") or FileLC.endswith("_log"):
                    if CalcFileSizes == 0:
                        Filename = "%s" % File
                    else:
                        Size = getsize(Dir + File)
# 48 is kinda sorta the average number of characters per line in .log files.
# Kinda. I just read a ton of log files at some point and came up with it.
                        Filename = "%s (~%s lines)" % (File, fmti(Size / 48))
                    if SortBy == "type":
                        Filebox.insert(END, Filename)
                    elif SortBy == "alpha":
                        # .lower() version so "a" does not follow "Z".
                        FilesAlpha.append([Filename.lower(), Filename])
                    LOGSFound += 1
        if SortBy == "type" and LOGSFound > 0:
            Filebox.insert(END, "")
        if eval("%sShowRefsCVar" % VarSet).get() == 1:
            for File in Files:
                if File.startswith(".") or File.startswith("_"):
                    continue
                FileLC = File.lower()
                if FileLC.endswith(".ref"):
                    if CalcFileSizes == 0:
                        Filename = "%s" % File
                    else:
                        Size = getsize(Dir + File)
                        Filename = "%s (%s)" % (
                            File, diskSizeFormat("b", Size))
                    if SortBy == "type":
                        Filebox.insert(END, Filename)
                    elif SortBy == "alpha":
                        FilesAlpha.append([Filename.lower(), Filename])
                    REFSFound += 1
        if SortBy == "type" and REFSFound > 0:
            Filebox.insert(END, "")
        if eval("%sShowUCFDCVar" % VarSet).get() == 1:
            for File in Files:
                if File.startswith(".") or File.startswith("_"):
                    continue
                FileLC = File.lower()
# Get the DAS(s) in "File(s)" (it really must be a directory) and list them.
                if FileLC.endswith(".cf"):
                    if isdir(Dir + File):
                        DASs = rt130FindCFDASs(Dir + File)
                        if len(DASs) > 0:
                            UCFDFound += 1
                            for DAS in DASs:
                                # Combine the .cf directory and the DAS so we
                                # have a clue where to go looking for data
                                # when the user clicks.
                                if CalcFileSizes == 0:
                                    Filename = "%s%s%s" % (File, sep, DAS)
                                else:
                                    Size = getFolderSize(Dir + File)
                                    Filename = "%s%s%s (%s)" % (File, sep, DAS,
                                                                diskSizeFormat("b", Size))  # noqa: E501
                                if SortBy == "type":
                                    Filebox.insert(END, Filename)
                                elif SortBy == "alpha":
                                    FilesAlpha.append([Filename.lower(),
                                                       Filename])
# If the current CWDDir is a directory with only YYYYDOY sub-directories in it
# (plus a possible saved parameters .CFG file) then we must be "in" a CF card.
# Grab the DAS IDs and list them.
            InCF = True
            for File in Files:
                if File.startswith(".") or File.startswith("_") or \
                        File.endswith(".CFG"):
                    continue
                if isdir(Dir + File) is False or \
                        rtnPattern(File) != "0000000":
                    InCF = False
                    break
            if InCF is True:
                DASs = rt130FindCFDASs(Dir)
                for DAS in DASs:
                    # Just insert the DAS ID. When it is selected the lack of
                    # any extension will tell us what needs to be done.
                    if SortBy == "type":
                        Filebox.insert(END, DAS)
                    elif SortBy == "alpha":
                        FilesAlpha.append([DAS.lower(), DAS])
                    UCFDFound += 1
        if SortBy == "type" and UCFDFound > 0:
            Filebox.insert(END, "")
        if eval("%sShowZCFCVar" % VarSet).get() == 1:
            for File in Files:
                if File.startswith(".") or File.startswith("_"):
                    continue
                FileLC = File.lower()
                if FileLC.endswith(".zip"):
                    if CalcFileSizes == 0:
                        Filename = "%s" % File
                    else:
                        Size = getsize(Dir + File)
                        Filename = "%s (%s)" % (
                            File, diskSizeFormat("b", Size))
                    if SortBy == "type":
                        Filebox.insert(END, Filename)
                    elif SortBy == "alpha":
                        FilesAlpha.append([Filename.lower(), Filename])
                    ZIPSFound += 1
        if SortBy == "type" and ZIPSFound > 0:
            Filebox.insert(END, "")
        if eval("%sShowMSLOGSCVar" % VarSet).get() == 1:
            for File in Files:
                if File.startswith(".") or File.startswith("_"):
                    continue
                FileLC = File.lower()
                if FileLC.endswith(".mslogs"):
                    if CalcFileSizes == 0:
                        Filename = "%s" % File
                    else:
                        # 48, again, about the length of a log file line.
                        Size = getFolderSize(Dir + File) / 48
                        Filename = "%s (~%s lines)" % (File, fmti(Size))
                    if SortBy == "type":
                        Filebox.insert(END, Filename)
                    elif SortBy == "alpha":
                        FilesAlpha.append([Filename.lower(), Filename])
                    MSLOGSFound += 1
        if SortBy == "type" and MSLOGSFound > 0:
            Filebox.insert(END, "")
# Sort and insert what we have (if any).
        if SortBy == "alpha":
            FilesAlpha.sort()
            for Filename in FilesAlpha:
                Filebox.insert(END, Filename[1])
        if LOGSFound == 0 and REFSFound == 0 and ZIPSFound == 0 and \
                UCFDFound == 0 and MSLOGSFound == 0:
            setMsg(VarSet, "YB",
                   "No data source files found in\n   %s" % Dir)
        else:
            Message = "Found"
            if eval("%sShowLogsCVar" % VarSet).get() == 1:
                Message += "   .log files: %d" % LOGSFound
            if eval("%sShowRefsCVar" % VarSet).get() == 1:
                Message += "   .ref files: %d" % REFSFound
            if eval("%sShowUCFDCVar" % VarSet).get() == 1:
                Message += "   .cf DASs: %d" % UCFDFound
            if eval("%sShowZCFCVar" % VarSet).get() == 1:
                Message += "   .zip files: %d" % ZIPSFound
            if eval("%sShowMSLOGSCVar" % VarSet).get() == 1:
                Message += "   .mslogs folders: %d" % MSLOGSFound
            setMsg(VarSet, "WB", Message)
    else:
        File = basename(CLAFile)
        if CalcFileSizes == 0:
            Filebox.insert(END, File)
        else:
            Size = getsize(CLAFile)
            Filebox.insert(END, File + " (~%s lines)" % fmti((Size / 48)))
    if len(FileFind) != 0:
        Finds = []
        for Index in arange(0, Filebox.size()):
            File = Filebox.get(Index)
            if FileFind in File.upper():
                Finds.append(File)
        if len(Finds) > 0:
            Finds.sort()
            Finds.reverse()
            Filebox.insert(0, "")
            for Find in Finds:
                Filebox.insert(0, Find)
    return
##################################################
# BEGIN: loadRTFilesCmd(Filebox, VarSet, e = None)
# FUNC:loadRTFilesCmd():2018.345


def loadRTFilesCmd(Filebox, VarSet, e=None):
    # 2018-12-11 - I don't think it needs this.
    #    if VarSet == "MF":
    #        setMsg("INFO", "", "")
    setMsg(VarSet, "", "")
    loadRTFiles(Filebox, VarSet)
    return
############################################################
# BEGIN: loadRTFilesClearLbxFindVar(Filebox, VarSet, Reload)
# FUNC:loadRTFilesClearLbxFindVar()2012.093


def loadRTFilesClearLbxFindVar(Filebox, VarSet, Reload):
    eval("%sLbxFindVar" % VarSet).set("")
    if Reload is True:
        loadRTFiles(Filebox, VarSet)
    return
######################################
# BEGIN: loadRTFilesSelectAll(Filebox)
# FUNC:loadRTFilesSelectAll():2018.235


def loadRTFilesSelectAll(Filebox):
    Size = Filebox.size()
    NotSelected = 0
    for Index in arange(0, Size):
        if len(Filebox.get(Index)) == 0:
            continue
        if Filebox.selection_includes(Index) is False:
            NotSelected += 1
# If they are not all on turn them all on.
    if NotSelected > 0:
        for Index in arange(0, Size):
            if len(Filebox.get(Index)) == 0:
                Filebox.selection_clear(Index)
            else:
                Filebox.selection_set(Index)
# If they are all on turn them all off.
    elif NotSelected == 0:
        Filebox.selection_clear(0, END)
    return
# END: loadRTFiles


##############################
# BEGIN: nsew2pm(Which, Value)
# LIB:nsew2pm():2012.089
def nsew2pm(Which, Value):
    if Value == "?":
        return "?"
    if Which == "lat":
        try:
            if Value[0] == "N":
                return Value[1:]
            elif Value[0] == "S":
                return "-" + Value[1:]
# Allow positions that are already + or -.
            elif Value[0].isdigit() or Value[0] == "-" or Value[0] == "+":
                return Value
            else:
                return "?"
        except IndexError:
            return "?"
    elif Which == "lon":
        try:
            if Value[0] == "E":
                return Value[1:]
            elif Value[0] == "W":
                return "-" + Value[1:]
            elif Value[0].isdigit() or Value[0] == "-" or Value[0] == "+":
                return Value
            else:
                return "?"
        except IndexError:
            return "?"
###############################
# BEGIN: pm2nsew(Which, SValue)
# FUNC:pm2nsew():2012.089
#   A little trickier that originally suspected since Value can be a number or
#   a string, and if a string it may or may not have a leading + sign, and we
#   don't just want to convert it to a float and go from there because of
#   introducing binary conversion errors and extra digits, and we never want
#   the - sign.


def pm2nsew(Which, Value):
    if Value == "?":
        return "?"
    Value = str(Value)
    if Value.startswith("+"):
        Value = Value[1:]
    FValue = floatt(Value)
    if Which == "lat":
        # Just in case the caller passes the wrong thing.
        if Value.startswith("N") or Value.startswith("S"):
            return Value
        if FValue >= 0.0:
            return "N%09.6f" % FValue
        else:
            return "S%09.6f" % abs(FValue)
    elif Which == "lon":
        if Value.startswith("E") or Value.startswith("W"):
            return Value
        if FValue >= 0.0:
            return "E%010.6f" % FValue
        else:
            return "W%010.6f" % abs(FValue)
# END: nsew2pm


###########################
# BEGIN: nullCall(e = None)
# FUNC:nullCall():2013.164
def nullCall(e=None):
    return "break"
# END: nullCall


##########################
# BEGIN: openCWD(e = None)
# FUNC:openCWD():2014.323
def openCWD(e=None):
    Cmd = "open %s" % PROGDataDirVar.get()
    call(Cmd, shell=True)
    return
###############################
# BEGIN: openCWDPopup(e = None)
# FUNC:openCWDPopup():2013.282


def openCWDPopup(e=None):
    Xx = Root.winfo_pointerx()
    Yy = Root.winfo_pointery()
    PMenu = Menu(e.widget, font=PROGOrigPropFont, tearoff=0,
                 bg=Clr["D"], bd=2, relief=GROOVE)
    PMenu.add_command(label="Open This Directory", command=openCWD)
    PMenu.tk_popup(Xx, Yy)
    return
# END: openCWD


########################################
# BEGIN: overwriteFile(Parent, Filespec)
# LIB:overwriteFile():2018.234
#   Returns False if the file does not exist, or the Answer.
def overwriteFile(Parent, Filespec):
    if exists(Filespec):
        if isinstance(Parent, astring):
            Parent = PROGFrm[Parent]
        Answer = formMYD(Parent,
                         (("Overwrite", LEFT, "over"),
                          ("(Stop)", LEFT, "stop")),
                         "stop", "YB", "Well?",
                         "File %s already exists. Do you want to overwrite it "
                         "or stop?" % basename(Filespec))
        return Answer
    return False
# END: overwriteFile


###################################
# BEGIN: plotMF(WhereMsg, e = None)
# FUNC:plotMF():2019.023
def plotMF(WhereMsg, e=None):
    global MFPlotted
    global MFLead
    global CHeight
    global CWidth
    global CurTimerange
    global MFGW
    global MFTimeMode
    global TopSpace
    global BotSpace
    global MFTickY
    global TickX
    GFontH = PROGPropFont.metrics("linespace")
    GFontH12 = GFontH / 2.0
# This is faster than doing the .get() all the time.
    OPTIgTEC = OPTIgTECVar.get()
    ColorMode = PROGColorModeRVar.get()
# Collect the state of the data stream checkbuttons so we only plot the DSs
# that the user wants to see (the log line from binary data will only have the
# target event lines, but .log files may have more than the user has checked
# off). Also, if DS1 is selected, but there are no events for DS1 the line
# will still show up.
    DSsButts = []
    for i in arange(1, 1 + PROG_DSS):
        if eval("Stream%dVar" % i).get() == 1:
            DSsButts.append(i)
# Let the user know that there isn't going to be much going on on these forms
# if they are even up.
    if len(DSsButts) == 0:
        setMsg("RAW", "", "No DS (Data Stream) checkbuttons were selected.")
        setMsg("TPS", "", "No DS (Data Stream) checkbuttons were selected.")
# Each plot has a weight which determines how much screen space it will get.
# Those weights need to be added up before we start so we will know how many
# pixels each weight point will get. A GFontH height gap will also be added
# between each plot group, so keep track of how many of those there will be and
# include that adjustment.
    GrfWeights = 0.0
    GrfFGaps = 0
# If the checkbuttons are checked reserve space even if, for example, there is
# no DS 2 points to plot.
    if DGrf["EVT"].get() == 1:
        GrfWeights += (DPlotWts["EVT"] * len(DSsButts))
        GrfFGaps += 1
# If we are plotting errors and there is any err data then reserve space for
# it. This is a little funny, err's are channel-based, instead of data stream
# based, so we need to scan through them and figure out which channels we will
# be plotting.
    ErrChans = []
    if DGrf["ERR"].get() == 1 and len(TMJMP) > 0:
        for Jump in TMJMP:
            if Jump[TMJMP_CHAN + 1] not in ErrChans:
                ErrChans.append(Jump[TMJMP_CHAN + 1])
        ErrChans.sort()
        GrfWeights += (DPlotWts["ERR"] * len(ErrChans))
        GrfFGaps += 1
# Now go through the rest of the plot types.
    for Key in list(DPlotWts.keys()):
        # These were done above.
        if Key == "EVT" or Key == "ERR":
            continue
        if DGrf[Key].get() == 1:
            GrfWeights += DPlotWts[Key]
            GrfFGaps += 1
    if GrfWeights == 0.0:
        formMYD(Root, (("(Sigh)", LEFT, "ok"), ), "ok", "RW", "Picky Picky",
                "You need to select at least one item of interest to plot.",
                "", 1)
        return ""
    setMsg("MF", "CB", "Preparing to plot...")
# Get the size of the canvas.
    LCan = PROGCan["MF"]
    CHeight = LCan.winfo_height()
    CWidth = LCan.winfo_width()
# Get the time range for what we are about to plot for other stuff to use.
    CurTimerange = yRange(CurMainStart, CurMainEnd)
    Timerange = CurTimerange
    LRBorder = PROGPropFont.measure(" ")
    LRBorder2 = LRBorder * 2
# Go through this list of labels and figure out which one is the longest.
# There's probably a better way to do this, but since they are proprtional
# it's hard to know which is the longest. Make sure this list stays current.
# There are just the long-ish labels.
    MFLead = 0
    for Label in ("DSP-CLK Diff", "Jerks/DSP Sets", "GPS On/Off/Err",
                  "GPS Lk-Unlk", "Battery Volts", "Reset/Powerup",
                  "Error/Warning", "Discrepancies", "SOH/Data Defs",
                  "Disk 1 Usage"):
        if PROGPropFont.measure(Label) > MFLead:
            MFLead = PROGPropFont.measure(Label)
# Add a gap before the labels.
    MFLead += LRBorder
# Add a gap between the end of the label and the plot start.
    MFLead += LRBorder * 2.0
    GFontH = PROGPropFont.metrics("linespace")
# TopSpace - the amount of space above the plots for the clock display.
    TopSpace = GFontH * 2.0
# BotSpace - the amount of space below the plots for the time axis.
    BotSpace = GFontH * 2.0
# EndSpace - the amount of space to leave following the plots for the point
# counts.
    EndSpace = PROGPropFont.measure(" 000000 ")
# AGHeight - the amount of space the plots get. The -1 is because no gap will
# be "added" after the last plot, so might as well use the space.
    AGHeight = CHeight - TopSpace - BotSpace - ((GrfFGaps - 1) * GFontH)
# Calculate how much screen per weight unit the plots will get. Here's where
# we throw in the number of gaps*font height.
    PixPerWeight = AGHeight / GrfWeights
# The width of the canvas, minus the space for the labels, minus some space at
# the trailing end.
    MFGW = float(CWidth - MFLead - EndSpace - LRBorder)
    MFGW5 = MFGW * 5
# The first plot starts after leaving a bit of space at the top.
    Top = TopSpace
# === Clock difference messages ===
    if DGrf["DCDIFF"].get() == 1:
        setMsg("MF", "CB", "Plotting clock difference messages...")
        GTop = Top
        GHeight = PixPerWeight * DPlotWts["DCDIFF"]
        if len(DCDIFF) == 0:
            canText(LCan, LRBorder, GTop + GHeight / 2, DClr["Label"],
                    "DSP-CLK Diff: No messages found.")
        else:
            DCDIFFMin = maxFloat
            DCDIFFMax = -maxFloat
            for Data in DCDIFF:
                if Data[1] >= CurMainStart and Data[1] <= CurMainEnd:
                    if Data[2] < DCDIFFMin:
                        DCDIFFMin = Data[2]
                    if Data[2] > DCDIFFMax:
                        DCDIFFMax = Data[2]
                elif Data[1] > CurMainEnd:
                    break
# We only need to check one of them. This just makes the display nicer.
            if DCDIFFMin == maxFloat:
                DCDIFFMin = 0.0
                DCDIFFMax = 0.0
            LCan.create_line(MFLead, GTop, MFLead + MFGW, GTop,
                             fill=DClr["MaxMinL"])
            LCan.create_line(MFLead, GTop + GHeight, MFLead + MFGW,
                             GTop + GHeight, fill=DClr["MaxMinL"])
            canText(LCan, LRBorder, GTop + GHeight / 2, DClr["Label"],
                    "DSP-CLK Diff")
            YRange = yRange(DCDIFFMin, DCDIFFMax)
# Label the lines at the top and bottom with the max and min range
# of the data values.
            if abs(DCDIFFMax) > 1000.0:
                Label = str(DCDIFFMax / 1000.0) + "s"
            elif abs(DCDIFFMax) > 1.0:
                Label = str(DCDIFFMax) + "ms"
            else:
                Label = str(DCDIFFMax * 1000.0) + "us"
# "-5" just leaves a bit of space between the end of the number and the start
# of the line.
            Xx = MFLead - PROGPropFont.measure(Label) - 5
            canText(LCan, Xx, GTop, DClr["CanText"], Label)
            if abs(DCDIFFMin) > 1000.0:
                Label = str(DCDIFFMin / 1000.0) + "s"
            elif abs(DCDIFFMin) > 1.0:
                Label = str(DCDIFFMin) + "ms"
            else:
                Label = str(DCDIFFMin * 1000.0) + "us"
            Xx = MFLead - PROGPropFont.measure(Label) - 5
            canText(LCan, Xx, GTop + GHeight, DClr["CanText"], Label)
# Will stay 0 if there is nothing to plot in the current time range.
            Xx = 0
            Yy = 0
            LastData = []
# Draw the connecting line, then the points on top of it. Find the first point
# that is greater than the start time of the plot to use as a starting point.
            for Data in DCDIFF:
                if Data[1] >= CurMainStart:
                    LastX = MFLead + MFGW * \
                        (Data[1] - CurMainStart) / Timerange
                    LastY = GTop + (GHeight - GHeight *
                                    (Data[2] - DCDIFFMin) / YRange)
                    LastData = Data
                    break
# The lines between two dot positions are draw then the first dot is drawn to
# write over the line in the same loop (it used to be two -- it looked a bit
# better than this on the screen, but...).
            Plotted = 0
            for Data in DCDIFF:
                if Data[1] >= CurMainStart:
                    if Data[1] <= CurMainEnd:
                        Xx = MFLead + MFGW * \
                            (Data[1] - CurMainStart) / Timerange
                        Yy = GTop + (GHeight - GHeight *
                                     (Data[2] - DCDIFFMin) / YRange)
                        LCan.create_line(LastX, LastY, Xx, Yy,
                                         fill=DClr["DCDIFF"])
                        ID = LCan.create_rectangle((LastX - 1.5, LastY - 1.5,
                                                    LastX + 1.5, LastY + 1.5),
                                                   fill=DClr["DCDIFF"],
                                                   outline=DClr["DCDIFF"])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFPointClick, "DCDIFF",
                                              LastData[0]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                        if B2Glitch is True:
                            LCan.tag_bind(ID, "<Button-2>",
                                          Command(plotMFZapClick, "DCDIFF",
                                                  LastData[0]))
                        LCan.tag_bind(ID, "<Button-3>",
                                      Command(plotMFZapClick, "DCDIFF",
                                              LastData[0]))
                        LastX = Xx
                        LastY = Yy
                        LastData = Data
                        Plotted += 1
                    else:
                        # Breaking out at this point will speed things up a bit
                        # when zoomed in, but if we are ignoring timing errors
                        # then we still need to go through all of the data
                        # points.
                        if OPTIgTEC == 0:
                            break
# If we plotted anything this draws the plot's last dot. Plotted is not
# incremented by these sections.
            if Xx != 0 and Yy != 0:
                ID = LCan.create_rectangle((Xx - 1.5, Yy - 1.5, Xx + 1.5,
                                            Yy + 1.5),
                                           fill=DClr["DCDIFF"],
                                           outline=DClr["DCDIFF"])
                LCan.tag_bind(ID, "<Button-1>", Command(plotMFPointClick,
                                                        "DCDIFF", Data[0]))
                LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                if B2Glitch is True:
                    LCan.tag_bind(ID, "<Button-2>", Command(plotMFZapClick,
                                                            "DCDIFF",
                                                            LastData[0]))
                LCan.tag_bind(ID, "<Button-3>", Command(plotMFZapClick,
                                                        "DCDIFF", LastData[0]))
# Write the number of data points plotted at the end of the plot.
            canText(LCan, MFLead + MFGW + LRBorder2, GTop + GHeight / 2,
                    DClr["DCDIFF"], str(Plotted))
# Adjust this for the start of the next plot's area.
        Top += GHeight + GFontH
# Some plots can take a long time to plot, so check to see if the user is bored
# after each one.
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            return ""
# === Internal clock phase errors plot ===
    if DGrf["ICPE"].get() == 1:
        GTop = Top
        GHeight = PixPerWeight * DPlotWts["ICPE"]
        if len(ICPE) == 0:
            canText(LCan, LRBorder, GTop + GHeight / 2, DClr["Label"],
                    "Phase Error: No messages found.")
        else:
            setMsg("MF", "CB", "Plotting internal clock phase errors...")
            ICPEMin = maxFloat
            ICPEMax = -maxFloat
            Plotting = 0
            for Data in ICPE:
                if Data[1] >= CurMainStart and Data[1] <= CurMainEnd:
                    if Data[2] < ICPEMin:
                        ICPEMin = Data[2]
                    if Data[2] > ICPEMax:
                        ICPEMax = Data[2]
                    Plotting += 1
                elif Data[1] > CurMainEnd:
                    break
            if ICPEMin == maxFloat:
                ICPEMin = 0.0
                ICPEMax = 0.0
            LCan.create_line(MFLead, GTop, MFLead + MFGW, GTop,
                             fill=DClr["MaxMinL"])
            LCan.create_line(MFLead, GTop + GHeight, MFLead + MFGW,
                             GTop + GHeight, fill=DClr["MaxMinL"])
            canText(LCan, LRBorder, GTop + GHeight / 2, DClr["Label"],
                    "Phase Error")
            YRange = yRange(ICPEMin, ICPEMax)
# Add labels to the lines with the max and min range.
            if abs(ICPEMax) > 1000.0:
                Label = str(ICPEMax / 1000.0) + "s"
            elif abs(ICPEMax) > 1.0:
                Label = str(ICPEMax) + "ms"
            else:
                Label = str(ICPEMax * 1000.0) + "us"
# "-5" just leaves a bit of space between the end of the number and the start
# of the line.
            Xx = MFLead - PROGPropFont.measure(Label) - 5
            canText(LCan, Xx, GTop, DClr["CanText"], Label)
            if abs(ICPEMin) > 1000.0:
                Label = str(ICPEMin / 1000.0) + "s"
            elif abs(ICPEMin) > 1.0:
                Label = str(ICPEMin) + "ms"
            else:
                Label = str(ICPEMin * 1000.0) + "us"
            Xx = MFLead - PROGPropFont.measure(Label) - 5
            canText(LCan, Xx, GTop + GHeight, DClr["CanText"], Label)
# Will stay 0 if there is nothing to plot.
            Xx = 0
            Yy = 0
            LastData = []
# Go through the points and find the X,Y of the first one that we will plot.
            for Data in ICPE:
                if Data[1] >= CurMainStart:
                    LastX = MFLead + MFGW * \
                        (Data[1] - CurMainStart) / Timerange
                    LastY = GTop + (GHeight - GHeight *
                                    (Data[2] - ICPEMin) / YRange)
                    LastData = Data
                    break
            Plotted = 0
# Either create the line, then dot, then bothe binds, or just make the line
# segment and do the one bind to it.
# If there are A LOT of points this will pay off by not having to do this if()
# 1000's of times.
            if Plotting < MFGW5:
                for Data in ICPE:
                    if Data[1] >= CurMainStart:
                        if Data[1] <= CurMainEnd:
                            Xx = MFLead + MFGW * \
                                (Data[1] - CurMainStart) / Timerange
                            Yy = GTop + (GHeight - GHeight *
                                         (Data[2] - ICPEMin) / YRange)
                            LCan.create_line(LastX, LastY, Xx, Yy,
                                             fill=DClr["ICPE"])
                            ID = LCan.create_rectangle((LastX - 1.5,
                                                        LastY - 1.5,
                                                        LastX + 1.5,
                                                        LastY + 1.5),
                                                       fill=DClr["ICPE"],
                                                       outline=DClr["ICPE"])
                            LCan.tag_bind(ID, "<Button-1>",
                                          Command(plotMFPointClick, "ICPE",
                                                  LastData[0]))
                            LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                            if B2Glitch is True:
                                LCan.tag_bind(ID, "<Button-2>",
                                              Command(plotMFZapClick, "ICPE",
                                                      LastData[0]))
                            LCan.tag_bind(ID, "<Button-3>",
                                          Command(plotMFZapClick, "ICPE",
                                                  LastData[0]))
                            LastX = Xx
                            LastY = Yy
                            LastData = Data
                            Plotted += 1
# The last data point.
                if Xx != 0 and Yy != 0:
                    ID = LCan.create_rectangle((Xx - 1.5, Yy - 1.5, Xx + 1.5,
                                                Yy + 1.5), fill=DClr["ICPE"],
                                               outline=DClr["ICPE"])
                    LCan.tag_bind(ID, "<Button-1>", Command(plotMFPointClick,
                                                            "ICPE", Data[0]))
                    LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                    if B2Glitch is True:
                        LCan.tag_bind(ID, "<Button-2>",
                                      Command(plotMFZapClick, "ICPE", Data[0]))
                    LCan.tag_bind(ID, "<Button-3>", Command(plotMFZapClick,
                                                            "ICPE", Data[0]))
            else:
                for Data in ICPE:
                    if Data[1] >= CurMainStart:
                        if Data[1] <= CurMainEnd:
                            Xx = MFLead + MFGW * \
                                (Data[1] - CurMainStart) / Timerange
                            Yy = GTop + (GHeight - GHeight *
                                         (Data[2] - ICPEMin) / YRange)
# We can't just collect all of the Xx's and Yy's and do one create_line(),
# because we still want to be able to click on the line without dots and get
# to the general area in the log lines.
                            ID = LCan.create_line(LastX, LastY, Xx, Yy,
                                                  fill=DClr["ICPE"])
                            LCan.tag_bind(ID, "<Button-1>",
                                          Command(plotMFPointClick, "ICPE",
                                                  LastData[0]))
                            LastX = Xx
                            LastY = Yy
                            LastData = Data
                            Plotted += 1
                    else:
                        if OPTIgTEC == 0:
                            break
            canText(LCan, MFLead + MFGW + LRBorder2, GTop + GHeight / 2,
                    DClr["ICPE"], str(Plotted))
        Top += GHeight + GFontH
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            return ""
# === Time Jerks and DSP resets(72's) or DSP sets(130's) ===
    if DGrf["JERK"].get() == 1:
        setMsg("MF", "CB", "Plotting clock related messages...")
        GTop = Top + PixPerWeight * DPlotWts["JERK"] / 2.0
        LCan.create_line(MFLead, GTop, MFLead + MFGW, GTop,
                         fill=DClr["MaxMinL"])
        canText(LCan, LRBorder, GTop, DClr["Label"], "Jerks/DSP Sets")
        PlottedJ = 0
        PlottedD = 0
        for Data in JERK:
            if Data[1] >= CurMainStart:
                if Data[1] <= CurMainEnd:
                    Xx = MFLead + MFGW * (Data[1] - CurMainStart) / Timerange
                    if Data[2] == 0:
                        ID = LCan.create_rectangle((Xx - 1.5, GTop - 4.0,
                                                    Xx + 1.5, GTop - 1),
                                                   fill=DClr["JERK"],
                                                   outline=DClr["JERK"])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFPointClick, "JERK",
                                              Data[0]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                        PlottedJ += 1
                    else:
                        ID = LCan.create_rectangle((Xx - 1.5, GTop + 1,
                                                    Xx + 1.5, GTop + 4.0),
                                                   fill=DClr["DRSET"],
                                                   outline=DClr["DRSET"])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFPointClick, "DRSET",
                                              Data[0]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                        PlottedD += 1
                else:
                    if OPTIgTEC == 0:
                        break
        canText(LCan, MFLead + MFGW + LRBorder2, GTop - GFontH12, DClr["JERK"],
                str(PlottedJ))
        canText(LCan, MFLead + MFGW + LRBorder2, GTop + GFontH12,
                DClr["DRSET"], str(PlottedD))
        Top += PixPerWeight * DPlotWts["JERK"] + GFontH
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            return ""
# === .err file plots ===
    if DGrf["ERR"].get() == 1 and len(TMJMP) > 0:
        setMsg("MF", "CB", "Plotting err file data...")
# Will be 'DSx:Y'.
        GTops = {}
# Will be DS:number of points plotted.
        PlottedCount = {}
        Count = 0
# Set the GTops values, draw the lines and labels and get ready for the next
# loop to plot.
        for Chan in ErrChans:
            GTops[Chan] = Top + (PixPerWeight * DPlotWts["ERR"] * Count)
            LCan.create_line(MFLead, GTops[Chan], MFLead + MFGW, GTops[Chan],
                             fill=DClr["MaxMinL"])
            canText(LCan, LRBorder, GTops[Chan], DClr["Label"],
                    "ERR CH%d" % Chan)
            PlottedCount[Chan] = 0
            Count += 1
        for Data in TMJMP:
            # There may have been log lines for data streams that the user
            # does not want plotted.
            Chan = Data[3]
            if Data[1] >= CurMainStart and Data[1] <= CurMainEnd and \
                    Data[2] >= CurMainStart and Data[2] <= CurMainEnd:
                X1 = MFLead + MFGW * (Data[1] - CurMainStart) / Timerange
                X2 = MFLead + MFGW * (Data[2] - CurMainStart) / Timerange
                if X1 < X2:
                    ID = LCan.create_rectangle((X1, GTops[Chan] - 4.0, X2,
                                                GTops[Chan] - 1.0),
                                               fill=DClr["TMJMPPos"],
                                               outline=DClr["TMJMPPos"])
                    LCan.tag_bind(ID, "<Button-1>",
                                  Command(plotMFPointClick, "ERRP", Data[0]))
                    LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                else:
                    ID = LCan.create_rectangle((X1, GTops[Chan] + 1.0, X2,
                                                GTops[Chan] + 4.0),
                                               fill=DClr["TMJMPNeg"],
                                               outline=DClr["TMJMPNeg"])
                    LCan.tag_bind(ID, "<Button-1>",
                                  Command(plotMFPointClick, "ERRN", Data[0]))
                    LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                PlottedCount[Chan] += 1
        for Chan in ErrChans:
            canText(LCan, MFLead + MFGW + LRBorder2, GTops[Chan],
                    DClr["Label"], str(PlottedCount[Chan]))
        Top += PixPerWeight * DPlotWts["ERR"] * len(ErrChans) + GFontH
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            return ""
# === GPS on/off/error ===
    if DGrf["GPSON"].get() == 1:
        setMsg("MF", "CB", "Plotting GPS power/status messages...")
        GTop1 = Top
        GTop0 = Top + PixPerWeight * DPlotWts["GPSON"] / 2.0
        GTopE = Top + PixPerWeight * DPlotWts["GPSON"]
# On
        LCan.create_line(MFLead, GTop1, MFLead + MFGW, GTop1,
                         fill=DClr["MaxMinL"])
# Off
        LCan.create_line(MFLead, GTop0, MFLead + MFGW, GTop0,
                         fill=DClr["MaxMinL"])
# Err
        LCan.create_line(MFLead, GTopE, MFLead + MFGW, GTopE,
                         fill=DClr["MaxMinL"])
        canText(LCan, LRBorder, GTop1, DClr["Label"], "GPS On/Off/Err")
        Plotted0 = 0
        Plotted1 = 0
        for Data in GPS:
            if Data[1] >= CurMainStart:
                if Data[1] <= CurMainEnd:
                    Xx = MFLead + MFGW * (Data[1] - CurMainStart) / Timerange
                    if Data[2] == 1:
                        ID = LCan.create_rectangle((Xx - 1.5, GTop1 - 1.5,
                                                    Xx + 1.5, GTop1 + 1.5),
                                                   fill=DClr["GPSOn"],
                                                   outline=DClr["GPSOn"])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFPointClick, "GPS1",
                                              Data[0]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                        Plotted1 += 1
                    else:
                        ID = LCan.create_rectangle((Xx - 1.5, GTop0 - 1.5,
                                                    Xx + 1.5, GTop0 + 1.5),
                                                   fill=DClr["GPSOff"],
                                                   outline=DClr["GPSOff"])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFPointClick, "GPS0",
                                              Data[0]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                        Plotted0 += 1
                else:
                    if OPTIgTEC == 0:
                        break
        canText(LCan, MFLead + MFGW + LRBorder2, GTop1, DClr["GPSOn"],
                str(Plotted1))
        canText(LCan, MFLead + MFGW + LRBorder2, GTop0, DClr["GPSOff"],
                str(Plotted0))
        Plotted = 0
        for Data in GPSERR:
            if Data[1] >= CurMainStart:
                if Data[1] <= CurMainEnd:
                    Xx = MFLead + MFGW * (Data[1] - CurMainStart) / Timerange
                    ID = LCan.create_rectangle((Xx - 1.5, GTopE - 1.5,
                                                Xx + 1.5, GTopE + 1.5),
                                               fill=DClr["GPSErr"],
                                               outline=DClr["GPSErr"])
                    LCan.tag_bind(ID, "<Button-1>", Command(plotMFPointClick,
                                                            "GPSE", Data[0]))
                    LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                    Plotted += 1
                else:
                    if OPTIgTEC == 0:
                        break
        canText(LCan, MFLead + MFGW + LRBorder2, GTopE, DClr["GPSErr"],
                str(Plotted))
        Top += PixPerWeight * DPlotWts["GPSON"] + GFontH
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            return ""
# === External clock locked/unlocked, power state ===
    if DGrf["GPSLK"].get() == 1:
        setMsg("MF", "CB", "Plotting GPS lock messages...")
        GTopL = Top
        GTopS = Top + PixPerWeight * DPlotWts["GPSLK"] / 2.0
        GTopU = Top + PixPerWeight * DPlotWts["GPSLK"]
        if len(GPSLK) == 0:
            canText(LCan, LRBorder, GTopS, DClr["Label"],
                    "GPS Lk-Unlk: No GPS status messages found.")
        else:
            LCan.create_line(MFLead, GTopL, MFLead + MFGW, GTopL,
                             fill=DClr["MaxMinL"])
            LCan.create_line(MFLead, GTopU, MFLead + MFGW, GTopU,
                             fill=DClr["MaxMinL"])
            canText(LCan, LRBorder, GTopS, DClr["Label"], "GPS Lk-Unlk")
            LastX = 0
            LastY = 0
            LastData = []
            for Data in GPSLK:
                if Data[1] >= CurMainStart:
                    if Data[2] == 0:
                        LastY = GTopU
                    else:
                        LastY = GTopL
                    LastX = MFLead + MFGW * \
                        (Data[1] - CurMainStart) / Timerange
                    LastData = Data
                    break
            Plotted1 = 0
            Plotted0 = 0
            for Data in GPSLK:
                if Data[1] >= CurMainStart:
                    if Data[1] <= CurMainEnd:
                        Xx = MFLead + MFGW * \
                            (Data[1] - CurMainStart) / Timerange
                        if Data[2] == 0:
                            LCan.create_line(LastX, LastY, Xx, GTopU,
                                             fill=DClr["GPSLK"])
                            ID = LCan.create_rectangle((LastX - 1.5,
                                                        LastY - 1.5,
                                                        LastX + 1.5,
                                                        LastY + 1.5),
                                                       fill=DClr["GPSLK"],
                                                       outline=DClr["GPSLK"])
# GPSL is used for both since they are the same color,
                            LCan.tag_bind(ID, "<Button-1>",
                                          Command(plotMFPointClick, "GPSL",
                                                  LastData[0]))
                            LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                            LastY = GTopU
                            Plotted0 += 1
                        else:
                            LCan.create_line(LastX, LastY, Xx, GTopL,
                                             fill=DClr["GPSLK"])
                            ID = LCan.create_rectangle((LastX - 1.5,
                                                        LastY - 1.5,
                                                        LastX + 1.5,
                                                        LastY + 1.5),
                                                       fill=DClr["GPSLK"],
                                                       outline=DClr["GPSLK"])
                            LCan.tag_bind(ID, "<Button-1>",
                                          Command(plotMFPointClick, "GPSL",
                                                  LastData[0]))
                            LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                            LastY = GTopL
                            Plotted1 += 1
                        LastX = Xx
                        LastData = Data
                    else:
                        if OPTIgTEC == 0:
                            break
            if LastX != 0 and LastY != 0:
                ID = LCan.create_rectangle((Xx - 1.5, LastY - 1.5, Xx + 1.5,
                                            LastY + 1.5), fill=DClr["GPSLK"],
                                           outline=DClr["GPSLK"])
                LCan.tag_bind(ID, "<Button-1>", Command(plotMFPointClick,
                                                        "GPSL", LastData[0]))
                LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
            canText(LCan, MFLead + MFGW + LRBorder2, GTopL, DClr["GPSLK"],
                    str(Plotted1))
            canText(LCan, MFLead + MFGW + LRBorder2, GTopU, DClr["GPSLK"],
                    str(Plotted0))
            Plotted = 0
            for Data in CLKPWR:
                if Data[1] >= CurMainStart:
                    if Data[1] <= CurMainEnd:
                        Xx = MFLead + \
                            (MFGW * (Data[1] - CurMainStart) / Timerange)
                        ID = LCan.create_rectangle((Xx - 1.5, GTopS - 1.5,
                                                    Xx + 1.5, GTopS + 1.5),
                                                   fill=DClr["CLKPWR"],
                                                   outline=DClr["CLKPWR"])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFPointClick, "CLKP",
                                              Data[0]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                        Plotted += 1
                    else:
                        if OPTIgTEC == 0:
                            break
            canText(LCan, MFLead + MFGW + LRBorder2, GTopS, DClr["CLKPWR"],
                    str(Plotted))
        Top += PixPerWeight * DPlotWts["GPSLK"] + GFontH
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            return ""
# === Temperature ===
    if DGrf["TEMP"].get() == 1:
        setMsg("MF", "CB", "Plotting temperature...")
        GTop = Top
        GHeight = PixPerWeight * DPlotWts["TEMP"]
        if len(TEMP) == 0:
            canText(LCan, LRBorder, GTop + GHeight / 2, DClr["Label"],
                    "DAS Temp: No temperature messages found.")
        else:
            TEMPMin = maxFloat
            TEMPMax = -maxFloat
            for Data in TEMP:
                if Data[1] >= CurMainStart and Data[1] <= CurMainEnd:
                    if Data[2] < TEMPMin:
                        TEMPMin = Data[2]
                    if Data[2] > TEMPMax:
                        TEMPMax = Data[2]
                elif Data[1] > CurMainEnd:
                    break
            if TEMPMin == maxFloat:
                TEMPMin = 0.0
                TEMPMax = 0.0
            LCan.create_line(MFLead, GTop, MFLead + MFGW, GTop,
                             fill=DClr["MaxMinL"])
            LCan.create_line(MFLead, GTop + GHeight, MFLead + MFGW,
                             GTop + GHeight, fill=DClr["MaxMinL"])
            canText(LCan, LRBorder, GTop + GHeight / 2, DClr["Label"],
                    "DAS Temp")
            Label = str(TEMPMax) + "C"
# "-5" just leaves a bit of space between the end of the number and the start
# of the line.
            Xx = MFLead - PROGPropFont.measure(Label) - 5
            canText(LCan, Xx, GTop, DClr["CanText"], Label)
            Label = str(TEMPMin) + "C"
            Xx = MFLead - PROGPropFont.measure(Label) - 5
            canText(LCan, Xx, GTop + GHeight, DClr["CanText"], Label)
            YRange = yRange(TEMPMin, TEMPMax)
            Xx = 0
            Yy = 0
            LastData = []
# Draw the connecting line, then the points on top of it.
            for Data in TEMP:
                if Data[1] >= CurMainStart:
                    LastX = MFLead + MFGW * \
                        (Data[1] - CurMainStart) / Timerange
                    LastY = GTop + (GHeight - GHeight *
                                    (Data[2] - TEMPMin) / YRange)
                    LastData = Data
                    break
            Plotted = 0
            for Data in TEMP:
                if Data[1] >= CurMainStart:
                    if Data[1] <= CurMainEnd:
                        Xx = MFLead + MFGW * \
                            (Data[1] - CurMainStart) / Timerange
                        Yy = GTop + (GHeight - GHeight *
                                     (Data[2] - TEMPMin) / YRange)
                        LCan.create_line(LastX, LastY, Xx, Yy,
                                         fill=DClr["TEMP"])
                        ID = LCan.create_rectangle((LastX - 1.5, LastY - 1.5,
                                                    LastX + 1.5, LastY + 1.5),
                                                   fill=DClr["TEMP"],
                                                   outline=DClr["TEMP"])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFPointClick, "TEMP",
                                              LastData[0]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                        if B2Glitch is True:
                            LCan.tag_bind(ID, "<Button-2>",
                                          Command(plotMFZapClick, "TEMP",
                                                  LastData[0]))
                        LCan.tag_bind(ID, "<Button-3>",
                                      Command(plotMFZapClick, "TEMP",
                                              LastData[0]))
                        LastX = Xx
                        LastY = Yy
                        LastData = Data
                        Plotted += 1
                    else:
                        if OPTIgTEC == 0:
                            break
            if Xx != 0 and Yy != 0:
                ID = LCan.create_rectangle((Xx - 1.5, Yy - 1.5, Xx + 1.5,
                                            Yy + 1.5), fill=DClr["TEMP"],
                                           outline=DClr["TEMP"])
                LCan.tag_bind(ID, "<Button-1>", Command(plotMFPointClick,
                                                        "TEMP", Data[0]))
                LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                if B2Glitch is True:
                    LCan.tag_bind(ID, "<Button-2>", Command(plotMFZapClick,
                                                            "TEMP", Data[0]))
                LCan.tag_bind(ID, "<Button-3>", Command(plotMFZapClick,
                                                        "TEMP", Data[0]))
            canText(LCan, MFLead + MFGW + LRBorder2, GTop + GHeight / 2,
                    DClr["TEMP"], str(Plotted))
        Top += PixPerWeight * DPlotWts["TEMP"] + GFontH
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            return ""
# === Battery voltages ===
    if DGrf["VOLT"].get() == 1:
        setMsg("MF", "CB", "Plotting battery voltages...")
        GTop = Top
        GHeight = PixPerWeight * DPlotWts["VOLT"]
        if len(VOLT) == 0:
            canText(LCan, LRBorder, GTop + GHeight / 2, DClr["Label"],
                    "Battery Volts: No battery voltage messages found.")
        else:
            VOLTMin = maxFloat
            VOLTMax = -maxFloat
            for Data in VOLT:
                if Data[1] >= CurMainStart and Data[1] <= CurMainEnd:
                    if Data[2] < VOLTMin:
                        VOLTMin = Data[2]
                    if Data[2] > VOLTMax:
                        VOLTMax = Data[2]
                elif Data[1] > CurMainEnd:
                    break
            if VOLTMin == maxFloat:
                VOLTMin = 0.0
                VOLTMax = 0.0
            YRange = yRange(VOLTMin, VOLTMax)
            LCan.create_line(MFLead, GTop, MFLead + MFGW, GTop,
                             fill=DClr["MaxMinL"])
            LCan.create_line(MFLead, GTop + GHeight, MFLead + MFGW,
                             GTop + GHeight, fill=DClr["MaxMinL"])
            canText(LCan, LRBorder, GTop + GHeight / 2, DClr["Label"],
                    "Battery Volts")
            Label = str(VOLTMax) + "V"
# "-5" just leaves a bit of space between the end of the number and the start
# of the line.
            Xx = MFLead - PROGPropFont.measure(Label) - 5
            canText(LCan, Xx, GTop, DClr["CanText"], Label)
            Label = str(VOLTMin) + "V"
            Xx = MFLead - PROGPropFont.measure(Label) - 5
            canText(LCan, Xx, GTop + GHeight, DClr["CanText"], Label)
            Xx = 0
            Yy = 0
            LastData = []
# Find the first data point.
            for Data in VOLT:
                if Data[1] >= CurMainStart:
                    LastX = MFLead + MFGW * \
                        (Data[1] - CurMainStart) / Timerange
                    LastY = GTop + (GHeight - GHeight *
                                    (Data[2] - VOLTMin) / YRange)
                    LastData = Data
                    break
            Plotted = 0
            for Data in VOLT:
                if Data[1] >= CurMainStart:
                    if Data[1] <= CurMainEnd:
                        Xx = MFLead + MFGW * \
                            (Data[1] - CurMainStart) / Timerange
                        Yy = GTop + (GHeight - GHeight *
                                     (Data[2] - VOLTMin) / YRange)
                        LCan.create_line(LastX, LastY, Xx, Yy,
                                         fill=DClr["VOLT"])
                        ID = LCan.create_rectangle((LastX - 1.5, LastY - 1.5,
                                                    LastX + 1.5, LastY + 1.5),
                                                   fill=DClr["VOLT"],
                                                   outline=DClr["VOLT"])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFPointClick, "VOLT",
                                              LastData[0]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                        if B2Glitch is True:
                            LCan.tag_bind(ID, "<Button-2>",
                                          Command(plotMFZapClick, "VOLT",
                                                  LastData[0]))
                        LCan.tag_bind(ID, "<Button-3>",
                                      Command(plotMFZapClick, "VOLT",
                                              LastData[0]))
                        LastX = Xx
                        LastY = Yy
                        LastData = Data
                        Plotted += 1
                    else:
                        if OPTIgTEC == 0:
                            break
            if Xx != 0 and Yy != 0:
                ID = LCan.create_rectangle((Xx - 1.5, Yy - 1.5, Xx + 1.5,
                                            Yy + 1.5), fill=DClr["VOLT"],
                                           outline=DClr["VOLT"])
                LCan.tag_bind(ID, "<Button-1>", Command(plotMFPointClick,
                                                        "VOLT", Data[0]))
                LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                if B2Glitch is True:
                    LCan.tag_bind(ID, "<Button-2>", Command(plotMFZapClick,
                                                            "VOLT", Data[0]))
                LCan.tag_bind(ID, "<Button-3>", Command(plotMFZapClick,
                                                        "VOLT", Data[0]))
            canText(LCan, MFLead + MFGW + LRBorder2, GTop + GHeight / 2,
                    DClr["VOLT"], str(Plotted))
        Top += PixPerWeight * DPlotWts["VOLT"] + GFontH
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            return ""
# === BKUP battery voltage ===
    if DGrf["BKUP"].get() == 1 and RTModel == "RT130":
        setMsg("MF", "CB", "Plotting backup battery voltage information...")
        GTop = Top + PixPerWeight * DPlotWts["BKUP"] / 2.0
        LCan.create_line(MFLead, GTop, MFLead + MFGW, GTop,
                         fill=DClr["MaxMinL"])
        canText(LCan, LRBorder, GTop, DClr["Label"], "Backup Volts")
        Plotted = 0
        for Data in BKUP:
            if Data[1] >= CurMainStart:
                if Data[1] <= CurMainEnd:
                    Xx = MFLead + MFGW * (Data[1] - CurMainStart) / Timerange
                    if Data[3] == 1:
                        C = "BKUPG"
                    elif Data[3] == 2:
                        C = "BKUPB"
                    else:
                        C = "BKUPU"
                    ID = LCan.create_rectangle((Xx - 1.5, GTop - 1.5, Xx + 1.5,
                                                GTop + 1.5), fill=DClr[C],
                                               outline=DClr[C])
                    LCan.tag_bind(ID, "<Button-1>",
                                  Command(plotMFPointClick, "BKUP", Data[0],
                                          C))
                    LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                    Plotted += 1
                else:
                    if OPTIgTEC == 0:
                        break
        canText(LCan, MFLead + MFGW + LRBorder2, GTop, DClr["BKUP"],
                str(Plotted))
        Top += PixPerWeight * DPlotWts["BKUP"] + GFontH
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            return ""
# === Auto Dump Called/Completed ===
    if DGrf["DUMP"].get() == 1:
        setMsg("MF", "CB", "Plotting dump call messages...")
        GTop = Top + PixPerWeight * DPlotWts["DUMP"] / 2.0
        LCan.create_line(MFLead, GTop, MFLead + MFGW, GTop,
                         fill=DClr["MaxMinL"])
        canText(LCan, LRBorder, GTop, DClr["Label"], "Dump Call")
        Plotted0 = 0
        Plotted1 = 0
        for Data in DUMP:
            if Data[1] >= CurMainStart:
                if Data[1] <= CurMainEnd:
                    Xx = MFLead + MFGW * (Data[1] - CurMainStart) / Timerange
                    if Data[2] == 0:
                        ID = LCan.create_rectangle((Xx - 1.5, GTop + 1.0,
                                                    Xx + 1.5, GTop + 4.0),
                                                   fill=DClr["DUMPOff"],
                                                   outline=DClr["DUMPOff"])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFPointClick, "DUMP0",
                                              Data[0]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                        Plotted0 += 1
                    else:
                        ID = LCan.create_rectangle((Xx - 1.5, GTop - 4.0,
                                                    Xx + 1.5, GTop - 1),
                                                   fill=DClr["DUMPOn"],
                                                   outline=DClr["DUMPOn"])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFPointClick, "DUMP1",
                                              Data[0]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                        Plotted1 += 1
                else:
                    if OPTIgTEC == 0:
                        break
        canText(LCan, MFLead + MFGW + LRBorder2, GTop - GFontH12,
                DClr["DUMPOn"], str(Plotted1))
        canText(LCan, MFLead + MFGW + LRBorder2, GTop + GFontH12,
                DClr["DUMPOff"], str(Plotted0))
        Top += PixPerWeight * DPlotWts["DUMP"] + GFontH
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            return ""
# === Acquisition on/off ===
    if DGrf["ACQ"].get() == 1:
        GTop = Top + PixPerWeight * DPlotWts["ACQ"] / 2.0
        LCan.create_line(MFLead, GTop, MFLead + MFGW, GTop,
                         fill=DClr["MaxMinL"])
        canText(LCan, LRBorder, GTop, DClr["Label"], "ACQ On-Off")
        Plotted0 = 0
        Plotted1 = 0
        for Data in ACQ:
            if Data[1] >= CurMainStart:
                if Data[1] <= CurMainEnd:
                    Xx = MFLead + MFGW * (Data[1] - CurMainStart) / Timerange
                    if Data[2] == 0:
                        ID = LCan.create_rectangle((Xx - 1.5, GTop + 1.0,
                                                    Xx + 1.5, GTop + 4.0),
                                                   fill=DClr["ACQStop"],
                                                   outline=DClr["ACQStop"])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFPointClick, "ACQ0",
                                              Data[0]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                        Plotted0 += 1
                    else:
                        ID = LCan.create_rectangle((Xx - 1.5, GTop - 4.0,
                                                    Xx + 1.5, GTop - 1.0),
                                                   fill=DClr["ACQStart"],
                                                   outline=DClr["ACQStart"])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFPointClick, "ACQ1",
                                              Data[0]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                        Plotted1 += 1
                else:
                    if OPTIgTEC == 0:
                        break
        canText(LCan, MFLead + MFGW + LRBorder2, GTop - GFontH12,
                DClr["ACQStart"], str(Plotted1))
        canText(LCan, MFLead + MFGW + LRBorder2, GTop + GFontH12,
                DClr["ACQStop"], str(Plotted0))
        Top += PixPerWeight * DPlotWts["ACQ"] + GFontH
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            return ""
# === Station reset plot ===
    if DGrf["RESET"].get() == 1:
        setMsg("MF", "CB", "Plotting reset information...")
        GTop = Top + PixPerWeight * DPlotWts["RESET"] / 2.0
        LCan.create_line(MFLead, GTop, MFLead + MFGW, GTop,
                         fill=DClr["MaxMinL"])
        canText(LCan, LRBorder, GTop, DClr["Label"], "Reset/Powerup")
        Plotted = 0
        for Data in RESET:
            if Data[1] >= CurMainStart:
                if Data[1] <= CurMainEnd:
                    Xx = MFLead + MFGW * (Data[1] - CurMainStart) / Timerange
                    ID = LCan.create_rectangle((Xx - 1.5, GTop - 4.0,
                                                Xx + 1.5, GTop - 1.0),
                                               fill=DClr["RESET"],
                                               outline=DClr["RESET"])
                    LCan.tag_bind(ID, "<Button-1>", Command(plotMFPointClick,
                                                            "RES", Data[0]))
                    LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                    Plotted += 1
                else:
                    break
        canText(LCan, MFLead + MFGW + LRBorder2, GTop - GFontH12,
                DClr["RESET"], str(Plotted))
        Plotted = 0
        for Data in PWRUP:
            if Data[1] >= CurMainStart:
                if Data[1] <= CurMainEnd:
                    Xx = MFLead + MFGW * (Data[1] - CurMainStart) / Timerange
                    ID = LCan.create_rectangle((Xx - 1.5, GTop + 1.0, Xx + 1.5,
                                                GTop + 4.0),
                                               fill=DClr["PWRUP"],
                                               outline=DClr["PWRUP"])
                    LCan.tag_bind(ID, "<Button-1>", Command(plotMFPointClick,
                                                            "PUP", Data[0]))
                    LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                    Plotted += 1
                else:
                    if OPTIgTEC == 0:
                        break
        canText(LCan, MFLead + MFGW + LRBorder2, GTop + GFontH12,
                DClr["PWRUP"], str(Plotted))
        Top += PixPerWeight * DPlotWts["RESET"] + GFontH
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            return ""
# === WARNING/ERROR messages ===
    if DGrf["ERRWARN"].get() == 1:
        setMsg("MF", "CB", "Plotting warning/error messages...")
        GTop = Top + PixPerWeight * DPlotWts["ERRWARN"] / 2.0
        LCan.create_line(MFLead, GTop, MFLead + MFGW, GTop,
                         fill=DClr["MaxMinL"])
        canText(LCan, LRBorder, GTop, DClr["Label"], "Error/Warning")
        Plotted = 0
        for Data in ERROR:
            if Data[1] >= CurMainStart:
                if Data[1] <= CurMainEnd:
                    Xx = MFLead + MFGW * (Data[1] - CurMainStart) / Timerange
                    ID = LCan.create_rectangle((Xx - 1.5, GTop - 4.0, Xx + 1.5,
                                                GTop - 1.0),
                                               fill=DClr["ERROR"],
                                               outline=DClr["ERROR"])
                    LCan.tag_bind(ID, "<Button-1>", Command(plotMFPointClick,
                                                            "ERR", Data[0]))
                    LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                    Plotted += 1
                else:
                    if OPTIgTEC == 0:
                        break
        canText(LCan, MFLead + MFGW + LRBorder2, GTop - GFontH12,
                DClr["ERROR"], str(Plotted))
        Plotted = 0
        for Data in WARN:
            if Data[1] >= CurMainStart:
                if Data[1] <= CurMainEnd:
                    Xx = MFLead + MFGW * (Data[1] - CurMainStart) / Timerange
                    if Data[2] == 1:
                        ID = LCan.create_rectangle((Xx - 1.5, GTop + 1.0,
                                                    Xx + 1.5, GTop + 4.0),
                                                   fill=DClr["WARN"],
                                                   outline=DClr["WARN"])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFPointClick, "WARN",
                                              Data[0]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                    elif Data[2] == 2:
                        ID = LCan.create_rectangle((Xx - 1.0, GTop + 1.0,
                                                    Xx + 1.0, GTop + 4.0),
                                                   fill=DClr["WARNM"],
                                                   outline=DClr["WARNM"])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFPointClick, "WARNM",
                                              Data[0]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                    Plotted += 1
                else:
                    if OPTIgTEC == 0:
                        break
        canText(LCan, MFLead + MFGW + LRBorder2, GTop + GFontH12, DClr["WARN"],
                str(Plotted))
        Top += PixPerWeight * DPlotWts["ERRWARN"] + GFontH
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            return ""
# === DISCREP messages ===
    if DGrf["DISCREP"].get() == 1:
        setMsg("MF", "CB", "Plotting discrepancy information...")
        GTop = Top + PixPerWeight * DPlotWts["DISCREP"] / 2.0
        LCan.create_line(MFLead, GTop, MFLead + MFGW, GTop,
                         fill=DClr["MaxMinL"])
        canText(LCan, LRBorder, GTop, DClr["Label"], "Discrepancies")
        Plotted = 0
        for Data in DISCREP:
            if Data[1] >= CurMainStart:
                if Data[1] <= CurMainEnd:
                    Xx = MFLead + MFGW * (Data[1] - CurMainStart) / Timerange
                    ID = LCan.create_rectangle((Xx - 1.5, GTop - 1.5, Xx + 1.5,
                                                GTop + 1.5),
                                               fill=DClr["DISCREP"],
                                               outline=DClr["DISCREP"])
                    LCan.tag_bind(ID, "<Button-1>", Command(plotMFPointClick,
                                                            "DISCREP",
                                                            Data[0]))
                    LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                    Plotted += 1
                else:
                    if OPTIgTEC == 0:
                        break
        canText(LCan, MFLead + MFGW + LRBorder2, GTop, DClr["DISCREP"],
                str(Plotted))
        Top += PixPerWeight * DPlotWts["DISCREP"] + GFontH
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            return ""
# === Station Channel definitions/SOH info ===
    if DGrf["SOHDEF"].get() == 1:
        GTop = Top + PixPerWeight * DPlotWts["SOHDEF"] / 2.0
        LCan.create_line(MFLead, GTop, MFLead + MFGW, GTop,
                         fill=DClr["MaxMinL"])
        canText(LCan, LRBorder, GTop, DClr["Label"], "SOH/Data Defs")
        Plotted = 0
        for Data in SOH:
            if Data[1] >= CurMainStart:
                if Data[1] <= CurMainEnd:
                    Xx = MFLead + MFGW * (Data[1] - CurMainStart) / Timerange
                    ID = LCan.create_rectangle((Xx - 1.5, GTop - 4.0, Xx + 1.5,
                                                GTop - 1.0), fill=DClr["SOH"],
                                               outline=DClr["SOH"])
                    LCan.tag_bind(ID, "<Button-1>", Command(plotMFPointClick,
                                                            "SOH", Data[0]))
                    LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                    Plotted += 1
                else:
                    if OPTIgTEC == 0:
                        break
        canText(LCan, MFLead + MFGW + LRBorder2, GTop - GFontH12, DClr["SOH"],
                str(Plotted))
        Plotted = 0
        for Data in DEFS:
            if Data[1] >= CurMainStart:
                if Data[1] <= CurMainEnd:
                    Xx = MFLead + MFGW * (Data[1] - CurMainStart) / Timerange
                    ID = LCan.create_rectangle((Xx - 1.5, GTop + 1.0, Xx + 1.5,
                                                GTop + 4.0), fill=DClr["DEFS"],
                                               outline=DClr["DEFS"])
                    LCan.tag_bind(ID, "<Button-1>", Command(plotMFPointClick,
                                                            "DEFS", Data[0]))
                    LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                    Plotted += 1
                else:
                    if OPTIgTEC == 0:
                        break
        canText(LCan, MFLead + MFGW + LRBorder2, GTop + GFontH12, DClr["DEFS"],
                str(Plotted))
        Top += PixPerWeight * DPlotWts["SOHDEF"] + GFontH
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            return ""
# === Network Up/Down ===
    if DGrf["NET"].get() == 1:
        GTop = Top + PixPerWeight * DPlotWts["NET"] / 2.0
        LCan.create_line(MFLead, GTop, MFLead + MFGW, GTop,
                         fill=DClr["MaxMinL"])
        canText(LCan, LRBorder, GTop, DClr["Label"], "Net Up-Down")
        Plotted0 = 0
        Plotted1 = 0
        for Data in NET:
            if Data[1] >= CurMainStart:
                if Data[1] <= CurMainEnd:
                    Xx = MFLead + MFGW * (Data[1] - CurMainStart) / Timerange
                    if Data[2] == 0:
                        ID = LCan.create_rectangle((Xx - 1.5, GTop + 1.0,
                                                    Xx + 1.5, GTop + 4.0),
                                                   fill=DClr["NETDown"],
                                                   outline=DClr["NETDown"])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFPointClick, "NET0",
                                              Data[0]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                        Plotted0 += 1
                    else:
                        ID = LCan.create_rectangle((Xx - 1.5, GTop - 4.0,
                                                    Xx + 1.5, GTop - 1.0),
                                                   fill=DClr["NETUp"],
                                                   outline=DClr["NETUp"])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFPointClick, "NET1",
                                              Data[0]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                        Plotted1 += 1
                else:
                    if OPTIgTEC == 0:
                        break
        canText(LCan, MFLead + MFGW + LRBorder2, GTop - GFontH12,
                DClr["NETUp"], str(Plotted1))
        canText(LCan, MFLead + MFGW + LRBorder2, GTop + GFontH12,
                DClr["NETDown"], str(Plotted0))
        Top += PixPerWeight * DPlotWts["NET"] + GFontH
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            return ""
# === Events/data streams plot(s) ===
    if DGrf["EVT"].get() == 1 and len(DSsButts) > 0:
        setMsg("MF", "CB", "Plotting events/data streams...")
# There may be multiple DSs, so set up the GTops in here.
        GTops = {}
# Where the number of points for each DS will be kept. We'll just create and
# fill in the info with 0 in this loop, so we don't have to has_key in the next
# loop.
        PlottedCount = {}
        Count = 0
        for DS in DSsButts:
            GTops[DS] = Top + (Count * PixPerWeight * DPlotWts["EVT"])
# Might as well draw the lines and labels while we're here.
            LCan.create_line(MFLead, GTops[DS], MFLead + MFGW, GTops[DS],
                             fill=DClr["MaxMinL"])
            canText(LCan, LRBorder, GTops[DS], DClr["Label"],
                    "Events DS%d" % DS)
            PlottedCount[DS] = 0
            Count += 1
# This seems to be the only place where changing the DSs buttons selected and
# just re-plotting (not re-reading) crashes, so try.
        try:
            for Data in EVT:
                if Data[1] >= CurMainStart:
                    if Data[1] <= CurMainEnd:
                        Xx = MFLead + MFGW * \
                            (Data[1] - CurMainStart) / Timerange
                        DS = Data[2]
                        ID = LCan.create_rectangle((Xx - 1.5, GTops[DS] - 1.5,
                                                    Xx + 1.5, GTops[DS] + 1.5),
                                                   fill=DClr["EVT"],
                                                   outline=DClr["EVT"])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFPointClick, "EVT",
                                              Data[0]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                        PlottedCount[DS] += 1
                    else:
                        if OPTIgTEC == 0:
                            break
        except KeyError:
            pass
        for DS in DSsButts:
            canText(LCan, MFLead + MFGW + LRBorder2, GTops[DS], DClr["EVT"],
                    str(PlottedCount[DS]))
        Top += (PixPerWeight * DPlotWts["EVT"] * len(DSsButts)) + GFontH
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            return ""
# === Disk Used messages ===
# Disk 1.
    if DGrf["DU1"].get() == 1:
        setMsg("MF", "CB", "Plotting disk 1 usage...")
        GTop = Top
        GHeight = PixPerWeight * DPlotWts["DU1"]
        if len(DU1) == 0:
            canText(LCan, LRBorder, GTop + GHeight / 2, DClr["Label"],
                    "Disk 1 Usage: No messages found.")
        else:
            DU1Min = maxFloat
            DU1Max = -maxFloat
            for Data in DU1:
                if Data[1] >= CurMainStart and Data[1] <= CurMainEnd:
                    if Data[2] < DU1Min:
                        DU1Min = Data[2]
                    if Data[2] > DU1Max:
                        DU1Max = Data[2]
                elif Data[1] > CurMainEnd:
                    break
            if DU1Min == maxFloat:
                DU1Min = 0.0
                DU1Max = 0.0
            LCan.create_line(MFLead, GTop, MFLead + MFGW, GTop,
                             fill=DClr["MaxMinL"])
            LCan.create_line(MFLead, GTop + GHeight, MFLead + MFGW,
                             GTop + GHeight, fill=DClr["MaxMinL"])
            canText(LCan, LRBorder, GTop + GHeight / 2, DClr["Label"],
                    "Disk 1 Usage")
            Label = diskSizeFormat("b", DU1Max * 1024.0)
# "-5" just leaves a bit of space between the end of the number and the start
# of the line.
            Xx = MFLead - PROGPropFont.measure(Label) - 5
            canText(LCan, Xx, GTop, DClr["CanText"], Label)
            Label = diskSizeFormat("b", DU1Min * 1024.0)
            Xx = MFLead - PROGPropFont.measure(Label) - 5
            canText(LCan, Xx, GTop + GHeight, DClr["CanText"], Label)
            YRange = yRange(DU1Min, DU1Max)
            Xx = 0
            Yy = 0
            LastData = []
# Find the first data point.
            for Data in DU1:
                if Data[1] >= CurMainStart:
                    LastX = MFLead + MFGW * \
                        (Data[1] - CurMainStart) / Timerange
                    LastY = GTop + (GHeight - GHeight *
                                    (Data[2] - DU1Min) / YRange)
                    LastData = Data
                    break
            Plotted = 0
            for Data in DU1:
                if Data[1] >= CurMainStart:
                    if Data[1] <= CurMainEnd:
                        Xx = MFLead + MFGW * \
                            (Data[1] - CurMainStart) / Timerange
                        Yy = GTop + (GHeight - GHeight * (Data[2] -
                                                          DU1Min) / YRange)
                        LCan.create_line(LastX, LastY, Xx, Yy,
                                         fill=DClr["DU"])
                        ID = LCan.create_rectangle((LastX - 1.5, LastY - 1.5,
                                                    LastX + 1.5, LastY + 1.5),
                                                   fill=DClr["DU"],
                                                   outline=DClr["DU"])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFPointClick, "DU",
                                              LastData[0]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                        LastX = Xx
                        LastY = Yy
                        LastData = Data
                        Plotted += 1
                    else:
                        if OPTIgTEC == 0:
                            break
            if Xx != 0 and Yy != 0:
                ID = LCan.create_rectangle((Xx - 1.5, Yy - 1.5, Xx + 1.5,
                                            Yy + 1.5), fill=DClr["DU"],
                                           outline=DClr["DU"])
                LCan.tag_bind(ID, "<Button-1>", Command(plotMFPointClick,
                                                        "DU", Data[0]))
                LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
            canText(LCan, MFLead + MFGW + LRBorder2, GTop + GHeight / 2,
                    DClr["DU"], str(Plotted))
        Top += GHeight + GFontH
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            return ""
# Disk 2.
    if DGrf["DU2"].get() == 1:
        setMsg("MF", "CB", "Plotting disk 2 usage...")
        GTop = Top
        GHeight = PixPerWeight * DPlotWts["DU2"]
        if len(DU2) == 0:
            canText(LCan, LRBorder, GTop + GHeight / 2, DClr["Label"],
                    "Disk 2 Usage: No messages found.")
        else:
            DU2Min = maxFloat
            DU2Max = -maxFloat
            for Data in DU2:
                if Data[1] >= CurMainStart and Data[1] <= CurMainEnd:
                    if Data[2] < DU2Min:
                        DU2Min = Data[2]
                    if Data[2] > DU2Max:
                        DU2Max = Data[2]
                elif Data[1] > CurMainEnd:
                    break
            if DU2Min == maxFloat:
                DU2Min = 0.0
                DU2Max = 0.0
            LCan.create_line(MFLead, GTop, MFLead + MFGW, GTop,
                             fill=DClr["MaxMinL"])
            LCan.create_line(MFLead, GTop + GHeight, MFLead + MFGW,
                             GTop + GHeight, fill=DClr["MaxMinL"])
            canText(LCan, LRBorder, GTop + GHeight / 2, DClr["Label"],
                    "Disk 2 Usage")
            Label = diskSizeFormat("b", DU2Max * 1024.0)
# "-5" just leaves a bit of space between the end of the number and the start
# of the line.
            Xx = MFLead - PROGPropFont.measure(Label) - 5
            canText(LCan, Xx, GTop, DClr["CanText"], Label)
            Label = diskSizeFormat("b", DU2Min * 1024.0)
            Xx = MFLead - PROGPropFont.measure(Label) - 5
            canText(LCan, Xx, GTop + GHeight, DClr["CanText"], Label)
            YRange = yRange(DU2Min, DU2Max)
            Xx = 0
            Yy = 0
            LastData = []
# Draw the connecting line, then the points on top of it.
            for Data in DU2:
                if Data[1] >= CurMainStart:
                    LastX = MFLead + MFGW * \
                        (Data[1] - CurMainStart) / Timerange
                    LastY = GTop + (GHeight - GHeight *
                                    (Data[2] - DU2Min) / YRange)
                    LastData = Data
                    break
            Plotted = 0
            for Data in DU2:
                if Data[1] >= CurMainStart:
                    if Data[1] <= CurMainEnd:
                        Xx = MFLead + MFGW * \
                            (Data[1] - CurMainStart) / Timerange
                        Yy = GTop + (GHeight - GHeight *
                                     (Data[2] - DU2Min) / YRange)
                        LCan.create_line(LastX, LastY, Xx, Yy,
                                         fill=DClr["DU"])
                        ID = LCan.create_rectangle((LastX - 1.5, LastY - 1.5,
                                                    LastX + 1.5, LastY + 1.5),
                                                   fill=DClr["DU"],
                                                   outline=DClr["DU"])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFPointClick, "DU",
                                              LastData[0]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                        LastX = Xx
                        LastY = Yy
                        LastData = Data
                        Plotted += 1
                    else:
                        if OPTIgTEC == 0:
                            break
            if Xx != 0 and Yy != 0:
                ID = LCan.create_rectangle((Xx - 1.5, Yy - 1.5, Xx + 1.5,
                                            Yy + 1.5), fill=DClr["DU"],
                                           outline=DClr["DU"])
                LCan.tag_bind(ID, "<Button-1>",
                              Command(plotMFPointClick, "DU", Data[0]))
                LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
            canText(LCan, MFLead + MFGW + LRBorder2, GTop + GHeight / 2,
                    DClr["DU"], str(Plotted))
        Top += GHeight + GFontH
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            return ""
# === Mass Re-centering messages ===
    if DGrf["MRC"].get() == 1:
        setMsg("MF", "CB", "Plotting centering commands information...")
        GTop = Top
        LCan.create_line(MFLead, GTop, MFLead + MFGW, GTop,
                         fill=DClr["MaxMinL"])
        canText(LCan, LRBorder, GTop, DClr["Label"], "Re-center")
        Plotted = 0
        for Data in MRC:
            if Data[1] >= CurMainStart:
                if Data[1] <= CurMainEnd:
                    Xx = MFLead + MFGW * (Data[1] - CurMainStart) / Timerange
                    ID = LCan.create_rectangle((Xx - 1.5, GTop - 1.5, Xx + 1.5,
                                                GTop + 1.5), fill=DClr["MRC"],
                                               outline=DClr["MRC"])
                    LCan.tag_bind(ID, "<Button-1>", Command(plotMFPointClick,
                                                            "MRC", Data[0]))
                    LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                    Plotted += 1
                else:
                    if OPTIgTEC == 0:
                        break
        canText(LCan, MFLead + MFGW + LRBorder2, GTop, DClr["MRC"],
                str(Plotted))
        Top += PixPerWeight * DPlotWts["MRC"] + GFontH
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            return ""
# === MP123 mass position data ===
# These are not messages in the file, but data extracted and left in the MPx
# Lists.
    if DGrf["MP123"].get() == 1:
        setMsg("MF", "CB", "Plotting 123 mass positions...")
        for M in (1, 2, 3):
            if M == 1:
                GTop = Top
            elif M == 2:
                GTop = Top + PixPerWeight * DPlotWts["MP123"] / 2.0
            else:
                GTop = Top + PixPerWeight * DPlotWts["MP123"]
            LCan.create_line(MFLead, GTop, MFLead + MFGW, GTop,
                             fill=DClr["MaxMinL"])
            canText(LCan, LRBorder, GTop, DClr["Label"], "Mass Pos %d" % M)
            Plotted = 0
            Datas = eval("MP%d" % M)
# MPx = (Epoch, MP Value, Color Range Code)
            for Data in Datas:
                if Data[0] >= CurMainStart:
                    if Data[0] <= CurMainEnd:
                        Xx = MFLead + MFGW * \
                            (Data[0] - CurMainStart) / Timerange
                        if Data[2] == 0:
                            C = "MPP"
                        elif Data[2] == 1:
                            C = "MPG"
                        elif Data[2] == 2:
                            C = "MPB"
                        elif Data[2] == 3:
                            C = "MPU"
                        elif Data[2] == 4:
                            C = "MPH"
                        ID = LCan.create_rectangle((Xx - 1.5, GTop - 1.5,
                                                    Xx + 1.5, GTop + 1.5),
                                                   fill=DClr[C],
                                                   outline=DClr[C])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFPointClick, "MP%d" % M,
                                              Data[0], Data[1]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                        Plotted += 1
                    else:
                        if OPTIgTEC == 0:
                            break
            canText(LCan, MFLead + MFGW + LRBorder2, GTop, DClr["MP"],
                    str(Plotted))
        Top += PixPerWeight * DPlotWts["MP123"] + GFontH
        PROGStopBut.update()
        if PROGRunning.get() == 0:
            return ""
# === MP456 mass position (aux data) messages ===
    if DGrf["MP456"].get() == 1:
        setMsg("MF", "CB", "Plotting 456 mass positions...")
        GTop = Top
        for M in (4, 5, 6):
            if M == 4:
                GTop = Top
            elif M == 5:
                GTop = Top + PixPerWeight * DPlotWts["MP456"] / 2.0
            else:
                GTop = Top + PixPerWeight * DPlotWts["MP456"]
            LCan.create_line(MFLead, GTop, MFLead + MFGW, GTop,
                             fill=DClr["MaxMinL"])
            canText(LCan, LRBorder, GTop, DClr["Label"], "Mass Pos %d" % M)
            Plotted = 0
            Datas = eval("MP%d" % M)
# MPx = (Epoch, MP Value, Color Range Code)
            for Data in Datas:
                if Data[0] >= CurMainStart:
                    if Data[0] <= CurMainEnd:
                        Xx = MFLead + MFGW * \
                            (Data[0] - CurMainStart) / Timerange
                        if Data[2] == 0:
                            C = "MPP"
                        elif Data[2] == 1:
                            C = "MPG"
                        elif Data[2] == 2:
                            C = "MPB"
                        elif Data[2] == 3:
                            C = "MPU"
                        elif Data[2] == 4:
                            C = "MPH"
                        ID = LCan.create_rectangle((Xx - 1.5, Yy - 1.5,
                                                    Xx + 1.5, Yy + 1.5),
                                                   fill=DClr[C],
                                                   outline=DClr[C])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotMFPointClick, "MP%d" % M,
                                              Data[0], Data[1]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
                        Plotted += 1
                    else:
                        if OPTIgTEC == 0:
                            break
            canText(LCan, MFLead + MFGW + LRBorder2,
                    Yy, DClr["MP"], str(Plotted))
        Top += PixPerWeight * DPlotWts["MP456"] + GFontH
# Now the X-axis scale across the bottom of the plot area.
    GTop = CHeight - BotSpace
    LCan.create_line(MFLead, GTop, MFLead + MFGW, GTop, fill=DClr["Ticks"])
    MFTickY = GTop
    TickX = []
# Plot tick marks showing months, days, hours, minutes or seconds depending
# on the range of the currently displayed plot.
    Zero = dt2Time(-1, 11, CurMainStart)
# 30 days.
    if Timerange >= 2592000.0:
        canText(LCan, LRBorder, GTop, DClr["Label"], "10Days")
        MFTimeMode = "DD"
        Zero = Zero[:9] + "00:00:00"
        Add = 864000.0
    elif Timerange >= 864000.0:
        canText(LCan, LRBorder, GTop, DClr["Label"], "Days")
        MFTimeMode = "D"
        Zero = Zero[:9] + "00:00:00"
        Add = 86400.0
    elif Timerange >= 3600.0:
        canText(LCan, LRBorder, GTop, DClr["Label"], "Hours")
        MFTimeMode = "H"
        Zero = Zero[:12] + "00:00"
        Add = 3600.0
    elif Timerange >= 60.0:
        canText(LCan, LRBorder, GTop, DClr["Label"], "Minutes")
        MFTimeMode = "M"
        Zero = Zero[:15] + "00"
        Add = 60.0
    else:
        canText(LCan, LRBorder, GTop, DClr["Label"], "Seconds")
        MFTimeMode = "S"
        Zero = Zero[:17]
        Add = 1.0
    Time = dt2Timeystr2Epoch(None, Zero)
    LastTimeEndX = 0.0
    First = True
    while True:
        Time += Add
        if Time < CurMainEnd:
            if Time >= CurMainStart:
                GX = MFLead + MFGW * (Time - CurMainStart) / Timerange
                LCan.create_line(GX, GTop - 2, GX, GTop, fill=DClr["Ticks"])
                TickX.append(GX)
                Zero = dt2Time(-1, 80, Time)[:-4]
                Length = PROGPropFont.measure(Zero)
                Length2 = Length / 2
# Print the date/time of the first and last ticks.
                if First is True or (GX - Length2) > (LastTimeEndX + 100):
                    LCan.create_line(GX, GTop - 6, GX, GTop,
                                     fill=DClr["Ticks"])
# Keep the times away from extending to far left or right.
                    if (GX - Length2) < MFLead:
                        GXX = MFLead
                    elif (GX + Length2) > (MFLead + MFGW):
                        GXX = MFLead + MFGW - Length
                    else:
                        GXX = GX - Length2
                    LastTimeEndX = GXX + Length
                    canText(LCan, GXX, CHeight - GFontH, DClr["Ticks"], Zero)
                    First = False
        else:
            break
    Msg = "Showing: %s to %s" % (dt2Time(-1, 80, CurMainStart)[:-4],
                                 dt2Time(-1, 80, CurMainEnd)[:-4])
    if CurMainStart != FStart or CurMainEnd != FEnd:
        Msg += " (zoomed %d)" % len(StartEndRecord)
    if MFZapped != 0:
        Msg += "   Points zapped: %d" % MFZapped
# If the caller is also going to plot raw data then they won't want this stuff
# to happen. The message will be saved by the caller until everything is done.
    if WhereMsg is not None:
        setMsg(WhereMsg, "", Msg)
    MFPlotted = True
    return Msg
##########################
# BEGIN: plotMFClear(What)
# FUNC:plotMFClear():2018.337


def plotMFClear(What):
    global MFGridOn
    global FirstSelRule
    global SecondSelRule
    global MFPlotted
    global MFZapped
# The user may have selected a different color scheme, so get everything
# ready for that.
    setColors()
# These may crash if nothing has yet to be plotted (like reading was stopped).
    try:
        LCan = PROGCan["MF"]
        LCan.delete("all")
        LCan.delete(FirstSelRule)
        LCan.delete(SecondSelRule)
        LCan.configure(bg=DClr["MFCAN"])
    except Exception:
        pass
    MFGridOn = False
    FirstSelRule = None
    SecondSelRule = None
    setMsg("MF", "", "")
    MFPlotted = False
# If What is not "all" then we must just be zooming or something.
    if What == "all":
        # Clear out the lines from an old file.
        if PROGFrm["LOG"] is not None:
            txLn("LOG", "", "", False)
            setMsg("LOG", "")
# Close any old err file window.
        if PROGFrm["ERR"] is not None:
            formClose("ERR")
# Close any old GPS plot display.
        if PROGFrm["GPS"] is not None:
            formClose("GPS")
# Close any log search window.
        if PROGFrm["SEARCH"] is not None:
            formClose("SEARCH")
        setMsg("INFO", "", "")
        MFZapped = 0
    updateMe(0)
    return
#############################
# BEGIN: plotMFClock(Who, CX)
# FUNC:plotMFClock():2013.190
#   Actually draws the main clock rule and time.


def plotMFClock(Who, CX):
    LCan = PROGCan["MF"]
    LCan.delete("Clock")
# Just to the left of the plots...
    if CX < MFLead:
        CX = MFLead
# Just to the right of the plots...
    elif CX > MFLead + MFGW:
        CX = MFLead + MFGW
# Draw the rule then figure out where to draw the date/time.
    if Who == "TPS":
        LCan.create_line(CX, TopSpace, CX, CHeight - TopSpace - BotSpace / 2,
                         fill=DClr["SelTPS"], tags="Clock")
    else:
        LCan.create_line(CX, TopSpace, CX, CHeight - TopSpace - BotSpace / 2,
                         fill=DClr["Sel"], tags="Clock")
    Epoch = plotMFEpochAtX(CX)
    if MFTimeMode != "S":
        Time = dt2Time(-1, 80, Epoch)[:-4]
    else:
        Time = dt2Time(-1, 82, Epoch)[:-4]
# Get a copy of the current proportional font and set it to size 8 which is
# close to the hand-drawn plot labels.
    CFont = Font(font=Entry()["font"])
    CFont["size"] = 8
# FUDGE - I think the metrics' return an acsent of some tall character, which
# is never a number, so without the -2 there is a 1 pixel overlap with the
# rule.
    CFontH = CFont.metrics("ascent") + CFont.metrics("descent") - 2
# Adjust the time to be centered over the rule, but don't print it beyond the
# beginning or the end of the plot.
    Length12 = CFont.measure(Time) / 2
    if CX + Length12 > MFLead + MFGW:
        CX = MFLead + MFGW - Length12
    if CX - Length12 < MFLead:
        CX = MFLead + Length12
    LCan.create_text(CX, CFontH, fill=DClr["CanText"], font=CFont,
                     text=Time, tags="Clock")
    return Epoch
###########################
# BEGIN: plotMFEpochAtX(CX)
# FUNC:plotMFEpochAtX():2010.251
#    Takes the passed CX pixel location and returns the epoch time at that
#    plot location on the main form.


def plotMFEpochAtX(CX):
    # How far across the plot did we click?
    Time = (CX - MFLead) / MFGW
    Time = CurMainStart + (CurTimerange * Time)
    return Time
##############################
# BEGIN: plotMFXAtEpoch(Epoch)
# FUNC:plotMFXAtEpoch():2010.251


def plotMFXAtEpoch(Epoch):
    XX = (Epoch - CurMainStart) / CurTimerange
    XX = MFLead + (MFGW * XX)
    return XX
########################################################
# BEGIN: plotMFPointClick(Grf, Line, Aux = "", e = None)
# FUNC:plotMFPointClick():2019.002
#   Handles all regular clicking on the MF Canvas.


def plotMFPointClick(Grf, Line, Aux="", e=None):
    global CurMainStart
    global CurMainEnd
    if MFPlotted is False:
        setMsg("MF", "RW", "Nothing is plotted.", 2)
        return
    LCan = PROGCan["MF"]
    if len(Grf) == 0:
        CX = LCan.canvasx(e.x)
        CY = LCan.canvasy(e.y)
# Check to see if the user did a regular click below the tick marks and scroll
# the timerange accordingly.
        if CY >= MFTickY:
            if CurMainStart != FStart or CurMainEnd != FEnd:
                # We will move 90% of the currently displayed range.
                MoveRange = yRange(CurMainStart, CurMainEnd) * .90
                if CX < LCan.winfo_width() / 2.0:
                    # Check to see if we are already against the start time.
                    if CurMainStart == FStart:
                        beep(1)
                        return
# Adjust the start and end times checking to make sure we don't try to plot
# stuff beyond either end of the file start and end times.
                    if CurMainStart - MoveRange < FStart:
                        CurMainStart = FStart
                        CurMainEnd = CurMainStart + MoveRange
                    else:
                        CurMainStart -= MoveRange
                        CurMainEnd -= MoveRange
                else:
                    if CurMainEnd == FEnd:
                        beep(1)
                        return
                    if CurMainEnd + MoveRange > FEnd:
                        CurMainEnd = FEnd
                        CurMainStart = CurMainEnd - MoveRange
                    else:
                        CurMainStart += MoveRange
                        CurMainEnd += MoveRange
                progControl("gozoom")
                plotMFClear("")
                Msge = plotMF(None)
                PROGStopBut.update()
                if PROGRunning.get() == 0:
                    setMsg("INFO")
                    setMsg("MF", "YB", "Stopped.", 2)
                    progControl("stopped")
                    return
                plotRAW()
                PROGStopBut.update()
                if PROGRunning.get() == 0:
                    setMsg("RAW", "YB", "Stopped.")
                    setMsg("INFO")
                    setMsg("MF", "YB", "Stopped.", 2)
                    progControl("stopped")
                    return
                plotTPS()
                PROGStopBut.update()
                if PROGRunning.get() == 0:
                    setMsg("RAW", "YB", "Stopped.")
                    setMsg("INFO")
                    setMsg("MF", "YB", "Stopped.", 2)
                    progControl("stopped")
                    return
                setMsg("MF", "", Msge)
                progControl("stopped")
            else:
                beep(1)
            return
    else:
        # These don't have anything to do with the LOG or ERR lines.
        if Grf.startswith("MP"):
            # The passed info for these are
            #     (Grf=Channel, Line=Epoch time, Aux=MassPos value)
            setMsg("MF", "", "%s-%s  %sV" % (Grf, dt2Time(-1, 80, Line), Aux))
            return
# These affect the ERR form lines.
        if Grf == "ERRN" or Grf == "ERRP":
            LFrm = PROGFrm["ERR"]
            if LFrm is None:
                setMsg("MF", "RW", "There is no ERR Messages window.", 2)
                return
# Clear these at the same time.
            if PROGFrm["LOG"] is not None:
                PROGTxt["LOG"].tag_delete("standout")
            LTxt = PROGTxt["ERR"]
            LTxt.tag_delete("standout")
            LTxt.yview(Line - 5)
            LTxt.tag_add("standout", "%d.0" % Line, "%d.end" % Line)
            if Grf == "ERRN":
                Bg = "TMJMPNegBG"
            elif Grf == "ERRP":
                Bg = "TMJMPPosBG"
            LTxt.tag_configure("standout", background=DClr[Bg],
                               foreground=Clr["B"])
            setMsg("ERR", "", "Line %d." % Line)
            LFrm.deiconify()
# 2018-12: Don't want this popping to the front all of the time.
#            LFrm.lift()
            return
# From here on we handle the regular graphs.
        LFrm = PROGFrm["LOG"]
        if LFrm is None:
            setMsg("MF", "RW", "There is no LOG Messages window.", 2)
            return
# Clear these at the same time.
        if PROGFrm["ERR"] is not None:
            PROGTxt["ERR"].tag_delete("standout")
        LTxt = PROGTxt["LOG"]
# Get rid of any previously selected line(s).
        LTxt.tag_delete("standout")
        LTxt.yview(Line - 5)
        LTxt.tag_add("standout", "%d.0" % Line, "%d.end" % Line)
        if Grf == "ACQ0":
            Bg = "ACQStopBG"
        elif Grf == "ACQ1":
            Bg = "ACQStartBG"
        elif Grf == "BKUP":
            Bg = Aux + "BG"
        elif Grf == "CLKP":
            Bg = "CLKPWR"
        elif Grf == "DCDIFF":
            Bg = "DCDIFFBG"
        elif Grf == "DEFS":
            Bg = "DEFSBG"
        elif Grf == "DISCREP":
            Bg = "DISCREPBG"
        elif Grf == "DRSET":
            Bg = "DRSETBG"
        elif Grf == "DU":
            Bg = "DUBG"
        elif Grf == "DUMP0":
            Bg = "DUMPOffBG"
        elif Grf == "DUMP1":
            Bg = "DUMPOnBG"
        elif Grf == "ERR":
            Bg = "ERRORBG"
        elif Grf == "EVT":
            Bg = "EVTBG"
        elif Grf == "GPS0":
            Bg = "GPSOffBG"
        elif Grf == "GPS1":
            Bg = "GPSOnBG"
        elif Grf == "GPSE":
            Bg = "GPSErrBG"
        elif Grf == "GPSL":
            Bg = "GPSLK"
        elif Grf == "ICPE":
            Bg = "ICPEBG"
        elif Grf == "JERK":
            Bg = "JERKBG"
        elif Grf == "MRC":
            Bg = "MRCBG"
        elif Grf == "NET0":
            Bg = "NETDownBG"
        elif Grf == "NET1":
            Bg = "NETUpBG"
        elif Grf == "PUP":
            Bg = "PWRUPBG"
        elif Grf == "RES":
            Bg = "RESETBG"
        elif Grf == "SOH":
            Bg = "SOHBG"
        elif Grf == "TEMP":
            Bg = "TEMPBG"
        elif Grf == "VOLT":
            Bg = "VOLTBG"
        elif Grf == "WARN":
            Bg = "WARNBG"
        elif Grf == "WARNM":
            Bg = "WARNBGM"
        else:
            beep(1)
            return
        LTxt.tag_configure("standout", background=DClr[Bg],
                           foreground=Clr["B"])
        setMsg("LOG", "", "Line %d." % Line)
        LFrm.deiconify()
        LFrm.lift()
    return
#######################
# BEGIN: plotMFReplot()
# FUNC:plotMFReplot():2018.344
#   For the Replot button on the main form.


def plotMFReplot():
    if MFPlotted is False:
        setMsg("MF", "RW", "There is nothing to replot.", 2)
        return
    plotMFClear("")
    plotRAWClear()
    plotTPSClear()
    progControl("go")
    Msge = plotMF(None)
    PROGStopBut.update()
    if PROGRunning.get() == 0:
        setMsg("INFO")
        setMsg("MF", "YB", "Stopped.", 2)
        progControl("stopped")
        return
    plotRAW()
    PROGStopBut.update()
    if PROGRunning.get() == 0:
        setMsg("RAW", "YB", "Stopped.")
        setMsg("INFO")
        setMsg("MF", "YB", "Stopped.", 2)
        progControl("stopped")
        return
    plotTPS()
    PROGStopBut.update()
    if PROGRunning.get() == 0:
        setMsg("TPS", "YB", "Stopped.")
        setMsg("INFO")
        setMsg("MF", "YB", "Stopped.", 2)
        progControl("stopped")
        return
    setMsg("MF", "", Msge)
    progControl("stopped")
    return
############################
# BEGIN: plotMFShiftClick(e)
# FUNC:plotMFShiftClick():2018.337
#   Handles shift-clicks on the MF Canvas.


def plotMFShiftClick(e):
    global CurMainStart
    global CurMainEnd
    global FirstSelRule
    global SecondSelRule
    global FirstSelRuleX
    global SecondSelRuleX
    global StartEndRecord
    if MFPlotted is False:
        beep(1)
        return
    LCan = PROGCan["MF"]
    Xx = LCan.canvasx(e.x)
# If the user has shift-clicked over the labels...
    if Xx < MFLead - 25:
        if FirstSelRule is not None:
            LCan.delete(FirstSelRule)
            FirstSelRule = None
            LCan.delete(SecondSelRule)
            SecondSelRule = None
        elif CurMainStart != FStart or CurMainEnd != FEnd:
            progControl("gozoom")
            Item = len(StartEndRecord)
# If there are no more remembered items then go back to the original range.
            if Item == 0:
                CurMainStart = FStart
                CurMainEnd = FEnd
            else:
                # "Pop" the last remembered range.
                Data = StartEndRecord[Item - 1]
                CurMainStart = Data[0]
                CurMainEnd = Data[1]
                del StartEndRecord[Item - 1]
            plotMFClear("")
            plotRAWClear()
            plotTPSClear()
            Msge = plotMF(None)
            PROGStopBut.update()
            if PROGRunning.get() == 0:
                setMsg("INFO")
                setMsg("MF", "YB", "Stopped.", 2)
                progControl("stopped")
                return
            plotRAW()
            PROGStopBut.update()
            if PROGRunning.get() == 0:
                setMsg("RAW", "YB", "Stopped.")
                setMsg("INFO")
                setMsg("MF", "YB", "Stopped.", 2)
                progControl("stopped")
                return
            plotTPS()
            PROGStopBut.update()
            if PROGRunning.get() == 0:
                setMsg("TPS", "YB", "Stopped.")
                setMsg("INFO")
                setMsg("MF", "YB", "Stopped.", 2)
                progControl("stopped")
                return
            setMsg("MF", "WB", Msge)
            progControl("stopped")
        else:
            beep(1)
        return
# If the user shift-clicks just to the left of the plots then set Xx to the
# beginning of the plots.
    elif Xx < MFLead:
        Xx = MFLead
# If the user clicks beyond the end of the plot then set Xx to the end of the
# plots.
    elif Xx > MFLead + MFGW:
        Xx = MFLead + MFGW
# If this is the first shift-click...
    if FirstSelRule is None:
        FirstSelRule = LCan.create_line(Xx, TopSpace, Xx,
                                        CHeight - TopSpace - BotSpace / 2,
                                        fill=DClr["Sel"])
        FirstSelRuleX = Xx
# If this is the second shift-click...
    elif SecondSelRule is None:
        SecondSelRule = LCan.create_line(Xx, TopSpace, Xx,
                                         CHeight - TopSpace - BotSpace / 2,
                                         fill=DClr["Sel"])
        SecondSelRuleX = Xx
        updateMe(0)
        sleep(.2)
# If the user clicks in the same place twice then just cancel the whole thing.
        if FirstSelRuleX == SecondSelRuleX:
            LCan.delete(FirstSelRule)
            FirstSelRule = None
            LCan.delete(SecondSelRule)
            SecondSelRule = None
        else:
            progControl("gozoom")
# Remember the current range before we set things up for the new range.
            StartEndRecord.append((CurMainStart, CurMainEnd))
# The user can click the two points in either time order.
            if FirstSelRuleX < SecondSelRuleX:
                Wait = plotMFEpochAtX(FirstSelRuleX)
                CurMainEnd = plotMFEpochAtX(SecondSelRuleX)
                CurMainStart = Wait
            else:
                Wait = plotMFEpochAtX(SecondSelRuleX)
                CurMainEnd = plotMFEpochAtX(FirstSelRuleX)
                CurMainStart = Wait
# Plot the new range.
            plotMFClear("")
            plotRAWClear()
            plotTPSClear()
            Msge = plotMF(None)
            PROGStopBut.update()
            if PROGRunning.get() == 0:
                setMsg("INFO")
                setMsg("MF", "YB", "Stopped.", 2)
                progControl("stopped")
                return
            plotRAW()
            PROGStopBut.update()
            if PROGRunning.get() == 0:
                setMsg("RAW", "YB", "Stopped.")
                setMsg("INFO")
                setMsg("MF", "YB", "Stopped.", 2)
                progControl("stopped")
                return
            plotTPS()
            PROGStopBut.update()
            if PROGRunning.get() == 0:
                setMsg("TPS", "YB", "Stopped.")
                setMsg("INFO")
                setMsg("MF", "YB", "Stopped.", 2)
                progControl("stopped")
                return
            setMsg("MF", "WB", Msge)
            progControl("stopped")
    return
###############################
# BEGIN: plotMFTime(Who, Epoch)
# FUNC:plotMFTime():2013.035
#   For external callers.


def plotMFTime(Who, Epoch):
    if MFPlotted is False:
        return
    if Epoch == -1:
        PROGCan["MF"].delete("Clock")
        setMsg("MF", "", "")
    else:
        CX = plotMFXAtEpoch(Epoch)
        plotMFClock(Who, CX)
    return


##################################
# BEGIN: plotMFTimeClick(e = None)
# FUNC:plotMFTimeClick():2012.103
#   For time clicks (i.e. Control-Button-1) on the MF Canvas.
MFGridOn = False


def plotMFTimeClick(e=None):
    global MFGridOn
    if MFPlotted is False:
        beep(1)
        return
    LCan = PROGCan["MF"]
    CX = LCan.canvasx(e.x)
    CY = LCan.canvasx(e.y)
# If the click is way to the left erase everything (MF stuff done above).
    if CX < MFLead / 2:
        LCan.delete("Clock")
        LCan.delete("Grid")
        MFGridOn = False
        plotRAWTime(-1)
        plotTPSTime(-1)
# If the click is on the plots above the bottom tick marks.
    elif CY < MFTickY:
        Epoch = plotMFClock("MF", CX)
        plotRAWTime(Epoch)
        plotTPSTime(Epoch)
        return
# Must be below the tick marks. Turn the grid on or off.
    elif CY >= MFTickY:
        if MFGridOn is False:
            for CX in TickX:
                LCan.create_line(CX, MFTickY, CX, TopSpace,
                                 fill=DClr["Grid"], tags="Grid")
            MFGridOn = True
        else:
            LCan.delete("Grid")
            MFGridOn = False
    return
######################################################
# BEGIN: plotMFZapClick(Grf, Arg, Arg2 = "", e = None)
# FUNC:plotMFZapClick():2016.235
#   Removes a point from existance.


def plotMFZapClick(Grf, Arg, Arg2="", e=None):
    global MFZapped
    if MFPlotted is False:
        beep(1)
        return
    if Grf == "DCDIFF" or Grf == "ICPE" or Grf == "TEMP" or Grf == "VOLT":
        TheData = eval(Grf)
    else:
        stdout.write("ZapClick: %s\n" % Grf)
        beep(1)
        return
    for Data in TheData:
        if Data[0] == Arg:
            TheData.remove(Data)
            break
    MFZapped += 1
    plotMFClear("")
    plotMF("MF")
    return
# END: plotMF


##################
# BEGIN: plotRAW()
# FUNC:plotRAW():2018.348
RAWFitTallRVar = StringVar()
RAWFitTallRVar.set("fit")
RAWFitWideRVar = StringVar()
RAWFitWideRVar.set("fit")
PROGSetups += ["RAWFitTallRVar", "RAWFitWideRVar"]
# The size of the plotted bits, not the window.
# This may get changed by the Fit/Tall radiobuttons.
RAWGH = 125.0
# This may get changed by the Fit/Wide radiobuttons.
RAWGW = 2000.0
RAWXMargin = 25.0
RAWPlotted = False
RAWClockBottom = 0


def plotRAW():
    global RAWPlotted
    global RAWClockBottom
    global RAWGH
    global RAWGW
# If the user doesn't want to do the RAW plot get rid of it if it is up.
    if OPTPlotRAWCVar.get() == 0:
        formClose("RAW")
        return
# Bring up the form if necessary.
    if PROGFrm["RAW"] is None:
        LFrm = PROGFrm["RAW"] = Toplevel(Root)
        LFrm.withdraw()
        LFrm.protocol("WM_DELETE_WINDOW", Command(beep, 1))
        Sub = Frame(LFrm)
        SSub = Frame(Sub)
        WH = Root.geometry().split("+", 1)[0]
        Parts = WH.split("x")
        Width = int(Parts[0])
        Height = int(Parts[1])
# This gets the window about the right size for 3 channels of plots, the text,
# times and other fudging.
        Height = RAWGH * 3 + PROGPropFontHeight * 15
        Width = Width * .90
        LCan = PROGCan["RAW"] = Canvas(SSub, bg=DClr["MFCAN"],
                                       relief=SUNKEN, height=Height,
                                       width=Width,
                                       scrollregion=(0, 0, Width, Height))
        LCan.pack(side=LEFT, expand=YES, fill=BOTH)
        LCan.bind("<Control-Button-1>", plotRAWTimeClick)
        Sb = Scrollbar(SSub, orient=VERTICAL, command=LCan.yview)
        Sb.pack(side=RIGHT, expand=NO, fill=Y)
        LCan.configure(yscrollcommand=Sb.set)
        SSub.pack(side=TOP, expand=YES, fill=BOTH)
        Sb = Scrollbar(Sub, orient=HORIZONTAL, command=LCan.xview)
        Sb.pack(side=BOTTOM, expand=NO, fill=X)
        LCan.configure(xscrollcommand=Sb.set)
        Sub.pack(side=TOP, expand=YES, fill=BOTH)
        Sub = Frame(LFrm)
        BButton(Sub, text="Write .ps",
                command=Command(formWritePS, "RAW", "RAW",
                                LastRAWPSFilespecVar)).pack(side=LEFT)
        SSub = Frame(Sub)
        LRb = Radiobutton(SSub, text="Fit Y", variable=RAWFitTallRVar,
                          value="fit")
        LRb.pack(side=TOP, anchor="w")
        ToolTip(LRb, 35,
                "Select this to plot/replot the raw data so all of the plots "
                "will fit in the current window (150 pixels max height for "
                "the plots on large displays).")
        LRb = Radiobutton(SSub, text="Tall", variable=RAWFitTallRVar,
                          value="tall")
        LRb.pack(side=TOP, anchor="w")
        ToolTip(LRb, 35,
                "Select this to plot/replot the raw data so the Y-axis is 150 "
                "pixels tall for each plot.")
        SSub.pack(side=LEFT)
        SSub = Frame(Sub)
        LRb = Radiobutton(SSub, text="Fit X", variable=RAWFitWideRVar,
                          value="fit")
        LRb.pack(side=TOP, anchor="w")
        ToolTip(LRb, 35,
                "Select this to plot/replot the raw data so the X-axis of all "
                "of the plots fit in the current window.")
        LRb = Radiobutton(SSub, text="Wide", variable=RAWFitWideRVar,
                          value="wide")
        LRb.pack(side=TOP, anchor="w")
        ToolTip(LRb, 35,
                "Select this to plot/replot the raw data so the X-axis is "
                "spread out over 2000 pixels (for small displays).")
        SSub.pack(side=LEFT)
        PROGMsg["RAW"] = Text(Sub, font=PROGPropFont, height=2,
                              wrap=WORD, cursor="")
        PROGMsg["RAW"].pack(side=LEFT, fill=X, expand=YES)
        LLb = Label(Sub, text=" Hints ")
        LLb.pack(side=LEFT)
        ToolTip(LLb, 35, "PLOT AREA HINT:\n--Control-click: Control-click to "
                "show the time at the position of the click.\n--Control-click "
                "on left: Control-click on the far left to remove the time "
                "marker.")
        Sub.pack(side=TOP, fill=X, expand=NO)
        center(Root, LFrm, "W", "O", True)
    else:
        LFrm = PROGFrm["RAW"]
        LCan = PROGCan["RAW"]
        plotRAWClear()
    if RAWFitWideRVar.get() == "fit":
        RAWGW = LCan.winfo_width() - 2 * RAWXMargin
    elif RAWFitWideRVar.get() == "wide":
        RAWGW = 2000
    LFrm.title("Raw Data Plot - %s" % MainFilename)
# Just in case.
    if len(RawData) == 0:
        setMsg("RAW", "", "%s: No raw data found for selected data streams." %
               MainFilename)
        return
    ColorMode = PROGColorModeRVar.get()
# Collect the state of the data stream checkbuttons.
    DSsButts = []
    for i in arange(1, 1 + PROG_DSS):
        if eval("Stream%dVar" % i).get() == 1:
            DSsButts.append(i)
    if len(DSsButts) == 0:
        setMsg("RAW", "", "No DS (Data Stream) checkbuttons were selected.")
        return
# Just in case.
    CurMainTimerange = CurMainEnd - CurMainStart
    if CurMainTimerange <= 0.0:
        setMsg("RAW", "", "%s: Not enough time selected to plot anything." %
               MainFilename)
        return
# Finish gatering info before starting.
    PlotMass123 = False
    if DGrf["MP123"].get() == 1:
        PlotMass123 = True
    PlotMass456 = False
    if DGrf["MP456"].get() == 1:
        PlotMass456 = True
    if RAWFitTallRVar.get() == "fit":
        Graphs = len(list(RawData.keys()))
# Height - stuff at the top - stuff above plots - stuff at bottom.
        RAWGH = (LCan.winfo_height() -
                 (PROGPropFontHeight * (4 + (3 * Graphs) + 4))) / Graphs
        if RAWGH > 150:
            RAWGH = 150
    elif RAWFitTallRVar.get() == "tall":
        RAWGH = 150
    progControl("gozoom")
    setMsg("MF", "CB", "Plotting raw data...")
# The keys are (DS, Chan).
    Plots = sorted(RawData.keys())
    GY = PROGPropFontHeight
# Display the actual time of the data, because this may differ greatly from the
# time range of the SOH messages (there may be "old" SOH messages that got
# recorded before acquisition was started, for example).
    LCan.create_text(10, GY, anchor="w", fill=DClr["RAWT"],
                     font=PROGPropFont, text="%s: Time range: %s to %s" %
                     (MainFilename, dt2Time(-1, 80, CurMainStart)[:-4],
                      dt2Time(-1, 80, CurMainEnd)[:-4]))
    GY += PROGPropFontHeight * 1.5
# We know how wide the plot area is in pixels and in time, so figure out how
# much time per pixel there will be and then go through the points and bin
# as needed.
    TimerangePerPixel = CurMainTimerange / RAWGW
# Just so these four exists if the loops goes bad.
    BinStart = 0
    BinEnd = 0
    BinMin = maxInt
    BinMax = -maxInt
# Main loop. Each iteration plots a DS/Chan's plot.
    for Plot in Plots:
        setMsg("RAW", "CB", "Plotting data plot DS%d CH%d..." % (Plot[0],
                                                                 Plot[1]))
        TheData = RawData[Plot]
# Normally there should always be something to plot, otherwise nothing would
# have been extracted, or this condition would have been caught by the
# DataTimerange above, but you never know.
        if len(TheData) == 0:
            if ColorMode == "B":
                C = "Y"
            elif ColorMode == "W":
                C = "O"
            LCan.create_text(10, GY, anchor="w", fill=Clr[C],
                             font=PROGPropFont,
                             text="Data stream: %d  Channel: %d  No data to "
                             "plot." % (Plot[0], Plot[1]))
            GY += PROGPropFontHeight * 2
            continue
# Go through all of the data points for this ds/chan and count how many data
# points there is going to be over the ManTimerange and how much of the full
# ploting width (RAWGW) will be used (i.e. the data may not start when the log
# information did). Also get the max and min data values for scaling.
        DataMin = maxInt
        DataMax = -maxInt
        DataCount = 0
# TESTING
#        if Plot[0] == 1 and Plot[1] == 1:
#            for Point in TheData:
#                stdout.write("%s\n"%Point)
#            return
# Since I'm going to go through all of the data points anyway, I might as well
# set these variables which will help the plotting routine go a little faster
# when zoomed in. These will be TheData index positions.
        FirstGoodSample = -1
        LastGoodSample = 0
        Index = 0
        for DataPoint in TheData:
            if DataPoint[0] >= CurMainStart:
                # Jump over any points that are beyond the end time, but keep
                # going. There can be timing errors where the time jumps way
                # ahead, but then returns.
                if DataPoint[0] > CurMainEnd:
                    continue
                DataCount += 1
                if DataPoint[1] > DataMax:
                    DataMax = DataPoint[1]
                if DataPoint[1] < DataMin:
                    DataMin = DataPoint[1]
                if FirstGoodSample == -1:
                    FirstGoodSample = Index
                LastGoodSample = Index
            Index += 1
# If no data was found in the current time range...
        if DataCount == 0:
            if ColorMode == "B":
                C = "Y"
            elif ColorMode == "W":
                C = "O"
            LCan.create_text(10, GY, anchor="w", fill=Clr[C],
                             font=PROGPropFont,
                             text="Data stream: %d  Channel: %d  No data in "
                                  "timerange/%s samples" %
                                  (Plot[0], Plot[1], fmti(len(TheData))))
            GY += PROGPropFontHeight * 2
            continue
        DataRange = float(getRange(DataMin, DataMax))
        if DataRange == 0.0:
            DataRange = 1.0
# DataNorm will be added to the data values so that the lowest of the values to
# be plotted will have a value of 0 and all values will go up from there to
# make figuring out where to plot a point easier.
        if DataMin > 0:
            DataNorm = float(-DataMin)
        else:
            DataNorm = float(abs(DataMin))
# We're ready to start.
        LCan.create_text(10, GY, anchor="w", fill=DClr["RAWT"],
                         font=PROGPropFont,
                         text="Data stream: %d     Channel: %d     "
                              "Points: %s / %s     Max/Min: %s / %s     "
                              "Range: %s" % (Plot[0], Plot[1], fmti(DataCount),
                                             fmti(len(TheData)), fmti(DataMax),
                                             fmti(DataMin), fmti(DataRange)))
        GY += PROGPropFontHeight
# GGY will be the lowest point/bottom of each plot.
        GGY = GY + RAWGH
# As each bin is filled this will be reset to the last point that was used so
# the for loop below will not have to start over at the beginning of the data
# each time through.
        Cmd = []
        LastUsedi = FirstGoodSample
        Jumps = []
        for Pixel in arange(0, int(RAWGW)):
            i = LastUsedi
# Calculate this each time to minimize rounding errors.
            BinStart = CurMainStart + (Pixel * TimerangePerPixel)
            BinEnd = BinStart + TimerangePerPixel
            BinMin = maxInt
            BinMax = -maxInt
# Look through the data points to find points that are in the current bin.
            while i <= LastGoodSample:
                Point = TheData[i]
                SampleTime = Point[0]
# If the point is way out of range then skip on.
                if SampleTime < CurMainStart or SampleTime > CurMainEnd:
                    i += 1
                    continue
# Too early...
                if SampleTime < BinStart:
                    # If BinMin is is not maxInt it means that points were
                    # found for this bin, so do the right thing, and then go on
                    # to the next bin (like there was a jump backwards in
                    # time).
                    if BinMin != maxInt:
                        PlotMin = GGY - \
                            ((BinMin + DataNorm) / DataRange) * RAWGH
                        PlotMax = GGY - \
                            ((BinMax + DataNorm) / DataRange) * RAWGH
                        Cmd += [RAWXMargin + Pixel, PlotMin,
                                RAWXMargin + Pixel, PlotMax]
# Set this to indicate that all points have been processed.
                        BinMin = maxInt
                        break
# 2018-12: FINISHME-Working on showing jumps...someday.
                    i += 1
#                    if i > LastGoodSample:
#                        if -Pixel not in Jumps:
#                            Jumps.append(-Pixel)
                    continue
# Too late...
                if SampleTime > BinEnd:
                    # If BinMin is is not maxInt it means that points were
                    # found for this bin, so do the right thing, then go on to
                    # the next bin (like there was a jump forward in time).
                    if BinMin != maxInt:
                        PlotMin = GGY - \
                            ((BinMin + DataNorm) / DataRange) * RAWGH
                        PlotMax = GGY - \
                            ((BinMax + DataNorm) / DataRange) * RAWGH
                        Cmd += [RAWXMargin + Pixel, PlotMin,
                                RAWXMargin + Pixel, PlotMax]
                        BinMin = maxInt
                        break
#                    i += 1
#                    if i < LastGoodSample:
#                        if Pixel not in Jumps:
#                            Jumps.append(Pixel)
                    break
# In the bin...
                if Point[1] < BinMin:
                    BinMin = Point[1]
                if Point[1] > BinMax:
                    BinMax = Point[1]
                LastUsedi = i
                i += 1
# This should be the normal thing to happen at the end of the data when the
# while runs out.
            if BinMin != maxInt:
                PlotMin = GGY - ((BinMin + DataNorm) / DataRange) * RAWGH
                PlotMax = GGY - ((BinMax + DataNorm) / DataRange) * RAWGH
                Cmd += [RAWXMargin + Pixel, PlotMin,
                        RAWXMargin + Pixel, PlotMax]
                BinMin = maxInt
# Just in case. I don't think this will ever happen.
        if BinMin != maxInt:
            PlotMin = GGY - ((BinMin + DataNorm) / DataRange) * RAWGH
            PlotMax = GGY - ((BinMax + DataNorm) / DataRange) * RAWGH
            Cmd += [RAWXMargin + Pixel, PlotMin, RAWXMargin + Pixel, PlotMax]
# Plot what we got.
        if len(Cmd) > 0:
            if DataMin < 0.0 and 0.0 < DataMax:
                PlotZero = GGY - (DataNorm / DataRange) * RAWGH
                LCan.create_line((RAWXMargin, PlotZero, RAWXMargin + RAWGW,
                                  PlotZero), fill=Clr["U"])
            LCan.create_line(Cmd, fill=DClr["RAWD"])
# Shows time jumps backwards and forwards.
            for Jump in Jumps:
                if Jump < 0:
                    LCan.create_polygon(
                        RAWXMargin + abs(Jump) - 3, PlotZero - 10,
                        RAWXMargin + abs(Jump) + 3, PlotZero - 10 + 3,
                        RAWXMargin + abs(Jump) + 3, PlotZero - 10 - 3,
                        RAWXMargin + abs(Jump) - 3, PlotZero - 10,
                        fill=Clr["O"], outline=Clr["O"])
                else:
                    LCan.create_polygon(
                        RAWXMargin + Jump + 3, PlotZero - 10,
                        RAWXMargin + Jump - 3, PlotZero - 10 + 3,
                        RAWXMargin + Jump - 3, PlotZero - 10 - 3,
                        RAWXMargin + Jump + 3, PlotZero - 10,
                        fill=Clr["U"], outline=Clr["U"])
        else:
            LCan.create_text(RAWXMargin, GY, anchor="w",
                             fill=DClr["RAWT"], font=PROGPropFont,
                             text="No data points for this channel in the "
                             "selected timerange.")
        GY += RAWGH
# If the user is looking for mass positions, and there is mass position info
# then let 'er plot.
        if PlotMass123 is True or PlotMass456 is True:
            Chan = Plot[1]
# Just beyond the bottom of the plot.
            GM = GY + 4
            Masses = eval("MP%d" % Chan)
            if (PlotMass123 is True and (Chan >= 1 and Chan <= 3) and
                    len(Masses) > 0) or \
                    (PlotMass456 is True and (Chan >= 4 and Chan <= 6) and
                     len(Masses) > 0):
                for Mass in Masses:
                    if Mass[0] >= CurMainStart and Mass[0] <= CurMainEnd:
                        XX = RAWXMargin + (Mass[0] - CurMainStart) / \
                            CurMainTimerange * RAWGW
                        if Mass[2] == 0:
                            C = "MPP"
                        elif Mass[2] == 1:
                            C = "MPG"
                        elif Mass[2] == 2:
                            C = "MPB"
                        elif Mass[2] == 3:
                            C = "MPU"
                        elif Mass[2] == 4:
                            C = "MPH"
                        ID = LCan.create_rectangle(XX - 2, GM - 2, XX + 2,
                                                   GM + 2, fill=DClr[C],
                                                   outline=DClr[C])
                        LCan.tag_bind(ID, "<Button-1>",
                                      Command(plotRAWShowMass, Chan, Mass[0],
                                              Mass[1]))
                        LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
            GY += PROGPropFontHeight
        GY += PROGPropFontHeight * 1.5
        L, T, R, B = LCan.bbox(ALL)
        LCan.configure(scrollregion=(0, 0, R + RAWXMargin, (B + 15)))
        updateMe(0)
        if PROGRunning.get() == 0:
            return
# Add just a little more space between the bottom of the last plot and the axis
# stuff mostly so the recenter dots (below) don't hit a pegged-low signal.
    GY += 5
# Now the X-axis scale across the bottom of the plot area.
    LCan.create_line((RAWXMargin, GY, RAWXMargin +
                      RAWGW, GY), fill=DClr["RAWT"])
# Plot tick marks showing 10 days, 1 day, hours, minutes or seconds depending
# on the range of the currently displayed plot.
    Zero = dt2Time(-1, 11, CurMainStart)
# 30 days.
    if CurMainTimerange >= 2592000.0:
        Zero = Zero[:9] + "00:00:00"
        Add = 864000.0
    elif CurMainTimerange >= 864000.0:
        Zero = Zero[:9] + "00:00:00"
        Add = 86400.0
    elif CurMainTimerange >= 3600.0:
        Zero = Zero[:12] + "00:00"
        Add = 3600.0
    elif CurMainTimerange >= 60.0:
        Zero = Zero[:15] + "00"
        Add = 60.0
    else:
        Zero = Zero[:17]
        Add = 1.0
    Time = dt2Timeystr2Epoch(None, Zero)
    First = True
    LastTimeEndX = 0.0
    while True:
        Time += Add
        if Time < CurMainEnd:
            if Time >= CurMainStart:
                GX = RAWXMargin + RAWGW * \
                    ((Time - CurMainStart) / CurMainTimerange)
                LCan.create_line((GX, GY - 2, GX, GY), fill=DClr["RAWT"])
                Zero = dt2Time(-1, 80, Time)[:-4]
                Length = PROGPropFont.measure(Zero)
# Print the date/time of the first tick and all of the ones there is room for
# after that.
                if First is True or (GX - Length / 2) > (LastTimeEndX + 100):
                    LCan.create_line((GX, GY - 6, GX, GY), fill=DClr["RAWT"])
# Make sure the times are not off either edge of the display.
                    if (GX - Length / 2) < 1:
                        GX = RAWXMargin + Length / 2
                    if (GX + Length / 2) > (RAWXMargin + RAWGW):
                        GX = (RAWXMargin + RAWGW) - Length / 2
# This won't take into account the first time needing to be shoved to the
# right a little, but there should always be enough space.
                    LastTimeEndX = GX + Length / 2
                    LCan.create_text(GX, GY + (PROGPropFontHeight / 2),
                                     anchor="n", fill=DClr["RAWT"],
                                     font=PROGPropFont, text=Zero)
                    First = False
        else:
            break
# Preserve this for the control-click clock rule. The tall ticks above are
# GY-6, so make this a little higher.
    RAWClockBottom = GY - 8
# Add the recentering pulses along the axis.
    for Data in MRC:
        if Data[1] >= CurMainStart:
            if Data[1] <= CurMainEnd:
                GX = RAWXMargin + RAWGW * \
                    (Data[1] - CurMainStart) / CurMainTimerange
                ID = LCan.create_rectangle(GX - 2, GY - 2 - 12, GX + 2,
                                           GY + 2 - 12, fill=DClr["MRC"],
                                           outline=DClr["MRC"])
                LCan.tag_bind(ID, "<Button-1>",
                              Command(plotRAWShowTime, Data[1]))
                LCan.tag_bind(ID, "<Shift-Button-1>", nullCall)
    L, T, R, B = LCan.bbox(ALL)
    LCan.configure(scrollregion=(0, 0, R + RAWXMargin, (B + 15)))
    PROGStopBut.update()
    if PROGRunning.get() == 0:
        return
    else:
        setMsg("MF", "", "")
        setMsg("RAW", "", "")
    RAWPlotted = True
    return
#######################
# BEGIN: plotRAWClear()
# FUNC:plotRAWClear():2018.236


def plotRAWClear():
    global RAWPlotted
    if PROGFrm["RAW"] is not None:
        PROGFrm["RAW"].title("Raw Data Plot")
        LCan = PROGCan["RAW"]
        LCan.delete(ALL)
        LCan.xview_moveto(0.0)
        LCan.yview_moveto(0.0)
        LCan.configure(bg=DClr["MFCAN"])
        setMsg("RAW", "", "")
    RAWPlotted = False
    return
###################################################
# BEGIN: plotRAWShowMass(Chan, TimeValue, MassValue, e)
# FUNC:plotRAWShowMass():2013.037


def plotRAWShowMass(Chan, TimeValue, MassValue, e):
    setMsg("RAW", "", "Channel %d mass position voltage %.1fV at "
           "%s." % (Chan, MassValue, dt2Time(-1, 80, TimeValue)[:-4]))
    return
##################################
# BEGIN: plotRAWShowTime(Value, e)
# FUNC:plotRAWShowTime():2013.037


def plotRAWShowTime(Value, e):
    setMsg("RAW", "", "Mass recenter time %s." % dt2Time(-1, 80, Value)[:-4])
    return
############################
# BEGIN: plotRAWTimeClick(e)
# FUNC:plotRAWTimeClick():2013.035
#   For time clicks (i.e. Control-Button-3) on the RAW Canvas().


def plotRAWTimeClick(e):
    if RAWPlotted is False:
        beep(1)
    else:
        LCan = PROGCan["RAW"]
        CX = LCan.canvasx(e.x)
        CY = LCan.canvasy(e.y)
# Way to the left.
        if CX < RAWXMargin / 2:
            LCan.delete("Clock")
            setMsg("TPS", "", "")
            plotMFTime("RAW", -1)
            plotTPSTime(-1)
# Somewhere on the plots.
        else:
            if CY < RAWClockBottom:
                Epoch = plotRAWClock(CX)
                plotMFTime("RAW", Epoch)
                plotTPSTime(Epoch)
# Blow the plots.
            else:
                beep(1)
    return
###########################
# BEGIN: plotRAWTime(Epoch)
# FUNC:plotRAWTime():2018.236
#   For external callers to control the RAW clock rule.


def plotRAWTime(Epoch):
    if PROGFrm["RAW"] is None or RAWPlotted is False:
        return
    if Epoch == -1:
        PROGCan["RAW"].delete("Clock")
        setMsg("RAW", "", "")
    else:
        RAWCX = plotRAWXAtEpoch(Epoch)
        plotRAWClock(RAWCX)
    return
############################
# BEGIN: plotRAWEpochAtX(CX)
# FUNC:plotRAWEpochAtX():2010.251
#   Takes the passed CX pixel location and returns the epoch time at that
#   plot location.


def plotRAWEpochAtX(CX):
    # How far across the plot did we click?
    Epoch = (CX - RAWXMargin) / RAWGW
    Epoch = CurMainStart + (CurTimerange * Epoch)
    return Epoch
###############################
# BEGIN: plotRAWXAtEpoch(Epoch)
# FUNC:plotRAWXAtEpoch():2010.251
#   Takes the passed Epoch and figures out where on the RAW plots that is.


def plotRAWXAtEpoch(Epoch):
    XX = (Epoch - CurMainStart) / CurTimerange
    XX = RAWXMargin + (RAWGW * XX)
    return XX
#########################
# BEGIN: plotRAWClock(CX)
# FUNC:plotRAWClock():2013.037


def plotRAWClock(CX):
    LCan = PROGCan["RAW"]
    LCan.delete("Clock")
# Select min or max time if a little off-plot.
    if CX < RAWXMargin:
        CX = RAWXMargin
    elif CX > RAWXMargin + RAWGW:
        CX = RAWXMargin + RAWGW
    Epoch = plotRAWEpochAtX(CX)
# Draw the rule and put the time in the message field.
    LCan.create_line(CX, PROGPropFontHeight * 3, CX,
                     RAWClockBottom, fill=DClr["Sel"], tags="Clock")
    setMsg("RAW", "", "Time: %s" % dt2Time(-1, 80, Epoch)[:-4])

    return Epoch
# END: plotRAW


##################
# BEGIN: plotTPS()
# FUNC:plotTPS():2018.337
TPSColorRangeRVar = StringVar()
TPSColorRangeRVar.set("med")
PROGSetups += ["TPSColorRangeRVar"]
TPSStart = 0
TPSPlotted = False


def plotTPS():
    global TPSStart
    global TPSPlotted
# If the user doesn't want to do the TPS plot get rid of it if it is up.
    if OPTPlotTPSCVar.get() == 0:
        formClose("TPS")
        TPSPlotted = False
        return
# Create the form if necessary.
    if PROGFrm["TPS"] is None:
        LFrm = PROGFrm["TPS"] = Toplevel(Root)
        LFrm.withdraw()
        LFrm.protocol("WM_DELETE_WINDOW", Command(beep, 1))
        Sub = Frame(LFrm)
        SSub = Frame(Sub)
# 915 = 25+(3*288)+25, plus 1
        LCan = PROGCan["TPS"] = Canvas(SSub, bg=DClr["MFCAN"],
                                       relief=SUNKEN, height=500, width=915,
                                       scrollregion=(0, 0, 915, 500))
        LCan.bind("<Button-1>", Command(plotTPSTimeClick, -1, ""))
        LCan.bind("<Control-Button-1>", Command(plotTPSTimeClick, -1, ""))
        LCan.pack(side=LEFT, expand=YES, fill=BOTH)
        Sb = Scrollbar(SSub, orient=VERTICAL, command=LCan.yview)
        Sb.pack(side=RIGHT, expand=NO, fill=Y)
        LCan.configure(yscrollcommand=Sb.set)
        SSub.pack(side=TOP, expand=YES, fill=BOTH)
        Sb = Scrollbar(Sub, orient=HORIZONTAL, command=LCan.xview)
        Sb.pack(side=BOTTOM, expand=NO, fill=X)
        LCan.configure(xscrollcommand=Sb.set)
        Sub.pack(side=TOP, expand=YES, fill=BOTH)
        Sub = Frame(LFrm)
        LRb = Radiobutton(Sub, text="A", variable=TPSColorRangeRVar,
                          value="antarctica")
        LRb.pack(side=LEFT)
        ToolTip(LRb, 30, "\"Antarctica\" color range.")
        LRb = Radiobutton(Sub, text="L", variable=TPSColorRangeRVar,
                          value="low")
        LRb.pack(side=LEFT)
        ToolTip(LRb, 30, "\"Low\" color range.")
        LRb = Radiobutton(Sub, text="M", variable=TPSColorRangeRVar,
                          value="med")
        LRb.pack(side=LEFT)
        ToolTip(LRb, 30, "\"Medium\" color range.")
        LRb = Radiobutton(Sub, text="H", variable=TPSColorRangeRVar,
                          value="high")
        LRb.pack(side=LEFT)
        ToolTip(LRb, 30, "\"High\" color range.")
        Label(Sub, text=" ").pack(side=LEFT)
        BButton(Sub, text="Replot",
                command=plotTPSReplot).pack(side=LEFT)
        BButton(Sub, text="Write .ps",
                command=Command(formWritePS, "TPS", "TPS",
                                LastTPSPSFilespecVar)).pack(side=LEFT)
        Label(Sub, text=" ").pack(side=LEFT)
        PROGMsg["TPS"] = Text(Sub, font=PROGPropFont, height=2,
                              wrap=WORD, cursor="")
        PROGMsg["TPS"].pack(side=LEFT, fill=X, expand=YES)
        LLb = Label(Sub, text=" Hints ")
        LLb.pack(side=LEFT)
        ToolTip(LLb, 35, "PLOT AREA HINT:\n--Click on plot: Click on a point "
                "on the plot to show the time and signal level value at that "
                "position and show a rule at that position in time on the "
                "main plot display and on the raw data display. (Control-"
                "click also does this.)\n--Click on left: Click on the far "
                "left to clear the time and signal value.\n--Click on dotted "
                "line: Clicking on a dotted day line will display the number "
                "of days that were skipped to produced that dotted day line.")
        Sub.pack(side=TOP, fill=X, expand=NO)
        center(Root, LFrm, "E", "O", True)
        TPSPlotted = False
    else:
        LFrm = PROGFrm["TPS"]
        LCan = PROGCan["TPS"]
        plotTPSClear()
    LFrm.title("TPS Plot - %s" % MainFilename)
# Collect the state of the data stream checkbuttons.
    DSsButts = []
    ColorMode = PROGColorModeRVar.get()
    for i in arange(1, 1 + PROG_DSS):
        if eval("Stream%dVar" % i).get() == 1:
            DSsButts.append(i)
    if len(DSsButts) == 0:
        setMsg("TPS", "", "No DS (Data Stream) checkbuttons were selected.")
        return
# Just in case.
    if len(RawData) == 0:
        setMsg("TPS", "", "%s: No raw data found for selected data streams." %
               MainFilename)
        return
    ColorRange = TPSColorRangeRVar.get()
# Set the ranges for the colors based on the A H M L radiobuttons.
    if ColorRange == "antarctica":
        Bin0 = 0.0
        Bin1 = 100.0   # 10
        Bin2 = 10000.0   # 100
        Bin3 = 1000000.0   # 1000
        Bin4 = 100000000.0   # 10000
        Bin5 = 10000000000.0   # 100000
        Bin6 = 1000000000000.0   # 1000000
    elif ColorRange == "low":
        Bin0 = 0.0
        Bin1 = 400.0   # 20
        Bin2 = 40000.0   # 200
        Bin3 = 4000000.0   # 2000
        Bin4 = 400000000.0   # 20000
        Bin5 = 40000000000.0   # 200000
        Bin6 = 4000000000000.0   # 2000000
    elif ColorRange == "med":
        Bin0 = 0.0
        Bin1 = 2500.0   # 50
        Bin2 = 250000.0   # 500
        Bin3 = 25000000.0   # 5000
        Bin4 = 2500000000.0   # 50000
        Bin5 = 250000000000.0   # 500000
        Bin6 = 25000000000000.0   # 5000000
    elif ColorRange == "high":
        Bin0 = 0.0
        Bin1 = 6400.0   # 80
        Bin2 = 640000.0   # 800
        Bin3 = 64000000.0   # 8000
        Bin4 = 6400000000.0   # 80000
        Bin5 = 640000000000.0   # 800000
        Bin6 = 64000000000000.0   # 8000000
# Will be a list of lists with a list for each 5-minute bin as
#    [[sum of squared counts, number of data points], ...]
    PlotData = []
# The keys are (DS, Chan).
    Plots = sorted(RawData.keys())
    progControl("gozoom")
    setMsg("MF", "CB", "Plotting TPS data...")
    setMsg("TPS", "CB", "Working...")
# Make sure GY never becomes a float, otherwise it could make the day plot
# lines run together and generally look really bad on some operating/X11
# systems.
    GY = PROGPropFontHeight
    TPSMargin = 25
# The display will always show whole days, but only plot the data corresponding
# to the main form's time range.
    TPSStart = (CurMainStart // 86400.0) * 86400.0
    TPSEnd = (CurMainEnd // 86400.0) * 86400.0 + 86400.0
# Put the name of the file at the top in case someone wants to print the plots.
    ID = LCan.create_text(10, GY, anchor="w", font=PROGPropFont,
                          fill=DClr["RAWT"], text=MainFilename)
    GY += PROGPropFontHeight
# DS/Channel loop.
    for Plot in Plots:
        TheData = RawData[Plot]
        if len(TheData) == 0:
            ID = LCan.create_text(10, GY, anchor="w", font=PROGPropFont,
                                  fill=DClr["RAWT"],
                                  text="No data to plot for DS%d CH%d" %
                                       (Plot[0], Plot[1]))
            L, T, R, B = LCan.bbox(ID)
            ID2 = LCan.create_rectangle(L, T - 1, R, B, fill=DClr["MFCAN"],
                                        outline=DClr["MFCAN"])
            LCan.tag_raise(ID, ID2)
            GY += PROGPropFontHeight
            continue
# The labels "in" the plot need to be printed, then a box the same color as
# the background be made, then the text lifted above the box, so the hour
# lines that will be generated at the end will not be visible through the text.
        ID = LCan.create_text(10, GY, anchor="w", font=PROGPropFont,
                              fill=DClr["RAWT"], text="DS%d CH%d" %
                              (Plot[0], Plot[1]))
        L, T, R, B = LCan.bbox(ID)
        ID2 = LCan.create_rectangle(L, T - 1, R, B, fill=DClr["MFCAN"],
                                    outline=DClr["MFCAN"])
        LCan.tag_raise(ID, ID2)
# Put in the hour numbers here.
        for i in arange(4, 24, 4):
            XX = TPSMargin + (i * (3.0 * 288.0 / 24.0))
            ID = LCan.create_text(XX, GY, justify=CENTER,
                                  font=PROGPropFont, fill=DClr["RAWT"],
                                  text="%02d" % i)
            L, T, R, B = LCan.bbox(ID)
            ID2 = LCan.create_rectangle(L, T - 1, R, B, fill=DClr["MFCAN"],
                                        outline=DClr["MFCAN"])
            LCan.tag_raise(ID, ID2)
        GY += PROGPropFontHeight
# Create a list with as many 5-minute bins as we are going to need to cover
# TPSStart to TPSEnd days.
        del PlotData[:]
        PlotData = [[0, 0] for i in arange(0, int((TPSEnd - TPSStart) / 86400 * 288))]  # noqa: E501
# Now go through all of the raw data points, calculate which bin created above
# each data point value should be added to based on the epoch time of each
# point and add it in.
# There was a data file where channels 1 and 2 stopped plotting early (ch3 was
# fine). All of the data was there for 1 and 2, but one data point jumped way
# ahead in time (original timestamp may have been scrambled), and they stopped
# early. We'll give each channel 3 chances before we jump out. These break in
# the first place to keep from going through a bunch of data that we don't
# want to plot.
        Glitches = 0
        for Data in TheData:
            if Data[0] < TPSStart:
                continue
            if Data[0] > TPSEnd:
                Glitches += 1
                if Glitches > 3:
                    break
                continue
# Filter once more so we only plot what the user has selected and not whole
# days (that's what the above lets pass).
            if Data[0] < CurMainStart:
                continue
            if Data[0] > CurMainEnd:
                # Same 3 chances.
                Glitches += 1
                if Glitches > 3:
                    break
                continue
            Index = int((Data[0] - TPSStart) // 300)
            PlotData[Index][0] += Data[1]**2
            PlotData[Index][1] += 1
# Now go through the bins by day and each bin within each day.
# Was there data the day(s) before?
        WasData = True
        Missing = 0
        for DayIndex in arange(0, len(PlotData), 288):
            PROGStopBut.update()
            if PROGRunning.get() == 0:
                return
            setMsg("TPS", "CB",
                   "Plotting time-power-squared plot DS%d CH%d..." %
                   (Plot[0], Plot[1]))
            GX = TPSMargin
# Take a quick glance at this day's data. If there is none for a day or more
# in-a-row, then we will just draw a special line covering that time, instead
# of drawing a black line for each empty day.
# Was there data for this day?
            HasData = False
            for BinIndex in arange(DayIndex, DayIndex + 288):
                if PlotData[BinIndex][1] != 0:
                    HasData = True
                    break
# We've just run out of data or there still is no data, so keep looking.
            if HasData is False:
                Missing += 1
                WasData = False
                continue
# We're back, so draw the special line.
            if WasData is False:
                for i in arange(0, 288):
                    # Draw a dotted line.
                    if i % 2 == 0:
                        ID = LCan.create_rectangle(GX - 1, GY - 1, GX + 1,
                                                   GY + 1, fill=Clr["O"],
                                                   outline=Clr["O"])
                    else:
                        ID = LCan.create_rectangle(GX - 1, GY - 1, GX + 1,
                                                   GY + 1, fill=Clr["K"],
                                                   outline=Clr["K"])
                    LCan.tag_bind(ID, "<Button-1>",
                                  Command(plotTPSDaysMissing, Missing))
# For next 5-minute block.
                    GX += 3
                updateMe(0)
# For the next line, today's line, that has data.
                GX = TPSMargin
                GY += 5
                WasData = True
                Missing = 0
# The user may only want to plot a little and stop, so set this here,
# otherwise they won't be able to click on any points.
                TPSPlotted = True
# Draw a normal day's line.
            for BinIndex in arange(DayIndex, DayIndex + 288):
                # If there are no points in this bin check the bins on either
                # side. If they both have values then calculate an average and
                # use that.
                if PlotData[BinIndex][1] == 0:
                    try:
                        if PlotData[BinIndex - 1][1] != 0 and \
                                PlotData[BinIndex + 1][1] != 0:
                            Value = (PlotData[BinIndex - 1][0] +
                                     PlotData[BinIndex + 1][0])
                            Points = (PlotData[BinIndex - 1][1] +
                                      PlotData[BinIndex + 1][1])
# Save the results so we don't have to do this again.
                            PlotData[BinIndex][0] = Value
                            PlotData[BinIndex][1] = Points
                            AveValue = Value / Points
                        else:
                            AveValue = 0.0
                    except Exception:
                        AveValue = 0.0
                else:
                    AveValue = float(PlotData[BinIndex][0]) / \
                        PlotData[BinIndex][1]
                if AveValue == Bin0:
                    C = "K"
                elif AveValue < Bin1:
                    C = "U"
                elif AveValue < Bin2:
                    C = "C"
                elif AveValue < Bin3:
                    C = "G"
                elif AveValue < Bin4:
                    C = "Y"
                elif AveValue < Bin5:
                    C = "R"
                elif AveValue < Bin6:
                    C = "M"
                else:
                    C = "E"
                ID = LCan.create_rectangle(GX - 1, GY - 1, GX + 1, GY + 2,
                                           fill=Clr[C], outline=Clr[C])
                LCan.tag_bind(ID, "<Button-1>",
                              Command(plotTPSTimeClick, BinIndex,
                                      fmti(sqrt(AveValue))))
                LCan.tag_bind(ID, "<Control-Button-1>",
                              Command(plotTPSTimeClick, BinIndex,
                                      fmti(sqrt(AveValue))))
# For next 5-minute block.
                GX += 3
# For next day.
            GY += 5
            updateMe(0)
# The user may only want to plot a little and stop, so set this here,
# otherwise they won't be able to click on any points.
            TPSPlotted = True
# For next ds/channel.
        GY += PROGPropFontHeight
        L, T, R, B = LCan.bbox(ALL)
        LCan.configure(scrollregion=(0, 0, R + 25, (B + 15)))
        updateMe(0)
        if PROGRunning.get() == 0:
            return
    L, T, R, B = LCan.bbox(ALL)
    for i in arange(1, 24):
        XX = TPSMargin + (i * (3.0 * 288.0 / 24.0))
        ID = LCan.create_line(XX, T + PROGPropFontHeight * 2, XX, B + 5,
                              fill=Clr["A"])
        LCan.tag_lower(ID, ALL)
# Print a key to the colors.
    GY = B + 25
    Index = 0
    for C in ("K", "U", "C", "G", "Y", "R", "M", "E"):
        GYY = GY + PROGPropFontHeight * Index
        LCan.create_rectangle(25, GYY - 5, 40, GYY + 5, fill=Clr[C],
                              outline=Clr["B"])
        if Index == 0:
            LCan.create_text(45, GYY, anchor="w", font=PROGPropFont,
                             fill=DClr["RAWT"], text="%s counts" %
                             fmti(int(sqrt(eval("Bin%d" % Index)))))
        elif Index != 7:
            LCan.create_text(45, GYY, anchor="w", font=PROGPropFont,
                             fill=DClr["RAWT"], text="+/- %s counts" %
                             fmti(int(sqrt(eval("Bin%d" % Index)))))
        else:
            LCan.create_text(45, GYY, anchor="w", font=PROGPropFont,
                             fill=DClr["RAWT"], text="> %s counts" %
                             fmti(int(sqrt(Bin6))))
        Index += 1
    L, T, R, B = LCan.bbox(ALL)
    LCan.configure(scrollregion=(0, 0, R + 25, (B + 15)))
    PROGStopBut.update()
# Even though we are finished let the user know this happened.
    if PROGRunning.get() == 0:
        setMsg("TPS", "YB", "Stopped.")
        setMsg("MF", "YB", "Stopped.")
    else:
        setMsg("TPS", "", "")
        setMsg("MF", "", "")
    return
#######################
# BEGIN: plotTPSClear()
# FUNC:plotTPSClear():2018.236


def plotTPSClear():
    global TPSPlotted
# Clear the TPS plot if it is up.
    if PROGFrm["TPS"] is not None:
        PROGFrm["TPS"].title("TPS Plot")
        LCan = PROGCan["TPS"]
        LCan.delete("all")
        LCan.xview_moveto(0.0)
        LCan.yview_moveto(0.0)
        LCan.configure(bg=DClr["MFCAN"])
        setMsg("TPS", "", "")
    TPSPlotted = False
    return
########################
# BEGIN: plotTPSReplot()
# FUNC:plotTPSReplot():2013.035


def plotTPSReplot():
    if TPSPlotted is False:
        setMsg("TPS", "RW", "There is nothing to replot.", 2)
        return
    plotMFTime("TPS", -1)
    plotRAWTime(-1)
    plotTPS()
    progControl("stopped")
    return
###########################
# BEGIN: plotTPSTime(Epoch)
# FUNC:plotTPSTime():2018.236
#   For external callers to use to control the TPS clock display (of which
#   there is none. Maybe someday.).


def plotTPSTime(Epoch):
    if PROGFrm["TPS"] is None:
        return
# Callers cannot get anything displayed on this plot, so just erase anything
# that might be there.
    PROGCan["TPS"].delete("TClick")
    setMsg("TPS", "", "")
    return
#####################################
# BEGIN: plotTPSDaysMissing(Value, e)
# FUNC:plotTPSDaysMissing():2011.298
#   Just displays the number of days missing from clicks on the special
#   'days missing' lines.


def plotTPSDaysMissing(Value, e):
    if TPSPlotted is False:
        beep(1)
        return
    setMsg("TPS", "", "Days missing: %d" % Value)
    return


##########################################
# BEGIN: plotTPSTimeClick(Index, Value, e)
# FUNC:plotTPSTimeClick():2013.037
#   Handles Canvas clicks.
TPSTimeClick = False


def plotTPSTimeClick(Index, Value, e):
    global TPSTimeClick
    if TPSPlotted is False:
        beep(1)
        TPSTimeClick = False
        return
    LCan = PROGCan["TPS"]
    Cx = LCan.canvasx(e.x)
    Cy = LCan.canvasy(e.y)
# This is a click from the Canvas().
    if Index == -1:
        TPSTimeClick = False
# Keep the same value as TPSMargin.
        if Cx < 25:
            setMsg("TPS", "", "")
            plotMFTime("TPS", -1)
            plotRAWTime(-1)
# Clear out any time square from before.
            LCan.delete("TClick")
# This is a click from one of the rectangles.
# This is a click from one of the rectangles.
    else:
        LCan.delete("TClick")
        LCan.create_rectangle(Cx - 6, Cy - 6, Cx + 6, Cy + 6, width=5,
                              outline=Clr["O"], tags="TClick")
        Epoch = float(TPSStart + (Index * 300) + 150)
        setMsg("TPS", "", "%s: %s counts" % (dt2Time(-1, 80, Epoch)[:-4],
                                             Value))
        plotMFTime("TPS", Epoch)
        plotRAWTime(Epoch)
    return
# END: plotTPS


##########################
# BEGIN: progControl(What)
# FUNC:progControl():2008.145
def progControl(What):
    if What == "go":
        buttonBG(PROGReadBut, "G", NORMAL)
        buttonBG(PROGStopBut, "R", NORMAL)
        PROGRunning.set(1)
        busyCursor(1)
        return
    elif What == "gozoom":
        buttonBG(PROGStopBut, "R", NORMAL)
        PROGRunning.set(1)
        busyCursor(1)
        return
    elif What == "stop":
        if PROGRunning.get() == 0:
            beep(1)
        else:
            buttonBG(PROGStopBut, "Y", NORMAL)
    elif What == "stopped":
        buttonBG(PROGReadBut, "D", NORMAL)
        buttonBG(PROGStopBut, "D", DISABLED)
    PROGRunning.set(0)
    busyCursor(0)
    return
# END: progControl


##########################
# BEGIN: progQuitter(Save)
# FUNC:progQuitter():2014.076
def progQuitter(Save):
    if Save is True:
        # Load this with the current main display position and size.
        PROGGeometryVar.set(Root.geometry())
        Ret = savePROGSetups()
        if Ret[0] != 0:
            formMYD(Root, (("(OK)", TOP, "ok"),), "ok", Ret[1], "Oh Oh.",
                    Ret[2])
    setMsg("MF", "CB", "Quitting...")
    Ret = formCloseAll()
    if Ret[0] == 2:
        setMsg("MF", "", "Not quitting... Is something still running?")
        return
    exit()
# END: progQuitter


#####################
# BEGIN: pushBrowse()
# FUNC:pushBrowse():2018.235
#  Needed to handle the Main Data Dir button press.
def pushBrowse():
    setMsg("MF", "", "")
    Answer = formMYDF(Root, 1, "Pick A Directory...", PROGDataDirVar.get(), "")
    if len(Answer) != 0:
        # Clears the screen off and sets things up for the next plot.
        plotMFClear("all")
        plotRAWClear()
        plotTPSClear()
        PROGDataDirVar.set(Answer)
# Keeping them in sync.
        PROGMsgsDirVar.set(Answer)
        PROGWorkDirVar.set(Answer)
        loadRTFilesCmd(MFFiles, "MF")
        PROGEnt["DATA"].icursor(END)
    return
# END: pushBrowse


###########################################################
# BEGIN: readFileLinesRB(Filespec, Strip = False, Bees = 0)
# LIB:readFileLinesRB():2019.032
#   This is the same idea as readFileLines(), but the Filespec is passed and
#   the file is treated as a 'hostile text file' that may be corrupted. This
#   can be used any time, but was developed for reading Reftek LOG files which
#   can be corrupted, or just have a lot of extra junk added by processing
#   programs.
#   This is based on the method used in rt72130ExtractLogData().
#   The return value is (0, [lines]) if things go OK, or a standard error
#   message if not, except the "e" of the exception also will be returned
#   after the passed Filespec, so the caller can construct their own error
#   message if needed.
#   If Bees is not 0 then that many bytes of the file will be returned and
#   converted to lines. If Bees is less than the size of the file the last
#   line will be discarded since it's a good bet that it will be a partial
#   line.
#   Weird little kludge: Setting Bees to -42 tells the function that Filespec
#   contains a bunch of text and that it should be split up into lines and
#   returned just as if the text had come from reading a file.
def readFileLinesRB(Filespec, Strip=False, Bees=0):
    Lines = []
    if Bees != -42:
        try:
            # These should be text files, but there's no way to know if they
            # are ASCII or Unicode or garbage, especially if they are
            # corrupted, so open binarially.
            Fp = open(Filespec, "rb")
# This will be trouble if the file is huge, but that should be rare. Bees can
# be used if the file is known to be yuge. This should result in one long
# string. This and the "rb" above seems to work on Py2 and 3.
            if Bees == 0:
                Raw = Fp.read().decode("latin-1")
            else:
                Raw = Fp.read(Bees).decode("latin-1")
            Fp.close()
            if len(Raw) == 0:
                return (0, Lines)
        except Exception as e:
            try:
                Fp.close()
            except Exception:
                pass
            return (1, "MW", "%s: Error opening/reading file.\n%s" %
                    (basename(Filespec), e), 3, Filespec, e)
    else:
        Raw = Filespec
# Yes, this is weird. These should be "text" files and in a non-corrupted file
# there should be either all \n or all \r or the same number of \r\n and \n
# and \r. Try and split the file up based on these results.
    RN = Raw.count("\r\n")
    N = Raw.count("\n")
    R = Raw.count("\r")
# Just one line by itself with no delimiter? OK.
    if RN == 0 and N == 0 and R == 0:
        return (0, [Raw])
# Perfect \n. May be the most popular, so we'll check for it first.
    if N != 0 and R == 0 and RN == 0:
        RawLines = Raw.split("\n")
# Perfect \r\n file. We checked for RN=0 above.
    elif RN == N and RN == R:
        RawLines = Raw.split("\r\n")
# Perfect \r.
    elif R != 0 and N == 0 and RN == 0:
        RawLines = Raw.split("\r")
    else:
        # There was something in the file, so make a best guess based on the
        # largest number. It might be complete crap, but what else can we do?
        if N >= RN and N >= R:
            RawLines = Raw.split("\n")
        elif N >= RN and N >= R:
            RawLines = Raw.split("\r\n")
        elif R >= N and R >= RN:
            RawLines = Raw.split("\n")
# If all of those if's couldn't figure it out.
        else:
            return (1, "RW", "%s: Unrecognized file format." %
                    basename(Filespec), 2, Filespec)
# If Bees is not 0 then throw away the last line if the file is larger than
# the number of bytes requested.
    if Bees != 0 and Bees < getsize(Filespec):
        RawLines = RawLines[:-1]
# Get rid of trailing empty lines. They can sneak in from various places
# depending on who wrote the file. Do the strip in case there are something
# like leftover \r's when \n was used for splitting.
    while RawLines[-1].strip() == "":
        RawLines = RawLines[:-1]
# It must be all the file had in it.
        if len(RawLines) == 0:
            return (0, Lines)
# If the caller doesn't want anything else then just go through and get rid of
# any trailing spaces, else get rid of all the spaces.
    if Strip is False:
        for Line in RawLines:
            Lines.append(Line.rstrip())
    else:
        for Line in RawLines:
            Lines.append(Line.strip())
    return (0, Lines)
# END: readFileLinesRB


#########################################################
# BEGIN: readMSLOGSFiles(Folderspec, StopBut, RunningVar)
# LIB:readMSLOGSFiles():2018.346
#   This is for reading the LOG channel files from the DMC. It returns all of
#   the SOH messages and should clean out most of the rt2ms-like non-SOH lines.
def readMSLOGSFiles(Folderspec, StopBut, RunningVar):
    Files = listdir(Folderspec)
    LOGFiles = []
# These used to need .LOG. in the file name. Now it's the user's responsibility
# to only put LOG files in the folder.
    for File in Files:
        if isfile(Folderspec + File):
            LOGFiles.append(File)
    LOGFiles.sort()
    if len(LOGFiles) == 0:
        return (2, "YB", "No files found in folder %s." % Folderspec, 2)
    Lines = ["%s:  Version Number  %s  Folder: %s" % (PROG_NAMELC,
                                                      PROG_VERSION,
                                                      MainFilename)]
    for File in LOGFiles:
        StopBut.update()
        if RunningVar.get() == 0:
            return (2, "YB", "Stopped.", 0)
        Ret = filterSOH(Folderspec + File, StopBut, RunningVar)
        if Ret[0] != 0:
            return (2, Ret[1], Ret[2], Ret[3], Ret[4])
        Lines += Ret[1]
    return (0, Lines)
# END: readMSLOGSFiles


##################################
# BEGIN: reconfigDisplay(e = None)
# FUNC:reconfigDisplay():2014.337
#   Handles the Redraw button for the main display.
def reconfigDisplay(e=None):
    if MFPlotted is False:
        return
    plotMFClear("")
    progControl("go")
    plotMF("MF")
    progControl("stopped")
    return
# END: reconfigDisplay


#########################
# BEGIN: returnReadDir(e)
# FUNC:returnReadDir():2018.235
#   Needed to handle the Return key press in the CWD field.
def returnReadDir(e):
    setMsg("MF", "", "")
    PROGDataDirVar.set(PROGDataDirVar.get().strip())
    if len(PROGDataDirVar.get()) == 0:
        if PROGSystem == "dar" or PROGSystem == "lin" or PROGSystem == "sun":
            PROGDataDirVar.set(sep)
        elif PROGSystem == "win":
            # For a lack of anyplace else.
            PROGDataDirVar.set("C:\\")
    if PROGDataDirVar.get().endswith(sep) is False:
        PROGDataDirVar.set(PROGDataDirVar.get() + sep)
# Sync sync sync.
    PROGMsgsDirVar.set(PROGDataDirVar.get())
    PROGWorkDirVar.set(PROGDataDirVar.get())
    plotMFClear("all")
    plotRAWClear()
    plotTPSClear()
    loadRTFilesCmd(MFFiles, "MF")
    PROGEnt["DATA"].icursor(END)
    return
# END: returnReadDir


################################################
# BEGIN: rt130MPDecode(FromDate, ToDate, Packet)
# LIB:rt130MPDecode():2018.241
#   Pulls out what should be the first mass position measurement from the
#   passed packet.  The caller is responsible for passing a packet with the
#   mass positions in it.
#   Returns (Epoch, Channel, MP Value, Color Range Code (0-4))
#   The Color Range Code is 0 for OK, 4 for pegged sensor element.
#   Different sensors have different ranges...
#      The first set is for "regular" sensors. The second for Trilliums
#      (the ones that started this).
MPColorRangeRVar = StringVar()
MPColorRangeRVar.set("0")
PROGSetups += ["MPColorRangeRVar"]
MPColorRanges = {"0": [.5, 2.0, 4.0, 7.0], "1": [.5, 1.8, 2.4, 3.5]}


def rt130MPDecode(FromDate, ToDate, Packet):
    # Check to see if the user even wants this packet.
    Epoch = rt72130HeaderTime2Epoch(Packet)
    if Epoch < FromDate or Epoch > ToDate:
        return (0, )
    Chan = bcd2Int(Packet[19]) + 1
    if Chan > 0 and Chan < 7:
        Value = b2Int(Packet[24:26])
        if Value > 0x7FFF:
            Value -= 0x10000
        Value = int((Value / 32767.0 * 10.0) * 10.0) / 10.0
        AValue = abs(Value)
# It's a little faster not doing the .get() if it goes through all of the if's.
        Range = MPColorRangeRVar.get()
        if AValue <= MPColorRanges[Range][0]:
            return (Epoch, Chan, Value, 0)
        elif AValue <= MPColorRanges[Range][1]:
            return (Epoch, Chan, Value, 1)
        elif AValue <= MPColorRanges[Range][2]:
            return (Epoch, Chan, Value, 2)
        elif AValue <= MPColorRanges[Range][3]:
            return (Epoch, Chan, Value, 3)
        else:
            return (Epoch, Chan, Value, 4)
    return (0, )
######################################
# BEGIN: rt130MPDecodeChange(WhereMsg)
# FUNC:rt130MPDecodeChange():2011.300


def rt130MPDecodeChange(WhereMsg):
    Message = "Mass position color range set to: %s" % \
        list2Str(MPColorRanges[MPColorRangeRVar.get()], ", ", False)
    setMsg(WhereMsg, "", Message)
    return
#########################################
# BEGIN: rt130MPDecodeGetColorCode(Value)
# FUNC:rt130MPDecodeGetColorCode():2013.207


def rt130MPDecodeGetColorCode(Value):
    Range = MPColorRangeRVar.get()
    AValue = abs(Value)
    if AValue <= MPColorRanges[Range][0]:
        return 0
    elif AValue <= MPColorRanges[Range][1]:
        return 1
    elif AValue <= MPColorRanges[Range][2]:
        return 2
    elif AValue <= MPColorRanges[Range][3]:
        return 3
    else:
        return 4
# END: rt130MPDecode


###################################################################
# BEGIN: rt130cfQGPSPosition(CFSpec, StopBut, RunningVar, WhereMsg)
# LIB:rt130cfQGPSPosition():2018.270
def rt130cfQGPSPosition(CFSpec, StopBut, RunningVar, WhereMsg):
    Dir = dirname(CFSpec)
    DAS = basename(CFSpec)
# We will want to return the blah.cf(sep)DAS version of the file name.
    CFDAS = "%s%s%s" % (basename(Dir), sep, DAS)
# Now do this since we are done with it.
    Dir += sep
    Ret = walkDirs(Dir, False, "", 1, False)
    if Ret[0] != 0:
        return (2, Ret[1], Ret[2], Ret[3], CFSpec)
    SOHFiles = []
    DASLookFor = sep + DAS + sep
    SOHLookFor = sep + "0" + sep
    for File in Ret[1]:
        if File.endswith(".CFG"):
            continue
        if File.find(DASLookFor) != -1 and File.find(SOHLookFor) != -1:
            SOHFiles.append(File)
    if len(SOHFiles) == 0:
        return (1, "YB", "%s: No SOH files found." % CFDAS, 2, CFSpec)
    SOHFiles.sort()
    Dict = {}
    DASID = ""
    Dict["idid"] = DASID
    Positions = []
    PositionsCount = 0
    Lines = []
# Read through all of the files and decode the header time stamp. Collect the
# earliest one and the latest one as we go.
    for Filespec in SOHFiles:
        if StopBut is not None:
            StopBut.update()
            if RunningVar.get() == 0:
                return (2, "YB", "Stopped.", 2, CFSpec)
# If the file is not a multiple of 1024 bytes it could mean the data on the CF
# card was corrupted and that could crash the function, so just skip over it.
        if getsize(Filespec) % 1024 != 0:
            continue
        try:
            Fp = open(Filespec, "rb")
        except Exception:
            return (1, "MW", "ERROR: Open error: %s" % Filespec, 3, Filespec)
        while True:
            # Read through the file 2000 packets at a time to speed things
            # along. This failed once on a corrupted CF card and the error
            # message meant nothing sensible: IOError: [Error 7] Argment list
            # too long. I suspect even Python couldn't figure out what was
            # going on with the card, so we'll try.
            try:
                Block = Fp.read(2048000)
            except Exception:
                Fp.close()
                break
# This will lose processing of the last block if the file ends badly, but that
# should be caught in the 1024 test above.
            if len(Block) < 1024:
                break
# Go through the packets read above. Use the length of the Block so when we
# get to the end of the file we will stop at the last block instead of just
# continuing for a full block.
            for Offset in arange(0, len(Block), 1024):
                Packet = Block[Offset:Offset + 1024]
                if len(Packet) < 1024:
                    break
                PacketType = Packet[:2].decode("latin-1")
                if PacketType == "SH":
                    del Lines[:]
# This will fill in Lines.
                    rt72130SHDecode(0, maxInt, Packet, Lines)
                    for Line in Lines:
                        # Ex: State of Health  01:251:09:41:35:656   ST: 0108
                        # We just want the DASID.
                        if Line.startswith("State of Health"):
                            # If anything is wrong (like maybe the line is
                            # scrambled) just go on.
                            try:
                                Parts = Line.split()
# The last thing on the line should be the unit ID. Grab it and then check to
# make sure it is a number/hex number. If it is not then keep looking.
                                if len(DASID) == 0:
                                    DASID = Parts[-1]
                                    try:
                                        int(DASID, 16)
                                        Dict["idid"] = DASID
                                    except Exception:
                                        DASID = ""
                            except Exception:
                                pass
# Ex: 253:22:41:25 GPS: POSITION: N43:44:17.12 W096:37:25.27 +00456M
                        elif Line.find("GPS: POSITION") != -1:
                            InParts = Line.split()
# Any number of scrambled data problems could set this off.
                            try:
                                Lat = rt72130Str2Dddd(InParts[3])
                                Long = rt72130Str2Dddd(InParts[4])
                                Elev = floatt(InParts[5])
                                if Lat != -1000.0 and Long != -1000.0:
                                    if len(Positions) == 5:
                                        Positions = Positions[1:]
                                    Positions.append([Lat, Long, Elev])
                                    PositionsCount += 1
                                    if PositionsCount == 10:
                                        break
                            except Exception:
                                pass
                if PositionsCount == 10:
                    break
            if PositionsCount == 10:
                break
        if PositionsCount == 10:
            break
    Fp.close()
    if len(Positions) == 0:
        return (1, "YB", "%s: No positions found in file." %
                basename(Filespec), 2, Filespec)
    Dict = aveQGPSPositions(Positions, Dict)
    return (0, Dict)
# END: rt130cfQGPSPosition


################################################################
# BEGIN: rt130cfTimeRange(CFSpec, StopBut, RunningVar, WhereMsg)
# LIB:rt130cfTimeRange():2018.270
#   Scans through just the SOH files of a CF card and collects the SOH packet
#   header times. Also scans the messages looking for ACQUISITION START/STOP
#   messages.
#   Expects the CFSpec to be <path>/file.cf(sep)DAS.
#   Returns a standard error message, or
#       (0, CF file, DASID, [time, time...], [start, stop, start...], \
#               [(stderrmsg), (stderrmsgs)...])
#   time, time are the packet header times.
#   start, stop are the start acq and stop acq times. It is assumed that the
#           times will always come in a start then stop pair, so if the first
#           thing the routine finds is a stop time, then the first time in the
#           List will be set to 0, and the second time in the List will be the
#           stop time that was found. If acq is started again and then the SOH
#           messages end then the last time will be the start time and there
#           will not be a stop time.
def rt130cfTimeRange(CFSpec, StopBut, RunningVar, WhereMsg):
    Dir = dirname(CFSpec)
    DAS = basename(CFSpec)
# We will want to return the blah.cf(sep)DAS version of the file name.
    CFDAS = "%s%s%s" % (basename(Dir), sep, DAS)
# Now do this since we are done with it.
    Dir += sep
    Ret = walkDirs(Dir, False, "", 1, False)
    if Ret[0] != 0:
        return (2, Ret[1], Ret[2], Ret[3], CFSpec)
    SOHFiles = []
    DASLookFor = sep + DAS + sep
    SOHLookFor = sep + "0" + sep
    for File in Ret[1]:
        if File.endswith(".CFG"):
            continue
        if File.find(DASLookFor) != -1 and File.find(SOHLookFor) != -1:
            SOHFiles.append(File)
    if len(SOHFiles) == 0:
        return (1, "YB", "%s: No SOH files found." % CFDAS, 2, CFSpec)
    SOHFiles.sort()
    DASID = ""
    Times = []
    SSTimes = []
    Messages = []
# Read through all of the files and decode the header time stamp. Collect the
# earliest one and the latest one as we go.
    for Filespec in SOHFiles:
        if StopBut is not None:
            StopBut.update()
            if RunningVar.get() == 0:
                return (2, "YB", "Stopped.", 2, CFSpec)
# If the file is not a multiple of 1024 bytes it could mean the data on the CF
# card was corrupted and that could crash LOGPEEK.
        if getsize(Filespec) % 1024 != 0:
            Messages.append((1, "MW", "WARNING: Not1024: %s" % Filespec, 3))
            continue
        try:
            Fp = open(Filespec, "rb")
        except Exception:
            Messages.append((1, "MW", "ERROR: Open error: %s" % Filespec, 3))
            continue
        while True:
            # Read through the file 2000 packets at a time to speed things
            # along. This failed once on a corrupted CF card and the error
            # message meant nothing sensible: IOError: [Error 7] Argment list
            # too long. I suspect even Python couldn't figure out what was
            # going on with the card, so we'll try.
            try:
                Block = Fp.read(2048000)
            except Exception:
                Fp.close()
                Messages.append((1, "MW", "ERROR: IOError: %s" % Filespec, 3))
                break
# This will lose processing of the last block if the file ends badly, but that
# should be caught in the 1024 test above.
            if len(Block) < 1024:
                break
# Go through the packets read above. Use the length of the Block so when we
# get to the end of the file we will stop at the last block instead of just
# continuing for a full block.
            for Offset in arange(0, len(Block), 1024):
                Packet = Block[Offset:Offset + 1024]
                if len(Packet) < 1024:
                    break
                PacketType = Packet[:2].decode("latin-1")
# We do this to try and keep packets of garbage from getting their times
# processed.
                if PacketType == "SH":
                    Time = rt72130HeaderTime2Epoch(Packet)
                    Times.append(Time)
                    SETimes = rt72130AcqStartStop(Time, Packet)
                    if len(SETimes) > 0:
                        SSTimes += SETimes
# Might as well add these to the regular times since the DAS must have been
# running when they were recorded.
                        for STime in SETimes:
                            Times.append(STime[1])
                    if len(DASID) == 0:
                        DASID = b2Hex(Packet[4:6])
        Fp.close()
    return (0, CFDAS, DASID, Times, SSTimes, Messages)
# END: rt130cfTimeRange


#####################################################################
# BEGIN: rt130zcfQGPSPosition(ZCFSpec, StopBut, RunningVar, WhereMsg)
# LIB:rt130zcfQGPSPosition():2018.345
def rt130zcfQGPSPosition(ZCFSpec, StopBut, RunningVar, WhereMsg):
    Dir = dirname(ZCFSpec) + sep
    CFFile = basename(ZCFSpec)
    if access(ZCFSpec, R_OK) == 0:
        return (1, "YB", "%s: I do not have permission to read this file." %
                CFFile, 2, ZCFSpec)
    if is_zipfile(ZCFSpec) is False:
        return (1, "YB", "%s: Not a valid zip file." % CFFile, 2, ZCFSpec)
    try:
        Zp = ZipFile(ZCFSpec)
    except Exception:
        return (1, "MW", "%s: Error opening file." % CFFile, 3, ZCFSpec)
    SOHFiles = []
    ZipSep = findZipSep(Zp)
    SOHLookFor = ZipSep + "0" + ZipSep
    for File in Zp.namelist():
        # Since we are only looking for SOH files just do this first.
        if SOHLookFor not in File:
            continue
        if basename(File).startswith(".") or File.startswith("_") or \
                File.endswith(".CFG") or File.endswith(ZipSep):
            continue
        SOHFiles.append(File)
    if len(SOHFiles) == 0:
        return (2, "RW", "%s: No SOH files were found in ZCF." %
                basename(ZCFSpec), 2, ZCFSpec)
    SOHFiles.sort()
    Dict = {}
    DASID = ""
    Dict["idid"] = DASID
    Positions = []
    PositionsCount = 0
    Lines = []
    for File in SOHFiles:
        if StopBut is not None:
            StopBut.update()
            if RunningVar.get() == 0:
                Zp.close()
                return (2, "YB", "Stopped.", 2, [])
        Packets = Zp.read(File)
# If Packets is not a multiple of 1024 bytes it could mean the data on the CF
# card was corrupted and that could crash the rest of this so just skip the
# bad file and keep on reading.
        if len(Packets) % 1024 != 0:
            continue
        for Offset in arange(0, len(Packets), 1024):
            Packet = Packets[Offset:Offset + 1024]
            if len(Packet) < 1024:
                break
            PacketType = Packet[:2].decode("latin-1")
# We do this to try and keep packets of garbage from getting their times
# processed.
            if PacketType == "SH":
                del Lines[:]
                rt72130SHDecode(0, maxInt, Packet, Lines)
                for Line in Lines:
                    # Ex: State of Health  01:251:09:41:35:656   ST: 0108
                    # We just want the DASID
                    if Line.startswith("State of Health"):
                        # If anything is wrong (like maybe the line is
                        # scrambled) just go on.
                        try:
                            Parts = Line.split()
# The last thing on the line should be the unit ID. Grab it and then check to
# make sure it is a number/hex number. If it is not then keep looking.
                            if len(DASID) == 0:
                                DASID = Parts[-1]
                                try:
                                    int(DASID, 16)
                                    Dict["idid"] = DASID
                                except Exception:
                                    DASID = ""
                        except Exception:
                            pass
# Ex: 253:22:41:25 GPS: POSITION: N43:44:17.12 W096:37:25.27 +00456M
                    elif Line.find("GPS: POSITION") != -1:
                        InParts = Line.split()
# Any number of scrambled data problems could set this off.
                        try:
                            Lat = rt72130Str2Dddd(InParts[3])
                            Long = rt72130Str2Dddd(InParts[4])
                            Elev = floatt(InParts[5])
                            if Lat != -1000.0 and Long != -1000.0:
                                if len(Positions) == 5:
                                    Positions = Positions[1:]
                                Positions.append([Lat, Long, Elev])
                                PositionsCount += 1
                                if PositionsCount == 10:
                                    break
                        except Exception:
                            pass
            if PositionsCount == 10:
                break
        if PositionsCount == 10:
            break
    try:
        Zp.close()
    except Exception:
        pass
    if len(Positions) == 0:
        return (1, "YB", "No positions found in file.", 2, Filespec)
    Dict = aveQGPSPositions(Positions, Dict)
    return (0, Dict)
# END: rt130zcfQGPSPosition


##################################################################
# BEGIN: rt130zcfTimeRange(ZCFSpec, StopBut, RunningVar, WhereMsg)
# LIB:rt130zcfTimeRange():2018.345
#   Scans through the SOH files of a zipped CF card and collects the header
#   times from each SOH packet.
#   Returns a standard error message or
#       (0, filename, DASID, [time, time...], [errors])
#   [errors] are a List of standard error messages that were generated while
#   reading the files. Since a .cf or zipped .cf can have many SOH files (one
#   for each day) the routine will generate error messages about problems it
#   finds and try to keep on reading times. The errors will not have the file
#   name at the end of the Tuple.
def rt130zcfTimeRange(ZCFSpec, StopBut, RunningVar, WhereMsg):
    if access(ZCFSpec, R_OK) == 0:
        return (1, "YB", "%s: I do not have permission to read this file." %
                ZCFSpec, 2, ZCFSpec)
    if is_zipfile(ZCFSpec) is False:
        return (1, "YB", "Not a valid zip file\n   %s." % ZCFSpec, 2)
    try:
        Zp = ZipFile(ZCFSpec)
    except Exception as e:
        return (1, "MW", "Error opening zip file\n   %s\n   %s" %
                (ZCFSpec, e), 3)
    ZipSep = findZipSep(Zp)
    SOHLookFor = ZipSep + "0" + ZipSep
    SOHFiles = []
    for File in Zp.namelist():
        # Since we are only looking for SOH files just do this first.
        if SOHLookFor not in File:
            continue
        if basename(File).startswith(".") or File.startswith("_") or \
                File.endswith(".CFG") or File.endswith(ZipSep):
            continue
        SOHFiles.append(File)
    if len(SOHFiles) == 0:
        return (2, "RW", "%s: No SOH files were found in ZCF." %
                basename(ZCFSpec), 2)
    DASID = ""
    Times = []
    SSTimes = []
    Messages = []
    FileCount = 0
# Read through the files 1K at a time just as if we were reading a .ref file.
    for File in SOHFiles:
        # Stop if the user wants to.
        if StopBut is not None:
            StopBut.update()
            if RunningVar.get() == 0:
                Zp.close()
                return (1, "YB", "Stopped.", 2)
        FileCount += 1
        if FileCount % 20 == 0:
            setMsg("TMRNG", "CB",
                   "Reading zipped file %d of %d.  Working on %s..." %
                   (FileCount, len(SOHFiles), File))
# Load (or map?) the whole file.
        Packets = Zp.read(File)
# If Packets is not a multiple of 1024 bytes it could mean the data on the CF
# card was corrupted and that could crash the rest of this so just make a note
# of the bad file and keep on reading.
        if len(Packets) % 1024 != 0:
            Messages.append(
                (1, "MW", "WARNING: Skipped. Not1024: %s" % File, 3))
            continue

# Read each packet in each file as if this were a .ref file.
        for Offset in arange(0, len(Packets), 1024):
            Packet = Packets[Offset:Offset + 1024]
            if len(Packets) < 1024:
                break
            PacketType = Packet[:2].decode("latin-1")
# We check the packet type to try and keep packets of garbage from getting
# their times processed.
            if PacketType == "SH":
                Time = rt72130HeaderTime2Epoch(Packet)
                Times.append(Time)
                SETimes = rt72130AcqStartStop(Time, Packet)
                if len(SETimes) > 0:
                    SSTimes += SETimes
# Might as well add these to the regular times since the DAS must have been
# running when they were recorded.
                    for STime in SETimes:
                        Times.append(STime[1])
                if len(DASID) == 0:
                    DASID = b2Hex(Packet[4:6])
    try:
        Zp.close()
    except Exception:
        pass
    return (0, basename(ZCFSpec), DASID, Times, SSTimes, Messages)
# END: rt130zcfTimeRange


###########################################################################
# BEGIN: rt130ExtractUCFData(Dir, DAS, StopBut, Running, WhereMsg, OnlySOH,
#                IncDS9, IncRAW, DS9s, DSsButts, FromDate = 0, ToDate = maxInt)
# LIB:rt130ExtractUCFData():2018.270
#   Extracts data from Unzipped CF card/image (UCF).
#   setMsg() being used in here in a LIB function is a bit abnormal, but it
#   keeps things simple.
def rt130ExtractUCFData(Dir, DAS, StopBut, Running, WhereMsg, OnlySOH,
                        IncDS9, IncRAW, DS9s, DSsButts, FromDate=0,
                        ToDate=maxInt):
    global RawData
    global SampleRates
    global RTModel
# WARNING: This part is highly specialized for LOGPEEK.
    global MP1
    global MP2
    global MP3
    global MP4
    global MP5
    global MP6
    setMsg(WhereMsg, "CB", "Working...")
# It must be.
    RTModel = "RT130"
# The passed "File" is really just a directory, so get a list of the files in
# it that are also for the passed DAS (the data from more than one DAS may be
# in there).
    Ret = walkDirs(Dir, False, "", 1, False)
    if Ret[0] != 0:
        return Ret
    CFFiles = Ret[1]
    TheCFFiles = []
# Decide which files to go through here.
# Somehow Python keeps all of this separator-stuff straight.
    DASLookFor = sep + DAS + sep
    SOHLookFor = sep + "0" + sep
# We always want the SOH files.
    for CFFile in CFFiles:
        # We don't process the saved configuration files. Directories and
        # hidden files are filtered out by walkDirs().
        if CFFile.endswith(".CFG"):
            continue
        if CFFile.find(DASLookFor) != -1 and CFFile.find(SOHLookFor) != -1:
            TheCFFiles.append(CFFile)
# Add the raw data files if we are going to be doing any of that.
    if OnlySOH == 0 and (IncDS9 == 1 or len(DSsButts) > 0):
        for CFFile in CFFiles:
            if CFFile.endswith(".CFG"):
                continue
            if CFFile.find(DASLookFor) != -1 and CFFile.find(SOHLookFor) == -1:
                TheCFFiles.append(CFFile)
    TotalCFFiles = len(TheCFFiles)
    if TotalCFFiles == 0:
        return (1, "YB", "No valid files for DAS %s found in\n   %s" %
                (DAS, Dir), 2)
    TheCFFiles.sort()
    DecodeMode = OPTDecodeModeCVar.get()
    RetMessages = []
# There may be 1000's of files on a CF card, so we'll try to process all of
# them and skip the ones we can't and keep track of their names in here. The
# caller can do what they want with the list (it will end up in RetMessages).
    FailedFiles = []
    Samples = {}
    FileCount = 0
    for Filespec in TheCFFiles:
        # Stop and don't return anything if the user wants to stop.
        if StopBut is not None:
            StopBut.update()
            if Running.get() == 0:
                return (1, "YB", "Stopped.", 2)
# If the file is not a multiple of 1024 bytes it could mean the data on the CF
# card was corrupted and that could crash LOGPEEK further on, so make a note
# of the file that was skipped and carry on.
        if getsize(Filespec) % 1024 != 0:
            FailedFiles.append("WARNING: Not1024: %s (%d bytes)" %
                               (Filespec, getsize(Filespec)))
            continue
        try:
            Fp = open(Filespec, "rb")
        except Exception as e:
            return (2, "MW", "(UCF) Error opening file\n   %s\n   %s" %
                    (Filespec, e), 3)
        FileCount += 1
        if FileCount % 20 == 0:
            setMsg(WhereMsg, "CB",
                   "Working on DAS %s. Reading file %d of %d..." %
                   (DAS, FileCount, TotalCFFiles))
        while True:
            # Read through the file 2000 blocks at a time to speed things
            # along. This has tripped a couple of times on corrupted CF cards
            # and the error message meant nothing sensible: "IOError:
            # [Error 7] Argment list too long". I suspect even Python couldn't
            # figure out what was going on with the card, so try, make a note
            # of the file, and go on if it happens.
            try:
                Block = Fp.read(2048000)
            except IOError:
                Fp.close()
                FailedFiles.append("WARNING: IOError. Corrupted?: %s" %
                                   Filespec)
                break
            if len(Block) < 1024:
                break
# Go through the packets read above. Use the length of the Block so when we
# get to the end of the file we will stop at the last block instead of just
# continuing for a full block (there is nothing to trigger when the last
# packet of the Block has been read unlike the RT125s where the ord(Page[0])
# flag can be used).
            for Offset in arange(0, len(Block), 1024):
                Packet = Block[Offset:Offset + 1024]
                PacketType = Packet[:2].decode("latin-1")
# Always do these.
                if PacketType == "SH":
                    rt72130SHDecode(FromDate, ToDate, Packet, RetMessages)
                    continue
                if OnlySOH == 0:
                    # These will be the majority, so check for them first.
                    if PacketType == "DT":
                        DS = bcd2Int(Packet[18]) + 1
# Only if the user wants it.
                        if IncRAW == 1 and DS in DSsButts:
                            DCE = (DS, bcd2Int(Packet[19]) + 1,
                                   bcd2Int(Packet[16:18]))
                            if DCE in Samples:
                                Samples[DCE] += bcd2Int(Packet[20:22])
                            else:
                                Samples[DCE] = bcd2Int(Packet[20:22])
# For raw data we don't care about the event number, just the data stream and
# the channel.
                            DC = (DCE[0], DCE[1])
                            if DC in RawData:
                                pass
                            else:
                                RawData[DC] = []
                                if DCE[0] not in SampleRates:
                                    MYDAnswerVar.set("")
                                    Answer = formMYD(Root,
                                                     (("Input10", TOP,
                                                       "input"),
                                                      ("I Don't Know", TOP,
                                                       "unk"),
                                                      ("OK", TOP, "input")),
                                                     "unk", "YB",
                                                     "Just Wondering...",
                                                     "Any idea what the "
                                                     "sample rate is for Data "
                                                     "Stream %d? (I'll figure "
                                                     "it out eventually, but "
                                                     "data will be skipped "
                                                     "until then.)" % DCE[0])
                                    if Answer != "unk":
                                        SR = intt(Answer)
                                        if rt72130CheckSampRate(SR) is True:
                                            SampleRates[DCE[0]] = SR
# This will tell the stuff downstream that we still don't know what the sample
# rate is, but it will keep the question from being asked again for this data
# stream.
                                    else:
                                        SampleRates[DCE[0]] = 0
                            rt72130DTDecode(DecodeMode, FromDate, ToDate, DC,
                                            Packet, RawData, RetMessages)
                            continue
# WARNING: This part is highly specialized for LOGPEEK.
# Check to see if the user wants to plot the mass positions.
                        elif IncDS9 == 1 and DS == 9:
                            # Info = (Epoch, Channel, MP Value, Color Range
                            # Code (0-4))
                            Info = rt130MPDecode(FromDate, ToDate, Packet)
                            if Info[0] != 0 and Info[1] in DS9s:
                                # MPx = (Epoch, MP Value, Color Range Code)
                                eval("MP%d" % Info[1]).append([Info[0],
                                                               Info[2],
                                                               Info[3]])
                                continue
                        continue
                    elif PacketType == "EH":
                        rt72130EHDecode(FromDate, ToDate, DSsButts,
                                        Packet, Samples, RetMessages)
                    elif PacketType == "ET":
                        rt72130ETDecode(FromDate, ToDate, DSsButts,
                                        Packet, Samples, RetMessages)
                    elif PacketType == "SC":
                        rt72130SCDecode(FromDate, ToDate, Packet,
                                        RetMessages)
                    elif PacketType == "DS":
                        rt72130DSDecode(FromDate, ToDate, Packet,
                                        RetMessages)
                    elif PacketType == "CD":
                        rt72130CDDecode(FromDate, ToDate, RTModel,
                                        Packet, RetMessages)
                    elif PacketType == "OM":
                        rt72130OMDecode(FromDate, ToDate, RTModel,
                                        Packet, RetMessages)
        Fp.close()
# List any Samples left with no ET packet found.
    for Leftover in Samples:
        RetMessages.append("WARNING: Missing ET packets?: DS %d, Chan %d, "
                           "Event %d: %d samples left over" %
                           (Leftover[0], Leftover[1], Leftover[2],
                            Samples[Leftover]))
# These messages will be displayed at the top of the log so the user will see
# them.
    if len(FailedFiles) != 0:
        RetMessages.append("")
        FailedFiles.reverse()
        for File in FailedFiles:
            RetMessages.insert(0, File)
    return (0, RetMessages)
# END: rt130ExtractUCFData


############################################################################
# BEGIN: rt130ExtractZCFData(Dir, File, StopBut, Running, WhereMsg, OnlySOH,
#                IncDS9, IncRAW, DS9s, DSsButts, FromDate = 0, ToDate = maxInt)
# LIB:rt130ExtractZCFData():2018.345
#   Extract data from a Zipped CF card image file (ZCF).
#   Passes back a List of all of the SOH messages in a ".zip" file (the CF card
#   image from one DAS zipped by, for example, rt130Cut(), or an error message.
#   setMsg() being used in here is a bit abnormal, but it keeps things simple.
def rt130ExtractZCFData(Dir, File, StopBut, Running, WhereMsg, OnlySOH,
                        IncDS9, IncRAW, DS9s, DSsButts, FromDate=0,
                        ToDate=maxInt):
    global RawData
    global SampleRates
    global RTModel
# WARNING: This part is highly specialized for LOGPEEK.
    global MP1
    global MP2
    global MP3
    global MP4
    global MP5
    global MP6
    setMsg(WhereMsg, "CB", "Working...")
# If it is a zipped CF then this must be true.
    RTModel = "RT130"
    ZipFilespec = Dir + File
    if access(ZipFilespec, R_OK) == 0:
        return (1, "YB", "%s: I do not have permission to read this file." %
                ZipFilespec, 2, ZipFilespec)
    if is_zipfile(ZipFilespec) is False:
        return (1, "YB", "Not a valid zip file\n   %s." % ZipFilespec, 2)
    try:
        Zp = ZipFile(ZipFilespec)
    except Exception as e:
        return (1, "MW", "Error opening zip file\n   %s\n   %s" %
                (ZipFilespec, e), 3)
    ZipSep = findZipSep(Zp)
    SOHLookFor = ZipSep + "0" + ZipSep
    CFFiles = sorted(Zp.namelist())
    TheCFFiles = []
# Decide which files to go through here.
# We always want the SOH files.
    for CFFile in CFFiles:
        # Stinking hidden files are EVERYWHERE from macOS. There may also be
        # others.
        if CFFile.find(ZipSep + ".") != -1:
            continue
# We don't process the saved configuration files.
        if CFFile.endswith(".CFG"):
            continue
        if CFFile.find(SOHLookFor) != -1:
            TheCFFiles.append(CFFile)
# Add the raw data files if we are going to be doing any of that.
    if OnlySOH == 0 and (IncDS9 == 1 or len(DSsButts) > 0):
        for CFFile in CFFiles:
            if CFFile.find(ZipSep + ".") != -1:
                continue
            if CFFile.endswith(".CFG"):
                continue
# Since we don't know which DAS we are looking for in these just include
# all non-SOH files.
            if CFFile.find(SOHLookFor) == -1:
                TheCFFiles.append(CFFile)
    TotalCFFiles = len(TheCFFiles)
    if TotalCFFiles == 0:
        return (2, "RW", "%s: No files in ZCF were found to read." %
                basename(ZipFilespec), 2)
    RetMessages = []
    DecodeMode = OPTDecodeModeCVar.get()
# There may have been 1000's of files on the original CF card. If there are
# problems with a file make a note of it and continue on. The list will be
# added to RetMessages at the end.
    FailedFiles = []
    Samples = {}
    FileCount = 0
# Read through the files 1K at a time just as if we were reading a .ref file.
    for CFFile in TheCFFiles:
        # Stop if the user wants to.
        if StopBut is not None:
            StopBut.update()
            if Running.get() == 0:
                Zp.close()
                return (1, "YB", "Stopped.", 2)
        FileCount += 1
        if FileCount % 20 == 0:
            setMsg("MF", "CB",
                   "Reading zipped file %d of %d.  Working on %s..." %
                   (FileCount, TotalCFFiles, CFFile))
# Load (or map?) the whole file.
        Packets = Zp.read(CFFile)
# If Packets is not a multiple of 1024 bytes it could mean the data on the CF
# card was corrupted and that could crash LOGPEEK.
        if len(Packets) % 1024 != 0:
            FailedFiles.append("WARNING: Skipped. Not1024: %s (bytes: %d)" %
                               (CFFile, len(Packets)))
            continue
# Read each packet in each file as if this were a .ref file.
        for Offset in arange(0, len(Packets), 1024):
            Packet = Packets[Offset:Offset + 1024]
            PacketType = Packet[:2].decode("latin-1")
# Always do the SOH packets.
            if PacketType == "SH":
                rt72130SHDecode(FromDate, ToDate, Packet, RetMessages)
                continue
            if OnlySOH == 0:
                # These are the most abundant so check them first.
                if PacketType == "DT":
                    DS = bcd2Int(Packet[18]) + 1
                    if IncRAW == 1 and DS in DSsButts:
                        # DCE = Data stream, Channel, Event#.
                        DCE = (DS, bcd2Int(Packet[19]) + 1,
                               bcd2Int(Packet[16:18]))
                        if DCE in Samples:
                            Samples[DCE] += bcd2Int(Packet[20:22])
                        else:
                            Samples[DCE] = bcd2Int(Packet[20:22])
# For raw data we don't care about the event number, just the data stream and
# the channel.
                        DC = (DCE[0], DCE[1])
                        if DC in RawData:
                            pass
                        else:
                            RawData[DC] = []
                            if DCE[0] not in SampleRates:
                                MYDAnswerVar.set("")
                                Answer = formMYD(Root,
                                                 (("Input10", TOP, "input"),
                                                  ("I Don't Know", TOP, "unk"),
                                                  ("OK", TOP, "input")),
                                                 "unk", "YB",
                                                 "Just Wondering...",
                                                 "Any idea what the sample "
                                                 "rate is for Data Stream %d? "
                                                 "(I'll figure it out "
                                                 "eventually, but data will "
                                                 "be skipped until then.)" %
                                                 DCE[0])
                                if Answer != "unk":
                                    SR = intt(Answer)
                                    if rt72130CheckSampRate(SR) is True:
                                        SampleRates[DCE[0]] = SR
# This will tell the stuff downstream that we still don't know what the sample
# rate is, but it will keep the question from being asked again for this data
# stream.
                                else:
                                    SampleRates[DCE[0]] = 0
                        rt72130DTDecode(DecodeMode, FromDate, ToDate,
                                        DC, Packet, RawData, RetMessages)
                        continue
# WARNING: This part is highly specialized for LOGPEEK.
# Check to see if the user wants to plot the mass positions. If this is a DS9
# packet then go for it.
                    if IncDS9 == 1 and DS == 9:
                        # Info = (Epoch, Channel, MP Value, Color Range Code
                        # (0-4))
                        Info = rt130MPDecode(FromDate, ToDate, Packet)
                        if Info[0] != 0 and Info[1] in DS9s:
                            # MPx = (Epoch, MP Value, Color Range Code)
                            eval("MP%d" % Info[1]).append([Info[0], Info[2],
                                                           Info[3]])
                            continue
                    continue
                elif PacketType == "EH":
                    rt72130EHDecode(FromDate, ToDate, DSsButts, Packet,
                                    Samples, RetMessages)
                elif PacketType == "ET":
                    rt72130ETDecode(FromDate, ToDate, DSsButts, Packet,
                                    Samples, RetMessages)
                elif PacketType == "SC":
                    rt72130SCDecode(FromDate, ToDate, Packet,
                                    RetMessages)
                elif PacketType == "DS":
                    rt72130DSDecode(FromDate, ToDate, Packet,
                                    RetMessages)
                elif PacketType == "CD":
                    rt72130CDDecode(FromDate, ToDate, RTModel, Packet,
                                    RetMessages)
                elif PacketType == "OM":
                    rt72130OMDecode(FromDate, ToDate, RTModel,
                                    Packet, RetMessages)
# It may already be closed.
    try:
        Zp.close()
    except Exception:
        pass
# List any Samples left with no ET packet found.
    for Leftover in Samples:
        RetMessages.append("WARNING: Missing ET packets?: DS %d, Chan %d, "
                           "Event %d: %d samples" % (Leftover[0], Leftover[1],
                                                     Leftover[2],
                                                     Samples[Leftover]))
# Add these messages to the top of the log so the user will see them.
    if len(FailedFiles) != 0:
        RetMessages.append("")
        FailedFiles.reverse()
        for File in FailedFiles:
            RetMessages.insert(0, File)
    return (0, RetMessages)
# END: rt130ExtractZCFData


#############################
# BEGIN: rt130FindCFDASs(Dir)
# LIB:rt130FindCFDASs():2013.035
#    Scans through the passed directory, assumed to be a directory where all
#    of the YYYYDOY directories are located, and returns the names of the
#    directories that look like RT130 DAS numbers (4 chars, hex).
def rt130FindCFDASs(Dir):
    DASs = []
    if Dir.endswith(sep) is False:
        Dir += sep
    DayDirs = listdir(Dir)
    for DayDir in DayDirs:
        if len(DayDir) == 7 and DayDir.isdigit():
            # The "directory" may not be a directory, because of corruption.
            # Happened to a TopoGreenland station.
            try:
                DASDirs = listdir(Dir + DayDir)
            except OSError:
                continue
            for DASDir in DASDirs:
                if len(DASDir) == 4 and isHex(DASDir):
                    if DASDir not in DASs:
                        DASs.append(DASDir)
    return DASs
# END: rt130FindCFDASs


##############################################
# BEGIN: rt72130AcqStartStop(HdrEpoch, Packet)
# LIB:rt72130AcqStartStop():2018.270
#   Returns a List of epoch times of ACQUISITION STARTED/STOPPED pairs found
#   in the passed Packet. The times will be Tuples of ("S", epoch) for start
#   and ("E", epoch) for stop.
#   Some attempt is made to get the right year for the message line. If
#   HdrEpoch is not None then it will be used to try and detect a New Year's
#   day change. If it is None then the function will get the Packet header
#   epoch time.
def rt72130AcqStartStop(HdrEpoch, Packet):
    # Try this first to see if there is even any acquisition messages in the
    # packet. Those Reftek/IRIS guys were so smart to make the messages ASCII
    # text back in the olden days.
    if Packet.decode("latin-1").find("ACQUISITION ST") == -1:
        return []
    Times = []
    if HdrEpoch is None:
        HdrEpoch = rt72130HeaderTime2Epoch(Packet)
    YYYY, DOY, HH, MM, SS, TTT = rt72130HeaderTime2ydhmst(Packet)
    if YYYY >= 70:
        YYYY += 1900
    else:
        YYYY += 2000
    EOP = bcd2Int(Packet[12:14])
# -1 is to not get a "" in the last List item.
    Messages = Packet[24:EOP].decode("latin-1").split("\r\n")[:-1]
    for Message in Messages:
        if Message.endswith("ACQUISITION STARTED"):
            # The lines will start with DOY:HH:MM:SS.
            MsgEpoch = dt2Timeystr2Epoch(YYYY, Message.split()[0])
            if (HdrEpoch - 86400) > MsgEpoch:
                MsgEpoch = dt2Timeystr2Epoch(YYYY + 1, Message.split()[0])
            Times.append(("S", MsgEpoch))
        if Message.endswith("ACQUISITION STOPPED"):
            # The lines will start with DOY:HH:MM:SS.
            MsgEpoch = dt2Timeystr2Epoch(YYYY, Message.split()[0])
            if (HdrEpoch - 86400) > MsgEpoch:
                MsgEpoch = dt2Timeystr2Epoch(YYYY + 1, Message.split()[0])
            Times.append(("E", MsgEpoch))
    return Times
# END: rt72130AcqStartStop


###################################################################
# BEGIN: rt72130CDDecode(FromDate, ToDate, Model, Packet, Messages)
# LIB:rt72130CDDecode():2018.241
def rt72130CDDecode(FromDate, ToDate, Model, Packet, Messages):
    # Check to see if the user even wants this packet.
    Epoch = rt72130HeaderTime2Epoch(Packet)
    if Epoch < FromDate or Epoch > ToDate:
        return
    YYYY, DOY, HH, MM, SS, TTT = rt72130HeaderTime2ydhmst(Packet)
    Messages.append("")
    Messages.append(
        "Calibration Definition  %02d:%03d:%02d:%02d:%02d:%03d  ST: %s" %
        (YYYY, DOY, HH, MM, SS, TTT, b2Hex(Packet[4:6])))
    Messages.append("    Implemented = %s" %
                    dt2TimeDT(6, Packet[1008:1008 + 16]))
    if Model == "RT130":
        First = True
        Any = False
        for Offset in arange(130, 130 + 64, 16):
            Sensor = intt(Packet[Offset:Offset + 1].decode("latin-1"))
            if Sensor == 0:
                continue
            if First is True:
                Messages.append("    130 Sensor Auto-Center Information")
                First = False
            Any = True
            Messages.append("        Sensor = %d" % Sensor)
            Value = Packet[Offset + 1:Offset + 1 + 1].decode("latin-1")
            if Value == " ":
                Messages.append("            Enable = Disabled")
            else:
                Messages.append("            Enable = Enabled")
            Messages.append("            Reading Interval = %s" %
                            Packet[Offset + 2:Offset + 2 + 4].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Cycle Interval = %s" %
                            Packet[Offset + 6:Offset + 6 + 2].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Level = %s" %
                            Packet[Offset + 8:Offset + 8 + 4].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Attempts = %s" %
                            Packet[Offset + 12:Offset + 12 + 2].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Attempt Interval = %s" %
                            Packet[Offset + 14:Offset + 14 + 2].decode("latin-1").strip())  # noqa: E501
        First = True
        for Offset in arange(194, 194 + 112, 28):
            Sensor = intt(Packet[Offset:Offset + 1].decode("latin-1"))
            if Sensor == 0:
                continue
            if First is True:
                Messages.append(
                    "    130 Sensor Calibration Signal Information")
                First = False
            Any = True
            Messages.append("        Sensor = %d" % Sensor)
            Value = Packet[Offset + 1:Offset + 1 + 1].decode("latin-1")
            if Value == " ":
                Messages.append("            Enable = Disabled")
            else:
                Messages.append("            Enable = Enabled")
            Messages.append("            Duration = %s" %
                            Packet[Offset + 4:Offset + 4 + 4].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Amplitude = %s" %
                            Packet[Offset + 8:Offset + 8 + 4].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Signal = %s" %
                            Packet[Offset + 12:Offset + 12 + 4].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Step Interval = %s" %
                            Packet[Offset + 16:Offset + 16 + 4].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Step Width = %s" %
                            Packet[Offset + 20:Offset + 20 + 4].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Sine Frequency = %s" %
                            Packet[Offset + 24:Offset + 24 + 4].decode("latin-1").strip())  # noqa: E501
        First = True
        for Offset in arange(306, 306 + 538, 28):
            Seq = intt(Packet[Offset:Offset + 1].decode("latin-1"))
            if Seq == 0:
                continue
            if First is True:
                Messages.append(
                    "    130 Sensor Calibration Sequence Information")
                First = False
            Any = True
            Messages.append("        Sequence = %d" % Seq)
            Value = Packet[Offset + 1:Offset + 1 + 1].decode("latin-1")
            if Value == " ":
                Messages.append("            Enable = Disabled")
            else:
                Messages.append("            Enable = Enabled")
            Messages.append("            Start Time = %s" %
                            dt2TimeDT(6, Packet[Offset + 4:Offset + 4 + 14]))
            Messages.append("            Interval = %s" %
                            dt2TimeDT(1, Packet[Offset + 18:Offset + 18 + 8]))
            Messages.append("            Count = %s" %
                            Packet[Offset + 26:Offset + 26 + 2].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Record Length = %s" %
                            Packet[Offset + 28:Offset + 28 + 8].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Sensor = %s" %
                            Packet[Offset + 36:Offset + 36 + 4].decode("latin-1").strip())  # noqa: E501
        if Any is False:
            Messages.append("    No parameters defined.")
    elif Model == "72A":
        # YYYYDOYHHMMSS is only 13 bytes, but the field is 14 bytes, so +14.
        Messages.append("    Start Time = %s" %
                        dt2TimeDT(6, Packet[16:16 + 14]))
        Messages.append("    Repeat Interval = %s" %
                        Packet[30:30 + 8].decode("latin-1").strip())
        Messages.append("    Number Of Intervals = %s" %
                        Packet[38:38 + 4].decode("latin-1").strip())
        Messages.append("    Length = %s" %
                        Packet[42:42 + 8].decode("latin-1").strip())
        Messages.append("    Step On/Off = %s" %
                        Packet[50:50 + 4].decode("latin-1").strip())
        Messages.append("    Step Period = %s" %
                        Packet[54:54 + 8].decode("latin-1").strip())
        Messages.append("    Step Size = %s" %
                        Packet[62:62 + 8].decode("latin-1").strip())
        Messages.append("    Step Amplitude = %s" %
                        Packet[70:70 + 8].decode("latin-1").strip())
        Messages.append("    Step Output = %s" %
                        Packet[78:78 + 4].decode("latin-1").strip())
    return
# END: rt72130CDDecode


###################################
# BEGIN: rt72130CheckSampRate(Rate)
# LIB:rt72130CheckSampRate():2018.234
#   If the passed sample rate is OK return True.
def rt72130CheckSampRate(Rate):
    if isinstance(Rate, astring):
        Rate = intt(Rate)
    if Rate != 0 and Rate != 1000 and Rate != 500 and Rate != 250 and \
            Rate != 200 and Rate != 125 and Rate != 100 and Rate != 50 and \
            Rate != 40 and Rate != 25 and Rate != 20 and Rate != 10 and \
            Rate != 5 and Rate != 1:
        return False
    return True
# END: rt72130CheckSampRate


############################################################
# BEGIN: rt72130DSDecode(FromDate, ToDate, Packet, Messages)
# LIB:rt72130DSDecode():2018.241
#   Needs PROG_DSS set to the max data stream number.
def rt72130DSDecode(FromDate, ToDate, Packet, Messages):
    # Check to see if the user even wants this packet.
    global SampleRates
    Epoch = rt72130HeaderTime2Epoch(Packet)
    if Epoch < FromDate or Epoch > ToDate:
        return
    YYYY, DOY, HH, MM, SS, TTT = rt72130HeaderTime2ydhmst(Packet)
    Messages.append("")
    Messages.append(
        "Data Stream Definition  %02d:%03d:%02d:%02d:%02d:%03d  ST: %s" %
        (YYYY, DOY, HH, MM, SS, TTT, b2Hex(Packet[4:6])))
    Messages.append("    Implemented = %s" %
                    dt2TimeDT(6, Packet[1008:1008 + 16]))
    for Offset in arange(16, 16 + 920, 230):
        DS = intt(Packet[Offset:Offset + 2].decode("latin-1"))
        if DS == 0 or DS > PROG_DSS:
            continue
        Messages.append("    Data Stream Number = %d" % DS)
        Messages.append("        Name = %s" %
                        Packet[Offset + 2:Offset + 2 + 16].decode("latin-1").strip())  # noqa: E501
        Value = Packet[Offset + 18:Offset + 18 + 4].decode("latin-1")
        Dest = ""
        if Value[0] != " ":
            Dest += "RAM "
        if Value[1] != " ":
            Dest += "Disk "
        if Value[2] != " ":
            Dest += "Ethernet "
        if Value[3] != " ":
            Dest += "Serial"
        Messages.append("        Recording Destination = %s" % Dest)
        Value = Packet[Offset + 26:Offset + 26 + 16].decode("latin-1")
        Chans = ""
        Ch = 0
        for C in Value:
            Ch += 1
            if C != " ":
                Chans += "%d " % Ch
        Messages.append("        Channels Included = %s" % Chans)
        SR = iFB(Packet[Offset + 42:Offset + 42 + 4])
        if rt72130CheckSampRate(SR) is True:
            SampleRates[DS] = SR
        Messages.append("        Sample Rate = %d" % SR)
        Messages.append("        Data Format = %s" %
                        Packet[Offset + 46:Offset + 46 + 2].decode("latin-1").strip())  # noqa: E501
        Trig = Packet[Offset + 64:Offset + 64 + 4].decode("latin-1").strip()
        Messages.append("        Trigger Type = %s" % Trig)
        TOffset = Offset + 68
        if Trig == "CON":
            Messages.append("            Record Length = %s" %
                            Packet[TOffset + 0:TOffset + 0 + 8].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Start Time = %s" %
                            dt2TimeDT(6, Packet[TOffset + 8:TOffset + 8 + 14]))
        elif Trig == "CRS":
            Messages.append("            Trigger Stream Number = %s" %
                            Packet[TOffset + 0:TOffset + 0 + 2].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Pretrigger Length = %s" %
                            Packet[TOffset + 2:TOffset + 2 + 8].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Record Length = %s" %
                            Packet[TOffset + 10:TOffset + 10 + 8].decode("latin-1").strip())  # noqa: E501
        elif Trig == "EXT":
            Messages.append("            Pretrigger Length = %s" %
                            Packet[TOffset + 0:TOffset + 0 + 8].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Record Length = %s" %
                            Packet[TOffset + 8:TOffset + 8 + 8].decode("latin-1").strip())  # noqa: E501
        elif Trig == "EVT":
            Value = Packet[TOffset + 0:TOffset + 0 + 16].decode("latin-1")
            Chans = ""
            Ch = 0
            for C in Value:
                Ch += 1
                if C != " ":
                    Chans += "%d " % Ch
            Messages.append("            Trigger Channels = %s" % Chans)
            Messages.append("            Minimum Channels = %s" %
                            Packet[TOffset + 16:TOffset + 16 + 2].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Trigger Window = %s" %
                            Packet[TOffset + 18:TOffset + 18 + 8].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Pretrigger Length = %s" %
                            Packet[TOffset + 26:TOffset + 26 + 8].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Post-trigger Length = %s" %
                            Packet[TOffset + 34:TOffset + 34 + 8].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Record Length = %s" %
                            Packet[TOffset + 42:TOffset + 42 + 8].decode("latin-1").strip())  # noqa: E501
            Messages.append("            STA Length = %s" %
                            Packet[TOffset + 58:TOffset + 58 + 8].decode("latin-1").strip())  # noqa: E501
            Messages.append("            LTA Length = %s" %
                            Packet[TOffset + 66:TOffset + 66 + 8].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Mean Removal = %s" %
                            Packet[TOffset + 74:TOffset + 74 + 8].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Trigger Ratio = %s" %
                            Packet[TOffset + 82:TOffset + 82 + 8].decode("latin-1").strip())  # noqa: E501
            Messages.append("            De-trigger Ratio = %s" %
                            Packet[TOffset + 90:TOffset + 90 + 8].decode("latin-1").strip())  # noqa: E501
            Messages.append("            LTA Hold = %s" %
                            Packet[TOffset + 98:TOffset + 98 + 4].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Low Pass Corner Freq = %s" %
                            Packet[TOffset + 102:TOffset + 102 + 4].decode("latin-1").strip())  # noqa: E501
            Messages.append("            High Pass Corner Freq = %s" %
                            Packet[TOffset + 106:TOffset + 106 + 4].decode("latin-1").strip())  # noqa: E501
        elif Trig == "LEV":
            Messages.append("            Level = %s" %
                            Packet[TOffset + 0:TOffset + 0 + 8].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Pretrigger Length = %s" %
                            Packet[TOffset + 8:TOffset + 8 + 8].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Record Length = %s" %
                            Packet[TOffset + 16:TOffset + 16 + 8].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Low Pass Corner Freq = %s" %
                            Packet[TOffset + 24:TOffset + 24 + 4].decode("latin-1").strip())  # noqa: E501
            Messages.append("            High Pass Corner Freq = %s" %
                            Packet[TOffset + 28:TOffset + 28 + 4].decode("latin-1").strip())  # noqa: E501
        elif Trig == "TIM":
            Messages.append("            Start Time = %s" %
                            dt2TimeDT(6, Packet[TOffset + 0:TOffset + 0 + 14]))
            Messages.append("            Repeat Interval = %s" %
                            dt2TimeDT(1,
                                      Packet[TOffset + 14:TOffset + 14 + 8]))
            Messages.append("            Number Of Intervals = %s" %
                            Packet[TOffset + 22:TOffset + 22 + 4].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Record Length = %s" %
                            Packet[TOffset + 34:TOffset + 34 + 8].decode("latin-1").strip())  # noqa: E501
        elif Trig == "TML":
            for i in arange(1, 12):
                Messages.append("            Start Time %d = %s" %
                                (i, dt2TimeDT(6, Packet[TOffset + (i * 14): TOffset + (i * 14) + 14])))  # noqa: E501
            Messages.append("            Record Length = %s" %
                            Packet[TOffset + 154:TOffset + 154 + 8].decode("latin-1").strip())  # noqa: E501
        elif Trig == "VOT":
            Messages.append("            Pretrigger Length = %s" %
                            Packet[TOffset + 0:TOffset + 0 + 8].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Post-trigger Length = %s" %
                            Packet[TOffset + 8:TOffset + 8 + 8].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Record Length = %s" %
                            Packet[TOffset + 16:TOffset + 16 + 8].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Level Units = %s" %
                            Packet[TOffset + 24:TOffset + 24 + 1].decode("latin-1").strip())  # noqa: E501
# 1-G is 1-16. Stupid.
            Value = Packet[TOffset + 28:TOffset +
                           28 + 6].decode("latin-1").upper()
            Chans = ""
            for C in Value:
                if C == " ":
                    continue
                if isHex(C):
                    Chans += "%d " % int(C, 16)
                elif C == "G":
                    Chans += "16 "
            Messages.append("            Trigger Channels = %s" % Chans)
            Value = Packet[TOffset + 34:TOffset + 34 + 6].decode("latin-1")
            Votes = ""
            for C in Value:
                if C == " ":
                    continue
                Votes += "%s " % C
            Messages.append("            Trigger Channel Votes = %s" % Votes)
            Levels = ""
            for i in arange(0, 8):
                Value = Packet[TOffset + 40 + (i * 6):
                               TOffset + 40 + (i * 6) + 6].decode("latin-1").strip()  # noqa: E501
                if len(Value) == 0:
                    continue
                Levels += "%s " % Value
            Messages.append("            Trigger Channel Levels = %s" % Levels)
            Messages.append("            Trigger Minimum Votes = %s" %
                            Packet[TOffset + 88:TOffset + 88 + 2].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Trigger Window = %s" %
                            Packet[TOffset + 90:TOffset + 90 + 8].decode("latin-1").strip())  # noqa: E501
            Value = Packet[TOffset + 98:TOffset + 98 + 6].decode("latin-1")
            Votes = ""
            for C in Value:
                if C == " ":
                    continue
                Votes += "%s " % C
            Messages.append(
                "            De-trigger Channel Votes = %s" % Votes)
            Levels = ""
            for i in arange(0, 8):
                Value = Packet[TOffset + 104 +
                               (i * 6):TOffset + 104 + (i * 6) + 6].decode("latin-1").strip()  # noqa: E501
                if len(Value) == 0:
                    continue
                Levels += "%s " % Value
            Messages.append("            De-trigger Channel Levels = %s" %
                            Levels)
            Messages.append("            De-trigger Minimum Votes = %s" %
                            Packet[TOffset + 152:TOffset + 152 + 2].decode("latin-1").strip())  # noqa: E501
            Messages.append("            Low Pass Corner Freq = %s" %
                            Packet[TOffset + 154:TOffset + 154 + 4].decode("latin-1").strip())  # noqa: E501
            Messages.append("            High Pass Corner Freq = %s" %
                            Packet[TOffset + 158:TOffset + 158 + 4].decode("latin-1").strip())  # noqa: E501
        else:
            Messages.append("            Unknown trigger type: %s" % Trig)
    return
# END: rt72130DSDecode


#######################################################################
# BEGIN: rt72130DTDecode(Mode, FromDate, ToDate, DC, Packet, RawData, \
#                RetMessages)
# LIB:rt72130DTDecode():2018.241
#   Extracts data from the passed packet.
#   Mode = 0 = Read only the first value from the packet
#              16, 32 - first value
#              C0, C2 - first value is max value
#   Mode = not 0 = Read all data points in the packet.
def rt72130DTDecode(Mode, FromDate, ToDate, DC, Packet, RawData, RetMessages):
    if Mode == 0:
        Epoch = rt72130HeaderTime2Epoch(Packet)
        if Epoch < FromDate or Epoch > ToDate:
            return
        Format = bcd2Int(Packet[23])
        if Format == 0xC0:
            # Start value for the first data value of the packet is at offset
            # 24+40+4+4=72.
            Value = iFB(Packet[72:76])
        elif Format == 0xC2:
            Value = iFB(Packet[72:76])
        elif Format >= 32:
            # First value after the standard header 24 bytes.
            Value = iFB(Packet[24:28])
        elif Format == 16:
            Value = iFB(Packet[24:26])
        else:
            Value = 0
        RawData[DC] += [Epoch, Value],
    else:
        # If we don't have a sample rate for this DS/Ch then just act like we
        # packets to be never saw this packet. Now this might cause the data
        # from some packets to be plotted badly if the DAS is changing sample
        # rates, and a few packets at the beginning could be lost if they come
        # in before an EH packet. Tuff. If I were doing this in C I'd worry
        # about it.
        try:
            SR = SampleRates[DC[0]]
# This means we still don't know what the sample rate is. Keep skipping.
            if SR == 0:
                raise KeyError
        except KeyError:
            return
        Epoch = rt72130HeaderTime2Epoch(Packet)
# We may miss the starting sample during this packet, but packets can only be
# up to 22 secs of data at 40sps, so never mind.
        if Epoch < FromDate or Epoch > ToDate:
            return
        Period = 1.0 / SR
        Format = bcd2Int(Packet[23])
        Flags = Packet[22]
        TotSamples = bcd2Int(Packet[20:22])
        if Format == 0xC0:
            SampleCt = 0
# There are always in these locations.
            FirstValue = iFB(Packet[72:76])
            RawData[DC] += [Epoch, FirstValue],
            SampleCt += 1
            LastValue = iFB(Packet[76:80])
# Offset for the first byte of the frame of the block of frames.
            StartOffset = 24 + 40
# Steps through each frame.
            for FrameOffset in arange(StartOffset, StartOffset + 64 * 15, 64):
                ICodes = iFB(Packet[FrameOffset:FrameOffset + 4])
# Check for these possible conditions first (they may happen quite often).
# Speedy if: Looking for all 01 8-bit.
                if ICodes == 0x15555555:
                    Values = []
                    for Byte in Packet[FrameOffset + 4:FrameOffset + 4 + 60]:
                        Values.append(Byte)
# We don't have to worry about the first frame first sample value math problem
# here like we do below, beacuse the first frame will never come here, or the
# other speedy if's below.
                    for Diff in Values:
                        Epoch += Period
                        FirstValue += Diff
                        RawData[DC] += [Epoch, FirstValue],
# Keep track of this for each sample, so we jump out at the right time, which
# may be in the middle of a frame.
                        SampleCt += 1
                        if SampleCt >= TotSamples:
                            return
# Speedy if: Looking for all 10 16-bit.
                elif ICodes == 0x2AAAAAAA:
                    Values = []
                    for Offset in arange(FrameOffset + 4, FrameOffset + 4 + 60,
                                         2):
                        Values.append(iFB(Packet[Offset:Offset + 2]))
                    for Diff in Values:
                        Epoch += Period
                        FirstValue += Diff
                        RawData[DC] += [Epoch, FirstValue],
# Keep track of this for each sample, so we jump out at the right time, which
# may be in the middle of a frame.
                        SampleCt += 1
                        if SampleCt >= TotSamples:
                            return
# Speedy if: Looking for all 11 32-bit.
                elif ICodes == 0x3FFFFFFF:
                    Values = []
                    for Offset in arange(FrameOffset + 4, FrameOffset + 4 + 60,
                                         4):
                        Values.append(iFB(Packet[Offset:Offset + 4]))
                    for Diff in Values:
                        Epoch += Period
                        FirstValue += Diff
                        RawData[DC] += [Epoch, FirstValue],
# Keep track of this for each sample, so we jump out at the right time, which
# may be in the middle of a frame.
                        SampleCt += 1
                        if SampleCt >= TotSamples:
                            return
                else:
                    # So we can add four first thing below.
                    LumpOffset = FrameOffset - 4
                    for Lump in arange(0, 16):
                        LumpOffset += 4
                        Codette = ICodes & 0xC0000000
                        if Codette == 0x40000000:   # 01
                            Values = []
                            Values.append(Packet[LumpOffset])
                            Values.append(Packet[LumpOffset + 1])
                            Values.append(Packet[LumpOffset + 2])
                            Values.append(Packet[LumpOffset + 3])
                        elif Codette == 0x80000000:   # 10
                            Values = []
                            Values.append(
                                iFB(Packet[LumpOffset:LumpOffset + 2]))
                            Values.append(
                                iFB(Packet[LumpOffset + 2:LumpOffset + 4]))
                        elif Codette == 0xC0000000:   # 11
                            Values = []
                            Values.append(
                                iFB(Packet[LumpOffset:LumpOffset + 4]))
# It must be zero. Just shift and go on.
                        else:   # 00
                            ICodes = ICodes << 2
                            continue
                        for Value in Values:
                            if Lump > 0:
                                for Diff in Values:
                                    Epoch += Period
                                    FirstValue += Diff
                                    RawData[DC] += [Epoch, FirstValue],
# Keep track of this for each sample, so we jump out at the right time, which
# may be in the middle of a frame.
                                    SampleCt += 1
                                    if SampleCt >= TotSamples:
                                        return
# Don't difference the first difference of the first lump. FirstValue at the
# beginning is that calculation. That's dumb. FirstValue should have been the
# last value of the previous packet without this difference then we wouldn't
# have to waste time not doing that here.
                            else:
                                for Diff in Values[1:]:
                                    Epoch += Period
                                    FirstValue += Diff
                                    RawData[DC] += [Epoch, FirstValue],
                                    SampleCt += 1
                                    if SampleCt >= TotSamples:
                                        return
                        ICodes = ICodes << 2
        elif Format == 0xC2:
            Value = iFB(Packet[72:76])
            RawData[DC] += [Epoch, Value],
        elif Format >= 32:
            for Offset in arange(24, 24 + (4 * TotSamples), 4):
                Value = iFB(Packet[Offset:Offset + 4])
                RawData[DC] += [Epoch, Value],
                Epoch += Period
        elif Format == 16:
            for Offset in arange(24, 24 + (2 * TotSamples), 2):
                Value = iFB(Packet[Offset:Offset + 2])
                RawData[DC] += [Epoch, Value],
                Epoch += Period
        else:
            Value = 0
            RawData[DC] += [Epoch, Value],
    return
# END: rt72130DTDecode


###############################################################################
# BEGIN: rt72130EHDecode(FromDate, ToDate, DSsButts, Packet, Samples, Messages)
# LIB:rt72130EHDecode():2018.240
#    Just reads the event header packets enough to let the rt72130ETDecode()
#    routine know that a DS/Chan was being used.
def rt72130EHDecode(FromDate, ToDate, DSsButts, Packet, Samples, Messages):
    global SampleRates
# Check to see if the user even wants this packet.
    DS = bcd2Int(Packet[18]) + 1
# This should weed out any data stream 9 or invalid data stream stuff.
    if DS not in DSsButts:
        return
    Epoch = rt72130HeaderTime2Epoch(Packet)
    if Epoch < FromDate or Epoch > ToDate:
        return
    Event = bcd2Int(Packet[16:18])
# Go through this exercise to determine which channels are in this data stream.
    Chans = []
    Ch = 0
    for Offset in arange(416, 416 + 16):
        Ch += 1
        if str(Packet[Offset]) != " ":
            Chans.append(Ch)
    if len(Chans) == 0:
        Messages.append(
            "WARNING: No channels set in ET packet, DS:%s Evt:%d." %
            (DS, Event))
        return
# Make an entry for the ET routine to find.
    DCE = (DS, Chans[0], Event)
    if DCE not in Samples:
        Samples[DCE] = -1
# Leave this for decoding raw data.
    SR = intt(Packet[88:88 + 4].decode("latin-1"))
    if rt72130CheckSampRate(SR) is True:
        SampleRates[DS] = SR
    return
# END: rt72130EHDecode


###############################################################################
# BEGIN: rt72130ETDecode(FromDate, ToDate, DSsButts, Packet, Samples, Messages)
# LIB:rt72130ETDecode():2018.241
def rt72130ETDecode(FromDate, ToDate, DSsButts, Packet, Samples, Messages):
    global SampleRates
# Check to see if the user even wants this packet.
    DS = bcd2Int(Packet[18]) + 1
# If this data stream is not one the user is interested in then just return.
    if DS not in DSsButts:
        return
    Epoch = rt72130HeaderTime2Epoch(Packet)
    if Epoch < FromDate or Epoch > ToDate:
        return
    Event = bcd2Int(Packet[16:18])
# Go through this exercise to determine which channels are in this data stream.
    Chans = []
    Ch = 0
    for Offset in arange(416, 416 + 16):
        Ch += 1
        if str(Packet[Offset]) != " ":
            Chans.append(Ch)
    if len(Chans) == 0:
        Messages.append(
            "WARNING: No channels set in ET packet, DS:%s Evt:%d." %
            (DS, Event))
        return
# FINISHME - we should check to make sure all of the channels have the same
#            sample count here some day.
# Just use the sample count from the first channel (they should all be the
# same, unless the file is truncated in which case the error message below will
# be listed in the source file).
    DCE = (DS, Chans[0], Event)
# We only want complaints about channels in data streams that the user is
# plotting. The number of samples are not counted unless the raw data is going
# to be plotted.
    try:
        NSamples = Samples[DCE]
    except KeyError:
        Messages.append("WARNING: KeyError: ET packet with no prior DT "
                        "packets: DS:%d Ch:%d Evt:%d  Event data truncated?" %
                        (DCE[0], DCE[1], DCE[2]))
        NSamples = -2
    SPS = intt(Packet[88:88 + 4].decode("latin-1"))
    FST = dt2TimeDT(6, Packet[112:112 + 16])
    TT = dt2TimeDT(6, Packet[96:96 + 16])
    if OPTAddETPosMsgsCVar.get() == 0:
        Messages.append("DAS: %s  EV: %04d  DS: %d  FST = %s  TT = %s  NS: "
                        "%d  SPS: %d  ETO: ? ET" %
                        (b2Hex(Packet[4:6]), Event, DS, FST, TT, NSamples,
                         SPS))
# Do a non-standard thing and append the position information to the end of the
# line if the user wants to have it (usually for exporting the trigger times)
    else:
        Pos = Packet[918:918 + 26].decode("latin-1")
        Messages.append("DAS: %s  EV: %04d  DS: %d  FST = %s  TT = %s  NS: "
                        "%d  SPS: %d  ETO: ? ET  ETPOS: %s %s %s" %
                        (b2Hex(Packet[4:6]), Event, DS, FST, TT, NSamples,
                         SPS, Pos[0:10].replace(
                             " ", ""), Pos[10:20].replace(" ", ""),
                         Pos[20:]))
# Now go through the Samples dictonary and delete entries matching the DCE with
# all of this data stream's channels. This will error for every data stream and
# channel that the user is not plotting (see above), but that's what try-except
# is for.
    for Chan in Chans:
        DCE = (DS, Chan, Event)
        try:
            del Samples[DCE]
        except KeyError:
            pass
# Leave this for decoding raw data. One of these packets might get written to
# a new disk before we see an EH.
    SR = intt(Packet[88:88 + 4].decode("latin-1"))
    if rt72130CheckSampRate(SR) is True:
        SampleRates[DS] = SR
    return
# END: rt72130ETDecode


###################################################################
# BEGIN: rt72130ExtractLogData(Filespec, Strip = False, Filter = 0)
# LIB:rt72130ExtractLogData():2019.004
#   readFileLinesRB() uses this opening/reading method. This function is where
#   this method started.
#   Opens and reads the passed Filespec returning a List with an item for each
#   line found, or a standard error message. Splits the file into lines by
#   looking for \r\n, \r or \n.
#   Strip=True will do a .strip() on each line, otherwise just the ending
#   delimiter and blanks will be removed.
#   Filter=True will remove lines that are from other programs who like to
#   leave status messages and stuff laying around. Everything else passes.
def rt72130ExtractLogData(Filespec, Strip=False, Filter=0):
    Lines = []
    try:
        # These should be text files, but there's no way to know if they are
        # ASCII or Unicode or what, especially if they are corrupted, so open
        # binarially.
        Fp = open(Filespec, "rb")
# This will be trouble if the file is huge, but that is rare for .log files.
# This should result in one long string. This and "rb" above seems to work
# on Py2 and 3.
        Raw = Fp.read().decode("latin-1")
        Fp.close()
        if len(Raw) == 0:
            return (0, Lines)
    except Exception as e:
        # Don't know when things went wrong. It might not even be open.
        try:
            Fp.close()
        except Exception:
            pass
        return (1, "MW", "%s: Error reading file: %s" % (basename(Filespec),
                                                         e), 3, Filespec)
# Yes, this is weird. These should be "text" files and in a non-corrupted file
# there should be either all \n or all \r or the same number of \r\n and \n
# and \r. Try and split the file up based on these results.
    RN = Raw.count("\r\n")
    N = Raw.count("\n")
    R = Raw.count("\r")
# Just one line by itself with no delimiter?
    if RN == 0 and N == 0 and R == 0:
        return (0, [Raw])
# Perfect \n. May be the most popular, so we'll do it first.
    if N != 0 and RN == 0 and R == 0:
        RawLines = Raw.split("\n")
# Perfect \r\n file.
    elif RN == R and RN == N:
        RawLines = Raw.split("\r\n")
# Perfect \r.
    elif RN == 0 and N == 0 and R != 0:
        RawLines = Raw.split("\r")
    else:
        # There was something in the file, so make a best guess based on the
        # largest number. It might be complete crap, but what else can we do?
        if N >= RN and N >= R:
            RawLines = Raw.split("\n")
        elif N >= RN and N >= R:
            RawLines = Raw.split("\r\n")
        elif R >= N and R >= RN:
            RawLines = Raw.split("\n")
# If all of those if's couldn't figure it out.
        else:
            return (1, "MW", "%s: Unrecognizable file." % basename(Filespec),
                    3, Filespec)
# Get rid of trailing empty lines. They can sneak in from various places
# depending on who wrote the file. Since the while loop is checking for the
# last line this shouldn't IndexError.
    while len(RawLines[-1]) == 0:
        RawLines = RawLines[:-1]
        if len(RawLines) == 0:
            return (0, RawLines)
# If the caller doesn't want anything else then just go through and get rid of
# any trailing spaces.
    if Strip is False:
        if Filter == 0:
            for Line in RawLines:
                Lines.append(Line.rstrip())
            return (0, Lines)
        else:
            for Line in RawLines:
                Line = Line.rstrip()
                if len(Line) == 0:
                    Lines.append(Line)
                    continue
                Pattern = rtnPattern(Line)
# These are lines added by rt2ms() and other programs. They can confuse some
# downstream programs and users.
                if Pattern.startswith("00000") or Pattern.startswith("==>"):
                    continue
                Lines.append(Line)
    else:
        if Filter == 0:
            for Line in RawLines:
                Lines.append(Line.rstrip())
            return (0, Lines)
        else:
            for Line in RawLines:
                Line = Line.strip()
                if len(Line) == 0:
                    Lines.append(Line)
                    continue
                Pattern = rtnPattern(Line)
                if Pattern.startswith("00000") or Pattern.startswith("==>"):
                    continue
                Lines.append(Line)
    return (0, Lines)
# END: rt72130ExtractLogData


############################################################################
# BEGIN: rt72130ExtractRefData(Dir, File, Model, StopBut, Running, WhereMsg,
#                OnlySOH, IncDS9, IncRAW, DS9s, DSsButts, FromDate = 0,
#                ToDate = maxInt)
# LIB:rt72130ExtractRefData():2018.270
#   Passes back a List of all of the SOH messages in a ".ref" file (one large
#   file from one DAS), or an error message.
#   setMsg() being used in here is a bit abnormal, but it keeps things simple.
def rt72130ExtractRefData(Dir, File, Model, StopBut, Running, WhereMsg,
                          OnlySOH, IncDS9, IncRAW, DS9s, DSsButts, FromDate=0,
                          ToDate=maxInt):
    global RawData
    global SampleRates
    global RTModel
# WARNING: This part is highly specialized for LOGPEEK.
    global MP1
    global MP2
    global MP3
    global MP4
    global MP5
    global MP6
    setMsg(WhereMsg, "CB", "Working...")
    try:
        Fp = open(Dir + File, "rb")
    except Exception as e:
        return (2, "MW", "Error opening file\n   %s\n   %s" % ((Dir + File),
                                                               e), 3)
# If the caller did not supply the recorder model we'll need to go looking for
# in the the beginning of the .ref file.
    if len(Model) == 0:
        # There are getting to be more of these now.
        RTModel = "RT130"
        Packet = Fp.read(1024)
        if len(Packet) == 1024:
            UID = b2Hex(Packet[4:6])
            if UID < "9000":
                RTModel = "72A"
        Fp.seek(0)
    else:
        RTModel = Model
    RetMessages = []
    DecodeMode = OPTDecodeModeCVar.get()
    Samples = {}
    PacketTotal = int(getsize(Dir + File) / 1024)
    PacketCount = 0
    while True:
        if StopBut is not None:
            StopBut.update()
            if Running.get() == 0:
                Fp.close()
                return (1, "YB", "Stopped.", 2)
# Read through the file 2000 blocks at a time to speed things along.
        Block = Fp.read(2048000)
# This will lose the last block if the file ends badly.
        if len(Block) < 1024:
            Fp.close()
            break
        PacketCount += 2000
        if PacketCount % 10000 == 0:
            setMsg(WhereMsg, "CB",
                   "Working on %s.  Reading packet %d of %d..." %
                   (File, PacketCount, PacketTotal))
# Go through the packets read above. Use the length of the Block so when we
# get to the end of the file we will stop at the last block instead of just
# continuing for a full block (there is nothing to trigger when the last
# packet of the Block has been read unlike the RT125s where the ord(Page[0])
# flag can be used).
        for Offset in arange(0, len(Block), 1024):
            Packet = Block[Offset:Offset + 1024]
            PacketType = Packet[:2].decode("latin-1")
# Always do the SOH packets.
            if PacketType == "SH":
                rt72130SHDecode(FromDate, ToDate, Packet, RetMessages)
                continue
            if OnlySOH == 0:
                # These will be the most abundant.
                if PacketType == "DT":
                    DS = bcd2Int(Packet[18]) + 1
                    if IncRAW == 1 and DS in DSsButts:
                        DCE = (DS, bcd2Int(Packet[19]) + 1,
                               bcd2Int(Packet[16:18]))
                        if DCE in Samples:
                            Samples[DCE] += bcd2Int(Packet[20:22])
                        else:
                            Samples[DCE] = bcd2Int(Packet[20:22])
# For raw data we don't care about the event number, just the data stream and
# the channel.
                        DC = (DCE[0], DCE[1])
                        if DC in RawData:
                            pass
                        else:
                            RawData[DC] = []
                            if DCE[0] not in SampleRates:
                                MYDAnswerVar.set("")
                                Answer = formMYD(Root,
                                                 (("Input10", TOP, "input"),
                                                  ("I Don't Know", TOP, "unk"),
                                                  ("OK", TOP, "input")),
                                                 "unk", "YB",
                                                 "Just Wondering...",
                                                 "Any idea what the sample "
                                                 "rate is for Data Stream %d? "
                                                 "(I'll figure it out "
                                                 "eventually, but data will "
                                                 "be skipped until then.)" %
                                                 DCE[0])
                                if Answer != "unk":
                                    SR = intt(Answer)
                                    if rt72130CheckSampRate(SR) is True:
                                        SampleRates[DCE[0]] = SR
# This will tell the stuff downstream that we still don't know what the sample
# rate is, but it will keep the question from being asked again for this data
# stream.
                                else:
                                    SampleRates[DCE[0]] = 0
                        rt72130DTDecode(DecodeMode, FromDate, ToDate,
                                        DC, Packet, RawData, RetMessages)
                        continue
# WARNING: This part is highly specialized for LOGPEEK.
# Check to see if the user wants to plot the mass positions.
                    if DS == 9 and IncDS9 == 1:
                        # Info = (Epoch, Channel, MP Value, Color Range Code
                        # (0-4))
                        Info = rt130MPDecode(FromDate, ToDate, Packet)
                        if Info[0] != 0 and Info[1] in DS9s:
                            # MPx = (Epoch, MP Value, Color Range Code)
                            eval("MP%d" % Info[1]).append([Info[0], Info[2],
                                                           Info[3]])
                        continue
                    continue
                elif PacketType == "EH":
                    rt72130EHDecode(FromDate, ToDate, DSsButts, Packet,
                                    Samples, RetMessages)
                elif PacketType == "ET":
                    rt72130ETDecode(FromDate, ToDate, DSsButts, Packet,
                                    Samples, RetMessages)
                elif PacketType == "SC":
                    rt72130SCDecode(FromDate, ToDate, Packet,
                                    RetMessages)
                elif PacketType == "DS":
                    rt72130DSDecode(FromDate, ToDate, Packet, RetMessages)
                elif PacketType == "CD":
                    rt72130CDDecode(FromDate, ToDate, RTModel, Packet,
                                    RetMessages)
                elif PacketType == "OM":
                    rt72130OMDecode(FromDate, ToDate, RTModel,
                                    Packet, RetMessages)
# List any Samples left with no ET packet found.
    for Leftover in Samples:
        RetMessages.append("Missing ET packets?: DS %d, Chan %d, Event %d: "
                           "%d samples left over" % (Leftover[0], Leftover[1],
                                                     Leftover[2],
                                                     Samples[Leftover]))
    return (0, RetMessages)
# END: rt72130ExtractRefData


##############
# BEGIN: b2I()
# LIB:b2I():2019.014
#   Byte to integer routines for Py2 and Py3.
###################
# BEGIN: iFB(Bytes)
# FUNC:iFB():2018.241
#   Python 3. How brain dead. Hope this doesn't slow things down too much.
def iFB(Bytes):
    return int.from_bytes(Bytes, byteorder="big", signed=True)
####################
# BEGIN: iFBU(Bytes)
# FUNC:iFBU():2018.241


def iFBU(Bytes):
    return int.from_bytes(Bytes, byteorder="big", signed=False)
##################
# BEGIN: b2Hex(In)
# FUNC:b2Hex():2018.255
#   Takes the passed 2-byte/character UID code and returns the string version.


def b2Hex(In):
    return "%04X" % int.from_bytes(In, byteorder="big", signed=False)
##################
# BEGIN: b1Int(In)
# FUNC:b1Int(In):2018.255
#   One byte/character to an integer. This just saves a lot of if'ing in the
#   main code.


def b1Int(In):
    return In
##################
# BEGIN: b2Int(In)
# FUNC:b2Int(In):2018.255


def b2Int(In):
    # This could use int.from_bytes(), but I'm not sure what to do with signed.
    # I *think* everything passed would be unsigned, but I'm not sure. ???
    # Will come back to this.
    return ((In[0] * 256) + In[1])
##################
# BEGIN: b3Int(In)
# FUNC:b3Int(In):2018.256


def b3Int(In):
    return ((In[0] * 65536) + (In[1] * 256) + In[2])
###################
# BEGIN: b3TInt(In)
# FUNC:b3TInt(In):2018.267
#   Just for decoding 3-byte Texan raw data.


def b3TInt(In):
    Value = ((In[0] * 65536) + (In[1] * 256) + In[2])
    if Value >= 0x800000:
        Value -= 0x1000000
    return Value
# END: b2I


########################################
# BEGIN: rt72130HeaderTime2Epoch(Packet)
# LIB:rt72130HeaderTime2Epoch():2018.241
#    Returns the number of seconds since Jan 1, 1970.
#    Does not know anything about leap seconds.
def rt72130HeaderTime2Epoch(Packet):
    global Y2EPOCH
    YY = BCDTable[Packet[3]]
    try:
        # This bit of trickery ends up saving quite a bit of time. The Epoch
        # for each year only needs to be loop-calculated once. The rest of the
        # time it just gets looked up.
        Epoch = Y2EPOCH[YY]
    except Exception:
        Epoch = 0.0
# We'll use the 2-character year for Y2EPOCH, but do the math on the
# 4-character year. This way we are not doing this math every time through
# above.
        if YY >= 70:
            YYYY = 1900 + YY
        else:
            YYYY = 2000 + YY
        for YYY in arange(1970, YYYY):
            if YYY % 4 != 0:
                Epoch += 31536000.0
            elif YYY % 100 != 0 or YYY % 400 == 0:
                Epoch += 31622400.0
            else:
                Epoch += 31536000.0
        Y2EPOCH[YY] = Epoch
    try:
        DHMST = "%012X" % iFB(Packet[6:12])
        Epoch += ((float(DHMST[0:3]) - 1) * 86400.0) + \
            (float(DHMST[3:5]) * 3600.0) + \
            (float(DHMST[5:7]) * 60.0) + \
            float(DHMST[7:9]) + \
            (float(DHMST[9:12]) / 1000.0)
    except ValueError:
        Epoch = 0.0
    return Epoch
# END: rt72130HeaderTime2Epoch


#########################################
# BEGIN: rt72130HeaderTime2ydhmst(Packet)
# LIB:rt72130HeaderTime2ydhmst():2018.242
def rt72130HeaderTime2ydhmst(Packet):
    DHMST = "%012X" % iFB(Packet[6:12])
    return (BCDTable[Packet[3]], int(DHMST[0:3]), int(DHMST[3:5]),
            int(DHMST[5:7]), int(DHMST[7:9]), int(DHMST[9:12]))
# END: rt72130HeaderTime2ydhmst


#########################################################################
# BEGIN: rt72130logQGPSPosition(FileLines, Filespec, StopBut, RunningVar)
# LIB:rt72130logQGPSPosition():2018.283
#   Reads a .log file from a 72A or RT130 DAS and returns a quickly-determined
#   GPS position. The routine will try to skip the first five positions it
#   finds in the SOH messages and average the second set of five. This is
#   done to try and weed out bad values. If the DAS's power is going up and
#   down this might not work out. If there are only five or fewer readings in
#   the file then the routine will average those.
#
#   FileLines can be the SOH message lines already read (a List or Tuple).
#   FileLines can be a string and be the filespec to open.
#   Filespec should always be the filespec that is involved. It will be used
#   for error messages.
#   Returns a standard error message or
#       (0, {"file":<File>, "idid:<DASID>, "latf":<float Lat>,
#               "lonf":<float Long>, "elef":<float elev>,
#               "latn":<normalized Lat>, "lonn:":<normalized Long>})
#   The above is roughly like the dictionary structure that the
#   convertFunctions() use.
def rt72130logQGPSPosition(FileLines, Filespec, StopBut, RunningVar):
    if isinstance(FileLines, astring):
        Ret = rt72130ExtractLogData(FileLines)
        if Ret[0] != 0:
            return (2, Ret[1], Ret[2], Ret[3], Filespec)
        Lines = Ret[1]
    else:
        Lines = FileLines
    Positions = []
    PositionsCount = 0
# Start through the file and look for GPS position messages.
    LineCount = 0
    Dict = {}
    DASID = ""
    Dict["idid"] = DASID
    for Line in Lines:
        LineCount += 1
        if LineCount % 500 == 0:
            if StopBut is not None:
                StopBut.update()
                if RunningVar.get() == 0:
                    Fp.close()
                    return (2, "YB", "Stopped.", 2, Filespec)
# Ex: State of Health  01:251:09:41:35:656   ST: 0108
# We just want the DASID
        if Line.startswith("State of Health"):
            # If anything is wrong (like maybe the line is scrambled) just go
            # on.
            try:
                Parts = Line.split()
# The last thing on the line should be the unit ID. Grab it and then check to
# make sure it is a number/hex number. If it is not then keep looking.
                if len(DASID) == 0:
                    DASID = Parts[-1]
                    try:
                        int(DASID, 16)
                        Dict["idid"] = DASID
                    except Exception:
                        DASID = ""
            except Exception:
                pass
# Ex: 253:22:41:25 GPS: POSITION: N43:44:17.12 W096:37:25.27 +00456M
        elif Line.find("GPS: POSITION") != -1:
            InParts = Line.split()
# Any number of scrambled data problems could set this off.
            try:
                Lat = rt72130Str2Dddd(InParts[3])
                Long = rt72130Str2Dddd(InParts[4])
                Elev = floatt(InParts[5])
                if Lat != -1000.0 and Long != -1000.0:
                    if len(Positions) == 5:
                        Positions = Positions[1:]
                    Positions.append([Lat, Long, Elev])
                    PositionsCount += 1
                    if PositionsCount == 10:
                        break
            except Exception:
                pass
    if len(Positions) == 0:
        if isinstance(FileLines, astring):
            return (1, "YB", "%s: No positions found in file." %
                    basename(Filespec), 2, Filespec)
        else:
            return (1, "YB", "No positions found in lines.", 2, Filespec)
    Dict = aveQGPSPositions(Positions, Dict)
    return (0, Dict)
# END: rt72130logQGPSPosition


######################################################################
# BEGIN: rt72130logTimeRange(FileLines, Filespec, StopBut, RunningVar)
# LIB:rt72130logTimeRange():2018.283
#   Reads a .log file from a 72A or RT130 DAS and returns times corresponding
#   to the SOH header times, DSP CLOCK SET and Event Header lines. These three
#   line types are generally trusted to contain a valid time of some kind or
#   another. The caller can then decide to plot each individual time point or
#   lump them all together into a bar plot.
#
#   FileLines can be the SOH message lines already read (a List or Tuple).
#   FileLines can be a string and be the filespec to open.
#   Filespec should always be the filespec that is involved. It will be used
#   for error messages.
#   Returns a standard error message, except no file name at the end, since
#   what was passed may not be a file name, or
#       (0, File, DASID, [time, time, ...])
#   Times are in epoch seconds.

def rt72130logTimeRange(FileLines, Filespec, StopBut, RunningVar):
    if isinstance(FileLines, astring):
        Ret = rt72130ExtractLogData(FileLines)
        if Ret[0] != 0:
            return (2, Ret[1], Ret[2], Ret[3], Ret[4])
        Lines = Ret[1]
    else:
        Lines = FileLines
    Times = []
    SSTimes = []
    HdrEpoch = 0.0
# Start through the file and look for any of these things that have the year
# and date/time. The calls will update the globals.
    LineCount = 0
    DASID = ""
    for Line in Lines:
        LineCount += 1
        if LineCount % 500 == 0:
            if StopBut is not None:
                StopBut.update()
                if RunningVar.get() == 0:
                    Fp.close()
                    return (2, "YB", "Stopped.", 2, Filespec)
# Ex: State of Health  01:251:09:41:35:656   ST: 0108
        if Line.startswith("State of Health"):
            # If anything is wrong (like maybe the line is scrambled) just go
            # on.
            try:
                Parts = Line.split()
# Save this for the ACQ START/STOP section below.
                HdrYYYY = intt(Parts[3])
                if HdrYYYY >= 70:
                    HdrYYYY += 1900
                else:
                    HdrYYYY += 2000
                Time = dt2Timeystr2Epoch(None, Parts[3])
# Also for ACQ START/STOP section.
                HdrEpoch = Time
                Times.append(Time)
# The last thing on the line should be the unit ID. Grab it and then check to
# make sure it is a number/hex number. If it is not then keep looking.
                if len(DASID) == 0:
                    DASID = Parts[-1]
                    try:
                        int(DASID, 16)
                    except Exception:
                        DASID = ""
            except Exception:
                pass
            continue
# Ex 172:23:41:45 DSP CLOCK SET: OLD=01:172:23:41:45.996,
# NEW=01:172:23:41:45.007
        if Line.find("DSP CLOCK SET:") != -1:
            try:
                Parts = Line.split()
                if Parts[5].startswith("NEW="):
                    Time = dt2Timeystr2Epoch(None, Parts[5][4:])
                    Times.append(Time)
            except Exception:
                pass
            continue
# Ex: DAS: 933F   EV: 1131   DS: 1   FST = 2006:310:23:53:31:615
# TT  = 2006:310:23:53:31:615   NS:    360000   SPS:  100   ETO:         0
        if Line.find("FST =") != -1:
            try:
                Parts = Line.split()
# I'm not always sure which Part the FST is since these lines are made up by
# different programs, so search for it.
                Index = 0
                for Part in Parts:
                    if Part == "FST":
                        Time = dt2Timeystr2Epoch(None, Parts[Index + 2])
                        Times.append(Time)
                    if len(DASID) == 0 and Part == "DAS:":
                        DASID = Parts[Index + 1]
                        try:
                            int(DASID, 16)
                        except Exception:
                            DASID = ""
                    Index += 1
            except Exception:
                pass
            continue
# If HdeEpoch is 0.0 we have sot found a Sate Of Health line yet.
        if Line.endswith("ACQUISITION STARTED") and HdrEpoch != 0.0:
            MsgEpoch = dt2Timeystr2Epoch(HdrYYYY, Line.split()[0])
            if (HdrEpoch - 86400) > MsgEpoch:
                MsgEpoch = dt2Timeystr2Epoch(HdrYYYY + 1, Line.split()[0])
            SSTimes.append(("S", MsgEpoch))
# Might as well throw them in here too.
            Times.append(MsgEpoch)
            continue
        if Line.endswith("ACQUISITION STOPPED") and HdrEpoch != 0.0:
            MsgEpoch = dt2Timeystr2Epoch(HdrYYYY, Line.split()[0])
            if (HdrEpoch - 86400) > MsgEpoch:
                MsgEpoch = dt2Timeystr2Epoch(HdrYYYY + 1, Line.split()[0])
            SSTimes.append(("E", MsgEpoch))
            Times.append(MsgEpoch)
            continue
    if len(Times) == 0:
        if isinstance(FileLines, astring):
            return (1, "YB", "%s: No times found in file." %
                    basename(Filespec), 2, Filespec)
        else:
            return (1, "YB", "No times found in lines.", 2, Filespec)
    return (0, basename(Filespec), DASID, Times, SSTimes)
# END: rt72130logTimeRange


###############################################################
# BEGIN: rt72130mslogsQGPSPosition(Folder, StopBut, RunningVar)
# LIB:rt72130mslogsQGPSPosition():2018.243
#   Basically the same as rt72130logQGPAPosition(), except for first collecting
#   all of the log lines from the files in the passed .mslogs directory/folder.
def rt72130mslogsQGPSPosition(Folder, StopBut, RunningVar):
    # Keep the name without the sep at the end for messages.
    Folderspec = Folder
    if Folderspec.endswith(sep) is False:
        Folderspec += sep
    LOGLines = []
    Ret = readMSLOGSFiles(Folderspec, StopBut, RunningVar)
    if Ret[0] != 0:
        return Ret
    LOGLines = Ret[1]
    Positions = []
    PositionsCount = 0
# Start through the file and look for GPS position messages.
    LineCount = 0
    Dict = {}
    DASID = ""
    Dict["idid"] = DASID
    for Line in LOGLines:
        LineCount += 1
        if LineCount % 500 == 0:
            if StopBut is not None:
                StopBut.update()
                if RunningVar.get() == 0:
                    return (2, "YB", "Stopped.", 2, Folderspec)
# Ex: State of Health  01:251:09:41:35:656   ST: 0108
# We just want the DASID
        if Line.startswith("State of Health"):
            # If anything is wrong (like maybe the line is scrambled) just go
            # on.
            try:
                Parts = Line.split()
# The last thing on the line should be the unit ID. Grab it and then check to
# make sure it is a number/hex number. If it is not then keep looking.
                if len(DASID) == 0:
                    DASID = Parts[-1]
                    try:
                        int(DASID, 16)
                        Dict["idid"] = DASID
                    except Exception:
                        DASID = ""
            except Exception:
                pass
# Ex: 253:22:41:25 GPS: POSITION: N43:44:17.12 W096:37:25.27 +00456M
        elif Line.find("GPS: POSITION") != -1:
            InParts = Line.split()
# Any number of scrambled data problems could set this off.
            try:
                Lat = rt72130Str2Dddd(InParts[3])
                Long = rt72130Str2Dddd(InParts[4])
                Elev = floatt(InParts[5])
                if Lat != -1000.0 and Long != -1000.0:
                    if len(Positions) == 5:
                        Positions = Positions[1:]
                    Positions.append([Lat, Long, Elev])
                    PositionsCount += 1
                    if PositionsCount == 10:
                        break
            except Exception:
                pass
    if len(Positions) == 0:
        if isinstance(FileLines, astring):
            return (1, "YB", "%s: No positions found in file." %
                    basename(Filespec), 2, Filespec)
        else:
            return (1, "YB", "No positions found in lines.", 2, Filespec)
    Dict = aveQGPSPositions(Positions, Dict)
    return (0, Dict)
# END: rt72130mslogsQGPSPosition


############################################################
# BEGIN: rt72130mslogsTimeRange(Folder, StopBut, RunningVar)
# LIB:rt72130mslogsTimeRange():2018.243
#   Basically the same as rt72130logTimerange(), except for first collecting
#   all of the log lines from the files in the passed .mslogs directory/folder.
def rt72130mslogsTimeRange(Folder, StopBut, RunningVar):
    Folderspec = Folder
    if Folderspec.endswith(sep) is False:
        Folderspec += sep
    LOGLines = []
    Ret = readMSLOGSFiles(Folderspec, StopBut, RunningVar)
    if Ret[0] != 0:
        return Ret
    LOGLines = Ret[1]
    Times = []
    SSTimes = []
    HdrEpoch = 0.0
# Start through the file and look for any of these things that have the year
# and date/time. The calls will update the globals.
    LineCount = 0
    DASID = ""
    for Line in LOGLines:
        LineCount += 1
        if LineCount % 500 == 0:
            if StopBut is not None:
                StopBut.update()
                if RunningVar.get() == 0:
                    Fp.close()
                    return (2, "YB", "Stopped.", 2, Folderspec)
# Ex: State of Health  01:251:09:41:35:656   ST: 0108
        if Line.startswith("State of Health"):
            # If anything is wrong (like maybe the line is scrambled) just go
            # on.
            try:
                Parts = Line.split()
# Save this for the ACQ START/STOP section below.
                HdrYYYY = intt(Parts[3])
                if HdrYYYY >= 70:
                    HdrYYYY += 1900
                else:
                    HdrYYYY += 2000
                Time = dt2Timeystr2Epoch(None, Parts[3])
# Also for ACQ START/STOP section.
                HdrEpoch = Time
                Times.append(Time)
# The last thing on the line should be the unit ID. Grab it and then check to
# make sure it is a number/hex number. If it is not then keep looking.
                if len(DASID) == 0:
                    DASID = Parts[-1]
                    try:
                        int(DASID, 16)
                    except Exception:
                        DASID = ""
            except Exception:
                pass
            continue
# Ex 172:23:41:45 DSP CLOCK SET: OLD=01:172:23:41:45.996,
# NEW=01:172:23:41:45.007
        if Line.find("DSP CLOCK SET:") != -1:
            try:
                Parts = Line.split()
                if Parts[5].startswith("NEW="):
                    Time = dt2Timeystr2Epoch(None, Parts[5][4:])
                    Times.append(Time)
            except Exception:
                pass
            continue
# Ex: DAS: 933F   EV: 1131   DS: 1   FST = 2006:310:23:53:31:615
# TT  = 2006:310:23:53:31:615   NS:    360000   SPS:  100   ETO:         0
        if Line.find("FST =") != -1:
            try:
                Parts = Line.split()
# I'm not always sure which Part the FST is since these lines are made up by
# different programs, so search for it.
                Index = 0
                for Part in Parts:
                    if Part == "FST":
                        Time = dt2Timeystr2Epoch(None, Parts[Index + 2])
                        Times.append(Time)
                    if len(DASID) == 0 and Part == "DAS:":
                        DASID = Parts[Index + 1]
                        try:
                            int(DASID, 16)
                        except Exception:
                            DASID = ""
                    Index += 1
            except Exception:
                pass
            continue
# If HdeEpoch is 0.0 we have sot found a Sate Of Health line yet.
        if Line.endswith("ACQUISITION STARTED") and HdrEpoch != 0.0:
            MsgEpoch = dt2Timeystr2Epoch(HdrYYYY, Line.split()[0])
            if (HdrEpoch - 86400) > MsgEpoch:
                MsgEpoch = dt2Timeystr2Epoch(HdrYYYY + 1, Line.split()[0])
            SSTimes.append(("S", MsgEpoch))
# Might as well throw them in here too.
            Times.append(MsgEpoch)
            continue
        if Line.endswith("ACQUISITION STOPPED") and HdrEpoch != 0.0:
            MsgEpoch = dt2Timeystr2Epoch(HdrYYYY, Line.split()[0])
            if (HdrEpoch - 86400) > MsgEpoch:
                MsgEpoch = dt2Timeystr2Epoch(HdrYYYY + 1, Line.split()[0])
            SSTimes.append(("E", MsgEpoch))
            Times.append(MsgEpoch)
            continue
    if len(Times) == 0:
        return (1, "YB", "%s: No times found in folder." % Folder, 2,
                Folderspec)
    return (0, basename(Folder), DASID, Times, SSTimes)
# END: rt72130mslogsTimeRange


###################################################################
# BEGIN: rt72130OMDecode(FromDate, ToDate, Model, Packet, Messages)
# LIB:rt72130OMDecode():2018.256
def rt72130OMDecode(FromDate, ToDate, Model, Packet, Messages):
    # Check to see if the user even wants this packet.
    Epoch = rt72130HeaderTime2Epoch(Packet)
    if Epoch < FromDate or Epoch > ToDate:
        return
    YYYY, DOY, HH, MM, SS, TTT = rt72130HeaderTime2ydhmst(Packet)
    Messages.append("")
    Messages.append(
        "Operating Mode Definition  %02d:%03d:%02d:%02d:%02d:%03d  ST: %s" %
        (YYYY, DOY, HH, MM, SS, TTT, b2Hex(Packet[4:6])))
    Messages.append("    Implemented = %s" % dt2TimeDT(6, Packet[1008:1024]))
    if Model == "RT130":
        Messages.append("    Auto-Dump On ET = %s" %
                        Packet[24:25].decode("latin-1").strip())
        Messages.append("    Auto-Dump Threshold = %s" %
                        Packet[26:28].decode("latin-1").strip())
        Messages.append("    Disk Wrap = %s" %
                        Packet[32:33].decode("latin-1").strip())
        Messages.append("    Disk Retry = %s" %
                        Packet[36:37].decode("latin-1").strip())
    elif Model == "72A":
        Messages.append("    Power State = %s" %
                        Packet[16:18].decode("latin-1").strip())
        Messages.append("    Recording Mode = %s" %
                        Packet[18:20].decode("latin-1").strip())
        Messages.append("    Auto-Dump On ET = %s" %
                        Packet[24:25].decode("latin-1").strip())
        Messages.append("    Auto-Dump Threshold = %s" %
                        Packet[26:28].decode("latin-1").strip())
        Messages.append("    Power-Down Delay = %s" %
                        Packet[28:32].decode("latin-1").strip())
        Messages.append("    Disk Wrap = %s" %
                        Packet[32:33].decode("latin-1").strip())
        Messages.append("    Disk Power = %s" %
                        Packet[34:35].decode("latin-1").strip())
        Messages.append("    Terminator Power = %s" %
                        Packet[35:36].decode("latin-1").strip())
        Messages.append("    Disk Retry = %s" %
                        Packet[36:37].decode("latin-1").strip())
        Messages.append("    Wakeup Start Time = %s" %
                        dt2TimeDT(6, Packet[50:62]))
        Messages.append("    Wakeup Duration = %s" %
                        dt2TimeDT(1, Packet[62:68]))
        Messages.append("    Wakeup Repeat Interval = %s" %
                        dt2TimeDT(1, Packet[68:74]))
        Messages.append("    Wakeup Number Of Intervals = %s" %
                        Packet[74:76].decode("latin-1").strip())
# END: rt72130OMDecode


##############################################################
# BEGIN: rt72130refQGPSPosition(Filespec, StopBut, RunningVar)
# LIB:rt72130refQGPSPosition():2018.271
def rt72130refQGPSPosition(Filespec, StopBut, RunningVar):
    if getsize(Filespec) % 1024 != 0:
        return (1, "MW", "%s: Possibly corrupted." % basename(Filespec), 3,
                Filespec)
    try:
        Fp = open(Filespec, "rb")
    except Exception:
        return (1, "MW", "%s: Error opening file." % basename(Filespec),
                3, Filespec)
    ChunkCount = 0
    Positions = []
    PositionsCount = 0
    Dict = {}
    DASID = ""
    Dict["idid"] = DASID
    Lines = []
    while True:
        # Read through the file 2000 packets at a time to speed things along.
        # This failed once on a corrupted file and the error message meant
        # nothing sensible: IOError: [Error 7] Argment list too long. I
        # suspect even Python couldn't figure out what was going on, so we'll
        # try.
        try:
            Block = Fp.read(2048000)
        except Exception:
            Fp.close()
            return (1, "MW", "%s: IOError: Is the file corrupted?" %
                    basename(Filespec), 3, Filespec)
# This will lose processing of the last block if the file ends badly, but that
# should be caught in the 1024 test above.
        if len(Block) < 1024:
            break
        ChunkCount += 1
        if ChunkCount % 20 == 0:
            if StopBut is not None:
                StopBut.update()
                if RunningVar.get() == 0:
                    Fp.close()
                    return (2, "YB", "Stopped.", 2, Filespec)
# Go through the packets read above. Use the length of the Block so when we
# get to the end of the file we will stop at the last block instead of just
# continuing for a full block.
        for Offset in arange(0, len(Block), 1024):
            Packet = Block[Offset:Offset + 1024]
            if len(Packet) < 1024:
                break
            PacketType = Packet[:2].decode("latin-1")
# We do this to try and keep packets of garbage from getting their times
# processed.
            if PacketType == "SH":
                del Lines[:]
                rt72130SHDecode(0, maxInt, Packet, Lines)
                for Line in Lines:
                    # Ex: State of Health  01:251:09:41:35:656   ST: 0108
                    # We just want the DASID
                    if Line.startswith("State of Health"):
                        # If anything is wrong (like maybe the line is
                        # scrambled) just go on.
                        try:
                            Parts = Line.split()
# The last thing on the line should be the unit ID. Grab it and then check to
# make sure it is a number/hex number. If it is not then keep looking.
                            if len(DASID) == 0:
                                DASID = Parts[-1]
                                try:
                                    int(DASID, 16)
                                    Dict["idid"] = DASID
                                except Exception:
                                    DASID = ""
                        except Exception:
                            pass
# Ex: 253:22:41:25 GPS: POSITION: N43:44:17.12 W096:37:25.27 +00456M
                    elif Line.find("GPS: POSITION") != -1:
                        InParts = Line.split()
# Any number of scrambled data problems could set this off.
                        try:
                            Lat = rt72130Str2Dddd(InParts[3])
                            Long = rt72130Str2Dddd(InParts[4])
                            Elev = floatt(InParts[5])
                            if Lat != -1000.0 and Long - 1000.0:
                                if len(Positions) == 5:
                                    Positions = Positions[1:]
                                Positions.append([Lat, Long, Elev])
                                PositionsCount += 1
                                if PositionsCount == 10:
                                    break
                        except Exception:
                            pass
            if PositionsCount == 10:
                break
    Fp.close()
    if len(Positions) == 0:
        return (1, "YB", "No positions found in file.", 2, Filespec)
    Dict = aveQGPSPositions(Positions, Dict)
    return (0, Dict)
# END: rt72130refQGPSPosition


###########################################################
# BEGIN: rt72130refTimeRange(Filespec, StopBut, RunningVar)
# LIB:rt72130refTimeRange():2018.271
#   Returns (0, filename, DASID, [time, time...]) or
#       (1, color, msg, beep, filespec)
def rt72130refTimeRange(Filespec, StopBut, RunningVar):
    # If the file is not a multiple of 1024 bytes it could mean that the file
    # is corrupted and that could crash LOGPEEK.
    if getsize(Filespec) % 1024 != 0:
        return (1, "MW", "%s: Possibly corrupted." % basename(Filespec), 3,
                Filespec)
    try:
        Fp = open(Filespec, "rb")
    except Exception:
        return (1, "MW", "%s: Error opening file." % basename(Filespec),
                3, [])
    ChunkCount = 0
    DASID = ""
    Times = []
    SSTimes = []
    while True:
        # Read through the file 2000 packets at a time to speed things along.
        # This failed once on a corrupted file and the error message meant
        # nothing sensible: IOError: [Error 7] Argment list too long. I suspect
        # even Python couldn't figure out what was going on, so we'll try.
        try:
            Block = Fp.read(2048000)
        except Exception:
            Fp.close()
            return (1, "MW", "%s: IOError: Is the file corrupted?" %
                    basename(Filespec), 3, Filespec)
# This will lose processing of the last block if the file ends badly, but that
# should be caught in the 1024 test above.
        if len(Block) < 1024:
            break
        ChunkCount += 1
        if ChunkCount % 20 == 0:
            if StopBut is not None:
                StopBut.update()
                if RunningVar.get() == 0:
                    Fp.close()
                    return (2, "YB", "Stopped.", 2, Filespec)
# Go through the packets read above. Use the length of the Block so when we
# get to the end of the file we will stop at the last block instead of just
# continuing for a full block.
        for Offset in arange(0, len(Block), 1024):
            Packet = Block[Offset:Offset + 1024]
            if len(Packet) < 1024:
                break
            PacketType = Packet[:2].decode("latin-1")
# We do this to try and keep packets of garbage from getting their times
# processed.
            if PacketType == "SH":
                Time = rt72130HeaderTime2Epoch(Packet)
                Times.append(Time)
                SETimes = rt72130AcqStartStop(Time, Packet)
                if len(SETimes) > 0:
                    SSTimes += SETimes
# Might as well add these to the regular times since the DAS must have been
# running when they were recorded.
                    for STime in SETimes:
                        Times.append(STime[1])
                if len(DASID) == 0:
                    DASID = b2Hex(Packet[4:6])
    Fp.close()
    return (0, basename(Filespec), DASID, Times, SSTimes)
# END: rt72130refTimeRange


############################################################
# BEGIN: rt72130SCDecode(FromDate, ToDate, Packet, Messages)
# LIB:rt72130SCDecode():2018.241
def rt72130SCDecode(FromDate, ToDate, Packet, Messages):
    # Check to see if the user even wants this packet.
    Epoch = rt72130HeaderTime2Epoch(Packet)
    if Epoch < FromDate or Epoch > ToDate:
        return
    YYYY, DOY, HH, MM, SS, TTT = rt72130HeaderTime2ydhmst(Packet)
    Messages.append("")
    Messages.append(
        "Station Channel Definition  %02d:%03d:%02d:%02d:%02d:%03d  ST: %s" %
        (YYYY, DOY, HH, MM, SS, TTT, b2Hex(Packet[4:6])))
    Messages.append("    Implemented = %s" %
                    dt2TimeDT(6, Packet[1008:1008 + 16]))
    Messages.append("    Experiment Number = %s" %
                    Packet[16:16 + 2].decode("latin-1").strip())
    Messages.append("    Experiment Name = %s" %
                    Packet[18:18 + 24].decode("latin-1").strip())
    Messages.append("    Experiment Comment = %s" %
                    Packet[42:42 + 40].decode("latin-1").strip())
    Messages.append("    Station Number = %s" %
                    Packet[82:82 + 4].decode("latin-1").strip())
    Messages.append("    Station Name = %s" %
                    Packet[86:86 + 24].decode("latin-1").strip())
    Messages.append("    Station Comment = %s" %
                    Packet[110:110 + 40].decode("latin-1").strip())
    Messages.append("    DAS Model Number = %s" %
                    Packet[150:150 + 12].decode("latin-1").strip())
    Messages.append("    DAS Serial Number = %s" %
                    Packet[162:162 + 12].decode("latin-1").strip())
    Messages.append("    Experiment Start Time = %s" %
                    Packet[174:174 + 14].decode("latin-1").strip())
    Messages.append("    Time Clock Type = %s" %
                    Packet[188:188 + 4].decode("latin-1").strip())
    Messages.append("    Clock Serial Number = %s" %
                    Packet[192:192 + 10].decode("latin-1").strip())
    for Offset in arange(202, 202 + 730, 146):
        Channel = intt(Packet[Offset:Offset + 2].decode("latin-1"))
        if Channel == 0:
            continue
        Messages.append("    Channel Number = %d" % Channel)
        Messages.append("        Name = %s" %
                        Packet[Offset + 2:Offset + 2 + 10].decode("latin-1").strip())  # noqa: E501
        Messages.append("        Azimuth = %s" %
                        Packet[Offset + 12:Offset + 12 + 10].decode("latin-1").strip())  # noqa: E501
        Messages.append("        Inclination = %s" %
                        Packet[Offset + 22:Offset + 22 + 10].decode("latin-1").strip())  # noqa: E501
        Messages.append("        X Coordinate = %s" %
                        Packet[Offset + 32:Offset + 32 + 10].decode("latin-1").strip())  # noqa: E501
        Messages.append("        Y Coordinate = %s" %
                        Packet[Offset + 42:Offset + 42 + 10].decode("latin-1").strip())  # noqa: E501
        Messages.append("        Z Coordinate = %s" %
                        Packet[Offset + 52:Offset + 52 + 10].decode("latin-1").strip())  # noqa: E501
        Messages.append("        XY Unit Type = %s" %
                        Packet[Offset + 62:Offset + 62 + 4].decode("latin-1").strip())  # noqa: E501
        Messages.append("        Z Unit Type = %s" %
                        Packet[Offset + 66:Offset + 66 + 4].decode("latin-1").strip())  # noqa: E501
        Messages.append("        Preamp Gain = %s" %
                        Packet[Offset + 70:Offset + 70 + 4].decode("latin-1").strip())  # noqa: E501
        Messages.append("        Sensor Model = %s" %
                        Packet[Offset + 74:Offset + 74 + 12].decode("latin-1").strip())  # noqa: E501
        Messages.append("        Sensor Serial Number = %s" %
                        Packet[Offset + 86:Offset + 86 + 12].decode("latin-1").strip())  # noqa: E501
        Messages.append("        Comments = %s" %
                        Packet[Offset + 98:Offset + 98 + 40].decode("latin-1").strip())  # noqa: E501
        Messages.append("        Adjusted Nominal Bit Weight = %s" %
                        Packet[Offset + 138:Offset + 138 + 8].decode("latin-1").strip())  # noqa: E501
    return
# END: rt72130SCDecode


############################################################
# BEGIN: rt72130SHDecode(FromDate, ToDate, Packet, Messages)
# LIB:rt72130SHDecode():2018.270
def rt72130SHDecode(FromDate, ToDate, Packet, Messages):
    # Check to see if the user even wants this packet.
    Epoch = rt72130HeaderTime2Epoch(Packet)
    if Epoch < FromDate or Epoch > ToDate:
        return
    YYYY, DOY, HH, MM, SS, TTT = rt72130HeaderTime2ydhmst(Packet)
    Messages.append("")
    Messages.append(
        "State of Health  %02d:%03d:%02d:%02d:%02d:%03d   ST: %s" %
        (YYYY, DOY, HH, MM, SS, TTT, b2Hex(Packet[4:6])))
    EOP = bcd2Int(Packet[12:14])
# -1 is to not get a "" in the last List item.
    Messages += Packet[24:EOP].decode("latin-1").split("\r\n")[:-1]
    return
# END: rt72130SHDecode


############################
# BEGIN: rt72130Str2Dddd(LL)
# LIB:rt72130Str2Dddd():2013.190
#   Converts the Reftek Lat/Long from N43:44:17.12, W096:37:25.27 to decimal
#   degrees. Also does  NDDMM.MMM  and  EDDDMM.MMM. Note, the space between N
#   and DD must have been squeezed out (the latitude comes with a space from
#   most Reftek sources).
#   If anything goes wrong it returns -1000.0, which the caller should notice.
def rt72130Str2Dddd(LL):
    try:
        if LL.find(":") != -1:
            Parts = LL.split(":")
            Deg = floatt(Parts[0][1:])
            Deg += floatt(Parts[1]) / 60.0
            Deg += floatt(Parts[2]) / 3600.0
            if Parts[0][0] == "S" or Parts[0][0] == "W":
                Deg *= -1.0
        else:
            if LL.startswith("N") or LL.startswith("S"):
                Deg = floatt(LL[1:3])
                Deg += floatt(LL[3:]) / 60.0
            else:
                Deg = floatt(LL[1:4])
                Deg += floatt(LL[4:]) / 60.0
            if LL.startswith("S") or LL.startswith("W"):
                Deg *= -1.0
    except Exception:
        return -1000.0
    return Deg
# END: rt72130Str2Dddd


######################################
# BEGIN: rtnPattern(In, Upper = False)
# LIB:rtnPattern():2006.114
def rtnPattern(In, Upper=False):
    Rtn = ""
    for c in In:
        if c.isdigit():
            Rtn += "0"
        elif c.isupper():
            Rtn += "A"
        elif c.islower():
            Rtn += "a"
        else:
            Rtn += c
# So the A/a chars will always be A, so the caller knows what to look for.
    if Upper is True:
        return Rtn.upper()
    return Rtn
# END: rtnPattern


####################
# BEGIN: setColors()
# FUNC:setColors():2013.072
#   Uses the value of PROGColorModeRVar and sets the colors in the global DClr
#   dictionary.
def setColors():
    # These are the background colors for highlighting the text in the log
    # display window. We want them to always be the black background colors.
    DClr["ACQStartBG"] = Clr["G"]
    DClr["ACQStopBG"] = Clr["R"]
    DClr["ClockTimeBG"] = Clr["W"]
    DClr["DCDIFFBG"] = Clr["Y"]
    DClr["DEFSBG"] = Clr["W"]
    DClr["DISCREPBG"] = Clr["R"]
    DClr["DRSETBG"] = Clr["Y"]
    DClr["DUMPOnBG"] = Clr["G"]
    DClr["DUMPOffBG"] = Clr["R"]
    DClr["ERRORBG"] = Clr["R"]
    DClr["EVTBG"] = Clr["W"]
    DClr["DUBG"] = Clr["G"]
    DClr["GPSErrBG"] = Clr["M"]
    DClr["GPSOffBG"] = Clr["R"]
    DClr["GPSOnBG"] = Clr["G"]
    DClr["GPSLKBG"] = Clr["W"]
    DClr["ICPEBG"] = Clr["C"]
    DClr["JERKBG"] = Clr["R"]
    DClr["MRCBG"] = Clr["W"]
    DClr["NETDownBG"] = Clr["R"]
    DClr["NETUpBG"] = Clr["G"]
    DClr["PWRUPBG"] = Clr["G"]
    DClr["CLKPWRBG"] = Clr["G"]
    DClr["RESETBG"] = Clr["Y"]
    DClr["SOHBG"] = Clr["C"]
    DClr["TEMPBG"] = Clr["C"]
    DClr["TMJMPPosBG"] = Clr["R"]
    DClr["TMJMPNegBG"] = Clr["G"]
    DClr["VOLTBG"] = Clr["G"]
    DClr["BKUPGBG"] = Clr["G"]
    DClr["BKUPBBG"] = Clr["Y"]
    DClr["BKUPUBG"] = Clr["R"]
    DClr["WARNBG"] = Clr["Y"]
    DClr["WARNBGM"] = Clr["O"]
    if PROGColorModeRVar.get() == "B":
        DClr["CanText"] = Clr["W"]
        DClr["ACQStart"] = Clr["G"]
        DClr["ACQStop"] = Clr["R"]
        DClr["MFCAN"] = Clr["B"]
        DClr["GPSCanvas"] = Clr["K"]
        DClr["GPSDots"] = Clr["W"]
        DClr["DCDIFF"] = Clr["Y"]
        DClr["DEFS"] = Clr["W"]
        DClr["DISCREP"] = Clr["R"]
        DClr["DU"] = Clr["G"]
        DClr["DRSET"] = Clr["Y"]
        DClr["DUMPOn"] = Clr["G"]
        DClr["DUMPOff"] = Clr["R"]
        DClr["ERROR"] = Clr["R"]
        DClr["EVT"] = Clr["W"]
        DClr["GPSErr"] = Clr["M"]
        DClr["GPSOff"] = Clr["R"]
        DClr["GPSOn"] = Clr["G"]
        DClr["GPSLK"] = Clr["W"]
        DClr["Grid"] = Clr["y"]
        DClr["ICPE"] = Clr["C"]
        DClr["JERK"] = Clr["R"]
        DClr["Label"] = Clr["C"]
        DClr["MaxMinL"] = Clr["K"]
        DClr["MRC"] = Clr["W"]
        DClr["NETDown"] = Clr["R"]
        DClr["NETUp"] = Clr["G"]
        DClr["Plot"] = Clr["A"]
        DClr["PWRUP"] = Clr["G"]
        DClr["CLKPWR"] = Clr["G"]
        DClr["RESET"] = Clr["Y"]
        DClr["Sel"] = Clr["Y"]
        DClr["SelTPS"] = Clr["O"]
        DClr["SOH"] = Clr["C"]
        DClr["TEMP"] = Clr["C"]
        DClr["TMJMPPos"] = Clr["R"]
        DClr["Ticks"] = Clr["W"]
        DClr["TMJMPNeg"] = Clr["G"]
        DClr["VOLT"] = Clr["G"]
        DClr["BKUP"] = Clr["W"]
        DClr["BKUPG"] = Clr["G"]
        DClr["BKUPB"] = Clr["Y"]
        DClr["BKUPU"] = Clr["R"]
        DClr["MP"] = Clr["W"]
        DClr["MPP"] = Clr["C"]
        DClr["MPG"] = Clr["G"]
        DClr["MPB"] = Clr["Y"]
        DClr["MPU"] = Clr["R"]
        DClr["MPH"] = Clr["M"]
        DClr["WARN"] = Clr["Y"]
        DClr["WARNM"] = Clr["O"]
# For the raw data plot.
        DClr["RAWD"] = Clr["G"]
        DClr["RAWT"] = Clr["W"]
    elif PROGColorModeRVar.get() == "W":
        DClr["CanText"] = Clr["B"]
        DClr["ACQStart"] = Clr["U"]
        DClr["ACQStop"] = Clr["B"]
        DClr["MFCAN"] = Clr["W"]
        DClr["GPSCanvas"] = Clr["W"]
        DClr["GPSDots"] = Clr["B"]
        DClr["DCDIFF"] = Clr["B"]
        DClr["DEFS"] = Clr["U"]
        DClr["DISCREP"] = Clr["B"]
        DClr["DU"] = Clr["B"]
        DClr["DRSET"] = Clr["B"]
        DClr["DUMPOn"] = Clr["U"]
        DClr["DUMPOff"] = Clr["B"]
        DClr["ERROR"] = Clr["B"]
        DClr["EVT"] = Clr["B"]
        DClr["GPSErr"] = Clr["B"]
        DClr["GPSOff"] = Clr["B"]
        DClr["GPSOn"] = Clr["U"]
        DClr["GPSLK"] = Clr["B"]
        DClr["Grid"] = Clr["A"]
        DClr["ICPE"] = Clr["B"]
        DClr["JERK"] = Clr["B"]
        DClr["Label"] = Clr["B"]
        DClr["MaxMinL"] = Clr["E"]
        DClr["MRC"] = Clr["B"]
        DClr["NETDown"] = Clr["B"]
        DClr["NETUp"] = Clr["U"]
        DClr["Plot"] = Clr["A"]
        DClr["PWRUP"] = Clr["U"]
        DClr["CLKPWR"] = Clr["U"]
        DClr["RESET"] = Clr["B"]
        DClr["Sel"] = Clr["A"]
        DClr["SelTPS"] = Clr["O"]
        DClr["SOH"] = Clr["B"]
        DClr["TEMP"] = Clr["B"]
        DClr["Ticks"] = Clr["B"]
        DClr["TMJMPPos"] = Clr["B"]
        DClr["TMJMPNeg"] = Clr["U"]
        DClr["VOLT"] = Clr["U"]
        DClr["BKUP"] = Clr["U"]
        DClr["BKUPG"] = Clr["E"]
        DClr["BKUPB"] = Clr["A"]
        DClr["BKUPU"] = Clr["B"]
        DClr["MP"] = Clr["B"]
        DClr["MPP"] = Clr["E"]
        DClr["MPG"] = Clr["A"]
        DClr["MPB"] = Clr["K"]
        DClr["MPU"] = Clr["U"]
        DClr["MPH"] = Clr["B"]
        DClr["WARN"] = Clr["U"]
        DClr["WARNM"] = Clr["B"]
        DClr["RAWD"] = Clr["B"]
        DClr["RAWT"] = Clr["B"]
# END: setColors


########################################################################
# BEGIN: setMsg(WhichMsg, Colors = "", Message = "", Beep = 0, e = None)
# LIB:setMsg():2018.236
#   Be careful to pass all of the arguments if this is being called by an
#   event.
def setMsg(WhichMsg, Colors="", Message="", Beep=0, e=None):
    # So callers don't have to always be checking for this.
    if WhichMsg is None:
        return
# This might get called when a window is not, or never has been up so try
# everything.
    try:
        # This will be the common way to call it.
        if isinstance(WhichMsg, astring):
            LMsgs = [PROGMsg[WhichMsg]]
        elif isinstance(WhichMsg, (tuple, list)):
            LMsgs = []
            for Which in WhichMsg:
                if isinstance(Which, astring):
                    LMsgs.append(PROGMsg[Which])
                else:
                    LMsgs.append(Which)
        else:
            LMsgs = [WhichMsg]
# Colors may be a standard error message. If it is break it up such that the
# rest of the function won't know the difference.
        if isinstance(Colors, tuple):
            # Some callers may not pass a Beep value if it is 0.
            try:
                Beep = Colors[3]
            except IndexError:
                Beep = 0
# The passed Message may not be "". If it is 1 append the [4] part of the
# standard error message to part [2] with a space, if it is 2 append it with
# a \n, and if 3 append it with '\n   '. Leave the results in Message.
            if isinstance(Message, anint) is False and len(Message) == 0:
                Message = Colors[2]
            elif Message == 1:
                Message = Colors[2] + " " + Colors[4]
            elif Message == 2:
                Message = Colors[2] + "\n" + Colors[4]
            elif Message == 3:
                Message = Colors[2] + "\n   " + Colors[4]
            Colors = Colors[1]
        for LMsg in LMsgs:
            try:
                LMsg.configure(state=NORMAL)
                LMsg.delete("0.0", END)
# This might get passed. Just ignore it in this function.
                if Colors.find("X") != -1:
                    Colors = Colors.replace("X", "")
                if len(Colors) == 0:
                    LMsg.configure(bg=Clr["W"], fg=Clr["B"])
                else:
                    LMsg.configure(bg=Clr[Colors[0]], fg=Clr[Colors[1]])
                LMsg.insert(END, Message)
                LMsg.update()
                LMsg.configure(state=DISABLED)
            except Exception:
                pass
# This may get called from a generated event with no Beep value set.
        if isinstance(Beep, anint) and Beep > 0:
            beep(Beep)
        updateMe(0)
    except (KeyError, TclError):
        pass
    return
########################################################################
# BEGIN: setTxt(WhichTxt, Colors = "", Message = "", Beep = 0, e = None)
# FUNC:setTxt():2018.236
#   Same as above, but for Text()s.


def setTxt(WhichTxt, Colors="", Message="", Beep=0, e=None):
    if WhichTxt is None:
        return
    try:
        if isinstance(WhichTxt, astring):
            LTxt = PROGTxt[WhichTxt]
        else:
            LTxt = WhichTxt
        if isinstance(Colors, tuple):
            Message = Colors[2]
            try:
                Beep = Colors[3]
            except IndexError:
                Beep = 0
            Colors = Colors[1]
        LTxt.delete("0.0", END)
        if Colors.find("X") != -1:
            Colors = Colors.replace("X", "")
        if len(Colors) == 0:
            LTxt.configure(bg=Clr["W"], fg=Clr["B"])
        else:
            LTxt.configure(bg=Clr[Colors[0]], fg=Clr[Colors[1]])
        LTxt.insert(END, Message)
        LTxt.update()
        if isinstance(Beep, anint) and Beep > 0:
            beep(Beep)
        updateMe(0)
    except (KeyError, TclError):
        pass
    return
# END: setMsg


###########################
# BEGIN: formSHOW(e = None)
# FUNC:formSHOW():2019.004
#   A quick and dirty LOG/text file-shower.
PROGFrm["SHOW"] = None
SHOWFindLookForVar = StringVar()
SHOWFindLastLookForVar = StringVar()
SHOWFindLinesVar = StringVar()
SHOWFindIndexVar = IntVar()
SHOWFindUseCaseCVar = IntVar()
PROGSetups += ["SHOWFindLookForVar"]


def formSHOW(e=None):
    Indexes = MFFiles.curselection()
    if len(Indexes) == 0:
        beep(1)
        return
    PROGDataDirVar.set(PROGDataDirVar.get().strip())
    if PROGDataDirVar.get().endswith(sep) is False:
        PROGDataDirVar.set(PROGDataDirVar.get() + sep)
# Just a good place to always keep these in sync.
        PROGMsgsDirVar.set(PROGDataDirVar.get())
        PROGWorkDirVar.set(PROGDataDirVar.get())
    Dir = PROGDataDirVar.get()
    try:
        Files = []
        for ind_ in Indexes:
            File = MFFiles.get(ind_)
            if len(File) == 0:
                continue
# The entry will probably be something like "Filename (x bytes or lines)" so
# just get the Filename.
            Parts = File.split()
            if Parts[0].endswith(".log") is False and \
                    Parts[0].endswith("_log") is False:
                continue
            Files.append(Parts[0])
        if len(Files) == 0:
            beep(1)
            return
    except TclError:
        beep(1)
        return
# Won't use showUp() here. Don't want the form to show up and then go away.
    if PROGFrm["SHOW"] is not None:
        formClose("SHOW")
    LFrm = PROGFrm["SHOW"] = Toplevel(Root)
    LFrm.withdraw()
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "SHOW"))
    if len(Files) == 1:
        LFrm.title("Show - %s" % Files[0])
    else:
        LFrm.title("Show - %s..." % Files[0])
    Sub = Frame(LFrm)
# Since the lines do not need to be a fixed-width font then just make the font
# the Msg font.
    LTxt = PROGTxt["SHOW"] = Text(Sub, font=PROGPropFont, height=30,
                                  width=90, relief=SUNKEN, wrap=NONE)
    LTxt.pack(side=LEFT, expand=YES, fill=BOTH)
    LSb = Scrollbar(Sub, orient=VERTICAL, command=LTxt.yview)
    LSb.pack(side=RIGHT, fill=Y)
    LTxt.configure(yscrollcommand=LSb.set)
    Sub.pack(side=TOP, expand=YES, fill=BOTH)
    LSb = Scrollbar(LFrm, orient=HORIZONTAL, command=LTxt.xview)
    LSb.pack(side=TOP, fill=X)
    LTxt.configure(xscrollcommand=LSb.set)
    Sub = Frame(LFrm)
    labelTip(Sub, "Find:=", LEFT, 30, "[Find]")
    LEnt = Entry(Sub, width=20, textvariable=SHOWFindLookForVar)
    LEnt.pack(side=LEFT)
    LEnt.bind("<Return>", Command(formFind, "SHOW", "SHOW"))
    LEnt.bind("<KP_Enter>", Command(formFind, "SHOW", "SHOW"))
    BButton(Sub, text="Find", command=Command(formFind, "SHOW",
                                              "SHOW")).pack(side=LEFT)
    BButton(Sub, text="Next", command=Command(formFindNext,
                                              "SHOW", "SHOW")).pack(side=LEFT)
    Label(Sub, text=" ").pack(side=LEFT)
    BButton(Sub, text="Close", fg=Clr["R"],
            command=Command(formClose, "SHOW")).pack(side=LEFT)
    Sub.pack(side=TOP, padx=3, pady=3)
    PROGMsg["SHOW"] = Text(LFrm, cursor="", font=PROGPropFont,
                           height=3, wrap=WORD)
    PROGMsg["SHOW"].pack(side=TOP, fill=X)
    center(Root, "SHOW", "CX", "I", True)
# Open each file and stuff it into the Text().
    LinesRead = []
    FilterSOH = OPTFilterSOHCVar.get()
    for File in Files:
        setMsg("SHOW", "CB", "Reading %s..." % File)
        LTxt.insert(END, "#################\n")
        LTxt.insert(END, "FILE: %s\n" % File)
        LTxt.insert(END, "#################\n\n")
        Filespec = Dir + File
        if FilterSOH == 0:
            Ret = readFileLinesRB(Filespec)
        else:
            Ret = filterSOH(Filespec, PROGStopBut, PROGRunning)
        if Ret[0] != 0:
            setMsg("SHOW", Ret)
            return
        Lines = Ret[1]
        LinesRead.append(len(Lines))
        Count = 0
        for Line in Lines:
            # Just in case.
            Line = line2ASCII(Line)
            LTxt.insert(END, "%s\n" % Line)
            Count += 1
            if Count % 100000 == 0:
                setMsg("SHOW", "CB", "Reading %s (%d lines)..." %
                       (File, Count))
        LTxt.insert(END, "\n")
    setMsg("SHOW", "", "Done. %d lines." % sum(LinesRead))
    return "break"
# END: formSHOW


#####################
# BEGIN: showUp(Fram)
# LIB:showUp():2018.236
def showUp(Fram):
    # If anything should go wrong just close the form and let the caller fix it
    # (i.e. redraw it).
    try:
        if PROGFrm[Fram] is not None:
            PROGFrm[Fram].deiconify()
            PROGFrm[Fram].lift()
            PROGFrm[Fram].focus_set()
            return True
    except TclError:
        # This call makes sure that the PROGFrm[] value gets set to None.
        formClose(Fram)
    return False
# END: showUp


###########################
# BEGIN: sP(Count, Phrases)
# LIB:sP():2012.223
def sP(Count, Phrases):
    if Count == 1 or Count == -1:
        return Phrases[0]
    else:
        return Phrases[1]
# END: sP


########################
# BEGIN: startupThings()
# LIB:startupThings():2019.026
#   A set of functions that programs call to get things going. Some programs
#   may or may not use these.
#   If START_GETGMT is defined it can be used to change the time format in the
#   .msg start/stop messages.
# This can be used if needed. Copy the lines up to the command line argument
# loop area. PROG_SETUPSUSECWD will need to be there even if not used.
#    PROG_SETUPSUSECWD = False
#        elif Arg == "-cwd":
#            PROG_SETUPSUSECWD = True
#########################
# BEGIN: loadPROGSetups()
# FUNC:loadPROGSetups():2019.026
#   A standard setups file loader for programs that don't require anything
#   special loaded from the .set file just the PROGSetups items.
#   PROGSetupsFilespec is just for formABOUT() to use.
#   Note the return codes are a bit specific.
PROGSetupsFilespec = ""


def loadPROGSetups():
    global PROGSetupsFilespec
# If there is a local function we'll call it as we go through the lines.
    CallLocal = "loadPROGSetupsLocal" in globals()
    SetupsDir = PROGSetupsDirVar.get()
    if len(SetupsDir) == 0:
        SetupsDir = "%s%s" % (abspath("."), sep)
    PROGSetupsFilespec = "%sset%s%s.set" % (SetupsDir, PROG_NAMELC,
                                            PROGUserIDLC)
# The .set file may not be there. This may be the first time the program has
# been run. Try to create a blank file. If that fails just go on. The user
# will hear about it later.
    if exists(PROGSetupsFilespec) is False:
        try:
            Fp = open(PROGSetupsFilespec, "a")
            Fp.close()
        except Exception:
            pass
        return (1, "", "Setups file not found. Was looking for\n   %s" %
                PROGSetupsFilespec, 0, "")
    if PROGIgnoreSetups is True:
        return (0, "WB", "Setups ignored from\n   %s" % PROGSetupsFilespec,
                0, "")
# The file should not be corrupted, but we don't know where it has been, so
# use readFileLinesRB().
    Ret = readFileLinesRB(PROGSetupsFilespec, True)
    if Ret[0] != 0:
        return (5, Ret[1], Ret[2], Ret[3], Ret[4], Ret[5])
    Lines = Ret[1]
    if len(Lines) == 0:
        return (0, "WB", "No setup lines found in\n   %s" % PROGSetupsFilespec,
                0, "")
    Found = False
    VersGood = False
# Just catch anything that might go wrong and blame it on the .set file.
    try:
        for Line in Lines:
            if len(Line) == 0 or Line.startswith("#"):
                continue
            if Line.startswith(PROG_NAME + ":"):
                Parts = Line.split()
                for Index in arange(0, len(Parts)):
                    Parts[Index] = Parts[Index].strip()
# In case an old setups file is read that does not have the version item.
                Parts += [""] * (3 - len(Parts))
# If the version doesn't match what the program wants then don't set VersGood
# and everything will be left with it's default value.
                if Parts[2] == PROG_SETUPSVERS:
                    VersGood = True
                Found = True
                continue
            if Found is True and VersGood is True:
                if CallLocal is True:
                    # For handling a little more complicated return value.
                    Ret = loadPROGSetupsLocal(Line)
# The passed Line didn't have anything to do with the local stuff.
                    if Ret[0] == 0:
                        pass
# The local function processed the Line. Go on to the next.
                    elif Ret[0] == 1:
                        continue
# There was an exception. Do the same as the exception clause in here. The
# local function should return the same message as below.
                    elif Ret[0] == 2:
                        return Ret
                Parts = Line.split(";", 1)
                for Index in arange(0, len(Parts)):
                    Parts[Index] = Parts[Index].strip()
                for Item in PROGSetups:
                    if Parts[0] == Item:
                        if isinstance(eval(Item), StringVar):
                            eval(Item).set(Parts[1])
                            break
                        elif isinstance(eval(Item), IntVar):
                            eval(Item).set(int(Parts[1]))
                            break
        if Found is False:
            return (3, "YB", "No %s setups found in\n   %s" %
                    (PROG_NAME, PROGSetupsFilespec), 2, "")
        else:
            if VersGood is False:
                return (4, "YB", "Setups version mismatch. Using defaults.",
                        0, "")
            return (0, "WB", "Setups loaded from\n   %s" % PROGSetupsFilespec,
                    0, "")
    except Exception as e:
        return (5, "MW",
                "Error loading setups from\n   %s\n   %s\n   Was loading line "
                "%s" % (PROGSetupsFilespec, e, Line), 3, "")
#########################
# BEGIN: savePROGSetups()
# FUNC:savePROGSetups():2018.236


def savePROGSetups():
    # If there is a local function we'll call it as we go through.
    CallLocal = "savePROGSetupsLocal" in globals()
# Same as above.
    try:
        Fp = open(PROGSetupsFilespec, "w")
    except Exception as e:
        stdout.write("savePROGSetups(): %s\n" % e)
        return (2, "MW", "Error opening setups file\n   %s\n   %s" %
                (PROGSetupsFilespec, e), 3, "")
    try:
        Fp.write("%s: %s %s\n" % (PROG_NAME, PROG_VERSION, PROG_SETUPSVERS))
        for Item in PROGSetups:
            if isinstance(eval(Item), StringVar):
                Fp.write("%s; %s\n" % (Item, eval(Item).get()))
            elif isinstance(eval(Item), IntVar):
                Fp.write("%s; %d\n" % (Item, eval(Item).get()))
# Keep the local stuff last, since that's where stuff from individual programs
# usually needs to be.
        if CallLocal is True:
            Ret = savePROGSetupsLocal(Fp)
            if Ret[0] != 0:
                Fp.close()
                return Ret
        Fp.close()
        return (0, "", "Setups saved to\n   %s" % PROGSetupsFilespec, 0, "")
    except Exception as e:
        Fp.close()
        return (2, "MW", "Error saving setups to\n   %s\n   %s" %
                (PROGSetupsFilespec, e), 3, "")
###########################
# BEGIN: deletePROGSetups()
# FUNC:deletePROGSetups():2018.235


def deletePROGSetups():
    Answer = formMYD(Root, (("Delete And Quit", TOP, "doit"),
                            ("Cancel", TOP, "cancel")),
                     "cancel", "YB", "Be careful.",
                     "This will delete the current setups file and quit the "
                     "program. This should only need to be done if the "
                     "contents of the current setups file is known to be "
                     "causing the program problems.")
    if Answer == "cancel":
        return
    SetupsDir = PROGSetupsDirVar.get()
    if len(SetupsDir) == 0:
        SetupsDir = "%s%s" % (abspath("."), sep)
    Filespec = SetupsDir + "set" + PROG_NAMELC + PROGUserIDLC + ".set"
    try:
        remove(Filespec)
    except Exception as e:
        formMYD(Root, (("(OK)", TOP, "ok"), ), "ok", "MW", "Oh No.",
                "The setups file could not be deleted.\n\n%s" % e)
        return
    progQuitter(False)
    return


#######################
# BEGIN: setMachineID()
# FUNC:setMachineID():2018.235
#   Asks the user what the name of the computer is for log file names, etc.
PROGMachineIDUCVar = StringVar()
PROGMachineIDLCVar = StringVar()
PROGSetups += ["PROGMachineIDUCVar", "PROGMachineIDLCVar"]


def setMachineID():
    MYDAnswerVar.set(PROGMachineIDUCVar.get())
    if PROGKiosk is False:
        Answer = formMYD(Root, (("Input12", TOP, "input"),
                                ("(OK)", LEFT, "input"),
                                ("Quit", LEFT, "quit")),
                         "cancel", "", "Who Am I?",
                         "Enter the ID of this computer. The ID may be left "
                         "blank if appropriate.")
    else:
        Answer = formMYD(Root, (("Input12", TOP, "input"),
                                ("(OK)", LEFT, "input")),
                         "ok", "", "Who Am I?",
                         "Enter the ID of this computer. The ID may be left "
                         "blank if appropriate.")
# Keep going in here until the user gives a valid, or no answer.
    while True:
        if Answer == "quit":
            return (2, "", "Quit.", 0, "")
        elif Answer == "cancel" or len(Answer) == 0:
            Answer = ""
        else:
            if Answer.isalnum() is False:
                if PROGKiosk is False:
                    Answer = formMYD(Root,
                                     (("Input12", TOP, "input"),
                                      ("(OK)", LEFT, "input"),
                                      ("Quit", LEFT, "quit")),
                                     "cancel", "YB", "Who Am I?",
                                     "The ID may only be letters and numbers."
                                     "\n\nEnter the ID of this computer. The "
                                     "ID may be left blank if appropriate.")
                else:
                    Answer = formMYD(Root, (("Input12", TOP, "input"),
                                            ("(OK)", LEFT, "input")),
                                     "ok", "YB", "Who Am I?",
                                     "The ID may only be letters and numbers."
                                     "\n\nEnter the ID of this computer. The "
                                     "ID may be left blank if appropriate.")
                continue
        break
    PROGMachineIDUCVar.set(Answer.strip().upper())
    PROGMachineIDLCVar.set(Answer.strip().lower())
    return (0, )


##########################
# BEGIN: setPROGMsgsFile()
# FUNC:setPROGMsgsFile():2018.334
PROGMsgsFile = ""


def setPROGMsgsFile():
    global PROGMsgsFile
    PROGMsgsFile = ""
    StartsWith = PROGMachineIDLCVar.get()
    EndsWith = PROG_NAMELC + ".msg"
    if len(StartsWith) != 0:
        # Special case. Check to see if an "old style" (ccIDblah.msg) messages
        # file is laying around before looking for the ccID-YYYYDOY-blah.msg
        # style. If it is then use that one.
        if exists(PROGMsgsDirVar.get() + StartsWith + EndsWith):
            PROGMsgsFile = StartsWith + EndsWith
            msgLn(9, "", "Working with messages file\n   %s" %
                  (PROGMsgsDirVar.get() + PROGMsgsFile))
            return
# If there isn't one just add the dash and look for the new style.
        StartsWith += "-"
# In case the setups file came from another machine (usually the reason).
    if exists(PROGMsgsDirVar.get()) is False:
        # Hopefully PROGSetupsDirVar points to somewhere sensible.
        PROGMsgsDirVar.set(PROGSetupsDirVar.get())
    Files = sorted(listdir(PROGMsgsDirVar.get()))
# Go through all of the files so we get the "latest" one (since they have been
# sorted).
    Temp = ""
    for File in Files:
        if File.endswith(EndsWith):
            if len(StartsWith) != 0:
                if File.startswith(StartsWith):
                    Temp = File
# If the user didn't enter anything then these are the only two acceptable
# possibilities.
            elif rtnPattern(File).startswith("0000000-"):
                Temp = File
            elif File == EndsWith:
                Temp = File
    if len(Temp) == 0:
        if len(StartsWith) != 0:
            PROGMsgsFile = StartsWith
        PROGMsgsFile += (getGMT(1)[:7] + "-" + EndsWith)
    else:
        PROGMsgsFile = Temp
# If this file doesn't exist just create an empty one so there will be
# something for the program to find next time it is started on the off chance
# that nothing gets written to it this time around.
    if exists(PROGMsgsDirVar.get() + PROGMsgsFile) is False:
        try:
            Fp = open(PROGMsgsDirVar.get() + PROGMsgsFile, "w")
            Fp.close()
        except Exception as e:
            msgLn(9, "MW", "Error creating messages file\n   %s\n   %s" %
                  ((PROGMsgsDirVar.get() + PROGMsgsFile), e), True, 3)
    msgLn(9, "", "Working with messages file\n   %s" % (PROGMsgsDirVar.get() +
                                                        PROGMsgsFile))
    return
###############################################
# BEGIN: loadPROGMsgs(Speak = True, Ask = True)
# FUNC:loadPROGMsgs():2018.340


def loadPROGMsgs(Speak=True, Ask=True):
    try:
        if exists(PROGMsgsDirVar.get() + PROGMsgsFile):
            # These files should not be corrupted, but you never know, so use
            # RB.
            Ret = readFileLinesRB(PROGMsgsDirVar.get() + PROGMsgsFile)
            if Ret[0] != 0:
                # Don't write this to the messages file since we can't get it
                # open.
                msgLn(9, "M", Ret[2], True, Ret[3])
                msgLn(9, "M", "This should be fixed before continuing.")
                return
            Lines = Ret[1]
# An empty messages file may get created before coming here.
            if len(Lines) != 0:
                if Ask is True:
                    Answer = formMYD(Root,
                                     (("(Yes)", LEFT, "yes"),
                                      ("No", LEFT, "no")),
                                     "no", "", "Load It?",
                                     "There already is a messages file named"
                                     "\n\n%s\n\nNumber of lines: %d\n\nThis "
                                     "file will be used for the messages. Do "
                                     "you want to load its current contents "
                                     "into the messages section?" %
                                     (PROGMsgsFile, len(Lines)))
                else:
                    Answer = "yes"
                if Answer == "yes":
                    # Write directly to the messages section rather than use
                    # msgLn. It's much faster.
                    for Line in Lines:
                        MMsg.insert(END, Line + "\n")
                    MMsg.see(END)
        if Speak is True:
            GetGMT = 0
            if "START_GETGMT" in globals():
                GetGMT = START_GETGMT
            writeFile(0, "MSG", "==== %s %s started %s ====\n" %
                      (PROG_NAME, PROG_VERSION, getGMT(GetGMT)))
    except Exception as e:
        # Same here.
        msgLn(9, "M", "Error reading %s" % PROGMsgsFile, True, 3)
        msgLn(9, "M", "   %s" % str(e))
        msgLn(9, "M", "This should be fixed before continuing.")
    return


###########################
# BEGIN: setPROGSetupsDir()
# FUNC:setPROGSetupsDir():2018.334
#   Figures out what the "home" directory is.
PROGSetupsDirVar = StringVar()
# Default to the current directory if this is never called.
PROGSetupsDirVar.set("%s%s" % (abspath("."), sep))
if PROG_SETUPSUSECWD is False:
    PROGSetupsDirVar.set("%s%s" % (abspath("."), sep))
else:
    PROGSetupsDirVar.set(getCWD())


def setPROGSetupsDir():
    if PROG_SETUPSUSECWD is True:
        return (0,)
    try:
        if PROGSystem == 'dar' or PROGSystem == 'lin' or PROGSystem == 'sun':
            Dir = environ["HOME"]
            LookFor = "HOME"
# Of course Windows had to be different.
        elif PROGSystem == 'win':
            Dir = environ["HOMEDRIVE"] + environ["HOMEPATH"]
            LookFor = "HOMEDRIVE+HOMEPATH"
        else:
            return (2, "RW", "I don't know how to get the HOME directory on "
                    "this system. This system is not supported: %s" %
                    PROGSystem, 2, "")
        if Dir.endswith(sep) is False:
            Dir += sep
    except Exception as e:
        return (2, "MW", "There is an error building the directory to the "
                "setups file. The error is:\n\n%s\n\nThis will need to be "
                "corrected before this program can be used." % e, 3, "")
# I guess it's possible for Dir to just be ":" on Windows, so we better check.
    if exists(Dir) is False:
        return (2, "MW", "The %s directory\n\n%s\n\ndoes not exist. This will "
                "need to be corrected before this program may be used." %
                (LookFor, Dir), 3, "")
    if access(Dir, W_OK) is False:
        return (2, "MW", "The %s directory\n\n%s\n\nis not accessible for "
                "writing. This will need to be corrected before this program "
                "may be used." % (LookFor, Dir), 3, "")
    PROGSetupsDirVar.set(Dir)
    return (0,)


####################
# BEGIN: setUserID()
# FUNC:setUserID():2018.334
#   For programs that need to know the name of the user.
#   Not set up for kiosk-type programs (it has a Quit button).
PROGUserIDUC = ""
PROGUserIDLC = ""


def setUserID():
    global PROGUserIDUC
    global PROGUserIDLC
# This (usually) comes up before the main form of a program, so we have to
# figure out where it should go. We'll just use the center of displays less
# than the 2560 pixels of a modern iMac, or the center of a 1024x768 display.
    if PROGScreenWidthNow > 2560:
        CX = 512
        CY = 384
    else:
        CX = PROGScreenWidthNow / 2
        CY = PROGScreenHeightNow / 2
    MYDAnswerVar.set("")
# None Parent, otherwise it doesn't show up in Windows.
    Answer = formMYD(None,
                     (("Input12", TOP, "input"), ("(OK)", LEFT, "input"),
                      ("Quit", LEFT, "quit")),
                     "cancel", "", "Who Are You?",
                     "Enter your name or an ID that will be used for "
                     "finding/saving program related files. The field may be "
                     "left blank if appropriate.", "", 0, CX, CY)
# Keep going in here until the user gives a valid, or no, answer.
    while True:
        if Answer == "quit":
            return (2, "", "Quit.", 0, "")
        elif Answer == "cancel" or len(Answer) == 0:
            Answer = ""
            break
        else:
            if Answer.isalnum() is False:
                Answer = formMYD(None,
                                 (("Input12", TOP, "input"),
                                  ("(OK)", LEFT, "input"),
                                  ("Quit", LEFT, "quit")),
                                 "cancel", "YB", "Who Are You?",
                                 "The ID may only be letters and numbers."
                                 "\n\nEnter your name or an ID that will be "
                                 "used for finding/saving program related "
                                 "files. The field may be left blank if "
                                 "appropriate.", "", 0, CX, CY)
                continue
        break
    PROGUserIDUC = Answer.strip().upper()
    PROGUserIDLC = Answer.strip().lower()
    return (0, )
################################################
# BEGIN: setPROGStartDir(Which, WarnQuit = True)
# FUNC:setPROGStartDir():2018.334
#   The lines are about 60 chars long.


def setPROGStartDir(Which, WarnQuit=True):
    # 60 chars.
    # PETM
    if Which == 0:
        Answer = formMYDF(Root, 1, "Pick A Starting Directory",
                          PROGSetupsDirVar.get(), "",
                          "This may be the first time this program has been "
                          "started on\nthis computer or in this account. "
                          "Select a directory for\nthe program to use as the "
                          "directory where all of its files\nshould start out "
                          "being saved. Click the OK button if the\ndisplayed "
                          "directory is OK to use.")
# POCUS, CHANGEO, HOCUS...
    elif Which == 1:
        Answer = formMYDF(Root, 1, "Pick A Starting Directory",
                          PROGSetupsDirVar.get(), "",
                          "This may be the first time this program has been "
                          "started on\nthis computer or in this account. "
                          "Select a directory for\nthe program to use as the "
                          "directory where all of the bookkeeping\nfiles for "
                          "the program should start out being saved -- NOT\n"
                          "necessarily where any data files may be. We'll get "
                          "to that\nlater. Click the OK button if the "
                          "displayed directory is\nOK to use.")
    elif Which == 2:
        # LOGPEEK, QPEEK (no messages file)
        Answer = formMYDF(Root, 1, "Where's The Data?",
                          PROGSetupsDirVar.get(), "",
                          "This may be the first time this program has been "
                          "started\non this computer or in this account. "
                          "Select a directory\nfor the program to use as a "
                          "starting point -- generally\nwhere some data files "
                          "to read are located.\nClick the OK button to use "
                          "the displayed\ndirectory.")
# This is here just so they all say the same thing. The caller should call the
# quitter (didn't want to do that here). The caller can supress the message if
# it just wants to set it's own defaults.
    if WarnQuit is True and len(Answer) == 0:
        formMYD(Root, (("(OK)", TOP, "ok"), ), "ok", "YB",
                "Have It Your Way.",
                "You didn't enter anything, so I'm quitting.")
        quit(0)
    return Answer
# END: startupThings


######################
# BEGIN: class ToolTip
# LIB:ToolTip():2019.058
#   Add tooltips to objects.
#   Usage: ToolTip(obj, Len, "text")
#   Nice and clever.
#   Starting the text with a ^ signals that the cursor for this item should be
#   set to right_ptr black white when the widget is entered.
class ToolTipBase:
    def __init__(self, button):
        self.button = button
        self.tipwindow = None
        self.id = None
        self.x = self.y = 0
        self.button.bind("<Enter>", self.enter)
        self.button.bind("<Leave>", self.leave)
        self.button.bind("<ButtonPress>", self.leave)
        return

    def enter(self, event=None):
        self.schedule()
        if self.text.startswith("^"):
            self.button.config(cursor="right_ptr black white")
        return

    def leave(self, event=None):
        self.unschedule()
        self.hidetip()
        self.button.config(cursor="")
        return

    def schedule(self):
        self.unschedule()
        self.id = self.button.after(500, self.showtip)
        return

    def unschedule(self):
        id = self.id
        self.id = None
        if id:
            self.button.after_cancel(id)
        return

    def showtip(self):
        if self.text == "^":
            return
        if self.tipwindow:
            return
# The tip window must be completely clear of the mouse pointer so offset the
# x and y a little. This is all in a try because I started getting TclErrors
# to the effect that the window no longer existed by the time the geometry and
# deiconify functions were reached after adding the 'keep the tooltip off the
# edge of the display' stuff. I think this additional stuff was adding enough
# time to the whole process that it would get caught trying to bring up a tip
# that was no longer needed as the user quickly moved the pointer around the
# display.
        try:
            self.tipwindow = tw = Toplevel(self.button)
            tw.withdraw()
            tw.wm_overrideredirect(1)
            self.showcontents()
            x = self.button.winfo_pointerx()
            y = self.button.winfo_pointery()
# After much trial and error...keep the tooltip away from the right edge of the
# screen and the bottom of the screen.
            tw.update()
            if x + tw.winfo_reqwidth() + 5 > PROGScreenWidthNow:
                x -= (tw.winfo_reqwidth() + 5)
            else:
                x += 5
            if y + tw.winfo_reqheight() + 5 > PROGScreenHeightNow:
                y -= (tw.winfo_reqheight() + 5)
            else:
                y += 5
            tw.wm_geometry("+%d+%d" % (x, y))
            tw.deiconify()
            tw.lift()
        except TclError:
            self.hidetip()
        return

    def showcontents(self, Len, text, BF):
        # Break up the incoming message about every Len characters.
        if Len > 0 and len(text) > Len:
            Mssg = ""
            Count = 0
            for c in text:
                if Count == 0 and c == " ":
                    continue
                if Count > Len and c == " ":
                    Mssg += "\n"
                    Count = 0
                    continue
                if c == "\n":
                    Mssg += c
                    Count = 0
                    continue
                Count += 1
                Mssg += c
            text = Mssg
# Override this in derived class.
        Lab = Label(self.tipwindow, text=text, justify=LEFT,
                    bg=Clr[BF[0]], fg=Clr[BF[1]], bd=1, relief=SOLID,
                    padx=3, pady=3)
        Lab.pack()
        return

    def hidetip(self):
        # If it is already gone then just go back.
        try:
            tw = self.tipwindow
            self.tipwindow = None
            if tw:
                tw.destroy()
        except TclError:
            pass
        return


class ToolTip(ToolTipBase):
    def __init__(self, button, Len, text, BF="YB"):
        # If the caller doesn't pass any text then don't get this started.
        if len(text) == 0:
            return
        ToolTipBase.__init__(self, button)
        self.Len = Len
        self.text = text
        self.BF = BF
        return

    def showcontents(self):
        if self.text.startswith("^") is False:
            ToolTipBase.showcontents(self, self.Len, self.text, self.BF)
        else:
            ToolTipBase.showcontents(self, self.Len, self.text[1:], self.BF)
        return
# END: ToolTip


##############################################################################
# BEGIN: txLn(VarSet, Color, What, Newline = True, Bell = 0, Update = False, \
#                Tag = False)
# LIB:txLn():2018.348
#   Roughly the same as msgLn, but is made for regular Text() message areas.
def txLn(VarSet, Color, What, Newline=True, Bell=0, Update=False,
         Tag=False):
    LTxt = PROGTxt[VarSet]
    NormallyDisabled = False
    if LTxt.cget("state") == DISABLED:
        LTxt.config(state=NORMAL)
        NormallyDisabled = True
    if len(Color) == 0 and len(What) == 0 and Newline is False and \
            Bell == 0 and Update is False and Tag is False:
        # 2018-12 FINISHME-Is this where the program is getting hung up when
        # you stop reading a BIG file in LOGPEEK where a lot of lines have been
        # skipped (they are all tagged yellow)? Can't figure it out.
        LTxt.tag_delete(*LTxt.tag_names())
        LTxt.delete("0.0", END)
        LTxt.update()
        if NormallyDisabled is True:
            LTxt.config(state=DISABLED)
        return
# This gets passed back no matter what. It may be set to something or "".
    IdxS = ""
    if Tag is False:
        # We still need a tag if doing colors even if the caller says no.
        if len(Color) != 0:
            # This mouthful keeps user mouse clicks in the field from screwing
            # up the tag indexes. END always points to the "next" line so we
            # have to back that up one to get the "current" line which INSERT
            # (which used to be used) doesn't always point to if the user has
            # clicked somewhere in the Text widget (which I thought was covered
            # by the value in CURSOR, but isn't).
            IdxS = LTxt.index(str(intt(LTxt.index(END)) - 1) + ".end")
            LTxt.tag_config(IdxS, foreground=Clr[Color[0]])
            LTxt.insert(END, What, IdxS)
        else:
            LTxt.insert(END, What)
    else:
        # Generates a tag even if there is no Color choice (which we don't want
        # to do ALL of the time, but only when the caller needs the tag IdxS
        # passed back).
        IdxS = LTxt.index(str(intt(LTxt.index(END)) - 1) + ".end")
        if len(Color) != 0:
            LTxt.tag_config(IdxS, foreground=Clr[Color[0]])
        LTxt.insert(END, What, IdxS)
    if Newline is True:
        LTxt.insert(END, "\n")
# The Text field may not have a scrollbar defined for this behaviour, so try.
# Only scroll when the scrollbar is all the way down if we are doing a \n.
        try:
            if PROGSb[VarSet].get()[1] == 1.0:
                LTxt.see(END)
                LTxt.update()
        except Exception:
            pass
# Instead of updating everything.
    if Update is True:
        LTxt.update()
    if Bell != 0:
        beep(Bell)
    if NormallyDisabled is True:
        LTxt.config(state=DISABLED)
    return IdxS
# END: txLn


########################
# BEGIN: updateMe(Which)
# FUNC:updateMe():2006.112
def updateMe(Which):
    if Which == 0:
        Root.update_idletasks()
        Root.update()
    return
# END: updateMe


#####################################################
# BEGIN: walkDirs(Dir, IncHidden, End, Walk, IncDirs)
# LIB:walkDirs():2016.223
#   Walks the passed Dir and returns a list of every file in Dir with full
#   path.
#   IncHidden is True/False.
#   If End is not "" then only files ending with End will be passed back.
#   If Walk is 0 then files only in Dir will be returned and no
#   sub-directories will be walked.
#   If IncDirs is True then sub-directory entries will be be included with the
#   files.
def walkDirs(Dir, IncHidden, End, Walk, IncDirs):
    if (Dir.startswith(".") or Dir.startswith("_")) and IncHidden is False:
        return (1, "RW", "Directory %s is hidden or special." % Dir, 2, "")
    if exists(Dir) is False:
        return (1, "RW", "Directory %s does not exist." % Dir, 2, "")
    if isdir(Dir) is False:
        return (1, "RW", "%s is not a directory." % Dir, 2, "")
    Files = []
    if Walk == 0:
        if Dir.endswith(sep) is False:
            Dir += sep
        for Name in listdir(Dir):
            # No hidden files or directories, please.
            if (Name.startswith(".") or Name.startswith("_")) and \
                    IncHidden is False:
                continue
            Fullpath = Dir + Name
            if isdir(Fullpath):
                # The caller may not want to walk into the directories, but may
                # still want a list of them in the current directory.
                if IncDirs is True:
                    Files.append(Fullpath + sep)
                continue
            if Fullpath.endswith(End):
                Files.append(Fullpath)
    elif Walk != 0:
        # Collect the directories we come across them in here.
        Dirs = []
        Dirs.append(Dir)
        while len(Dirs) > 0:
            Dir = Dirs.pop()
            if Dir.endswith(sep) is False:
                Dir += sep
            if IncDirs is True:
                if Dir not in Files:
                    Files.append(Dir)
            for Name in listdir(Dir):
                # No hidden files or directories, please.
                if (Name.startswith(".") or Name.startswith("_")) and \
                        IncHidden is False:
                    continue
                Fullpath = Dir + Name
# Save this so we can pop it and do a listdir() on it.
                if isdir(Fullpath):
                    Dirs.append(Fullpath)
                    continue
                if Fullpath.endswith(End):
                    Files.append(Fullpath)
    Files.sort()
    return (0, Files)
#####################################################
# BEGIN: walkDirs2(Dir, IncHidden, Middle, End, Walk)
# FUNC:walkDirs2():2015.149
#   Walks the passed Dir and returns a list of every file in Dir with full
#   path.
#   If Middle is not "" then only full paths containing Middle will be
#   kept.
#   If End is not "" then only files ending with End will be passed
#   back.
#   If Walk is False then files only in Dir will be returned and no
#   sub-directories will be walked.


def walkDirs2(Dir, IncHidden, Middle, End, Walk):
    if (Dir.startswith(".") or Dir.startswith("_")) and IncHidden is False:
        return (1, "RW", "Directory %s is hidden or special." % Dir, 2, "")
    if exists(Dir) is False:
        return (1, "RW", "Directory %s does not exist." % Dir, 2, "")
    if isdir(Dir) is False:
        return (1, "RW", "%s is not a directory." % Dir, 2, "")
    Files = []
    if Walk == 0:
        if Dir.endswith(sep) is False:
            Dir += sep
        Names = listdir(Dir)
        for Name in Names:
            # No hidden files or directories, please.
            if (Name.startswith(".") or Name.startswith("_")) and \
                    IncHidden is False:
                continue
            Fullpath = Dir + Name
            if isdir(Fullpath):
                continue
# If Middle and/or End are "" this will still work.
            if Fullpath.find(Middle) != -1 and Fullpath.endswith(End):
                Files.append(Fullpath)
    elif Walk == 1:
        # Collect the directories we come across in here.
        Dirs = []
        Dirs.append(Dir)
        while len(Dirs) > 0:
            Dir = Dirs.pop()
            if Dir.endswith(sep) is False:
                Dir += sep
            for Name in listdir(Dir):
                # No hidden files or directories, please.
                if (Name.startswith(".") or Name.startswith("_")) and \
                        IncHidden is False:
                    continue
                Fullpath = Dir + Name
# Save this so we can pop it and do a listdir() on it.
                if isdir(Fullpath):
                    Dirs.append(Fullpath)
                    continue
                if Fullpath.find(Middle) == -1:
                    continue
                if Fullpath.endswith(End):
                    Files.append(Fullpath)
    Files.sort()
    return (0, Files)
# END: walkDirs


#########################
# BEGIN: yRange(Min, Max)
# FUNC:yRange():2008.090
#   Figure out the range of the passed Max and Min values.
def yRange(Min, Max):
    if Max >= 0.0 and Min <= 0.0:
        YRange = Max + abs(Min)
    elif Max <= 0.0 and Min <= 0.0:
        YRange = Max - Min
    elif Max >= 0.0 and Min >= 0.0:
        YRange = Max - Min
    else:
        YRange = 1.0
# Do this just in case so we don't divide anything by 0.
    if YRange == 0.0:
        YRange = 1.0
    return YRange
# END: yRange


# ============================================
# BEGIN: =============== SETUP ===============
# ============================================
######################
# BEGIN: menuMake(Win)
# FUNC:makeMenu():2018.347
# LOGPEEK: Close All Forms is commented out.
OPTIgTECVar = IntVar()
OPTIgTECVar.set(0)
OPTAddMPMsgsCVar = IntVar()
OPTAddETPosMsgsCVar = IntVar()
OPTDateFormatRVar = StringVar()
OPTDateFormatRVar.set("YYYY:DOY")
OPTAntelopeCVar = IntVar()
OPTSortFilesByRVar = StringVar()
OPTSortFilesByRVar.set("type")
OPTCalcFileSizesCVar = IntVar()
OPTFilterSOHCVar = IntVar()
PROGSetups += ["OPTIgTECVar", "OPTAddMPMsgsCVar", "OPTAddETPosMsgsCVar",
               "OPTDateFormatRVar", "OPTAntelopeCVar", "OPTSortFilesByRVar",
               "OPTCalcFileSizesCVar", "OPTFilterSOHCVar"]
MENUMenus = {}
MENUFont = PROGOrigPropFont


def menuMake(Win):
    global MENUMenus
    MENUMenus.clear()
    Top = Menu(Win, font=MENUFont)
    Win.configure(menu=Top)
    Fi = Menu(Top, font=MENUFont, tearoff=0)
    MENUMenus["File"] = Fi
    Top.add_cascade(label="File", menu=Fi)
    Fi.add_command(label="Delete Setups File", command=deletePROGSetups)
    Fi.add_separator()
    Fi.add_command(label="Quit %s" % PROG_NAME,
                   command=Command(progQuitter, True))
    Co = Menu(Top, font=MENUFont, tearoff=0)
    MENUMenus["Commands"] = Co
    Top.add_cascade(label="Commands", menu=Co)
    Co.add_command(label="GPS Data Plotter", command=formGPS)
    Co.add_command(label="Log Search", command=formSEARCH)
    Co.add_command(label="Plot Time Ranges Of Files",
                   command=Command(formTMRNG, Root))
    Co.add_command(label="Plot Positions From Files",
                   command=Command(formLPMAPR, Root))
    Co.add_separator()
    Co.add_command(label="Export TT Times As Deployment File (Line Format)",
                   command=Command(exportTT, "depl"))
    Co.add_command(label="Export TT Times As Deployment File (Block Format)",
                   command=Command(exportTT, "depb"))
    Co.add_command(label="Export TT Times As TSP Shotfile/Geometry",
                   command=Command(exportTT, "tspsf"))
    Co.add_command(label="Export TT Times As Shot Info",
                   command=Command(exportTT, "sinfo"))
    Gr = Menu(Top, font=MENUFont, tearoff=0)
    MENUMenus["Plots"] = Gr
    Top.add_cascade(label="Plots", menu=Gr)
    Gr.add_checkbutton(label="DSP-Clk Difference",
                       variable=DGrf["DCDIFF"], command=reconfigDisplay)
    Gr.add_checkbutton(label="Phase Error", variable=DGrf["ICPE"],
                       command=reconfigDisplay)
    Gr.add_checkbutton(label="Jerk/DSP Sets", variable=DGrf["JERK"],
                       command=reconfigDisplay)
    Gr.add_checkbutton(label=".err File Errors", variable=DGrf["ERR"],
                       command=reconfigDisplay)
    Gr.add_checkbutton(label="GPS On/Off/Err", variable=DGrf["GPSON"],
                       command=reconfigDisplay)
    Gr.add_checkbutton(label="GPS Lk-Unlk", variable=DGrf["GPSLK"],
                       command=reconfigDisplay)
    Gr.add_checkbutton(label="Temperature", variable=DGrf["TEMP"],
                       command=reconfigDisplay)
    Gr.add_checkbutton(label="Volts", variable=DGrf["VOLT"],
                       command=reconfigDisplay)
    Gr.add_checkbutton(label="Backup Volts", variable=DGrf["BKUP"],
                       command=reconfigDisplay)
    Gr.add_checkbutton(label="Dump Call", variable=DGrf["DUMP"],
                       command=reconfigDisplay)
    Gr.add_checkbutton(label="Acquisition On-Off", variable=DGrf["ACQ"],
                       command=reconfigDisplay)
    Gr.add_checkbutton(label="Reset/Powerup", variable=DGrf["RESET"],
                       command=reconfigDisplay)
    Gr.add_checkbutton(label="Error/Warning", variable=DGrf["ERRWARN"],
                       command=reconfigDisplay)
    Gr.add_checkbutton(label="Discrepancies", variable=DGrf["DISCREP"],
                       command=reconfigDisplay)
    Gr.add_checkbutton(label="SOH/Data Definitions",
                       variable=DGrf["SOHDEF"], command=reconfigDisplay)
    Gr.add_checkbutton(label="Network Up-Down", variable=DGrf["NET"],
                       command=reconfigDisplay)
    Gr.add_checkbutton(label="Events", variable=DGrf["EVT"],
                       command=reconfigDisplay)
    Gr.add_checkbutton(label="Re-center", variable=DGrf["MRC"],
                       command=reconfigDisplay)
    Gr.add_checkbutton(label="Disk 1 Usage",
                       variable=DGrf["DU1"], command=reconfigDisplay)
    Gr.add_checkbutton(label="Disk 2 Usage",
                       variable=DGrf["DU2"], command=reconfigDisplay)
    Gr.add_checkbutton(label="Mass Positions 123",
                       variable=DGrf["MP123"], command=reconfigDisplay)
    Gr.add_checkbutton(label="Mass Positions 456",
                       variable=DGrf["MP456"], command=reconfigDisplay)
    Gr.add_separator()
    Gr.add_command(label="All Plots On", command=allPlotsOn)
    Op = Menu(Top, font=MENUFont, tearoff=0)
    MENUMenus["Options"] = Op
    Top.add_cascade(label="Options", menu=Op)
    Op.add_checkbutton(label="Plot Timing Problems",
                       variable=OPTIgTECVar)
    Op.add_checkbutton(label="Filter Out Non-SOH Lines",
                       variable=OPTFilterSOHCVar)
    Op.add_separator()
    Op.add_radiobutton(label="Sort Files List By Type",
                       variable=OPTSortFilesByRVar, value="type")
    Op.add_radiobutton(label="Sort Files List Alphabetically",
                       variable=OPTSortFilesByRVar, value="alpha")
    Op.add_checkbutton(label="Calculate File Sizes",
                       variable=OPTCalcFileSizesCVar)
    Op.add_checkbutton(label="Warn If Big", variable=PROGWarnIfBigCVar)
    Op.add_separator()
    Op.add_checkbutton(label="Add Mass Positions To SOH Messages",
                       variable=OPTAddMPMsgsCVar)
# WARNING: Keep the Reg and TC labels straight with MPColorRanges values.
    Message = "MP Coloring: %s (Regular)" % list2Str(MPColorRanges["0"])
    Op.add_radiobutton(label=Message,
                       command=Command(rt130MPDecodeChange, "MF"),
                       variable=MPColorRangeRVar, value="0")
    Message = "MP Coloring: %s (Trillium)" % list2Str(MPColorRanges["1"])
    Op.add_radiobutton(label=Message,
                       command=Command(rt130MPDecodeChange, "MF"),
                       variable=MPColorRangeRVar, value="1")
    Op.add_separator()
    Op.add_checkbutton(label="Add Positions To ET Lines",
                       variable=OPTAddETPosMsgsCVar)
    Op.add_separator()
    Op.add_checkbutton(label="Read Antelope-Produced Log Files",
                       variable=OPTAntelopeCVar)
    Op.add_separator()
    Op.add_radiobutton(label="Show YYYY:DOY Dates",
                       command=changeDateFormat, variable=OPTDateFormatRVar,
                       value="YYYY:DOY")
    Op.add_radiobutton(label="Show YYYY-MM-DD Dates",
                       command=changeDateFormat, variable=OPTDateFormatRVar,
                       value="YYYY-MM-DD")
    Op.add_radiobutton(label="Show YYYYMMMDD Dates",
                       command=changeDateFormat, variable=OPTDateFormatRVar,
                       value="YYYYMMMDD")
    Op.add_separator()
    Op.add_command(label="Set Font Sizes", command=Command(formFONTSZ,
                                                           Root, 0))
    Fo = Menu(Top, font=MENUFont, tearoff=0, postcommand=menuMakeForms)
    MENUMenus["Forms"] = Fo
    Top.add_cascade(label="Forms", menu=Fo)
    Hp = Menu(Top, font=MENUFont, tearoff=0)
    MENUMenus["Help"] = Hp
    Top.add_cascade(label="Help", menu=Hp)
    Hp.add_command(label="Help", command=Command(formHELP, Root))
    Hp.add_command(label="Calendar", command=Command(formCAL, Root))
    Hp.add_command(label="Check For Updates", command=checkForUpdates)
    Hp.add_command(label="About", command=formABOUT)
#    Hp.add_command(label = "Test Command", command = testCmd)
    return
###############################################
# BEGIN: menuMakeSet(MenuText, ItemText, State)
# FUNC:menuMakeSet():2018.235


def menuMakeSet(MenuText, ItemText, State):
    try:
        Menu = MENUMenus[MenuText]
        Indexes = Menu.index("end") + 1
        for Item in arange(0, Indexes):
            Type = Menu.type(Item)
            if Type in ("tearoff", "separator"):
                continue
            if Menu.entrycget(Item, "label") == ItemText:
                Menu.entryconfigure(Item, state=State)
                break
# Just in case. This should never go off if I've done my job.
    except Exception:
        stdout.write("menuMakeSet: %s, %s\n\a" % (MenuText, ItemText))
    return
################################
# BEGIN: menuMakeForms(e = None)
# FUNC:menuMakeForms():2018.236logpeek


def menuMakeForms(e=None):
    FMenu = MENUMenus["Forms"]
    FMenu.delete(0, END)
# Build the list of forms so they can be sorted alphabetically.
    Forms = []
    for Frmm in list(PROGFrm.keys()):
        try:
            if PROGFrm[Frmm] is not None:
                Forms.append([PROGFrm[Frmm].title(), Frmm])
        except TclError:
            stdout.write("%s\n" % Frmm)
            formClose(Frmm)
    if len(Forms) == 0:
        FMenu.add_command(label="No Forms Are Open", state=DISABLED)
    else:
        Forms.sort()
        for Title, Frmm in Forms:
            FMenu.add_command(label=Title, command=Command(showUp, Frmm))
#        FMenu.add_separator()
#        FMenu.add_command(label = "Close All Forms", command = formCloseAll)
    return
# END: menuMake


##################
# BEGIN: testCmd()
# FUNC:testCmd():2018.241
def testCmd():
    return
# END: testCmd


###############
# BEGIN: main()
# FUNC:main():2019.024
PROGMsgsDirVar = StringVar()
PROGDataDirVar = StringVar()
PROGWorkDirVar = StringVar()
PROGSetups += ["PROGMsgsDirVar", "PROGDataDirVar", "PROGWorkDirVar"]
# These are linked to the Plot menu checkbuttons.
DGrf = {"DCDIFF": IntVar(), "DU1": IntVar(), "DU2": IntVar(),
        "ICPE": IntVar(), "JERK": IntVar(), "GPSON": IntVar(),
        "GPSLK": IntVar(), "CLKPWR": IntVar(), "DUMP": IntVar(),
        "EVT": IntVar(), "TEMP": IntVar(), "VOLT": IntVar(), "BKUP": IntVar(),
        "MP123": IntVar(), "MP456": IntVar(), "ERR": IntVar(),
        "RESET": IntVar(), "MRC": IntVar(), "DISCREP": IntVar(),
        "ERRWARN": IntVar(), "ACQ": IntVar(), "SOHDEF": IntVar(),
        "NET": IntVar()}
# These are the relative "weights" given to each plot that controls how much
# of the total plot space each plot will receive. Plots that show states
# like "on/off" should have a minimum weight of 2, single item plots 1, and
# plots that actually plot varing things like voltages 3 or above.
# These will be totaled up and then divied by that total, so they don't have
# to add up to 100 (like percent) or anything. The total will be based on
# which plots the user wants to plot according to selections in the Plots
# menu..
# These are floats just to keep truncating errors from messing things up.
DPlotWts = {"DCDIFF": 5.0, "DU1": 3.0, "DU2": 3.0, "ICPE": 5.0, "JERK": 1.5,
            "GPSON": 1.75, "GPSLK": 3.0, "CLKPWR": 1.0, "DUMP": 1.5,
            "EVT": 1.0, "TEMP": 3.0, "VOLT": 3.0, "BKUP": 1.0, "MP123": 2.0,
            "MP456": 2.0, "ERR": 1.0, "RESET": 1.5, "MRC": 1.0,
            "DISCREP": 1.0, "ERRWARN": 1.5, "ACQ": 1.5, "SOHDEF": 1.5,
            "NET": 1.5}
SearchVar = StringVar()
FromDateVar = StringVar()
ToDateVar = StringVar()
MFLbxFindVar = StringVar()
MFShowLogsCVar = IntVar()
MFShowLogsCVar.set(1)
MFShowRefsCVar = IntVar()
MFShowRefsCVar.set(1)
MFShowZCFCVar = IntVar()
MFShowZCFCVar.set(1)
MFShowUCFDCVar = IntVar()
MFShowUCFDCVar.set(1)
MFShowMSLOGSCVar = IntVar()
MFShowMSLOGSCVar.set(1)
LOGLastFilespecVar = StringVar()
SEARCHLastFilespecVar = StringVar()
LastMFPSFilespecVar = StringVar()
LastRAWPSFilespecVar = StringVar()
LastTPSPSFilespecVar = StringVar()
OPTOnlySOHCVar = IntVar()
Stream1Var = IntVar()
Stream2Var = IntVar()
Stream3Var = IntVar()
Stream4Var = IntVar()
Stream5Var = IntVar()
Stream6Var = IntVar()
Stream7Var = IntVar()
Stream8Var = IntVar()
OPTPlotRAWCVar = IntVar()
OPTDecodeModeCVar = IntVar()
OPTPlotTPSCVar = IntVar()
PROGSetups += ["SearchVar", "FromDateVar", "ToDateVar", "MFLbxFindVar",
               "MFShowLogsCVar", "MFShowRefsCVar", "MFShowZCFCVar",
               "MFShowMSLOGSCVar", "MFShowUCFDCVar", "LOGLastFilespecVar",
               "SEARCHLastFilespecVar", "LastMFPSFilespecVar",
               "LastRAWPSFilespecVar", "LastTPSPSFilespecVar",
               "OPTOnlySOHCVar", "Stream1Var", "Stream2Var", "Stream3Var",
               "Stream4Var", "Stream5Var", "Stream6Var", "Stream7Var",
               "Stream8Var", "OPTPlotRAWCVar", "OPTDecodeModeCVar",
               "OPTPlotTPSCVar"]
# Not in PROGSetups on purpose.
UseDatesVar = IntVar()
# Don't save the value of this in setups so it always ends up selected when
# starting up.
PROGWarnIfBigCVar = IntVar()
PROGWarnIfBigCVar.set(1)
FirstSelRule = None
FirstSelRuleX = 0
SecondSelRule = None
SecondSelRuleX = 0
MFTimeMode = ""
MFZapped = 0
PROGRunning = IntVar()
StartEndRecord = []
MFPlotted = False
FStart = 0.0
FEnd = 0.0
# The current start and end times of the main plot display.
CurMainStart = 0.0
CurMainEnd = 0.0
# This will get set by the first 'State of Health' line of a .log file, the
# UID of the first packet of a .ref file, or by the first call to the extract
# CF or zipped CF files.
RTModel = ""
MainFilename = ""
MainFilespec = ""
# All of the lists where data points end up being kept.
ACQ = []
BKUP = []
CLKPWR = []
DCDIFF = []
DEFS = []
DISCREP = []
DU1 = []
DU2 = []
# DSP resets use JERK
DUMP = []
ERROR = []
EVT = []
GPS = []
GPSERR = []
GPSLK = []
ICPE = []
JERK = []
MP1 = []
MP2 = []
MP3 = []
MP4 = []
MP5 = []
MP6 = []
MRC = []
NET = []
PWRUP = []
RESET = []
SOH = []
TEMP = []
TMJMP = []
VOLT = []
WARN = []
allPlotsOn()
# Start creating the display.
Root.protocol("WM_DELETE_WINDOW", Command(progQuitter, True))
menuMake(Root)
# If I don't create a sub-frame of Root resizing the main window causes a lot
# of plot updates to be caught and executed causing the plot to be redrawn
# a number of times before reaching the new size of the window (on some OS's
# and setups)..
SubRoot = Frame(Root)
SubRoot.pack(fill=BOTH, expand=YES)
# ----- Current directory area -----
Sub = Frame(SubRoot)
BButton(Sub, text="Main Data Dir", command=pushBrowse).pack(side=LEFT)
# BButton(Sub, text = "Read Directory",
#         command = pushReadDir).pack(side = LEFT)
LEnt = PROGEnt["DATA"] = Entry(Sub, textvariable=PROGDataDirVar)
LEnt.pack(side=LEFT, fill=X, expand=YES)
compFsSetup(Root, LEnt, PROGDataDirVar, None, "MF")
LEnt.bind('<Return>', returnReadDir)
if PROGSystem == "dar":
    if B2Glitch is True:
        LEnt.bind("<Button-2>", openCWDPopup)
    LEnt.bind("<Button-3>", openCWDPopup)
LCb = Checkbutton(Sub, variable=UseDatesVar)
LCb.pack(side=LEFT)
ToolTip(LCb, 35, "Select this to use the From/To dates, and deselect it to "
        "ignore them.")
Lb = Label(Sub, text="From:")
Lb.pack(side=LEFT)
ToolTip(Lb, 30, "Fill in the 'From' and 'To' fields to the right with start "
        "and end dates to display. The range is From 00:00 to To 24:00.")
LEnt = Entry(Sub, width=11, textvariable=FromDateVar)
LEnt.pack(side=LEFT)
BButton(Sub, text="C", command=Command(formPCAL, Root, "CX", "MF",
                                       FromDateVar, LEnt, False,
                                       OPTDateFormatRVar)).pack(side=LEFT)
Label(Sub, text=" To ").pack(side=LEFT)
LEnt = Entry(Sub, width=11, textvariable=ToDateVar)
LEnt.pack(side=LEFT)
BButton(Sub, text="C", command=Command(formPCAL, Root, "CX", "MF",
                                       ToDateVar, LEnt, False,
                                       OPTDateFormatRVar)).pack(side=LEFT)
LLb = Label(Sub, text=" Hints ")
LLb.pack(side=LEFT)
ToolTip(LLb, 45, "PLOT AREA HINTS:\n--Clicking on a point: Clicking on a "
        "non-err file or non-mass position plot point will bring up the SOH "
        "Messages window and highlight the line in the log messages that goes "
        "with the point that was clicked.\n--Right-click on points: This will "
        "'zap' the point and redraw the plots without it. The only way to "
        "recover zapped points is to re-read the source file.\n--Shift-click "
        "in plotting area: This will display a vertical selection rule at "
        "that point. Moving the mouse and shift-clicking again will display "
        "a second selection rule at the new spot, then LOGPEEK will zoom in "
        "on the area selected. Shift-clicking in the area of the plot labels "
        "will cancel the selection operation.\n--Shift-click on labels: If "
        "zoomed in (described above) this will zoom back out. Each step when "
        "zooming in will be repeated when zooming out.\n--Control-clicking on "
        "plot area: This will draw a line and show the date/time at that "
        "point at the top of the plotting area.\n--Clicking on times: If "
        "zoomed in clicking in dates/times area at the bottom will 'scroll' "
        "the display earlier in time. Clicking on the right side will "
        "'scroll' the display forward in time.\n--Control-click on times: "
        "This will extend the X-axis tick marks to the top of the plotting "
        "area.\nFILE LIST AREA HINTS:\n--Shift-Control-click on .log file: "
        "Shift-Control-clicking on a .log data source file name will read in "
        "and display that file in a separate window without plotting the "
        "contents.")
Sub.pack(side=TOP, fill=X, expand=NO, padx=3)
# Files list and plotting area.
Sub = Frame(SubRoot)
# ----- File list, scroll bar and text area -----
SSub = Frame(Sub)
# ----- File list and the scroll bars -----
SSSub = Frame(SSub)
MFFiles = Listbox(SSSub, relief=SUNKEN, bd=2, height=5,
                  selectmode=EXTENDED)
MFFiles.pack(side=LEFT, fill=BOTH, expand=YES)
MFFiles.bind("<Double-Button-1>", fileSelected)
MFFiles.bind("<Control-Shift-Button-1>", formSHOW)
LSb = Scrollbar(SSSub, command=MFFiles.yview, orient=VERTICAL)
LSb.pack(side=RIGHT, fill=Y, expand=NO)
MFFiles.configure(yscrollcommand=LSb.set)
SSSub.pack(side=TOP, fill=BOTH, expand=YES)
SSSub = Frame(SSub)
LSb = Scrollbar(SSSub, command=MFFiles.xview, orient=HORIZONTAL)
LSb.pack(side=BOTTOM, fill=X, expand=NO)
MFFiles.configure(xscrollcommand=LSb.set)
SSSub.pack(side=TOP, fill=X, expand=NO)
SSSub = Frame(SSub)
LEnt = labelEntry2(SSSub, 11, "Find:=", 35,
                   "[Reload] Show file names containing this at the top.",
                   MFLbxFindVar, 8)
LEnt.bind("<Return>", Command(loadRTFilesCmd, MFFiles, "MF"))
LEnt.bind("<KP_Enter>", Command(loadRTFilesCmd, MFFiles, "MF"))
BButton(SSSub, text="Clear", fg=Clr["U"],
        command=Command(loadRTFilesClearLbxFindVar, MFFiles, "MF",
                        True)).pack(side=LEFT)
SSSub.pack(side=TOP)
# ----- What-to-show buttons -----
SSSub = Frame(SSub)
LLb = Label(SSSub, text="List:")
LLb.pack(side=LEFT)
ToolTip(LLb, 35,
        "The types of data sources that should be displayed in the file list.")
SSSSub = Frame(SSSub)
SSSSSub = Frame(SSSSub)
SSSSSSub = Frame(SSSSSub)
LCb = Checkbutton(SSSSSSub, text=".log", variable=MFShowLogsCVar)
LCb.pack(side=TOP, anchor="w")
ToolTip(LCb, 30, "RT130/72A text log files.")
LCb = Checkbutton(SSSSSSub, text=".cf", variable=MFShowUCFDCVar)
LCb.pack(side=TOP, anchor="w")
ToolTip(LCb, 30, "RT130 CompactFlash card images (not zipped).")
SSSSSSub.pack(side=LEFT)
SSSSSSub = Frame(SSSSSub)
LCb = Checkbutton(SSSSSSub, text=".ref", variable=MFShowRefsCVar)
LCb.pack(side=TOP, anchor="w")
ToolTip(LCb, 30, "RT130/72A all-in-one raw data files.")
LCb = Checkbutton(SSSSSSub, text=".zip", variable=MFShowZCFCVar)
LCb.pack(side=TOP, anchor="w")
ToolTip(LCb, 30, "RT130 zipped CompactFlash card images.")
SSSSSSub.pack(side=LEFT)
SSSSSub.pack(side=TOP)
LCb = Checkbutton(SSSSub, text=".mslogs", variable=MFShowMSLOGSCVar)
LCb.pack(side=TOP)
ToolTip(LCb, 30, "RT130 miniseed format LOG day volumns in a folder.")
SSSSub.pack(side=LEFT)
Label(SSSub, text=" ").pack(side=LEFT)
BButton(SSSub, text="Reload", command=Command(loadRTFilesCmd, MFFiles,
                                              "MF")).pack(side=LEFT)
SSSub.pack(side=TOP)
SSSub = Frame(SSub)
labelTip(SSSub, "Bkgrnd:", LEFT, 30,
         "B = Black background for all plot areas\nW = White background")
Radiobutton(SSSub, text="B", variable=PROGColorModeRVar,
            value="B").pack(side=LEFT)
Radiobutton(SSSub, text="W", variable=PROGColorModeRVar,
            value="W").pack(side=LEFT)
BButton(SSSub, text="Replot", command=plotMFReplot).pack(side=LEFT)
SSSub.pack(side=TOP)
LCb = Checkbutton(SSub, text="Only SOH Items", variable=OPTOnlySOHCVar)
LCb.pack(side=TOP)
ToolTip(LCb, 30, "When this item is selected LOGPEEK will only read SOH "
        "information from .cf, .zip and .ref sources. This will make reading "
        "those sources much faster, but will generate less plotted "
        "information. It will have no affect when reading .log source files.")
SSSub = Frame(SSub)
LLb = Label(SSSub, text="Mass Pos:")
LLb.pack(side=LEFT)
ToolTip(LLb, 30, "Select which channel set of mass positions to plot (from "
        "raw data, or from the mass position messages in the .log file).")
LCb = Checkbutton(SSSub, text="123",
                  variable=DGrf["MP123"]).pack(side=LEFT)
LCb = Checkbutton(SSSub, text="456",
                  variable=DGrf["MP456"]).pack(side=LEFT)
SSSub.pack(side=TOP)
# Set this to the number of DSs buttons below.
PROG_DSS = 8
SSSub = Frame(SSub)
LLb = Label(SSSub, text="DSs:")
LLb.pack(side=LEFT)
ToolTip(LLb, 30, "The data stream(s) of raw data to plot.")
SSSSub = Frame(SSSub)
SSSSSub = Frame(SSSSub)
Checkbutton(SSSSSub, text="1", variable=Stream1Var).pack(side=LEFT)
Checkbutton(SSSSSub, text="2", variable=Stream2Var).pack(side=LEFT)
Checkbutton(SSSSSub, text="3", variable=Stream3Var).pack(side=LEFT)
Checkbutton(SSSSSub, text="4", variable=Stream4Var).pack(side=LEFT)
SSSSSub.pack(side=TOP)
SSSSSub = Frame(SSSSub)
Checkbutton(SSSSSub, text="5", variable=Stream5Var).pack(side=LEFT)
Checkbutton(SSSSSub, text="6", variable=Stream6Var).pack(side=LEFT)
Checkbutton(SSSSSub, text="7", variable=Stream7Var).pack(side=LEFT)
Checkbutton(SSSSSub, text="8", variable=Stream8Var).pack(side=LEFT)
SSSSSub.pack(side=TOP)
SSSSub.pack(side=LEFT)
SSSub.pack(side=TOP)
SSSub = Frame(SSub)
LCb = Checkbutton(SSSub, text="RAW", variable=OPTPlotRAWCVar)
LCb.pack(side=LEFT)
ToolTip(LCb, 40, "Opens a form and plots the raw data information. At least "
        "one data stream with data must also be selected.")
LCb = Checkbutton(SSSub, text="All", variable=OPTDecodeModeCVar,
                  fg=Clr["R"], command=changeDecodeMode)
LCb.pack(side=LEFT)
ToolTip(LCb, 40,
        "Forces LOGPEEK to read ALL seismic data points (for the Raw Data "
        "Plot). This will significantly increase the reading time, and "
        "possibly cause memory crashes, unless the data set is very small "
        "(<24-48 hours, low sample rates, etc.), or if you have selected "
        "just a day or two to plot using the From/To date fields.", "RW")
Label(SSSub, text="  ").pack(side=LEFT)
LCb = Checkbutton(SSSub, text="TPS", variable=OPTPlotTPSCVar)
LCb.pack(side=LEFT)
ToolTip(LCb, 40, "Opens a form and plots the Time-Power-Squared information. "
        "At least one data stream with data must also be selected.")
SSSub.pack(side=TOP)
SSSub = Frame(SSub)
PROGReadBut = BButton(SSSub, text="Read", command=fileSelected)
PROGReadBut.pack(side=LEFT)
PROGStopBut = BButton(SSSub, text="Stop", state=DISABLED,
                      command=Command(progControl, "stop"))
PROGStopBut.pack(side=LEFT)
BButton(SSSub, text="Write .ps",
        command=Command(formWritePS, Root, "MF",
                        LastMFPSFilespecVar)).pack(side=LEFT)
SSSub.pack(side=TOP, pady=3)
# ----- Information area -----
SSSub = Frame(SSub)
LMsg = PROGMsg["INFO"] = Text(SSSub, cursor="", font=PROGPropFont,
                              height=5, wrap=WORD)
LMsg.pack(side=LEFT, fill=X, expand=YES)
LSb = Scrollbar(SSSub, orient=VERTICAL, command=LMsg.yview)
LSb.pack(side=TOP, fill=Y, expand=YES)
LMsg.configure(yscrollcommand=LSb.set)
SSSub.pack(side=TOP, fill=X, expand=NO)
SSub.pack(side=LEFT, fill=Y, expand=NO, padx=3, pady=3)
# ----- The plot area -----
SSub = Frame(Sub, bd=2, relief=SUNKEN)
# Don't specify the color here.
LCan = PROGCan["MF"] = Canvas(SSub)
LCan.pack(side=TOP, expand=YES, fill=BOTH)
LCan.bind("<Button-1>", Command(plotMFPointClick, "", 0, 0))
LCan.bind("<Shift-Button-1>", plotMFShiftClick)
LCan.bind("<Control-Button-1>", plotMFTimeClick)
SSub.pack(side=RIGHT, expand=YES, fill=BOTH)
Sub.pack(side=TOP, expand=YES, fill=BOTH)
# ----- Status message field -----
Sub = Frame(SubRoot)
PROGMsg["MF"] = Text(Sub, cursor="", font=PROGPropFont, height=3,
                     wrap=WORD)
PROGMsg["MF"].pack(side=LEFT, fill=X, expand=YES)
Button(Sub, text="Redraw", command=reconfigDisplay).pack(side=LEFT)
Sub.pack(side=TOP, fill=X, expand=NO)
# Begin startup sequence.
# Ask who the user is so we can load their setups.
Ret = setUserID()
if Ret[0] != 0:
    updateMe(0)
    progQuitter(False)
Ret = setPROGSetupsDir()
if Ret[0] != 0:
    formMYD(None, (("(OK)", TOP, "ok"),), "ok", Ret[1], "That's Not Good.",
            Ret[2])
    progQuitter(False)
# We need to call this to get the setups loaded, but there may be errors, so
# Ret will be looked at after the main form is positioned and lifted below.
Ret = loadPROGSetups()
# Set these now that the setups have been loaded, but before we deiconify.
fontSetSize()
setColors()
changeDateFormat()
Title = "%s - %s" % (PROG_NAME, PROG_VERSION)
Title2 = ""
if len(PROGHostname) != 0:
    Title2 = "h:%s" % PROGHostname
if len(PROGUserIDLC) != 0:
    if len(Title2) == 0:
        Title2 = "u:%s" % PROGUserIDLC
    else:
        Title2 += " u:%s" % PROGUserIDLC
if len(Title2) != 0:
    Title = Title + " (%s)" % Title2
Root.title(Title)
# If the loaded PROGScreenWidthSaved (and Height) values are the same as they
# are now then use the PROGGeometryVar value that was last saved to set the
# window, otherwise set it to new values.
if PROGIgnoreGeometry is False:
    if PROGScreenWidthSaved.get() == PROGScreenWidthNow and \
            PROGScreenHeightSaved.get() == PROGScreenHeightNow and \
            len(PROGGeometryVar.get()) != 0:
        Root.geometry(PROGGeometryVar.get())
    else:
        # My 27" iMac.
        if PROGScreenWidthNow <= 2560:
            FW = int(PROGScreenWidthNow * .75)
            FH = int(PROGScreenHeightNow * .75)
            FX = int(PROGScreenWidthNow / 2 - FW / 2)
            if FX < 0:
                FX = 0
# Sometimes (OS) measurements don't include the size of the title bar, so the
# -20 compensates for that a bit.
            FY = int(PROGScreenHeightNow / 2 - FH / 2 - 20)
            if FY < 0:
                FY = 0
            Root.geometry("%dx%d+%d+%d" % (FW, FH, FX, FY))
        else:
            # Just let it come up however it wants. It will be too small, but
            # after the user resizes it it will remember.
            pass
else:
    # We'll just make a guess.
    FW = int(PROGScreenWidthNow * .75)
    FH = int(PROGScreenHeightNow * .75)
    FX = int(PROGScreenWidthNow / 2 - FW / 2)
    if FX < 0:
        FX = 0
    FY = int(PROGScreenHeightNow / 2 - FH / 2 - 20)
    if FY < 0:
        FY = 0
    Root.geometry("%dx%d+%d+%d" % (FW, FH, FX, FY))
Root.update()
# Set these so they get saved when we quit (even if they were the same).
PROGScreenHeightSaved.set(PROGScreenHeightNow)
PROGScreenWidthSaved.set(PROGScreenWidthNow)
Root.deiconify()
Root.lift()
# NOW look at any loadPROGSetups() messages.
# Setups file could not be found.
if Ret[0] == 1:
    formMYD(Root, (("(OK)", TOP, "ok"), ), "ok", Ret[1], "Really?", Ret[2])
# Error opening setups file. We'll just continue.
elif Ret[0] == 2 or Ret[0] == 3 or Ret[0] == 4:
    formMYD(Root, (("(OK)", TOP, "ok"), ), "ok", Ret[1], "Oh Oh. Maybe.",
            Ret[2])
# Something is wrong so quit and don't save the setups.
elif Ret[0] == 5:
    formMYD(Root, (("(Quit)", TOP, "quit"), ), "quit", Ret[1], "Oh Oh.",
            Ret[2])
    progQuitter(False)
PROGCan["MF"].configure(bg=DClr["MFCAN"])
# How things get set up differ depending on if the user has told the program
# that they are running it from the command line. In that case take a chance
# and get the Msgs and Work dirs from the getcwd() command.
if PROGCLRunning == 0:
    # This will be true if there is no setups file.
    if len(PROGDataDirVar.get()) == 0:
        Ret = setPROGStartDir(2)
        if len(Ret) == 0:
            progQuitter(False)
        elif len(Ret) != 0:
            # All three of these will always be kept set to the same thing in
            # LOGPEEK.
            PROGDataDirVar.set(Ret)
            PROGMsgsDirVar.set(Ret)
            PROGWorkDirVar.set(Ret)
# If the user doesn't pick any place.
    if len(PROGDataDirVar.get()) == 0:
        PROGDataDirVar.set(PROGSetupsDirVar.get())
    if len(PROGMsgsDirVar.get()) == 0:
        PROGMsgsDirVar.set(PROGDataDirVar.get())
    if len(PROGWorkDirVar.get()) == 0:
        PROGWorkDirVar.set(PROGDataDirVar.get())
    if len(LOGLastFilespecVar.get()) == 0:
        LOGLastFilespecVar.set(PROGDataDirVar.get())
    if len(SEARCHLastFilespecVar.get()) == 0:
        SEARCHLastFilespecVar.set(PROGDataDirVar.get())
# This really isn't necessary since the file name gets set when a file is
# selected, but it will keep the user out of trouble before anything is
# plotted.
    if len(LastMFPSFilespecVar.get()) == 0:
        LastMFPSFilespecVar.set(PROGDataDirVar.get())
    if len(LastRAWPSFilespecVar.get()) == 0:
        LastRAWPSFilespecVar.set(PROGDataDirVar.get())
    if len(LastTPSPSFilespecVar.get()) == 0:
        LastTPSPSFilespecVar.set(PROGDataDirVar.get())
elif PROGCLRunning == 1:
    CWD = getCWD()
    PROGDataDirVar.set(CWD)
    PROGMsgsDirVar.set(CWD)
    PROGWorkDirVar.set(CWD)
    LOGLastFilespecVar.set(CWD)
    SEARCHLastFilespecVar.set(CWD)
    LastMFPSFilespecVar.set(CWD)
    LastRAWPSFilespecVar.set(CWD)
    LastTPSPSFilespecVar.set(CWD)
if len(CLAFile) == 0:
    loadRTFilesCmd(MFFiles, "MF")
# If there was a passed path/filename then we now need to get the program to
# "select" and read the file.
elif len(CLAFile) != 0:
    CLAFileLC = CLAFile.lower()
    if CLAFile.endswith(".log") is False and \
            CLAFile.endswith("_log") is False and \
            CLAFile.endswith(".ref") is False and \
            CLAFile.endswith(".zip") is False and \
            CLAFile.endswith(".cf") is False and \
            CLAFile.endswith(".mslogs") is False:
        formMYD(Root, (("OK", TOP, "ok"), ), "ok", "RW", "No Good.",
                "The passed file\n\n%s\n\ndoes not end with .log, .ref, .zip, "
                ".cf or ,mslogs" % CLAFile)
        progQuitter(False)
    Dir = abspath(dirname(CLAFile))
    if Dir.endswith(sep) is False:
        Dir += sep
    PROGDataDirVar.set(Dir)
    PROGMsgsDirVar.set(Dir)
    PROGWorkDirVar.set(Dir)
    CLAFile = basename(CLAFile)
    MFFiles.insert(END, CLAFile)
    fileSelected()
# Setting to "" turns on epoch time displaying for troubleshooting.
# OPTDateFormatRVar.set("")
formMYD(Root, (("(OK)", TOP, "ok"),), "ok", "YB", "Be Careful.",
        "Running under Python 3 is new, and possibly exciting, so be sure "
        "to report any bugs to PASSCAL.", "", 1)
# TESTING-PROFILE. Uncomment this and comment out 'Root.mainloop()' line.
# profile.run("Root.mainloop()")
Root.mainloop()
# END: main
