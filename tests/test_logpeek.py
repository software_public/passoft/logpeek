#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `logpeek` package."""

import unittest
import sys

from unittest.mock import patch


class TestLogpeek(unittest.TestCase):
    """Tests for `logpeek` package."""

    def test_import(self):
        """Test logpeek import"""
        with patch.object(sys, 'argv', ['logpeek', '-#']):
            with self.assertRaises(SystemExit) as cmd:
                try:
                    import logpeek.logpeek as lp
                    self.assertTrue(lp.PROG_NAME, 'LOGPEEK')
                except ImportError as e:
                    print(e)
                    self.fail("logpeek import failed")
            self.assertEqual(cmd.exception.code, 0, "sys.exit(0) never called "
                             "- Failed to excercise logpeek")
